# 语法

CSS 规则集（rule-set）由选择器和声明块组成：

![image-20210521093138759](image-20210521093138759.png)

## 选择器

 CSS 选择器分为五类：

- 简单选择器：根据名称、id、类来选取元素
- 组合器选择器：根据它们之间的特定关系来选取元素
- 伪类选择器：根据特定状态选取元素
- 伪元素选择器：选取元素的一部分并设置其样式
- 属性选择器：根据属性或属性值来选取元素

### 元素选择器

根据元素名称来选择 HTML 元素

```css
p {
  text-align: center;
  color: red;
}
```

### id选择器

使用 HTML 元素的 id 属性来选择特定元素

元素的 id 在页面中是唯一的，因此 id 选择器用于选择一个唯一的元素

要选择具有特定 id 的元素，请写一个井号（`＃`），后跟该元素的 id

```css
#para1 {
  text-align: center;
  color: red;
}
```

### 类选择器

选择有特定 class 属性的 HTML 元素

如需选择拥有特定 class 的元素，请写一个句点（`.`）字符，后面跟类名

```css
.center {
  text-align: center;
  color: red;
}
```

### 通用选择器

通用选择器（`*`）选择页面上的所有的 HTML 元素

```css
* {
  text-align: center;
  color: blue;
}
```

### 分组选择器

分组选择器选取所有具有相同样式定义的 HTML 元素

```css
h1, h2, p {
  text-align: center;
  color: red;
}
```

## 伪类

伪类用于定义元素的特殊状态，可以用于：

- 设置鼠标悬停在元素上时的样式
- 为已访问和未访问链接设置不同的样式
- 设置元素获得焦点时的样式

```css
selector:pseudo-class {
  property: value;
}
```

## 元素显示模式

html元素一般分为块元素和行内元素

### 块元素

常见的块元素有：`<h1>~<h6>`，`<p>`，`<div>`，`<ul>`，`<li>`等

块元素的特点：

- 独占一行
- 高度、宽度、外边距及内边距都可以控制
- 宽度默认是容器（父级宽度）的100%
- 是一个容器，里面可以放行内元素或者块元素

**文字类的元素内不能使用块级元素，例如：`<p>`，`<h1>~<h6>`**

### 行内元素

常见的行内元素有：`<a>`，`<strong>`，`b`，`em`，`<i>`，`<del>`，`<ins>`，`<u>`，`<span>`等

行内元素的特点：

- 相邻行内元素在一行上，一行可以显示多个
- 高、宽直接设置是无效的
- 默认宽度就是本身内容的宽度
- 行内元素只能容纳文本或其他行内元素

**链接`<a>`不能再放链接`<a>`，可以放块元素**

### 行内块元素

在行内元素中有几个特殊的标签：`<img>`，`<input>`，`<td>`等，同时具有块元素和行内元素的特点

行内块元素的特点：

- 和相邻行内元素（行内块）在一行上，但之间会有空白缝隙，一行可以显示多个
- 默认宽度就是本身内容的宽度
- 高度、行高、外边距及内边距都可以控制

### 元素显示模式转换

- 转换为块元素：`display:black`
- 转换为行内元素：`display:inline`
- 转换为行内块：`display:inline-block`

# 元素

## 字体 font

### 字体样式

`font-style` 属性主要用于指定斜体文本

此属性可设置三个值：

- normal - 文字正常显示
- italic - 文本以斜体显示
- oblique - 文本为“倾斜”（倾斜与斜体非常相似，但支持较少）

```css
p.normal {
  font-style: normal;
}

p.italic {
  font-style: italic;
}

p.oblique {
  font-style: oblique;
}
```



### 字体大小

`font-size` 属性设置文本的大小，单位可以是`px`或`em`

W3C 建议使用 `em` 尺寸单位。

1em 等于当前字体大小。浏览器中的默认文本大小为 16px。因此，默认大小 1em 为 16px。

```css
h1 {
  font-size: 40px;
}

h1 {
  font-size: 2.5em; /* 40px/16=2.5em */
}
```

使用`body`元素可以设置页面所有字体大小，但是`h*`元素必须单独设置

### 字体粗细

`font-weight` 属性指定字体的粗细

```css
p.normal {
  font-weight: normal;
}

p.thick {
    /*实际开发中提倡使用数字，表示加粗或者变细*/
  font-weight: bold; /*700*/
}
```

### 文字样式

`font-style` 属性定义字体的风格

该属性设置使用斜体、倾斜或正常字体。斜体字体通常定义为字体系列中的一个单独的字体



### 字体变体

`font-variant` 属性指定是否以 `small-caps` 字体（小型大写字母）显示文本

在 `small-caps` 字体中，所有小写字母都将转换为大写字母。但是，转换后的大写字母的字体大小小于文本中原始大写字母的字体大小

| 值      | 描述                                   |
| :------ | :------------------------------------- |
| normal  | 默认值。浏览器显示一个标准的字体样式。 |
| italic  | 浏览器会显示一个斜体的字体样式。       |
| oblique | 浏览器会显示一个倾斜的字体样式。       |
| inherit | 规定应该从父元素继承字体样式。         |

### 复合属性

```css
body{
    /*格式
    font: font-style font-weight font-size/line-height font-family;
    顺序不可颠倒，font-siez和font-family不可省略
    */
    font: italic 700 16px 'Microsoft yahei';
}
```



## 文本 text

### 文本颜色

`color`属性用于设置文本的颜色。颜色由以下值指定：

- 颜色名 - 比如 "red"
- 十六进制值 - 比如 "#ff0000"
- RGB 值 - 比如 "rgb(255,0,0)"

### 文本对齐

`text-align` 属性用于设置文本的水平对齐方式。

文本可以左对齐或右对齐，或居中对齐

### 文本装饰

`text-decoration` 属性用于设置或删除文本装饰。有四种属性：

- `none`：默认，没有装饰线
- `underline`：下划线，链接a自带下划线
- `overline`：上划线
- `line-through`：删除线

### 文本间距

#### 文字缩进

`text-indent` 属性用于指定文本第一行的缩进

#### 字母间距

`letter-spacing` 属性用于指定文本中字符之间的间距

#### 行高

`line-height` 属性用于指定行之间的间距，只能设置单行文本

![image-20210531105258169](image-20210531105258169.png)

#### 字间距

`word-spacing` 属性用于指定文本中单词之间的间距

#### 空白

`white-space` 属性指定元素内部空白的处理方式

## 背景

### 背景颜色

`background-color` 属性指定元素的背景色

### 背景图片

`background-image` 属性指定用作元素背景的图像

#### 背景色透明度

```css
/**
*	最后一个参数是alpha透明度，取值范围0～1，0不显示，1不透明
*	IE9+版本才支持
*/
backgroup: rgba(0,0,0,0.3)
```



### 背景重复

默认情况下，`background-image` 属性在水平和垂直方向上都重复图像

可以使用`background-repeat` 属性指定图片的重复方式

- `no-repeat`：只显示一次
- `repeat-x`：水平方向重复
- `repeat-y`：垂直方向重复

### 背景位置

`background-position` 属性用于指定背景图像的位置

- 参数使用方位
  - 使用两个方位参数则两个值前后顺序无关，如`left,top`=`top,left`
  - 使用一个方位参数则另一个方位值默认居住对其
- 参数使用精确值
  - 第一个值是x坐标，第二个值是y坐标
  - 如果只指定一个值，则该值为x坐标，另一个默认垂直居中
- 参数是混合单位
  - 第一个值是x坐标，第二个值是y坐标

### 背景附着

`background-attachment` 属性指定背景图像是应该滚动还是固定的（不会随页面的其余部分一起滚动）

```css
/** 
	scroll: 背景图片随对象内容滚动
	fixed: 背景图片固定
*/
background-attachment: scroll | fixed
```

### 背景属性简写

使用简写属性 `background`

```css
body {
  background: #ffffff url("tree.png") no-repeat right top;
}
```

在使用简写属性时没有固定顺序，属性缺失也没有关系，一般约定属性值的顺序为：

- background-color
- background-image
- background-repeat
- background-attachment
- background-position

# 控制文字，超出部分显示省略号

<p style="width: 300px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">

```html
<p style="width: 300px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">
```

单行文本的溢出显示省略号

```css
overflow: hidden;	#内容会被修剪，并且其余内容是不可见的
text-overflow:ellipsis; #显示省略符号来代表被修剪的文本
white-space: nowrap;	#文本不会换行，文本会在在同一行上继续，直到遇到 <br> 标签为止。
```

