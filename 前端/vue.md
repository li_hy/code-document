# MVC与前端MVVM的区别

- MVC是后端的粉尘开放概念
- MVVM是前端视图层概念，主要关注于视图层分离
  - MVVM将前端的视图层分为Model，View，VM ViewModel

![1601170768491](1601170768491.png)

# VUE生命周期

从Vue实例创建、运行、到销毁期间，总是伴随着各种各样的事件，这些事件，统称为生命周期。

生命周期钩子](https://cn.vuejs.org/v2/api/#选项-生命周期钩子)：就是生命周期事件的别名而已





# 语法

## 插值操作

-  `Mustache`：`{{ 参数/表达式 }}`，将data中的文本插入到html中

- `v-once`：表示元素和组件只渲染一次，不会随数据的改变而改变

- `v-html`：按照HMTL格式进行解析

- `v-text`：与`{{ }}`类似，但会覆盖组件中的原始值

- `v-pre`：用于显示原本的`Mustache`语法（不解析）

- `v-cloak`：用于数据未加载前屏蔽代码

  - ```css
    <style>
    [v-cloak] {
        display: none;
    }
    </style>
    ```

## 动态绑定

- `v-bind`：用于将值动态的绑定到标签的属性中，可使用缩写`:`
  - 可绑定变量，对象，函数，数组
    - 绑定变量：`:class="active"`
    - 绑定对象：`:class="{'active':isActive,'line':isLine}"`
    - 绑定函数：`:class="getClass()"`
    - 绑定数组：`:class="[active, line]"`

## 路由

### `$router`和`$route`

- `$router`：是VueRouter的一个对象，通过``Vue.use(VueRouter)``和``VueRouter`构造函数得到一个router的实例对象，这个对象中是一个全局的对象，他包含了所有的路由包含了许多关键的对象和属性
- `$route`：是当前正在跳转的路由对象，可以从里面获取name,path,params,query等

### 路由传参

- `params`
  - 配置路由格式：`/router/:id`
  - 传递发那个是：在path后面跟上对应的值
  - 传递后形成的路径：`/router/123`
- `query`
  - 配置路由格式：`/router`
  - 传递发那个是：对象中使用query的key作为传递方式
  - 传递后形成的路径：`/router?id=123`

# 工具

## `npm`



## `nrm`


# 语法

## 驼峰与`-`

- 驼峰命名可以不加引号
- `-`命名必须加引号

##  `const`，`let`与`var`

- `const`常量，初始化后不可再赋值。定义对象时对象不可再赋值，但对象属性可再赋值
- `var`没有块作用域，相对于全局变量，在块中使用需要用闭包
- `let`有块级作用域

## `v-on`参数

- 如果该方法不需要额外参数，方法后面的`()`可省略
  - 如果方法本身中有一个参数，那么会默认将原生事件`event`参数传递进去
- 如果需要同时传入其他参数与`event`时，可以通过`$event`传入事件

## `v-on`修饰符

- `.stop`阻止冒泡
- `.prevent`阻止默认行为
- `.{ekyCode|keyAlias}`特定键才触发回调
- `.native`监听组件根元素的原生事件
- `.once`只触发一次回调

## `key`

vue在进行dom渲染时，出于性能考虑，会尽可能的复用以存在的元素，而不是重新创建新的元素。

在`if-else`或类似的场景下，vue认为`if`中的元素不再使用，直接作为`else`中的元素来使用。

可以给对应的元素添加`key`，并保证`key`的值不一致来阻止vue重复使用元素

## `v-show`与`v-if`

- `v-if`当条件为false时会删除对应的元素
  - 当只有一次切换时，通常使用`v-if`
- `v-show`当条件为false时只是将元素的``display`属性设置为`none`
  - 当需要在现实与隐藏之间频繁切换时，使用`v-show`

## 使用`v-for`时给对应的元素或组件添加`key`属性

这和vue的虚拟DOM的Diff算法有关

使用`key`只有是为了高效的更新虚拟DOM

若没有`key`属性，插入数据时相当于操作数组，需要循环将数据后移

若有`key`属性，插入数据则相当于操作链表，只需要直接插入

## 数组响应式方法

- `push`在数组末尾增加元素
- `pop`删除数组中的最后一个元素
- `shift`删除数组中的第一个元素
- `unshift`在数组最前面添加元素
- `splice`删除元素/插入元素/替换元素
- `Vue.set(arr, index, value)`修改对应索引的值

通过索引修改数组中的元素是非响应式的，例如`arr[1]=1`

## `v-bind`与`v-model`

- `v-bind`是单向的，只能用于model向view传递
- `v-model`是双向的，model和view之间可相互传递
  - `v-model`本质包含两个操作
    - `v-bind`绑定一个value属性
    - `v-on`指令给当前元素绑定input事件
    - `<input type="text" v-mode="message"/>`等同于`<input type="text" v-bind:value="message" v-oninput="message=$event.target.value"/>`

## `v-model`修饰符

- `.lazy`让数据在失去焦点或者回车时才更新
- `.number`让输入框中输入的内容自动转成数字类型
- `.trim`过滤内容作用两边的空格

# 组件化

## 父子组件的同通信

- 父组件向子组件传递数据时使用`props`
- 子组件向父组件发生消息使用自定义事件`$emit Events`





# 踩坑

## vue-element-admin

### node-sass 下载失败

在项目根目录下添加了一个.npmrc

```
sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
registry=https://registry.npm.taobao.org
```

使用淘宝镜像下载

```
npm install --registry=https://registry.npm.taobao.org
```

### MSBUILD : error MSB4132: 无法识别工具版本“2.0”。可用的工具版本为 "4.0"

安装.new framework框架，若有部分无法安装可使用

```
npm config set msvs_version 2015 --global
```

element-admin 动态加载菜单

#### 加载菜单找不到模块

>   Cannot find module '@/views/**

将动态生成路由的

```
() => import(`@/views${url}`)  
```

改为

```
resolve => require([`@/views${url}`], resolve)
```

#### 刷新后404

```
// 404 page must be placed at the end !!!
{ path: '*', redirect: '/404', hidden: true }
```

`*`路由原来定义在默认路由中，动态路由在其后面加入，刷新后`*`会优先接管路由。

可在默认路由中删除，并在动态路由后再加入

#### 刷新后白屏

将动态路由加载之后的`next()`改为

```
next({...to, replace: true})
```

确保动态路由创建成功再去执行其它代码
