

# href标签前加前缀

```html
<a th:href="@{${model中的name值}}"/>
```



# 在页面中直接显示内容

## 使用`th:block`

```html
<th:block th:text="${msg}" />
```

## 使用 `inline`

```html
[[${msg}]]
```

## 使用 `th:remove`

```html
<span th:text="${msg}" th:remove="tag"></span>
```

# 调整日期格式

```html
<p th:text="${#dates.format(birthday,'yyyy-MM-dd')}"/>
```

# 循环

`th:each`属性用于迭代循环，语法：`th:each="obj,iterStat:${objList}"`

迭代对象可以是`Java.util.List`,`java.util.Map`,数组等;

`iterStat`称作状态变量，属性有：

- ​    `index`:当前迭代对象的index（从0开始计算）
- ​    `count`: 当前迭代对象的index(从1开始计算)
- ​    `size`:被迭代对象的大小
- ​    `current`:当前迭代变量
- ​    `even/odd`:布尔值，当前循环是否是偶数/奇数（从0开始计算）
- ​    `first`:布尔值，当前循环是否是第一个
- ​    `last`:布尔值，当前循环是否是最后一个


```html
<th:block th:each="group,groupStat : ${groups}">
    <!-- -->
	<th:block th:if="${groupStat.index % 4 ==0}"><br/>&nbsp;</th:block>
     <input type='checkbox' name='groupCode' id='groupCode'
           th:value="${group.groupCode}"
           th:text="${group.groupName}"
           th:checked='${group.checked == 1}'/>
</th:block>
```

