# 创建项目

## 使用`vue-cli`创建

```bash
#查看@vue/cli版本，确保@vue/lic版本在4.5.0以上
vue --version

#安装或升级@vue/cli
npm install -g #vue/cli

#创建项目，选择3.x
vue create 项目名
```

## 使用`vite`创建

```bash
npm create vue@lastest
```

# 脚手架文件

![项目结构](vue3\项目结构.png)

## `public`文件夹

存放网站图标

## `src`文件夹

存放源代码文件

## `env.d.ts`文件

用于定义环境变量（文件类型）的类型，在文件中定义环境变量的类型， TypeScript 可以在编译时提供类型检查和自动补全功能

`env.d.ts` 文件只定义了环境变量的类型，并不实际设置这些变量的值

## `index.html`文件

项目入口

## `package-lock.json`、`package.json`文件

依赖管理文件

## `tsconfig.app.json`、`tsconfig.json` 、`tsconfig.node.json`文件

1. `tsconfig.json`:
   - 这是项目的根 TypeScript 配置文件，位于项目根目录下。
   - 它定义了整个项目的 TypeScript 编译器选项，包括编译目标、模块解析策略、类型检查选项等。
   - Vite 使用这个文件来理解如何编译 TypeScript 文件，以及如何进行类型检查。
   - 它通常包含了 `include` 和 `exclude` 数组，用来指定哪些文件应该被编译，哪些文件应该被排除。
2. `tsconfig.app.json` (非默认):
   - 如果存在，这个文件可能是为了配置专门针对应用程序源代码的 TypeScript 编译选项。
   - 它可能包含了一些特定的配置，比如为应用程序代码启用特定的编译器选项或插件。
3. `tsconfig.node.json` (非默认):
   - 如果存在，这个文件可能是为了配置在 Node.js 环境中运行的 TypeScript 代码的编译选项。
   - 它可能用于服务器端渲染（SSR）或其他 Node.js 服务，确保服务器端的代码按照正确的配置进行编译。
   - 它也可能包含了一些特定的配置，比如不同的模块解析策略或编译目标。

## `vite.config.ts`文件

用来配置 `vite`本身的行为，包括构建选项、插件配置等

# 项目入口

- `vite`项目中，`index.html`是项目的入口文件，在项目最外层
- 加载`index.html`后，`vite`解析`<script type="module" src="/src/main.ts"></script>`指向的`javaScript`
- `vue3`中是通过`createApp`函数创建一个应用实例

```html
<!-- 项目主目录下的 index.html 文件 -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" href="/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vite App</title>
  </head>
  <body>
    <!-- 应用程序的主要容器 -->
    <div id="app"></div>
    <!-- 加载应用程序的主脚本 -->
    <script type="module" src="/src/main.ts"></script>
  </body>
</html>
```

```tsx
/* src/mail.ts  */

/**
 * 导入主样式文件
 */
import './assets/main.css'

/**
 * 导入Vue创建应用的方法和App组件
 * @param {Function} createApp - Vue创建应用的函数
 * @param {Object} App - 应用的根组件
 * @returns 无返回值
 */
import { createApp } from 'vue'
import App from './App.vue'

/**
 * 创建并挂载Vue应用
 * 将App组件实例化并挂载到HTML中id为"app"的元素上
 */
createApp(App).mount('#app')

```

# 核心语法

## `OptionsAPI`与`CompositionAPI`

- `vue2`的`api`设计是`Options`（配置）风格的
- `vue3`的`api`设计是`Composition`（组合）风格的

**`OptionAPI`的弊端**

`Options`类型的`API`，数据、方法、计算属性等是分散在：`data`、`methers`、`computed`中的，若想新增一个需求，就需要分别修改`data`、`methers`、`computed`，不便于维护和复用

<center class="half">
<img src="vue3\optionsApi1.gif" width="300" height="450"/>
<img src="vue3\optionsApi2.gif" width="300" height="450"/>
</center>
**`CompositionAPI`的优势**

可以用函数的方式更加优雅的组织代码，让相关功能的代码更加有序的组织在一起

<center class="half">
<img src="vue3\compositionApi1.gif" width="300" height="450"/>
<img src="vue3\compositionApi2.gif" width="300" height="450"/>
</center>
## `setup`

`setup`是`vue3`中一个新的配置项，值是一个函数，组件中所用到的数据、方法、计算属性、监视等均配置在`setup`中

`setup`的特点：

- `setup`函数返回的对象中的内容可直接在模板中使用
- `setup`中访问`this`是`undefined`
- `setup`函数会在`befareCreate`之前调用，领先所有钩子执行
- `setup`的返回值是一个渲染函数

**`setup`语法糖**

```html
<script lange="ts">
    export default {
        name: "xxx"
    }
</script>
<script lange="ts" setup>
</script>
```

**注意**：`setup`语法糖中不能定义组件名，需要单独定义。或使用`vite-plugin-vue-setup-extend`插件在`<script>`中定义组件名

```html
<!-- npm i vite-plugin-vue-setup-extend -D 后，并且在vite.config.ts文件中引入后可使用-->
<script lange="ts" setup name="xxx">
</script>
```

## 响应式数据

### 创建

`ref` 可以定义基本类型或对象类型的响应式数据

`reactive`只能定义对象类型的响应式数据

**区别**

- `ref`创建的变量必须使用`.value`（可以使用`volar`插件自动添加`.value`）
- `reactive`重新分配一个新对象会失去响应式（可以使用`Object.assign`来整体替换）

```typescript
//使用ref创建基本类型的响应式数据
let name = ref("张三")
//使用
console.log(name.value)
//修改
name.value = "李四"

//使用ref创建对象类型的响应式数据
let person = ref({name:"张三", age:18})
//使用
console.log(person.value.name)
//修改
person.value = {name:"李四", age:20}

//使用reactive创建对象类型的响应式数据
let person = reactive({name:"张三", age:18})
//使用
console.log(person.name)
//修改
Object.assign(person, {name:"李四", age:20})
```

**使用原则**

- 若需要一个基本类型的响应式数据，必须用`ref`
- 若需要一个响应式对象，层级不深，`ref`、`reactive`都可以
- 若需要一个响应式对象，且层级较深，推荐使用`reactive`

### 转换

`toRef`将一个响应式对象中的某一个属性转换为`ref`对象

`toRefs`将一个响应式对象中属性批量转换成`ref`对象

```typescript
let person = reactive({name:"张三", age:18})

let name = toRef(person, name)

let{name, age} = toRefs(person)
```

## 计算属性

`computed`可以根据已有数据计算出新数据

```typescript
let firstName = ref('zhang')
let lastName = ref('san')

// 计算属性——只读取，不修改
let fullName = computed(()=>{
    return firstName.value + '-' + lastName.value
}) 


// 计算属性——既读取又修改
let fullName = computed({
  // 读取
  get(){
    return firstName.value + '-' + lastName.value
  },
  // 修改
  set(val){
    console.log('有人修改了fullName',val)
    firstName.value = val.split('-')[0]
    lastName.value = val.split('-')[1]
  }
})
```

