# UML分类

UML图可分为结构型和行为型两种。

结构型的图描述的是某种结构，这种结构在某段时间内应该是稳定的，“静态”的；行为型的图描述的是某种行为，是“动态”的。

- `Structure Diagram`：结构型图
  - `Class Diagram`：类图
  - `Object Diagram`：对象图
  - `Componet Diagram`：构件图
  - `Deployment Diagram`：部署图
  - `Package Diagram` ：包图
- `Behavior Diagram`：行为型图
  - `Activity Diagram`：活动图
  - `StateMachine Diagram`：状态图
  - `Sequence Diagram`：时序图
  - `Communication Diagram`：通信图
  - `Use Case Diagram`：用例图
  - `Timing Diagram`：时序图

## 用例图（`UseCase Diagrams`）

从银行的角度描述系统的功能，并指出各个功能的执行者，强调用户的使用者，系统为执行者完成那些功能。

![1552877788116](case diagram.png)

## 类图（`Class Diagrams`）

根据用例图抽象成类，描述类的内部结构和类与类之间的关系。

在UML类图中，常见的有以下几种关系：

- 泛化（`Generalization`）
- 实现（`Realization`）
- 关联（`Association`）
- 聚合（`Aggregation`）
- 组合（`Composition`）
- 依赖（`Dependency`）

各种关系的强弱顺序为：泛化 = 实现 > 组合 > 聚合 > 关联 > 依赖

### 泛化（`Generalization`）

是一种继承关系，标识一般与特殊的关系，指定了子类如果继承父类的所有特征和行为。例如：老虎是动物的一种，即有老虎的特性也有动物的共性。

![1552878243142](Generalization.png)

### 实现（`Realization`）

是一种类与接口的关系，表示类是接口所有特征和行为的实现。

![1552878418486](Realization.png)

### 关联（`Association`）

是一种拥有的关系，使一个类知道另一个类的属性和方法。如：老师和学生，账号与妻子。关联可以是双向的，也可以是单向的。双向的关联可以有两个箭头或者没有箭头，单向的关联有一个箭头。

![1552878606636](Association.png)

### 聚合（`Aggregation`）

是整体与部分的关系，且部分可以离开整体而单独存在。如车和轮胎。

聚合关系是关联关系的一种，是强的关联关系；关联和聚合在语法上无法区分，必须考察具体的逻辑关系。

![1552878740979](Aggregation.png)

### 组合（`Composition`）

是整体与部分的关系，但部分不能离开整体而单独存在。如公司和部门。

组合关系是关联关系的一种，是比聚合关系还有强的关系，要求普通的聚合关系中代表整体的对象负责代表部分的对象的生命周期。

![1552878929218](Composition.png)

### 依赖（`Dependency`）

是一种使用的关系，即一个类的实现需要另一个类的协助，尽量不使用双向的互相依赖。

![1552879056000](Dependency.png)

### 各种类图关系

![1552879106079](各种类图关系.png)

## 对象图（`Object Diagrams`）

描述的是参与交互的各个对象在交互过程中某一时刻的状态，可以看作是类图在某一时刻的实例。

对象图可以被想象成政治运行的系统在某一时刻的快照。

![1552880116561](Object Diagrams.png)

## 状态图（`Statechart Diagrams`）

是一种有状态、变迁、时间和活动组成的状态机，用来描述类的对象所有可能的状态以及世界发生时状态的转移条件。

![1552890434745](Statechart Diagrams.png)

## 活动图（`Activity Diagrams`）

是状态图的一种特殊情况，这些状态大都处于活动状态，本质是一种流程图，描述了活动到活动的控制流。

![1552890804431](Activity Diagrams.png)

## 时序图（`Sequence Diagrams`）

描述了对象之间消息发送的先后顺序，强调时间顺序。

主要用途是把用例表达的需求，转化为进一步、更加正式层次的精细表达。时序图能更有效的描述如何分配各个类的职责以及各个类具有相应职责的原因。

![1552891253710](Sequence Diagrams.png)

## 构建图（`Component Diagrams`）

用来表示系统中构件与构件之间，类或接口与构件之间的关系图。其中构件图之间的关系表现为依赖关系，定义的类或接口与类之间的关系表现为依赖关系或实现关系。

![1552891846480](Component Diagrams.png)

## 部署图（`Deployment Diagrams`）

描述了系统运行时进行处理的节点以及在节点上活动的构件的配置。强调了物理设备以及之间的连接关系。

![1552892026133](Deployment Diagrams.png)

## 包图（`Package Diagram`）

主要用于“打包”类图。用类图描述业务概念时，很多时候会因为业务类太多，而导致类图非常庞大，不利于阅读，这时可以将某些类放入“包”中，通过包图来组织业务概念图。

![1552892326902](Package Diagram.png)