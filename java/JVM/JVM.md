# 什么是JVM

>JVM是Java Virtual Machine（Java虚拟机）的缩写，JVM是一种用于计算设备的规范，它是一个虚构出来的计算机，是通过在实际的计算机上仿真模拟各种计算机功能来实现的。
>
>Java语言的一个非常重要的特点就是与平台的无关性。而使用Java虚拟机是实现这一特点的关键。一般的高级语言如果要在不同的平台上运行，至少需要编译成不同的目标代码。而引入Java语言虚拟机后，Java语言在不同平台上运行时不需要重新编译。Java语言使用Java虚拟机屏蔽了与具体平台相关的信息，使得Java语言编译程序只需生成在Java虚拟机上运行的目标代码（字节码），就可以在多种平台上不加修改地运行。Java虚拟机在执行字节码时，把字节码解释成具体平台上的机器指令执行。这就是Java的能够“一次编译，到处运行”的原因。

JVM主要用于软件层面机器码的翻译及内存管理。

![JVM版本信息](JVM版本信息.png)

HotSpot：

Sun JDK和OpenJDK中所带的虚拟机，也是目前使用范围最广的Java虚拟机。除此之外还有其他的java虚拟机：

- Oracle JDK——主要使用在sun系统和Wondows系统； 
- Open JDK——linux系统上，开源免费； 
- IBM IDK——主要使用在IBM相关的Unix系统上； 
- Harmony JDK——已基本废弃，开源免费； 
- HP JDK——主要使用在HP-UNIX系统上；

Server：

JVM分为server模式和client模式，最主要的差别在于：-Server模式启动时，速度较慢，但是一旦运行起来后，性能将会有很大的提升。

JVM会根据运用环境自动选择模式，32位系统会使用client模式（只有32位系统才有client模式），64位系统会使用server模式。

mixed mode：

混合模式，这是HotSpot默认的运行模式，意味着JVM在运行时可以动态的把字节码编译为本地代码。

它会同时使用编译模式和解释模式。对于字节码中多次被调用的部分，JVM会将其编译成本地代码以提高执行效率；而被调用很少（甚至只有一次）的方法在解释模式下会继续执行，从而减少编译和优化成本。



## JVM的位置

![JVM的位置](JVM的位置.png)

## JVM体系结构

![JVM体系结构](JVM体系结构.png)

### 类加载器ClassLoader

![类加载器](类加载器.png)

负责加载class文件，class文件在文件开头有特定的文件标示，并且ClassLoader只负责class文件的加载，至于是否可以运行，则由Execution Engine决定。

`Car.class`是静态文件，被`ClassLoader`加载后变成类的原信息描述（即`Car Class`对象），Car Class是模板类，其他实例都是基于此模板。

ClassLoader的父对象是`Object`



![类加载器结构](类加载器结构.png)

虚拟机自带的加载器：

- 启动类加载器（Bootstrap），C++开发

- 扩展类加载器（Extension），Java开发

- 应用程序类加载器（AppClassLoader），Java开发

  也就系统类加载器，加载当前应用的classpath的所有类

用户自定义加载器

- Java.lang.ClassLoader的子类，用户可以定制类的加载方式

**双亲委派机制**

某个特定的类加载器在接到加载类的请求时，首先将加载任务委托给父类加载器，依次递归，如果父类加载器可以完成类加载任务，就成功返回；只有父类加载器无法完成此加载任务时，才自己去加载。

**沙箱安全机制**

沙箱机制是由基于双亲委派机制上采取的一种JVM的自我保护机制，假设你要写一个java.lang.String 的类，由于双亲委派机制的原理，此请求会先交给Bootstrap试图进行加载，但是Bootstrap在加载类时首先通过包和类名查找rt.jar中有没有该类，有则优先加载rt.jar包中的类，因此就保证了java的运行机制不会被破坏。

### Execution Engine执行引擎

执行引擎负责解释命令，提交操作系统执行

### Native  Interface本地接口

本地接口的作用是融合不同的编程语言为java所用

### Native Method Stack本地方法栈

登记native方法，在Execution Engine执行时加载本地方法库

### Program Counter  Register程序计数器（PC寄存器）

每个线程都有一个程序计数器，是线程私有的，就是一个指针，指向方法区中的方法字节码（用来存储指向下一条指令的地址，也即将要执行的指令代码），由执行引擎读取下一条指令，是一个非常小的内存空间，几乎可以忽略不计。

### Method Area方法区

方法区是被所有线程共享，所有字段和方法字节码，以及一些特殊方法如构造函数，接口代码也在此定义。简单说，所有定义的方法的信息都保存在该区域，此区域属于共享区间。又叫做非堆（Non-Heap）。

静态变量+常量+类信息（构造方法/接口定义）+运行时常量池 都存在方法区中。

运行时常量池（Constant Pool）：存放常量、方法、字段、类的名称和描述符及全限定名。

方法区在JDK1.7中是使用永久代来实现，在JDK1.8以后是使用元空间（Meta Space）来实现的。

### Java Stack虚拟机栈

栈也叫栈内存，主管java程序的运行，是在线程创建时创建，生命周期跟随线程的生命周期，线程结束栈内存也就释放，对于栈来说不存在垃圾回收问题，只有线程一结束该栈就释放，是线程私有的。

8种基本类型的变量+对象的引用变量+实例方法都是在函数的栈内存中分配。

栈会存储以下信息：

- 本地变量（Local Variables）：输入参数和输出参数以及方法内的变量
- 栈操作（Operand Stack）：记录出栈、入栈的操作
- 栈帧数据（Frame Data）：包括类文件、方法等

#### 栈帧

![栈帧](栈帧.png)

栈帧是用来存储数据和部分过程结果的数据结构，同时也用来处理动态连接、方法返回值和异常分派。
栈帧随着方法调用而创建，随着方法结束而销毁——无论方法正常完成还是异常完成都算作方法结束。
栈帧的存储空间由创建它的线程分配在Java虚拟机栈之中，每一个栈帧都有自己的本地变量表(局部变量表)、操作数栈和指向当前方法所属的类的运行时常量池的引用。

**局部变量表**

局部变量表(Local Variable Table)是一组变量值存储空间，用于存放方法参数和方法内定义的局部变量。局部变量表的容量以变量槽(Variable Slot)为最小单位，Java虚拟机规范并没有定义一个槽所应该占用内存空间的大小，但是规定了一个槽应该可以存放一个32位以内的数据类型。

一个局部变量可以保存一个类型为boolean、byte、char、short、int、float、reference和returnAddress类型的数据。reference类型表示对一个对象实例的引用。

虚拟机通过索引定位的方法查找相应的局部变量，索引的范围是从0~局部变量表最大容量。如果Slot是32位的，则遇到一个64位数据类型的变量(如long或double型)，则会连续使用两个连续的Slot来存储。

**操作数栈**

操作数栈(Operand Stack)也常称为操作栈，它是一个后入先出栈(LIFO)。

操作数栈的每一个元素可以是任意Java数据类型，32位的数据类型占一个栈容量，64位的数据类型占2个栈容量,且在方法执行的任意时刻，操作数栈的深度都不会超过max_stacks中设置的最大值。

**动态连接**

在一个class文件中，一个方法要调用其他方法，需要将这些方法的符号引用转化为其在内存地址中的直接引用，而**符号引用存在于方法区中的运行时常量池**。

Java虚拟机栈中，每个栈帧都包含一个指向运行时常量池中该栈所属方法的符号引用，持有这个引用的目的是为了支持方法调用过程中的动态连接(Dynamic Linking)。

这些符号引用一部分会在类加载阶段或者第一次使用时就直接转化为直接引用，这类转化称为**静态解析**。另一部分将在每次运行期间转化为直接引用，这类转化称为动态连接。

**方法返回**

**当一个方法开始执行时，可能有两种方式退出该方法：**

- **正常完成出口**
- **异常完成出口**

**正常完成出口**是指方法正常完成并退出，没有抛出任何异常(包括Java虚拟机异常以及执行时通过throw语句显式抛出的异常)。如果当前方法正常完成，则根据当前方法返回的字节码指令，这时有可能会有返回值传递给方法调用者(调用它的方法)，或者无返回值。具体是否有返回值以及返回值的数据类型将根据该方法返回的字节码指令确定。

**异常完成出口**是指方法执行过程中遇到异常，并且这个异常在方法体内部没有得到处理，导致方法退出。

无论方法采用何种方式退出，在方法退出后都需要返回到方法被调用的位置，程序才能继续执行，方法返回时可能需要在当前栈帧中保存一些信息，用来帮他恢复它的上层方法执行状态。

**附加信息**

虚拟机规范允许具体的虚拟机实现增加一些规范中没有描述的信息到栈帧之中，例如和调试相关的信息，这部分信息完全取决于不同的虚拟机实现。在实际开发中，一般会把动态连接，方法返回地址与其他附加信息一起归为一类，称为栈帧信息。

![java stack](java stack.png)

图示在一个栈中有两栈帧：

- 栈帧 2是最先被调用的方法，先入栈
- 然后方法2调用了方法1，栈帧1处于栈顶位置
- 栈帧2处于栈底，执行完毕后，依次弹出栈帧1和栈帧2
- 线程结束，栈释放

每执行一个方法都会产生栈帧，保存到栈（后进先出）的顶部，顶部栈就是当前的方法，该方法执行完毕后会自动将此栈出栈。

#### 栈、堆、方法区的交互关系

![栈、堆、方法区的交互关系](栈、堆、方法区的交互关系.png)

HotSpot使用指针的方式来访问对象：

java堆中会存放访问类元数据的地址，reference存储的是对象的地址

### Heap堆

一个 JVM 实例只存在一个堆内，堆的大小是可以调节的。类加载器读取了文件后，需要把类、方法、常变量放到堆内存中，保存所有引用类型的真实信息，以方便执行器执行，堆内存**逻辑上**分为三部分：

- Young Generation Space 新生代（Young/New）
- Tenure Generation Space 老年代 （Old/Tenure）
- Permanent Space 永久代 （Perm）

**Java1.8堆模型**

![Java1.8堆模型](Java1.8堆模型.png)

**永久代**

永久代是一个常驻内存区域，用于存放JDK自身所携带的Class，Intrerface的元数据，也就是说存储的是运行环境必须的类信息，被装载进此区域的数据是不会被垃圾回收器回收掉，关闭JVM才会释放此区域所占用的内存。是**方法区的实现方式**。

如果出现`java.lang.OutOfMemoryError: PermGen space`，说明是java程序对永久代Perm内存设置不够。一般出现这种情况都是程序启动需要加载大量的第三方jar包。例如：在一个Tomcat下部署了太多的应用；或者大量动态反射生成的类不断被加载，最终导致Perm区被占满。

JDK1.6及之前：有永久代，常量池在方法区

JDK1.7：有永久代，但已经逐步“去永久代”，常量池在堆中

JDK1.8及之后：无永久代，常量池在元空间

# 对象

## 对象的创建

![对象的创建](对象的创建.png)

### 对象的创建方式

- 使用new关键字，调用了构造函数

  ```java
  Employee emp1 = new Employee();
  ```

- 使用Class类的newInstance方法，调用了构造函数

  ```java
  Employee emp2 = (Employee) Class.forName("org.programming.mitra.exercises.Employee").newInstance();
  //或者
  Employee emp2 = Employee.class.newInstance();
  ```

- 使用Constructor类的newInstance方法，调用了构造函数

  ```java
  Constructor<Employee> constructor = Employee.class.getConstructor();
  Employee emp3 = constructor.newInstance();
  ```

- 使用clone方法，没有调用构造函数

  ```java
  Employee emp4 = (Employee) emp3.clone();
  ```

- 使用反序列化，没有调用构造函数

  ```java
  ObjectInputStream in = new ObjectInputStream(new FileInputStream("data.obj"));
  Employee emp5 = (Employee) in.readObject();
  ```

### 对象分配内存

分配方式：

- 指针碰撞(Serial、ParNew等带Compact过程的收集器) 

  假设Java堆中内存是绝对规整的，所有用过的内存都放在一边，空闲的内存放在另一边，中间放着一个指针作为分界点的指示器，那所分配内存就仅仅是把那个指针向空闲空间那边挪动一段与对象大小相等的距离，这种分配方式称为“指针碰撞”（Bump the Pointer）。 

- 空闲列表(CMS这种基于Mark-Sweep算法的收集器) 

  如果Java堆中的内存并不是规整的，已使用的内存和空闲的内存相互交错，那就没有办法简单地进行指针碰撞了，虚拟机就必须维护一个列表，记录哪些内存块是可用的，在分配的时候从列表中找到一块足够大的空间划分给对象实例，并更新列表上的记录，这种分配方式称为“空闲列表”（Free List）。

分配流程：

![对象分配流程](对象分配流程.png)

#### 栈上分配

针对那些**作用域不会逃逸出方法的对象**，在分配内存时不再将对象分配在堆内存中，而是将对象属性**打散后分配在栈（线程私有的，属于栈内存）上**，这样，随着方法的调用结束，栈空间的回收就会随着将栈上分配的打散后的对象回收掉，不再给gc增加额外的无用负担，从而提升应用程序整体的性能。

栈上分配的前提：

- 开启逃逸分析 (-XX:+DoEscapeAnalysis)

  逃逸分析的作用就是分析对象的作用域是否会逃逸出方法之外，在server虚拟机模式下才可以开启（jdk1.6默认开启）

- 开启标量替换 (-XX:+EliminateAllocations)

  标量替换的作用是允许将对象根据属性打散后分配再栈上，默认该配置为开启

#### TLAB

全称叫做：Thread Local Allocation Buffer 即线程本地分配缓存。

给每个线程分配一小块私有的堆空间，在分配对象到堆空间时，先分配到自己所属的那一块堆空间中，**避免同步**带来的效率问题，从而**提高分配效率**。

JVM默认开启了TLAB功能，也可以使用-XX: +UseTLAB 显示开启。

#### 直接进入老年代的条件

- 对象很大

  -XX : PretenureSizeThreshold=xxx（超过指定大小直接进入老年代）

- 长期存活的对象

  -XX : MaxTenuringThreshold=x（超过指定回收次数直接进入老年代，默认15）

- 动态对象年龄判断

  相同年龄所有对象的大小总和>Surivor空间的一半，年龄大于或等于该年龄的对象就可以直接进入老年代
  
  -XX:TargetSurvivorRatio

## 对象的结构

- Header（对象头）
  - 自身运行时数据（Mark Word）
    - 哈希值
    - GC分代年龄
    - 锁状态标志
    - 线程持有锁
    - 偏向线程ID
    - 偏向时间戳
  - 类型指针
  - 数组长度（只有数组对象才有）
- InstanceData
  - 相同宽度的数据分配到一起（long，double）
- Padding（对齐填充）
  - 8个字节的整数倍

### Hotspot虚拟机对象头Mark Word

32位Mark Word

![32位mark word](32位mark word.png)

64位Mark word

![64位mark word](64位mark word.png)

## 对象的访问

- 使用句柄访问，Java堆中将会划分出一块内存来作为句柄池，obj（reference引用）中存储的是对象的句柄地址，而句柄中包含了类数据的地址和对象实例数据的地址。
- 直接指针访问，Java堆中也就是对象中存储所有的实例数据和类数据的地址，此时obj（reference引用）存放的是对象地址。 

两种访问方式的对比：

- 使用句柄时，当改变句柄中的实例数据指针时，reference本身不需要被修改。
- 使用直接指针访问最大的好处在于速度较快，因为其节省了一次指针定位的时间开销。

HotSpot虚拟机采用的是指针访问，因为对象的访问在Java程序运行过程中是比较频繁的，积少成多也会造成太多的时间开销。java堆中会存放访问类元数据的地址，refernece存储是对象的地址。

![对象的访问](对象的访问.png)

# 垃圾回收

## 如何判断对象为垃圾对象

- 引用计数法

  给对象中添加一个引用计数器，每当一个地方引用这个对象时，计数器值+1；当引用失效时，计数器值-1。任何时刻计数值为0的对象就是不可能再被使用的。

  这种算法使用场景很多，但是，Java中却没有使用这种算法，因为这种算法很难解决对象之间相互引用的情况。

- 可达性分析法

  这个算法的基本思想是通过一系列称为“GC Roots”的对象作为起始点，从这些节点向下搜索，搜索所走过的路径称为引用链，当一个对象到GC Roots没有任何引用链（即GC Roots到对象不可达）时，则证明此对象是不可用的。

**在可达性分析算法中不可达的对象，如果此对象实现了`finalize()`方法，并在`finalize()`方法中重新与引用链建立关联关系，则不会被回收。**

在Java语言中可以作为GC Roots的对象包括：

- 虚拟机栈中引用的对象
- 方法区中静态属性引用的对象
- 方法区中常量引用的对象
- 本地方法栈中JNI（即Native方法）引用的对象

## 类的引用类型

- 强引用

  代码中普遍存在的类似”Object obj = new Object()”这类的引用，只要强引用还存在，垃圾收集器永远不会回收掉被引用的对象

- 软引用

  描述有些还有用但并非必需的对象。在系统将要发生内存溢出异常之前，将会把这些对象列进回收范围进行二次回收。如果这次回收还没有足够的内存，才会抛出内存溢出异常。Java中的类SoftReference表示软引用

- 弱引用

  描述非必需对象。被弱引用关联的对象只能生存到下一次垃圾回收之前，垃圾收集器工作之后，无论当前内存是否足够，都会回收掉只被弱引用关联的对象。Java中的类WeakReference表示弱引用

- 虚引用

  这个引用存在的唯一目的就是在这个对象被收集器回收时收到一个系统通知，被虚引用关联的对象，和其生存时间完全没关系。必须与引用队列（ReferenceQueue）联合使用。使用Java中的类PhantomReference表示虚引用

## 回收类型

- Minor GC：从新生代回收内存
- Major GC：从老年代回收内存
- Full GC：清理整个堆空间。Full GC = Minor GC + Major GC

## 何时回收

### Minor GC触发条件

当Eden区满时，触发Minor GC。

### Full GC触发的条件

- 直接调用System.gc
-  老年代空间不足
-  方法区空间不足
-  通过Minor GC后进入老年代的平均大小大于老年代的可用内存
- 分配担保失败

### 分配担保

在发生Minor GC之前，JVM会检查老年代最大可用的连续空间是否大于新生代所有对象总空间。如果条件成立，那么Minor GC是安全的。反之，如果不成立，那么仍然要看HandlePromotionFailure值，是否允许担保失败。如果允许担保失败，那么会继续检查老年代最大可用的连续空间是否大于历次晋升到老年代对象的平均大小。如果大于，则冒险尝试一次Minor GC，如果小于或者不允许担保失败，则要进行一次Full GC。

### 方法区GC

方法区主要回收的内容有：废弃常量和无用的类。对于废弃常量也可通过引用的可达性来判断，但是对于无用的类则需要同时满足下面3个条件：

- 该类所有的实例都已经被回收，也就是Java堆中不存在该类的任何实例；
- 加载该类的`ClassLoader`已经被回收；
- 该类对应的`java.lang.Class`对象没有在任何地方被引用，无法在任何地方通过反射访问该类的方法。

## 如何回收

### 回收策略

- 标记清除算法

  首先标记出所有需要回收的对象，在标记完成后统一回收所有标记的对象

  - 不足：

    1.效率问题，标记和清除两个过程效率不高

    2.空间问题，标记清除产生大量内存碎片

- 复制回收算法

  将可用内存按容量划分为大小相等的两块，每次只使用其中的一块。当这一块的内存用完了，就将还存活着的对象复制到另外一块上面，然后再把已使用过的内存空间一次清理掉。这样使得每次都是对其中的一块进行内存回收，内存分配时也就不用考虑内存碎片等复杂情况，只要移动堆顶指针，按顺序分配内存即可，实现简单，运行高效。只是这种算法的代价是将内存缩小为原来的一半，内存利用率太低。

- 标记整理算法

  过程与标记-清除算法一样，但后续步骤不是直接对可回收对象进行清理，而是让所有存活的对象向一端移动，然后清理掉端边界以外的内存。

- 分代收集算法

  基于对对象生命周期分析后得出的垃圾回收算法。把对象分为年青代、年老代、持久代，对不同生命周期的对象使用不同的算法（上述方式中的一个）进行回收。

**分配担保**

在发生minor gc之前，虚拟机会检测 : 老年代最大可用的连续空间>新生代all对象总空间？

- 满足，minor gc是安全的，可以进行minor gc
- 不满足，虚拟机查看HandlePromotionFailure参数
  - 为true，允许担保失败，会继续检测老年代最大可用的连续空间>历次晋升到老年代对象的平均大小
    - 若大于，将尝试进行一次minor gc
    - 若失败，则重新进行一次full gc。
  - 为false，则不允许冒险，要进行full gc（对老年代进行gc）

### 垃圾回收器

垃圾收集器就是垃圾回收算法的具体实现。

![垃圾回收器](垃圾回收器.png)

上面有7种收集器，分为两块，上面为新生代收集器，下面是老年代收集器。如果两个收集器之间存在连线，就说明它们可以搭配使用。

新生代使用标记复制算法，老年代使用并发标记清除（CMS）或标记-压缩（内存向一端移动）。

收集器类型是 

- 年轻： 串行（Serial） 并行（ParNew）
- 老年： 并发（CMS） 串行（Serial Old） 并行（Parallel Old）

收集器的并行和并发的区别：并行是GC线程有多个， 但在运行GC线程时 ，用户线程是阻塞的；并发收集器 ，是大部分阶段用户线程和GC线程都在运行，这里说的大部分是因为CMS在初始标记和重新标记阶段仍会发生STW（Stop The World）的时候

#### Serial(串行GC)收集器

Serial收集器是一个新生代收集器，单线程执行，使用复制算法。在进行垃圾收集时，必须暂停其他所有的工作线程(用户线程)。是Jvm client模式下默认的新生代收集器。对于限定单个CPU的环境来说，Serial收集器由于没有线程交互的开销，专心做垃圾收集自然可以获得最高的单线程收集效率。

![Serial收集器](Serial收集器.png)

#### ParNew(并行GC)收集器

ParNew收集器其实就是serial收集器的多线程版本，除了使用多条线程进行垃圾收集之外，其余行为与Serial收集器一样。

![ParNew收集器](ParNew收集器.png)

#### Parallel Scavenge(并行回收GC)收集器

Parallel Scavenge收集器也是一个新生代收集器，它也是使用复制算法的收集器，又是并行多线程收集器。parallel Scavenge收集器的特点是它的关注点与其他收集器不同，CMS等收集器的关注点是尽可能地缩短垃圾收集时用户线程的停顿时间，而parallel Scavenge收集器的目标则是达到一个可控制的吞吐量。吞吐量= 程序运行时间/(程序运行时间 + 垃圾收集时间)。

```properties
-XX:+UseParNewGC  打开此开关参数后，使用ParNew+Serial Old收集器组合进行垃圾收集。
-XX:+UseParallelOldGC  打开此开关参数后，使用Parallel Scavenge+Parallel Old收集器组合进行垃圾收集。
-XX:+ParallelGCThreads   设置并行GC时进行内存回收的线程数。
-XX:+MaxGCPauseMillis    Parallel Scavenge收集器最大GC停顿时间。
-XX:+GCTimeRation    Parallel Scavenge收集器运行时间占总时间比率。
-XX:+UseAdaptiveSizePolicy   java虚拟机动态自适应策略，动态调整年老代对象年龄和各个区域大小。
```

#### CMS(并发GC)收集器

CMS(Concurrent Mark Sweep)收集器是一种以获取最短回收停顿时间为目标的收集器。CMS收集器是基于“标记-清除”算法实现的，整个收集过程大致分为4个步骤：

1. 初始标记(CMS initial mark)
2. 并发标记(CMS concurrenr mark)
3. 重新标记(CMS remark)
4. 并发清除(CMS concurrent sweep)

其中初始标记、重新标记这两个步骤任然需要停顿其他用户线程。初始标记仅仅只是标记出GC ROOTS能直接关联到的对象，速度很快，并发标记阶段是进行GC ROOTS 根搜索算法阶段，会判定对象是否存活。而重新标记阶段则是为了修正并发标记期间，因用户程序继续运行而导致标记产生变动的那一部分对象的标记记录，这个阶段的停顿时间会被初始标记阶段稍长，但比并发标记阶段要短。

由于整个过程中耗时最长的并发标记和并发清除过程中，收集器线程都可以与用户线程一起工作，所以整体来说，CMS收集器的内存回收过程是与用户线程一起并发执行的。

CMS收集器的优点：并发收集、低停顿，但是CMS还远远达不到完美，器主要有三个显著缺点：

- CMS收集器对CPU资源非常敏感。在并发阶段，虽然不会导致用户线程停顿，但是会占用CPU资源而导致引用程序变慢，总吞吐量下降。CMS默认启动的回收线程数是：(CPU数量+3) / 4。
- CMS收集器无法处理浮动垃圾，可能出现“Concurrent Mode Failure“，失败后而导致另一次Full  GC的产生。由于CMS并发清理阶段用户线程还在运行，伴随程序的运行自然会有新的垃圾不断产生，这一部分垃圾出现在标记过程之后，CMS无法在本次收集中处理它们，只好留待下一次GC时将其清理掉。这一部分垃圾称为“浮动垃圾”。也是由于在垃圾收集阶段用户线程还需要运行，需要预留足够的内存空间给用户线程使用，因此CMS收集器不能像其他收集器那样等到老年代几乎完全被填满了再进行收集，需要预留一部分内存空间提供并发收集时的程序运作使用。在默认设置下，CMS收集器在老年代使用了68%的空间时就会被激活，也可以通过参数-XX:CMSInitiatingOccupancyFraction的值来提供触发百分比，以降低内存回收次数提高性能。要是CMS运行期间预留的内存无法满足程序其他线程需要，就会出现“Concurrent Mode Failure”失败，这时候虚拟机将启动后备预案：临时启用Serial Old收集器来重新进行老年代的垃圾收集，这样停顿时间就很长了。所以说参数-XX:CMSInitiatingOccupancyFraction设置的过高将会很容易导致“Concurrent Mode Failure”失败，性能反而降低。
- 最后一个缺点，CMS是基于“标记-清除”算法实现的收集器，使用“标记-清除”算法收集后，会产生大量碎片。空间碎片太多时，将会给对象分配带来很多麻烦，比如说大对象，内存空间找不到连续的空间来分配不得不提前触发一次Full  GC。为了解决这个问题，CMS收集器提供了一个-XX:UseCMSCompactAtFullCollection开关参数，用于在Full  GC之后增加一个碎片整理过程，还可通过-XX:CMSFullGCBeforeCompaction参数设置执行多少次不压缩的Full  GC之后，跟着来一次碎片整理过程。



![CMS收集器](CMS收集器.png)

#### Serial Old(串行GC)收集器

Serial Old是Serial收集器的老年代版本，它同样使用一个单线程执行收集，使用“标记-整理”算法。主要使用在Client模式下的虚拟机。

#### Parallel Old(并行GC)收集器

Parallel Old是Parallel Scavenge收集器的老年代版本，使用多线程和“标记-整理”算法。

#### G1收集器

G1(Garbage First)收集器是JDK1.7提供的一个新收集器，G1收集器基于“标记-整理”算法实现，也就是说不会产生内存碎片。

G1收集器将整个Java堆分为多个大小相等的独立区域（Region），虽然还保留有新生代和老年代的概念，但新生代和老年代不再是物理隔离的了，它们都是一部分Region的集合。G1收集器跟踪各个Region里面的垃圾堆积的价值大小，在后台维护一个优先列表，每次根据允许的收集时间，优先回收价值最大的Region（这也是Garbage-First名称的由来）。这种使用Region划分内存空间以及有优先级的区域回收方式，保证了G1收集器在有限的时间内可以获取尽可能高的收集效率。

![G1收集器](G1收集器.png)

G1收集与以上三组收集器有很大不同：

- G1的设计原则是"首先收集尽可能多的垃圾(Garbage First)"。因此，G1并不会等内存耗尽(串行、并行)或者快耗尽(CMS)的时候开始垃圾收集，而是在内部采用了启发式算法，在老年代找出具有高收集收益的分区进行收集。同时G1可以根据用户设置的暂停时间目标自动调整年轻代和总堆大小，暂停目标越短年轻代空间越小、总空间就越大；
- G1采用内存分区(Region)的思路，将内存划分为一个个相等大小的内存分区，回收时则以分区为单位进行回收，存活的对象复制到另一个空闲分区中。由于都是以相等大小的分区为单位进行操作，因此G1天然就是一种压缩方案(局部压缩)；
- G1虽然也是分代收集器，但整个内存分区不存在物理上的年轻代与老年代的区别，也不需要完全独立的survivor(to space)堆做复制准备。G1只有逻辑上的分代概念，或者说每个分区都可能随G1的运行在不同代之间前后切换；
- G1的收集都是STW的，但年轻代和老年代的收集界限比较模糊，采用了混合(mixed)收集的方式。即每次收集既可能只收集年轻代分区(年轻代收集)，也可能在收集年轻代的同时，包含部分老年代分区(混合收集)，这样即使堆内存很大时，也可以限制收集范围，从而降低停顿。

```properties
-XX:+UseG1GC  开启G1收集器
```

![G1内存模型](G1内存模型.png)

#### ZGC

在JDK 11当中，加入了实验性质的ZGC。它的回收耗时平均不到2毫秒。它是一款低停顿高并发的收集器。

ZGC几乎在所有地方**并发**执行的，除了初始标记的是STW的。所以停顿时间几乎就耗费在初始标记上，这部分的实际是非常少的。

ZGC主要新增了两项技术，一个是着色指针Colored Pointer，另一个是读屏障Load Barrier。

ZGC 是一个并发、基于区域（region）、增量式压缩的收集器。Stop-The-World 阶段只会在根对象扫描（root scanning）阶段发生，这样的话 GC 暂停时间并不会随着堆和存活对象的数量而增加。

##### ZGC 的设计目标

- TB 级别的堆内存管理；
- 最大 GC Pause 不高于 10ms；
- 最大的吞吐率（Throughput）损耗不高于 15%；
- 关键点：GC Pause 不会随着 堆大小的增加 而增大。

##### ZGC 中关键技术

- 加载屏障（Load barriers）技术；
- 有色对象指针（Colored pointers）；
- 单一分代内存管理；
- 基于区域的内存管理；
- 部分内存压缩；
- 即时内存复用。

##### 并行化处理阶段

- 标记(Marking)；
- 重定位（Relocation）/压缩（Compaction）；
- 重新分配集的选择（Relocation set selection）；
- 引用处理（Reference processing）；
- 弱引用的清理（WeakRefs Cleaning）;
- 字符串常量池（String Table）和符号表（Symbol Table）的清理；
- 类卸载（Class unloading）。

##### 着色指针Colored Pointer

ZGC利用指针的64位中的几位表示Finalizable、Remapped、Marked1、Marked0（ZGC仅支持64位平台），以标记该指向内存的存储状态。相当于在对象的指针上标注了对象的信息。注意，这里的指针相当于Java术语当中的引用。

在这个被指向的内存发生变化的时候（内存在Compact被移动时），颜色就会发生变化。

##### 读屏障Load Barrier

由于着色指针的存在，在程序运行时访问对象的时候，可以轻易知道对象在内存的存储状态（通过指针访问对象），若请求读的内存在被着色了，那么则会触发读屏障。读屏障会更新指针再返回结果，此过程有一定的耗费，从而达到与用户线程并发的效果。

与标记对象的传统算法相比，ZGC在指针上做标记，在访问指针时加入Load Barrier（读屏障），比如当对象正被GC移动，指针上的颜色就会不对，这个屏障就会先把指针更新为有效地址再返回，也就是，永远只有单个对象读取时有概率被减速，而不存在为了保持应用与GC一致而粗暴整体的Stop The World。

## 常用垃圾收集器参数

| 参数                               | 描述                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| -XX:+UseSerialGC                   | Jvm运行在Client模式下的默认值，打开此开关后，使用Serial + Serial Old的收集器组合进行内存回收 |
| -XX:+UseParNewGC                   | 打开此开关后，使用ParNew + Serial Old的收集器进行垃圾回收    |
| -XX:+UseConcMarkSweepGC            | 使用ParNew + CMS + Serial Old的收集器组合进行内存回收，Serial Old作为CMS出现“Concurrent Mode Failure”失败后的后备收集器使用。 |
| -XX:+UseParallelGC                 | Jvm运行在Server模式下的默认值，打开此开关后，使用Parallel Scavenge +  Serial Old的收集器组合进行回收 |
| -XX:+UseParallelOldGC              | 使用Parallel Scavenge + Parallel Old的收集器组合进行回收     |
| -XX:SurvivorRatio                  | 新生代中Eden区域与Survivor区域的容量比值，默认为8，代表Eden:Subrvivor = 8:1 |
| -XX:PretenureSizeThreshold         | 直接晋升到老年代对象的大小，设置这个参数后，大于这个参数的对象将直接在老年代分配 |
| -XX:MaxTenuringThreshold           | 晋升到老年代的对象年龄，每次Minor GC之后，年龄就加1，当超过这个参数的值时进入老年代 |
| -XX:UseAdaptiveSizePolicy          | 动态调整java堆中各个区域的大小以及进入老年代的年龄           |
| -XX:+HandlePromotionFailure        | 是否允许新生代收集担保，进行一次minor gc后, 另一块Survivor空间不足时，将直接会在老年代中保留 |
| -XX:ParallelGCThreads              | 设置并行GC进行内存回收的线程数                               |
| -XX:GCTimeRatio                    | GC时间占总时间的比列，默认为99，即允许1%的GC时间，仅在使用Parallel Scavenge 收集器时有效 |
| -XX:MaxGCPauseMillis               | 设置GC的最大停顿时间，在Parallel Scavenge 收集器下有效       |
| -XX:CMSInitiatingOccupancyFraction | 设置CMS收集器在老年代空间被使用多少后出发垃圾收集，默认为68%，仅在CMS收集器时有效，-XX:CMSInitiatingOccupancyFraction=70 |
| -XX:+UseCMSCompactAtFullCollection | 由于CMS收集器会产生碎片，此参数设置在垃圾收集器后是否需要一次内存碎片整理过程，仅在CMS收集器时有效 |
| -XX:+CMSFullGCBeforeCompaction     | 设置CMS收集器在进行若干次垃圾收集后再进行一次内存碎片整理过程，通常与UseCMSCompactAtFullCollection参数一起使用 |
| -XX:+UseFastAccessorMethods        | 原始类型优化                                                 |
| -XX:+DisableExplicitGC             | 是否关闭手动System.gc                                        |
| -XX:+CMSParallelRemarkEnabled      | 降低标记停顿                                                 |
| -XX:LargePageSizeInBytes           | 内存页的大小不可设置过大，会影响Perm的大小，-XX:LargePageSizeInBytes=128m |

Client、Server模式默认GC

|        | 新生代GC方式                  | 老年代和持久**代**GC方式 |
| ------ | ----------------------------- | ------------------------ |
| Client | Serial 串行GC                 | Serial Old 串行GC        |
| Server | Parallel Scavenge  并行回收GC | Parallel Old 并行GC      |

Sun/oracle JDK GC组合方式

|                                          | 新生代GC方式                  | 老年代和持久**代**GC方式                                     |
| ---------------------------------------- | ----------------------------- | ------------------------------------------------------------ |
| -XX:+UseSerialGC                         | Serial 串行GC                 | Serial Old 串行GC                                            |
| -XX:+UseParallelGC                       | Parallel Scavenge  并行回收GC | Parallel Old 并行GC                                          |
| -XX:+UseConcMarkSweepGC                  | ParNew 并行GC                 | CMS 并发GC  当出现“Concurrent Mode Failure”时 采用Serial Old 串行GC |
| -XX:+UseParNewGC                         | ParNew 并行GC                 | Serial Old 串行GC                                            |
| -XX:+UseParallelOldGC                    | Parallel Scavenge  并行回收GC | Parallel Old 并行GC                                          |
| -XX:+UseConcMarkSweepGC -XX:+UseParNewGC | Serial 串行GC                 | CMS 并发GC  当出现“Concurrent Mode Failure”时 采用Serial Old 串行GC |

**内存泄漏（memory leak）**

指程序在申请内存后，无法释放已申请的内存空间

**内存溢出（out of memory）**

指程序在申请内存时，没有足够的内存空间供其使用

# 虚拟机工具

## jps

JVM机进程状况工具（JVM Process Status Tool）。用来查看基于HotSpot JVM里面所有进程的具体状态, 包括进程ID，进程启动的路径等等。与unix上的ps类似，用来显示本地有权限的java进程，可以查看本地运行着几个java程序，并显示他们的进程号。

命令格式：jps [ options ] [ hostid ]

常用参数说明：

- -m 输出传递给main方法的参数，如果是内嵌的JVM则输出为null。
- -l 输出应用程序主类的完整包名，或者是应用程序JAR文件的完整路径。
- -v 输出传给JVM的参数。

## jinfo

JVM配置信息工具（Configuration Info for Java）。可以输出并修改运行时的java 进程的opts。用处比较简单，用于输出JAVA系统参数及命令行参数。

命令格式：jinfo [ options ] [ pid ]

常用参数说明：

- -flag  输出，修改，JVM命令行参数

## jstack

JVM堆栈跟踪工具(Stack Trace for Java）。用于打印出给定的java进程ID或core file或远程调试服务的Java堆栈信息。

命令格式：jstack [ option ] pid

常用参数说明：

- -F 当’jstack [-l] pid’没有相应的时候强制打印栈信息
- -l  长列表. 打印关于锁的附加信息,例如属于java.util.concurrent的ownable synchronizers列表.
- -m 打印java和native c/c++框架的所有栈信息.
- -h | -help打印帮助信息

## jstat

JVM统计信息监视工具(JVM statistics Monitoriing Tool)。

对Java应用程序的资源和性能进行实时的命令行的监控，包括了对Heap size和垃圾回收状况的监控。

命令格式：jstat [ option  pid [interval [ s | ms ] [count] ] ] 

常用参数说明：

- -gcutil  输出已使用空间占总空间的百分比
- -gccapacity 输出堆中各个区域使用到的最大和最小空间

术语：

- S0C：年轻代中第一个survivor（幸存区）的容量 (字节)
-  S1C：年轻代中第二个survivor（幸存区）的容量 (字节)
- S0U：年轻代中第一个survivor（幸存区）目前已使用空间 (字节)
- S1U：年轻代中第二个survivor（幸存区）目前已使用空间 (字节)
- EC：年轻代中Eden（伊甸园）的容量 (字节)
- EU：年轻代中Eden（伊甸园）目前已使用空间 (字节)
- OC：Old代的容量 (字节)
- OU：Old代目前已使用空间 (字节)
- PC：Perm(持久代)的容量 (字节)
- PU：Perm(持久代)目前已使用空间 (字节)
- YGC：从应用程序启动到采样时年轻代中gc次数
- YGCT：从应用程序启动到采样时年轻代中gc所用时间(s)
- FGC：从应用程序启动到采样时old代(全gc)gc次数
- FGCT：从应用程序启动到采样时old代(全gc)gc所用时间(s)
- GCT：从应用程序启动到采样时gc用的总时间(s)

- NGCMN：年轻代(young)中初始化(最小)的大小 (字节)
- NGCMX：年轻代(young)的最大容量 (字节)
- NGC：年轻代(young)中当前的容量 (字节)
- OGCMN：old代中初始化(最小)的大小 (字节) 
- OGCMX：old代的最大容量 (字节)
- OGC：old代当前新生成的容量 (字节)
- PGCMN：perm代中初始化(最小)的大小 (字节) 
- PGCMX：perm代的最大容量 (字节)   
- PGC：perm代当前新生成的容量 (字节)
- S0：年轻代中第一个survivor（幸存区）已使用的占当前容量百分比
- S1：年轻代中第二个survivor（幸存区）已使用的占当前容量百分比
- E：年轻代中Eden（伊甸园）已使用的占当前容量百分比
- O：old代已使用的占当前容量百分比
- P：perm代已使用的占当前容量百分比
- S0CMX：年轻代中第一个survivor（幸存区）的最大容量 (字节)
- S1CMX ：年轻代中第二个survivor（幸存区）的最大容量 (字节)
- ECMX：年轻代中Eden（伊甸园）的最大容量 (字节)
- DSS：当前需要survivor（幸存区）的容量 (字节)（Eden区已满）
- TT： 持有次数限制
- MTT ： 最大持有次数限制

## jmap

JVM内存映像工具( Memory Map for Java)。打印出某个java进程（使用pid）内存内的所有‘对象’的情况（如：产生那些对象，及其数量）。

命令格式：jmap [ option ] pid

常用参数说明：

- -dump:[live,]format=b,file=<filename> 使用二进制形式输出jvm的heap内容到文件中， live子选项是可选的，假如指定live选项,那么只输出活的对象到文件。
- -histo[:live] 打印每个class的实例数目，内存占用，类全名信息。VM的内部类名字开头会加上前缀”*”。 如果live子参数加上后，只统计活的对象数量。
- -F 强迫。在pid没有相应的时候使用-dump或者-histo参数。 在这个模式下，live子参数无效。 

生成的文件可以使用jhat工具进行分析

通过使用如下参数启动JVM，也可以获取到dump文件：

```properties
  #在jvm发生内存溢出时生成内存映像文件
  -XX:+HeapDumpOnOutOfMemoryError
  -XX:HeapDumpPath=./java_pid<pid>.hprof
```

## jhat

JVM堆转储快照分析工具  (JVM Heap Analysis Tool)。用于对JAVA heap进行离线分析的工具，他可以对不同虚拟机中导出的heap信息文件进行分析，如LINUX上导出的文件可以拿到WINDOWS上进行分析，可以查找诸如内存方面的问题。

命令格式：jhat dumpfile(jmap生成的文件) 

 执行成功后，访问http://localhost:7000即可查看内存信息

