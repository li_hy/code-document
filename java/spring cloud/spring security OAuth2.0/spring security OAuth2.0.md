# 基本 概念

## 认证

认证是为了保护系统的隐私数据与资源，用户的身份合法方可访问该系统的资源。

用户认证就是判断一个用户的身份是否合法的过程。用户去访问系统资源时要求验证用户的身份信息。身份合法方可继续访问，不合法则拒绝访问。

常见的用户身份认证方式有：用户名密码登录；二维码登录；手机短信登录；指纹认证等。

## 会话

用户认证通过后，为了避免用户的每次操作都进行认证可将用户的信息保证在会话中。会话就是系统为了保持当前用户的登录状态所提供的机制。常见的有基于session方式；基于token方式等。

## 授权

认证是为了保证用户身份的合法性，授权则是为了更细粒度的对隐私数据进行划分，授权是在认证通过后发生的，控制不同的用户能够访问不同的资源。

授权是用户认证通过根据用户的权限来控制用户访问资源的过程，拥有资源的访问权限则正常访问，没有权限则拒绝访问。

### 授权的数据模型

授权可简单理解为Who对What(which)进行How操作

- Who，即主体（Subject），主体一般是指用户，也可以是程序，需要访问系统中的资源
- What，即资源（Resource），如系统菜单、页面、按钮、代码方法、系统商品信息、系统订单信息等
- How，权限/许可（Permission），规定了用户对资源的操作许可，权限离开资源没有意义，如用户查询权限、用户添加权限、某个代码方法的调用权限、编号为001的用户的修改权限等，通过权限可知用户对哪些资源都有哪些操作许可。

主体、资源、权限相关的数据模型如下：

- 主体（用户id、账号、密码、...）
- 资源（资源id、资源名称、访问地址、...）
- 权限（权限id、权限标识、权限名称、资源id、...）
- 角色（角色id、角色名称、...）
- 角色和权限关系（角色id、权限id、...）
- 主体（用户）和角色关系（用户id、角色id、...）

主体（用户）、资源、权限关系如下图：

![主体（用户）、资源、权限关系](主体（用户）、资源、权限关系.png)

通常企业开发中将资源和权限表合并为一张权限表

- 资源（资源id、资源名称、访问地址、...）

- 权限（权限id、权限标识、权限名称、资源id、...）

合并为：

- 权限（权限id、权限标识、权限名称、资源名称、资源访问地址、...）

修改后数据模型之间的关系如下图：

![主体（用户）、资源、权限关系-改](主体（用户）、资源、权限关系-改.png)

### RBAC

业界通常基于RBAC实现授权。

#### 基于角色的访问控制

RBAC基于角色的访问控制（Role-Based Access Control）是按角色进行授权，比如：主体的角色为总经理可以查询企业运营报表，查询员工工资信息等，访问控制流程如下：

![基于角色的访问控制](基于角色的访问控制.png)

根据上图中的判断逻辑，授权代码可表示如下：

```java
if(主体.hasRole("总经理角色id")){
	//TODO 查询工资
}
```

如果上图中查询工资所需要的角色变化为总经理和部门经理，此时就需要修改判断逻辑为“判断用户的角色是否是总经理或部门经理”，修改代码如下：

```java
if(主体.hasRole("总经理角色id") || 主体.hasRole("部门经理角色id")){
	//TODO 查询工资
}
```

**根据上边的例子发现，当需要修改角色的权限时就需要修改授权的相关代码，系统可扩展性差。**

#### 基于资源的访问控制

RBAC基于资源的访问控制（Resource-Based Access Control）是按资源（或权限）进行授权，比如：用户必须具有查询工资权限才可以查询员工工资信息等，访问控制流程如下：

![基于资源的访问控制](基于资源的访问控制.png)

根据上图中的判断，授权代码可以表示为：

```java
if(主体.hasPermission("查询工资权限标识")){
	//TODO 查询工资
}
```

优点：系统设计时定义好查询工资的权限标识，即使查询工资所需要的角色变化为总经理和部门经理也不需要修改授权代码，系统可扩展性强。

# Spring Security

Spring Security是一个能够为基于Spring的企业应用系统提供声明式的安全访问控制解决方案的安全框架。

Spring Security 采用“安全层”的概念， 使每一层都尽可能安全， 连续的安全层可以达到全面的防护。Spring Security 可以在Controller 层、Service 层、DAO 层等以加注解的方式来保护应用程序的安全。

Spring Security 提供了细粒度的权限控制， 可以精细到每一个API 接口、每一个业务的方法， 或者每一个操作数据库的DAO 层的方法。

Spring Security提供的是**应用程序层**的安全解决方案， 一个系统的安全还需要考虑**传输层**和**系统层**的安全，例如采用Htpps 协议、服务器部署防火墙等。

在Spring Security 框架中，主要包含了两个依赖Jar ， 分别是spring-security-web 依赖和
spring-security-config 依赖

```xml
	<dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-config</artifactId>
      <version>5.1.6.RELEASE</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-web</artifactId>
      <version>5.1.6.RELEASE</version>
      <scope>compile</scope>
    </dependency>
```

Spring Boot 对Spring Security 框架做了封装，仅仅是封装，并没有改动Spring Security 这两个包的内容，并加上了Spring Boot 的起步依赖的特性。

```xml
<dependency>    
    <groupId>org.springframework.boot</groupId>    
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

## Spring Boot Security 案例

### 增加依赖

```xml
	<dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
```

### 配置WebSecurityConfigureAdapter

```java
@Configuration
//@EnableWebSecurity //springBoot可自动装配，可以不写
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    /**
     * 配置加密方式，使用Bcrypt
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    /**
     * 配置用户信息服务,用户信息保存在内存中
     * @return
     */
    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        //增加admin用户，密码123，权限p1
 	    manager
            .createUser(User.withUsername("admin")
            .password(passwordEncoder.encode("123"))
            .authorities("p1").build());
        //增加user用户，密码123，权限p2
        manager
            .createUser(User.withUsername("user")
            .password(passwordEncoder.encode("123"))
            .authorities("p2").build());
        return manager;
    }
    
    /**
     * 设置了安全拦截规则
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // p/p1需要p1权限
                .antMatchers("/p/p1").hasAuthority("p1")
                // r/r1需要p2权限
                .antMatchers("/r/r2").hasAuthority("p2")
                //其余资源不需要验证，注意有先后顺序，必须放在最后
                .anyRequest().permitAll()
                .and()
                //使用表单登录
                .formLogin()
                //登录成功后跳转页面
                .successForwardUrl("/login‐success")
            	.and()
                //使用注销功能
                .logout()
                //注销链接
                .logoutUrl("/logout")
                //注销成功跳转页面
                .logoutSuccessUrl("/login‐view?logout");
        
        //设置会话
        //默认情况下，Spring Security会为每个登录成功的用户会新建一个Session，就是ifRequired 。
        //always：如果没有session存在就创建一个
        //never：SpringSecurity 将不会创建Session，但是如果应用中其他地方创建了Session，那么Spring Security将会使用它
        //stateless：SpringSecurity将绝对不会创建Session，也不使用Session。这种无状态架构适用于REST API及其无状态认证机制
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
    }
}
```

### 配置访问资源

```java
@RestController
public class SecurityController {

    @RequestMapping("/r/r1")
    public String r1(){
        return getUsername()+ " 访问 r1";
    }

    @RequestMapping("/r/r2")
    public String r2(){
        return getUsername()+ " 访问 r2";
    }

    @RequestMapping("/p/p1")
    public String p1(){
        return getUsername()+ " 访问 p1";
    }

    @PreAuthorize("hasAuthority('p2')")
    @RequestMapping("/p/p2")
    public String p2(){
        return getUsername()+ " 访问 p2";
    }

    @RequestMapping("/login‐success")
    public String loginSuccess(){
        return "login‐success";
    }

    /**
     * 获取当前登录用户名
     * @return
     */
    private String getUsername(){
        //当前登录用户信息
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!authentication.isAuthenticated()){
            return null;
        }
        Object principal = authentication.getPrincipal();
        String username;
        if(principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        }else {
            username = principal.toString();
        }
        return username;
    }
}
```



### 从数据库中读取用户认证信息

#### 在数据库中建立对应表

在数据库中建立用户表，权限表，用户权限表。字段名可自定义，也可多加字段。

```mysql
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `authorities_id` bigint(20) NOT NULL,
  KEY `FK2lybg7hi222csyc912id1arac` (`authorities_id`),
  KEY `FK859n2jvi8ivhui0rl0esws6o` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin

```



用户表需要实现`UserDetails`接口

```java
/**
* 实现security的UserDetails接口，重新getPassword()、getUsername()、getAuthorities()方法
 * getAuthorities()也可以不实现，在UserDetailsService实现类中插入权限
*/
@Entity
public class User implements UserDetails, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column
    private String password;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<Role> authorities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(List<Role> authorities) {
        this.authorities = authorities;
    }



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
```

权限表需要实现`GrantedAuthority`接口

```java
/**
* 实现security的GrantedAuthority接口，重新getAuthority()方法，使security可以获取权限
*/
@Entity
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
```

#### 数据库查询DAO

与普通数据库查询类似，略。

#### 实现`UserDetailsService`接口

实现`UserDetailsService`接口，并注入spring；同时删除`WebSecurityConfig`中`UserDetailsService`的定义

```java
@Service
public class DatabaseUserService implements UserDetailsService {

    @Autowired
    private UserDao userDao;


    /**
     * 查询用户与权限并注入userDetails中
     * 这里使用jpa在查询user时就将权限注入到UserDetails中，否则需要查询用户、权限后手动注入
     * UserDetails userDetails = User.withUsername(user.getName())
     * 					.password(user.getPassword())
     *					.authorities(authorities).build();
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDao.findByUsername(username);
    }

}
```













### security自定义退出功能

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
        //...
        .and()
        .logout() 								   //(1)
        .logoutUrl("/logout") 					    //(2)
        .logoutSuccessUrl("/login‐view?logout") 	 //(3)
        .logoutSuccessHandler(logoutSuccessHandler)  //(4)
        .addLogoutHandler(logoutHandler) 			//(5)
        .invalidateHttpSession(true); 				//(6)
}
```

- （1）提供系统退出支持，使用`WebSecurityConfigurerAdapter `会自动被应用
- （2）设置触发退出操作的URL (默认是`/logout` )。
  - 如果让logout在GET请求下生效，必须关闭防止CSRF攻击`csrf().disable()`。如果开启了CSRF，必须使用post方式请求`/logout`
- （3）退出之后跳转的URL。默认是`/login?logout` 。
- （4）定制的 `LogoutSuccessHandler` ，用于实现用户退出成功时的处理。如果指定了这个选项那么
  `logoutSuccessUrl()` 的设置会被忽略。
- （5）添加一个`LogoutHandler `，用于实现用户退出时的清理工作.默认`SecurityContextLogoutHandler `会被添加
  为最后一个`LogoutHandler `。
  - 一般来说， `LogoutHandler `的实现类被用来执行必要的清理，因而不应该抛出异常。
  - Spring Security提供的一些实现：
    - `PersistentTokenBasedRememberMeServices `基于持久化token的`RememberMe`功能的相关清理
    - `TokenBasedRememberMeService `基于token的`RememberMe`功能的相关清理
    - `CookieClearingLogoutHandler `退出时Cookie的相关清理
    - `CsrfLogoutHandler `负责在退出时移除`csrfToken`
    - `SecurityContextLogoutHandler `退出时`SecurityContext`的相关清理。
  - 链式API提供了调用相应的`LogoutHandler `实现的快捷方式，比如`deleteCookies()`。
- （6）指定是否在退出时让`HttpSession `无效。 默认设置为 true。









### 授权

授权的方式包括 web授权和方法授权，web授权是通过 url拦截进行授权，方法授权是通过 方法拦截进行授权。他们都会调用`accessDecisionManager`进行授权决策，若为web授权则拦截器为`FilterSecurityInterceptor`；若为方法授权则拦截器为`MethodSecurityInterceptor`。如果同时通过web授权和方法授权则先执行web授权，再执行方法授权，最后决策通过，则允许访问资源，否则将禁止访问。

#### web授权

通过给`http.authorizeRequests()` 添加多个子节点来定制需求到URL

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests() 												   //(1)
        .antMatchers("/r/r1").hasAuthority("p1") 								//(2)
        .antMatchers("/r/r2").hasAuthority("p2") 								//(3)
        .antMatchers("/r/r3").access("hasAuthority('p1') and hasAuthority('p2')")  //(4)
        .antMatchers("/r/**").authenticated() 									//(5)
        .anyRequest().permitAll() 											   //(6)
        .and()
        .formLogin()
        // ...
}
```

- （1） `http.authorizeRequests() `方法有多个子节点，每个`macher`按照他们的声明顺序执行。
- （2）指定"/r/r1"URL，拥有p1权限能够访问
- （3）指定"/r/r2"URL，拥有p2权限能够访问
- （4）指定了"/r/r3"URL，同时拥有p1和p2权限才能够访问
- （5）指定了除了r1、r2、r3之外"/r/**"资源，同时通过身份认证就能够访问，这里使用SpEL（Spring Expression Language）表达式。
- （6）剩余的尚未匹配的资源，不做保护。

规则的顺序是重要的,更具体的规则应该先写。

```java
//以/ admin开始的所有内容都需要具有ADMIN角色的身份验证用户
//即使是/admin/login路径(因为/admin/login已经被/admin/**规则匹配,因此第二个规则被忽略)
.antMatchers("/admin/**").hasRole("ADMIN")
.antMatchers("/admin/login").permitAll()
 
 /***********************************************************************/
    
//登录页面的规则应该在/admin/**规则之前    
.antMatchers("/admin/login").permitAll()
.antMatchers("/admin/**").hasRole("ADMIN")
```

保护URL常用的方法有：

- `authenticated() `保护URL，需要用户登录
- `permitAll() `指定URL无需保护，一般应用与静态资源文件
- `hasRole(String role)` 限制单个角色访问，角色将被增加 “ROLE_” 。所以”ADMIN” 将和“ROLE_ADMIN”进行比较
- `hasAuthority(String authority)` 限制单个权限访问
- `hasAnyRole(String… roles)`允许多个角色访问
- `hasAnyAuthority(String… authorities)` 允许多个权限访问
- `access(String attribute)` 该方法使用 SpEL表达式, 所以可以创建复杂的限制
- `hasIpAddress(String ipaddressExpression)` 限制IP地址或子网

#### 方法授权

，从Spring Security2.0版本开始，security支持服务层方法的安全性的支持。

在任何`@Component`实例上使用`@EnableGlobalMethodSecurity` 注释来启用基于注解的安全性。然后向方法（在类或接口上）添加注解就会限制对该方法的访问。

```java
//使用@Secured注解
@EnableGlobalMethodSecurity(securedEnabled = true)
public interface BankService {
    
    @Secured("IS_AUTHENTICATED_ANONYMOUSLY")
    public Account readAccount(Long id);
    
    @Secured("IS_AUTHENTICATED_ANONYMOUSLY")
    public Account[] findAccounts();
    
    @Secured("ROLE_TELLER")
    public Account post(Account account, double amount);
}

//使用@PreAuthorize注解
@EnableGlobalMethodSecurity(prePostEnabled = true)
public interface BankService {
    
    @PreAuthorize("isAnonymous()")
    public Account readAccount(Long id);
    
    @PreAuthorize("isAnonymous()")
    public Account[] findAccounts();
    
    @PreAuthorize("hasAuthority('p_transfer') and hasAuthority('p_read_account')")
    public Account post(Account account, double amount);
}
```

### Thymeleaf支持

通过 `thymeleaf-extras-springsecurity` 来添加`Thymeleaf`对Spring Security的支持。

```xml
<dependency>
    <groupId>org.thymeleaf.extras</groupId>
    <artifactId>thymeleaf-extras-springsecurity5</artifactId>
</dependency>
```

在html中添加 `xmlns:sec` 的支持

```html
<!DOCTYPE html>
<!-- 增加sec标签 -->
<html xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
</head>
<body>
    <p>This is a home page.</p>
    <p>Id: <th:block sec:authentication="principal.id"></th:block></p>
    <p>Username: <th:block sec:authentication="principal.username"></th:block></p>
    <p>Role: <th:block sec:authentication="principal.authorities"></th:block></p>
    
    <div sec:authorize="isAuthenticated()">
        <h1>当前登录用户：<span sec:authentication="name"></span>&nbsp;&nbsp;当前角色：
            <span sec:authentication="principal.authorities"></span><br></h1>
        <div sec:authorize="hasRole('ROLE_admin')">
            <h1>this is admin role</h1>
        </div>
        <hr/>
        <div sec:authorize="hasRole('ROLE_user')">
            <h1>this is user role</h1>
        </div>
    </div>
    <div>
        <a href="/logout">注销</a>
    </div>
</body>
</html>
```

## 核心原理

### Spring Security 对 Java Web 实现

#### Spring Security 对 Servlet 实现

通过`AbstractSecurityWebApplicationInitializer`实现Spring Web的`WebApplicationInitializer`接口来实现Web 自动装配。

> Servlet 3.0+ `javax.servlet.ServletContainerInitializer` 所有它的实现，配置在 `METAINF/services/javax.servlet.ServletContainerInitializer` 后，均会被 Servlet 3.0+ 容器回调方法

在`AbstractSecurityWebApplicationInitializer`中定义了security的过滤器链名称为`springSecurityFilterChain`，通过`org.springframework.web.filter.DelegatingFilterProxy`来动态读取，并且 `DelegatingFilterProxy `是一个 Filter，它通过配置形成获取 Spring 应用上下文中的 Filter Bean。

Spring Security 利用 Servlet 3.0+ Web 自动装配能力来引导 `Root ApplicationContext` 装配，并且将一个名为`springSecurityFilterChain` 动态地（编码）加入（注册）到`ServletContext `中。

#### Spring Security 对 WebFlux 实现

Servlet 核心扩展适配器 - `WebSecurityConfigurerAdapter`

核心配置接口 - `SecurityConfigurer`

HTTP 请求匹配器 - `RequestMatcher`

configure 重载方法

- configure(WebSecurity)：Web 安全接口
- configure(HttpSecurity)：HTTP 安全接口

`HttpSecurity `每种功能特性方法被调用时，它所对应的`SecurityConfigurer `实现类被激活

如果需要自定义 Filter 添加到 `Spring Security Filter Chain` 时，需要记住框架内部 Filter 实现类以及它对应顺序，即通过 `HttpSecurity#addFilterBefore`

# OAuth 2.0

OAuth（开放授权）是一个开放标准，允许用户授权第三方应用访问他们存储在另外的服务提供者上的信息，而不需要将用户名和密码提供给第三方应用或分享他们数据的所有内容。OAuth 2.0是OAuth协议的延续版本，但不向后兼容OAuth 1.0即完全废止了OAuth 1.0。

## OAuth 2.0流程

![OAuth 2.0流程](OAuth 2.0流程.png)

（A）用户打开客户端以后，客户端要求用户给予授权。

（B）用户同意给予客户端授权。

（C）客户端使用上一步获得的授权，向认证服务器申请令牌。

（D）认证服务器对客户端进行认证以后，确认无误，同意发放令牌。

（E）客户端使用令牌，向资源服务器申请获取资源。

（F）资源服务器确认令牌无误，同意向客户端开放资源。

## OAuth 2.0角色

- 客户端

  本身不存储资源，需要通过资源拥有者的授权去请求资源服务器的资源，比如：Android客户端、Web客户端（浏览器端）、微信客户端等。

- 资源拥有者

  通常为用户，也可以是应用程序，即该资源的拥有者。

- 授权服务器（也称认证服务器）

  用于服务提供商对资源拥有的身份进行认证、对访问资源进行授权，认证成功后会给客户端发放令牌（access_token）， 作为客户端访问资源服务器的凭据。

- 资源服务器

  存储资源的服务器。

## Spring Cloud Security OAuth2

Spring-Security-OAuth2是对OAuth2的一种实现，与Spring Security相辅相成。

OAuth2.0的服务提供方涵盖两个服务，即授权服务 (Authorization Server，也叫认证服务) 和资源服务 (Resource Server)，使用 Spring Security OAuth2 的时候你可以选择把它们在同一个应用程序中实现，也可以选择建立使用同一个授权服务的多个资源服务。

### 授权服务 (Authorization Server）

包含对接入端以及登入用户的合法性进行验证并颁发token等功能，对令牌的请求端点由 Spring MVC 控制器进行实现。

配置一个认证服务必须要实现的endpoints：

- `AuthorizationEndpoint `服务于认证请求。默认 URL：` /oauth/authorize` 。
- `TokenEndpoint `服务于访问令牌的请求。默认 URL： `/oauth/token` 。

### 资源服务 (Resource Server)

包含对资源的保护功能，对非法请求进行拦截，对请求中token进行解析鉴权等。

`OAuth2AuthenticationProcessingFilter`用来对请求给出的身份令牌解析鉴权

### 授权服务器配置

可以用 `@EnableAuthorizationServer` 注解并继承`AuthorizationServerConfigurerAdapter`来配置OAuth2.0 授权服务器。

```java
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {
	//TODO
}
```

`AuthorizationServerConfigurerAdapter`要求配置以下几个类，这几个类是由Spring创建的独立的配置对象，它们会被Spring传入`AuthorizationServerConfigurer`中进行配置。

```java
public class AuthorizationServerConfigurerAdapter implements AuthorizationServerConfigurer {
    public AuthorizationServerConfigurerAdapter() {}
    
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {}
    
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {}
    
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {}
}
```

- `ClientDetailsServiceConfigurer`：用来配置客户端详情服务（`ClientDetailsService`），客户端详情信息在这里进行初始化，可以把客户端详情信息写死在这里或者是通过数据库来存储调取详情信息。
- `AuthorizationServerEndpointsConfigurer`：用来配置令牌（`token`）的访问端点和令牌服务(`token services`)。
- `AuthorizationServerSecurityConfigurer`：用来配置令牌端点的安全约束。

#### 配置客户端详细信息

`ClientDetailsServiceConfigurer `能够使用内存（`clients.inMemory()`）或者JDBC来实现客户端详情服务（`clients.withClientDetails(clientDetailsService)`）。

`ClientDetailsService`负责查找`ClientDetails`，而`ClientDetails`有几个重要的属性：

- `clientId`：（必须的）用来标识客户的Id
- `secret`：（需要值得信任的客户端）客户端安全码，如果有的话
- `scope`：用来限制客户端的访问范围，如果为空（默认）的话，那么客户端拥有全部的访问范围
- `authorizedGrantTypes`：此客户端可以使用的授权类型，默认为空，有四种模式
  - 授权码模式：`authorization_code`
  - 简化模式：`implicit`
  - 密码模式：`password`
  - 客户端模式：`client_credentials`
  - 刷新token（非授权模式，用来控制token过期后自动刷新）：`refresh_token`
- `authorities`：此客户端可以使用的权限（基于Spring Security authorities）

客户端详情（Client Details）能够在应用程序运行的时候进行更新，可以通过访问底层的存储服务（例如将客户端详情存储在一个关系数据库的表中，就可以使用 `JdbcClientDetailsService`）或者通过自己实现`ClientRegistrationService`接口（同时也可以实现 `ClientDetailsService `接口）来进行管理。

```java
/**
* 内存存储方式
*/
@Override
public void configure(ClientDetailsServiceConfigurer clients)
throws Exception {
    // clients.withClientDetails(clientDetailsService);
    clients.inMemory()// 使用in‐memory存储
    .withClient("c1")// client_id
    .secret(new BCryptPasswordEncoder().encode("secret"))//secret
    .resourceIds("res1")//scope
    .authorizedGrantTypes("authorization_code",
    "password","client_credentials","implicit","refresh_token")// 该client允许的授权类型authorization_code,password,refresh_token,implicit,client_credentials
    .scopes("all")// 允许的授权范围
    .autoApprove(false)//登录后是否自动授权（绕过批准询问）
    .redirectUris("http://www.baidu.com");//验证回调地址
}
```



#### 管理令牌

`AuthorizationServerTokenServices `接口定义了一些操作可以对令牌进行一些必要的管理，令牌可以被用来加载身份信息，里面包含了这个令牌的相关权限。创建一个令牌的时候，默认是使用随机值来进行填充的，除了持久化令牌是委托一个 `TokenStore `接口来实现以外，这个类几乎帮你做了所有的事情。

`TokenStore `这个接口有一个默认的实现，它就是 `InMemoryTokenStore `，所有的令牌是被保存在了内存中。还可以使用一些其他的预定义实现，下面有几个版本，它们都实现了`TokenStore`接口：

- `InMemoryTokenStore`：默认采用，它可以完美的工作在单服务器上（即访问并发量压力不大的情况下，并且它在失败的时候不会进行备份），大多数的项目都可以使用这个版本的实现来进行尝试，你可以在开发的时候使用它来进行管理，因为不会被保存到磁盘中，所以更易于调试。

- `JdbcTokenStore`：基于JDBC的实现版本，令牌会被保存进关系型数据库。使用这个版本的实现时，可以在不同的服务器之间共享令牌信息，使用这个版本的时候需要`spring-jdbc`依赖。

- `JwtTokenStore`：这个版本的全称是 JSON Web Token（JWT），它可以把令牌相关的数据进行编码（因此对于后端服务来说，它不需要进行存储，这将是一个重大优势）。

  但是它有一个缺点，那就是撤销一个已经授权令牌将会非常困难，所以它通常用来处理一个生命周期较短的令牌以及撤销刷新令牌（refresh_token）。

  另外一个缺点就是这个令牌占用的空间会比较大，如果你加入了比较多用户凭证信息。`JwtTokenStore `不会保存任何数据，但是它在转换令牌值以及授权信息方面与 `DefaultTokenServices `所扮演的角色是一样的。

- `RedisTokenStore`：基于redis的实现版本，令牌会被保存进redis数据库，可以在不同的服务器之间共享令牌信息。使用时需要`spring-boot-starter-data-redis`依赖。

定义`TokenConfig`

```java
/**
* 定义令牌存储类型
**/
@Configuration
public class TokenConfig {
    
    /**
    * 内存存储
    */
    @Bean
    public TokenStore tokenStore() {
    	return new InMemoryTokenStore();
    }
    
    /***************************************************************/
    /**
    * 数据库存储
    */
    @Bean
    public TokenStore tokenStore(DataSource dataSource){
        return new JdbcTokenStore(dataSource);
    }
    
	/***************************************************************/
    /**
    * JWT存储
    */
    private final String SIGNING_KEY = "JWT123";

    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }

    /***************************************************************/
    /**
    * redis存储
    */
    @Autowired
    private RedisConnectionFactory connectionFactory;

    @Bean
    public TokenStore tokenStore(){
        return new RedisTokenStore(connectionFactory);
    }
}
```

定义`AuthorizationServerTokenServices`

在`AuthorizationServer`中定义`AuthorizationServerTokenServices`

```java
@Autowired
private TokenStore tokenStore;

@Autowired
private ClientDetailsService clientDetailsService;

@Bean
public AuthorizationServerTokenServices tokenService() {
    DefaultTokenServices service=new DefaultTokenServices();
    service.setClientDetailsService(clientDetailsService);
    service.setSupportRefreshToken(true);
    service.setTokenStore(tokenStore);
    service.setAccessTokenValiditySeconds(7200); // 令牌默认有效期2小时
    service.setRefreshTokenValiditySeconds(259200); // 刷新令牌默认有效期3天
    return service;
}
```

#### 令牌访问端点配置

`AuthorizationServerEndpointsConfigurer `这个对象的实例可以完成令牌服务以及令牌endpoint配置。

##### 配置授权类型（Grant Types）

`AuthorizationServerEndpointsConfigurer `通过设定以下属性决定支持的授权类型（Grant Types）:

- `authenticationManager`：认证管理器，当你选择了资源所有者密码（password）授权类型的时候，需要设置这个属性注入一个 `AuthenticationManager `对象。
- `userDetailsService`：如果设置了这个属性的话，那说明有一个自定义的 `UserDetailsService `接口的实现；或者可以将其设置到全局域上面去（例如 `GlobalAuthenticationManagerConfigurer `这个配置对象），当设置了这个之后，那么 "refresh_token" 即刷新令牌授权类型模式的流程中就会包含一个检查，用来确保这个账号是否仍然有效，例如这个账户是否被禁用。
- `authorizationCodeServices`：用来设置授权码服务（即 `AuthorizationCodeServices `的实例对象），主要用于 `authorization_code`授权码类型模式。
- `implicitGrantService`：用于设置隐式授权模式，用来管理隐式授权模式的状态。
- `tokenGranter`：当设置了此属性（即 `TokenGranter `接口实现），那么授权将会交由你来完全掌控，并且会忽略掉上面的这几个属性，这个属性一般是用作拓展用途的，即标准的四种授权模式已经满足不了需求的时候，才会考虑使用这个。

##### 配置授权端点的URL（Endpoint URLs）

`AuthorizationServerEndpointsConfigurer `这个配置对象有一个叫做 `pathMapping()` 的方法用来配置端点URL链接，它有两个参数：

- 第一个参数：String 类型，端点URL的默认链接。
- 第二个参数：String 类型，替代的URL链接。

以上的参数都将以 "/" 字符为开始的字符串，框架的默认URL链接如下列表，可以作为这个 `pathMapping() `方法的第一个参数：

- `/oauth/authorize`：授权端点
- `/oauth/token`：令牌端点
- `/oauth/confirm_access`：用户确认授权提交端点
- `/oauth/error`：授权服务错误信息端点
- `/oauth/check_token`：用于资源服务访问的令牌解析端点
- `/oauth/token_key`：提供公有密匙的端点，如果使用JWT令牌的话

需要注意的是授权端点这个URL应该被Spring Security保护起来只供授权用户访问。

在`AuthorizationServer`配置令牌访问端点

```java
@Autowired
private AuthorizationCodeServices authorizationCodeServices;

@Autowired
private AuthenticationManager authenticationManager;

@Override
public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
    endpoints
        .authenticationManager(authenticationManager)
        .authorizationCodeServices(authorizationCodeServices)
        .tokenServices(tokenService())
        .allowedTokenEndpointRequestMethods(HttpMethod.POST);
}

@Bean
public AuthorizationCodeServices authorizationCodeServices() { 
    //设置授权码模式的授权码如何存取，暂时采用内存方式
	return new InMemoryAuthorizationCodeServices();
}
```

#### 令牌端点的安全约束

`AuthorizationServerSecurityConfigurer`：用来配置令牌端点(Token Endpoint)的安全约束，在
`AuthorizationServer`中配置如下.

```java
@Override
public void configure(AuthorizationServerSecurityConfigurer security){
    security
        .tokenKeyAccess("permitAll()") 			//(1)
        .checkTokenAccess("permitAll()") 		//(2)
        .allowFormAuthenticationForClients() 	//(3)
        ;
}
```

- （1）`tokenkey`这个endpoint当使用JwtToken且使用非对称加密时，资源服务用于获取公钥而开放的，这里指这个endpoint完全公开
- （2）`checkToken`这个endpoint完全公开
- （3） 允许表单认证

#### 授权服务配置总结

授权服务配置分成三大块，可以关联记忆。

- 既然要完成认证，首先得知道客户端信息从哪儿读取，因此要进行客户端详情配置。
- 既然要颁发token，那必须得定义token的相关endpoint，以及token如何存取，以及客户端支持哪些类型的token。
- 既然暴露除了一些endpoint，那对这些endpoint可以定义一些安全上的约束等。

### web安全配置

与spring security类似，用来验证访问者是否有权限。

```java
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

	/**
	* 这里只要配置登录权限，客户端访问授权页面时没有权限会被重定向到登录页
	*/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/login*").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin();
    }
}
```

### 授权模式

#### 授权码模式

授权码模式（`authorization code`）是功能最完整、流程最严密的授权模式。它的特点就是通过客户端的后台服务器，与”服务提供商”的认证服务器进行互动。

这种模式是四种模式中最安全的一种模式。一般用于client是Web服务器端应用或第三方的原生App调用资源服务的时候。因为在这种模式中access_token不会经过浏览器或移动端的App，而是直接从服务端去交换，这样就最大限度的减小了令牌泄漏的风险。

![授权码模式](授权码模式.png)

##### 流程

- 资源拥有者打开客户端，客户端要求资源拥有者给予授权，它将浏览器被重定向到授权服务器，重定向时会附加客户端的身份信息。

  ```
   /uaa/oauth/authorize?client_id=c1&response_type=code&scope=all&redirect_uri=http://www.baidu.com
  ```

  - 参数列表如下：
    - `client_id`：客户端准入标识
    - `response_type`：授权码模式固定为code
    - `scope`：客户端权限
    - `redirect_uri`：跳转uri，当授权码申请成功后会跳转到此地址，并在后边带上code参数（授权码）。

- 浏览器出现向授权服务器授权页面，之后将用户同意授权。

- 授权服务器将授权码（`AuthorizationCode`）转经浏览器发送给client(通过`redirect_uri`)。

- 客户端拿着授权码向授权服务器索要访问`access_token`，这一步是在客户端的后台的服务器上完成的，对用户不可见。

  ```
  /uaa/oauth/token?
  client_id=c1&client_secret=secret&grant_type=authorization_code&code=5PgfcD&redirect_uri=http://www.baidu.com
  ```

  - 参数列表如下：
    - `client_id`：客户端准入标识
    - `client_secret`：客户端秘钥
    - `grant_type`：授权类型，填写`authorization_code`，表示授权码模式
    - `code`：授权码，就是刚刚获取的授权码，注意：授权码只使用一次就无效了，需要重新申请
    - `redirect_uri`：申请授权码时的跳转url，一定和申请授权码时用的`redirect_uri`一致

- 授权服务器返回令牌(access_token)

#### 简化模式

简化模式（`implicit grant type`）不通过第三方应用程序的服务器，直接在浏览器中向认证服务器申请令牌，跳过了”授权码”这个步骤，因此得名。所有步骤在浏览器中完成，令牌对访问者是可见的，且客户端不需要认证。

一般来说，简化模式用于没有服务器端的第三方单页面应用，因为没有服务器端就无法接收授权码。

![简化模式](简化模式.png)

##### 流程

- 资源拥有者打开客户端，客户端要求资源拥有者给予授权，它将浏览器被重定向到授权服务器，重定向时会附加客户端的身份信息

  ```
  /uaa/oauth/authorize?client_id=c1&response_type=token&scope=all&redirect_uri=http://www.baidu.com
  ```

  - 参数描述同授权码模式 ，注意`response_type=token`，说明是简化模式。

- 浏览器出现向授权服务器授权页面，之后将用户同意授权

- 授权服务器将授权码将令牌（`access_token`）以Hash的形式存放在重定向uri的`fargment`中发送给浏览器。

  - `fragment `主要是用来标识 URI 所标识资源里的某个资源，在 URI 的末尾通过（#）作为 fragment 的开头，其中 # 不属于 fragment 的值。如`https://domain/index#L18`这个 URI 中 `L18` 就是 fragment 的值。js通过响应浏览器地址栏变化的方式能获取到`fragment `。

#### 密码模式

密码模式（`Resource Owner Password Credentials Grant`）中，用户向客户端提供自己的用户名和密码。客户端使用这些信息，向”服务商提供商”索要授权。

在这种模式中，用户必须把自己的密码给客户端，但是客户端不得储存密码。这通常用在用户对客户端高度信任的情况下，比如客户端是操作系统的一部分，或者由一个著名公司出品，或者是自己开发的。而认证服务器只有在其他授权模式无法执行的情况下，才能考虑使用这种模式。

![密码模式](密码模式.png)

##### 流程

- 资源拥有者将用户名、密码发送给客户端

- 客户端拿着资源拥有者的用户名、密码向授权服务器请求令牌（access_token）

  ```
  /uaa/oauth/token?
  client_id=c1&client_secret=secret&grant_type=password&username=shangsan&password=123
  ```

  - 参数列表如下：
    - `client_id`：客户端准入标识
    - `client_secret`：客户端秘钥
    - `grant_type`：授权类型，填写`password`表示密码模式
    - `username`：资源拥有者用户名
    - `password`：资源拥有者密码

- 授权服务器将令牌（access_token）发送给client

#### 客户端模式

客户端模式（`Client Credentials Grant`）指客户端以自己的名义，而不是以用户的名义，向”服务提供商”进行认证。严格地说，客户端模式并不属于OAuth框架所要解决的问题。在这种模式中，用户直接向客户端注册，客户端以自己的名义要求”服务提供商”提供服务，其实不存在授权问题。

这种模式是最方便但最不安全的模式。因此这就要求对client完全的信任，而client本身也是安全的。因此这种模式一般用来提供给完全信任的服务器端服务。比如，合作方系统对接，拉取一组用户信息。

![客户端模式](客户端模式.png)

##### 流程

- 客户端向授权服务器发送自己的身份信息，并请求令牌（access_token）

- 确认客户端身份无误后，将令牌（access_token）发送给client

  ```
  /uaa/oauth/token?client_id=c1&client_secret=secret&grant_type=client_credentials
  ```

  - 参数列表如下：
    - `client_id`：客户端准入标识
    - `client_secret`：客户端秘钥
    - `grant_type`：授权类型，填写`client_credentials`表示客户端模式

### 资源服务器配置

`@EnableResourceServer` 注解到一个 `@Configuration` 配置类上，并且必须使用 `ResourceServerConfigurer `这个配置对象来进行配置（可以选择继承自 `ResourceServerConfigurerAdapter `然后覆写其中的方法，参数就是这个对象的实例）。

`@EnableResourceServer `注解自动增加了一个类型为 `OAuth2AuthenticationProcessingFilter `的过滤器链。

下面是一些可以配置的属性

#### `ResourceServerSecurityConfigurer`

- `tokenServices`：`ResourceServerTokenServices` 类的实例，用来实现令牌服务
- `tokenStore`：`TokenStore`类的实例，指定令牌如何访问，与`tokenServices`配置可选
- `resourceId`：这个资源服务的ID，这个属性是可选的，但是推荐设置并在授权服务中进行验证
- 其他的拓展属性例如 `tokenExtractor `令牌提取器用来提取请求中的令牌。

#### `HttpSecurity`

与Spring Security类似

- 请求匹配器，用来设置需要进行保护的资源路径，默认的情况下是保护资源服务的全部路径
- 通过`http.authorizeRequests()`来设置受保护资源的访问规则
- 其他的自定义权限保护规则通过 `HttpSecurity `来进行配置

```java
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResouceServerConfig extends ResourceServerConfigurerAdapter {
    
    public static final String RESOURCE_ID = "res1";
    
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID)
                .tokenServices(tokenService())
                .stateless(true);
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/**").access("#oauth2.hasScope('all')")
            .and().csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
```

#### 验证token

`ResourceServerTokenServices `是组成授权服务的另一半，如果授权服务和资源服务在同一个应用程序上的话，可以使用 `DefaultTokenServices `，这样的话，就不用考虑关于实现所有必要的接口的一致性问题。如果资源服务器是分离开的，那么就必须要确保能够有匹配授权服务提供的 `ResourceServerTokenServices`。

令牌解析方法： 

- 使用 `DefaultTokenServices `在资源服务器本地配置令牌存储、解码、解析方式 ；
- 使用`RemoteTokenServices `资源服务器通过 HTTP 请求来解码令牌，每次都请求授权服务器端点` /oauth/check_token`。使用授权服务的` /oauth/check_token` 端点需要在授权服务将这个端点暴露出去，以便资源服务可以进行访问。

在资源 服务配置`RemoteTokenServices `，在`ResouceServerConfig`中配置

```java
//资源服务令牌解析服务
@Bean
public ResourceServerTokenServices tokenService() {
    //使用远程服务请求授权服务器校验token,必须指定校验token 的url、client_id，client_secret
    RemoteTokenServices service=new RemoteTokenServices();
    service.setCheckTokenEndpointUrl("http://localhost:53020/oauth/check_token");
    service.setClientId("c1");
    service.setClientSecret("secret");
    return service;
}

@Override
public void configure(ResourceServerSecurityConfigurer resources) {
    resources.resourceId(RESOURCE_ID)
        .tokenServices(tokenService())
        .stateless(true);
}
```

#### 资源服务与安全访问控制

与spring security一样

```java
@RestController
public class OrderController {
    
    @GetMapping(value = "/r1")
    @PreAuthorize("hasAnyAuthority('p1')")
        public String r1(){
        	return "访问资源1";
    }
}
```

```java
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true,prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/r/**").authenticated()//所有/r/**的请求必须认证通过
                .anyRequest().permitAll()//除了/r/**，其它的请求可以访问
                ;
    }
}
```

### OAuth 2.0  数据库配置

### 在数据库中建立对应表

#### 建表语句

官方提供的数据库建表语句https://github.com/spring-projects/spring-security-oauth/blob/master/spring-security-oauth2/src/test/resources/schema.sql

使用mysql需要将`LONGVARBINARY`修改为`blob`

```sql
-- used in tests that use HSQL
create table oauth_client_details (
  client_id VARCHAR(256) PRIMARY KEY,
  resource_ids VARCHAR(256),
  client_secret VARCHAR(256),
  scope VARCHAR(256),
  authorized_grant_types VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(256)
);

create table oauth_client_token (
  token_id VARCHAR(256),
  token LONGVARBINARY,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name VARCHAR(256),
  client_id VARCHAR(256)
);

create table oauth_access_token (
  token_id VARCHAR(256),
  token LONGVARBINARY,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name VARCHAR(256),
  client_id VARCHAR(256),
  authentication LONGVARBINARY,
  refresh_token VARCHAR(256)
);

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token LONGVARBINARY,
  authentication LONGVARBINARY
);

create table oauth_code (
  code VARCHAR(256), authentication LONGVARBINARY
);

create table oauth_approvals (
	userId VARCHAR(256),
	clientId VARCHAR(256),
	scope VARCHAR(256),
	status VARCHAR(10),
	expiresAt TIMESTAMP,
	lastModifiedAt TIMESTAMP
);


-- customized oauth_client_details table
create table ClientDetails (
  appId VARCHAR(256) PRIMARY KEY,
  resourceIds VARCHAR(256),
  appSecret VARCHAR(256),
  scope VARCHAR(256),
  grantTypes VARCHAR(256),
  redirectUrl VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additionalInformation VARCHAR(4096),
  autoApproveScopes VARCHAR(256)
);
```

#### 数据字典

##### `oauth_client_details`

该表用于保存接入客户端信息，主要操作`oauth_client_details`表的类是`JdbcClientDetailsService.java`，可以根据实际的需要去扩展或修改该类的实现。

| 字段名                  | 字段说明                                                     |
| ----------------------- | ------------------------------------------------------------ |
| client_id               | 主键,必须唯一,不能为空.  用于唯一标识每一个客户端(client); 在注册时必须填写(也可由服务端自动生成).  对于不同的grant_type,该字段都是必须的. 在实际应用中的另一个名称叫appKey,与client_id是同一个概念. |
| resource_ids            | 客户端所能访问的资源id集合,多个资源时用逗号(,)分隔,如: "unity-resource,mobile-resource".  该字段的值必须来源于与`security.xml`中标签`‹oauth2:resource-server`的属性`resource-id`值一致. 在`security.xml`配置有几个`‹oauth2:resource-server`标签, 则该字段可以使用几个该值.  在实际应用中, 我们一般将资源进行分类,并分别配置对应的`‹oauth2:resource-server`,如订单资源配置一个`‹oauth2:resource-server`, 用户资源又配置一个`‹oauth2:resource-server`. 当注册客户端时,根据实际需要可选择资源id,也可根据不同的注册流程,赋予对应的资源id. |
| client_secret           | 用于指定客户端(client)的访问密匙; 在注册时必须填写(也可由服务端自动生成).  对于不同的grant_type,该字段都是必须的. 在实际应用中的另一个名称叫appSecret,与client_secret是同一个概念. |
| scope                   | 指定客户端申请的权限范围,可选值包括*read*,*write*,*trust*;若有多个权限范围用逗号(,)分隔,如: "read,write".  scope的值与`security.xml`中配置的`‹intercept-url`的`access`属性有关系. 如`‹intercept-url`的配置为`‹intercept-url pattern="/m/**" access="ROLE_MOBILE,SCOPE_READ"/>`则说明访问该URL时的客户端必须有*read*权限范围. *write*的配置值为*SCOPE_WRITE*, *trust*的配置值为*SCOPE_TRUST*.  在实际应该中, 该值一般由服务端指定, 常用的值为*read,write*. |
| authorized_grant_types  | 指定客户端支持的grant_type,可选值包括*authorization_code*,*password*,*refresh_token*,*implicit*,*client_credentials*, 若支持多个grant_type用逗号(,)分隔,如: "authorization_code,password".  在实际应用中,当注册时,该字段是一般由服务器端指定的,而不是由申请者去选择的,最常用的grant_type组合有: "authorization_code,refresh_token"(针对通过浏览器访问的客户端); "password,refresh_token"(针对移动设备的客户端).  *implicit*与*client_credentials*在实际中很少使用. |
| web_server_redirect_uri | 客户端的重定向URI,可为空, 当grant_type为`authorization_code`或`implicit`时, 在Oauth的流程中会使用并检查与注册时填写的redirect_uri是否一致. 下面分别说明:当grant_type=`authorization_code`时, 第一步 `从 spring-oauth-server获取 'code'`时客户端发起请求时必须有`redirect_uri`参数, 该参数的值必须与 `web_server_redirect_uri`的值一致. 第二步 `用 'code' 换取 'access_token'` 时客户也必须传递相同的`redirect_uri`.  在实际应用中, *web_server_redirect_uri*在注册时是必须填写的, 一般用来处理服务器返回的`code`, 验证`state`是否合法与通过`code`去换取`access_token`值.  在[spring-oauth-client](http://git.oschina.net/mkk/spring-oauth-client)项目中, 可具体参考`AuthorizationCodeController.java`中的`authorizationCodeCallback`方法.当grant_type=`implicit`时通过`redirect_uri`的hash值来传递`access_token`值.如:`http://localhost:7777/spring-oauth-client/implicit#access_token=dc891f4a-ac88-4ba6-8224-a2497e013865&token_type=bearer&expires_in=43199`然后客户端通过JS等从hash值中取到`access_token`值. |
| authorities             | 指定客户端所拥有的Spring Security的权限值,可选, 若有多个权限值,用逗号(,)分隔, 如: "ROLE_UNITY,ROLE_USER".  对于是否要设置该字段的值,要根据不同的grant_type来判断, 若客户端在Oauth流程中需要用户的用户名(username)与密码(password)的(`authorization_code`,`password`),  则该字段可以不需要设置值,因为服务端将根据用户在服务端所拥有的权限来判断是否有权限访问对应的API.  但如果客户端在Oauth流程中不需要用户信息的(`implicit`,`client_credentials`),  则该字段必须要设置对应的权限值, 因为服务端将根据该字段值的权限来判断是否有权限访问对应的API.  (请在[spring-oauth-client](http://git.oschina.net/mkk/spring-oauth-client)项目中来测试不同grant_type时authorities的变化) |
| access_token_validity   | 设定客户端的access_token的有效时间值(单位:秒),可选, 若不设定值则使用默认的有效时间值(60 * 60 * 12, 12小时).  在服务端获取的access_token JSON数据中的`expires_in`字段的值即为当前access_token的有效时间值.  在项目中, 可具体参考`DefaultTokenServices.java`中属性`accessTokenValiditySeconds`.  在实际应用中, 该值一般是由服务端处理的, 不需要客户端自定义. |
| refresh_token_validity  | 设定客户端的refresh_token的有效时间值(单位:秒),可选, 若不设定值则使用默认的有效时间值(60 * 60 * 24 * 30, 30天).  若客户端的grant_type不包括`refresh_token`,则不用关心该字段 在项目中, 可具体参考`DefaultTokenServices.java`中属性`refreshTokenValiditySeconds`.   在实际应用中, 该值一般是由服务端处理的, 不需要客户端自定义. |
| additional_information  | 这是一个预留的字段,在Oauth的流程中没有实际的使用,可选,但若设置值,必须是JSON格式的数据,如:`{"country":"CN","country_code":"086"}`按照`spring-security-oauth`项目中对该字段的描述  *Additional information for this client, not need by the vanilla OAuth protocol but might be useful, for example,for storing descriptive information.*  (详见`ClientDetails.java`的`getAdditionalInformation()`方法的注释) 在实际应用中, 可以用该字段来存储关于客户端的一些其他信息,如客户端的国家,地区,注册时的IP地址等等. |
| create_time             | 数据的创建时间,精确到秒,由数据库在插入数据时取当前系统时间自动生成(扩展字段) |
| autoapprove             | 设置用户是否自动Approval操作, 默认值为 'false', 可选值包括 'true','false', 'read','write'.  该字段只适用于grant_type="authorization_code"的情况,当用户登录成功后,若该值为'true'或支持的scope值,则会跳过用户Approve的页面, 直接授权. |

##### `oauth_client_token`

该表用于在客户端系统中存储从服务端获取的token数据。

| 字段名            | 字段说明                                                     |
| ----------------- | ------------------------------------------------------------ |
| create_time       | 数据的创建时间,精确到秒,由数据库在插入数据时取当前系统时间自动生成(扩展字段) |
| token_id          | 从服务器端获取到的`access_token`的值.                        |
| token             | 这是一个二进制的字段, 存储的数据是`OAuth2AccessToken.java`对象序列化后的二进制数据. |
| authentication_id | 该字段具有唯一性, 是根据当前的username(如果有),client_id与scope通过MD5加密生成的.  具体实现请参考`DefaultClientKeyGenerator.java`类. |
| user_name         | 登录时的用户名                                               |
| client_id         |                                                              |

##### `oauth_access_token`

该表用于保存已授权的token。

主要操作`oauth_access_token`表的对象是`JdbcTokenStore.java`

| 字段名            | 字段说明                                                     |
| ----------------- | ------------------------------------------------------------ |
| create_time       | 数据的创建时间,精确到秒,由数据库在插入数据时取当前系统时间自动生成(扩展字段) |
| token_id          | 该字段的值是将`access_token`的值通过MD5加密后存储的.         |
| token             | 存储将`OAuth2AccessToken.java`对象序列化后的二进制数据, 是真实的AccessToken的数据值. |
| authentication_id | 该字段具有唯一性, 其值是根据当前的username(如果有),client_id与scope通过MD5加密生成的. 具体实现请参考`DefaultAuthenticationKeyGenerator.java`类. |
| user_name         | 登录时的用户名, 若客户端没有用户名(如grant_type="client_credentials"),则该值等于client_id |
| client_id         |                                                              |
| authentication    | 存储将`OAuth2Authentication.java`对象序列化后的二进制数据.   |
| refresh_token     | 该字段的值是将`refresh_token`的值通过MD5加密后存储的.        |

##### `oauth_refresh_token`

该表用于保存刷新token。

如果客户端的grant_type不支持`refresh_token`,则不会使用该表。主要操作`oauth_refresh_token`表的对象是`JdbcTokenStore.java`

| 字段名         | 字段说明                                                     |
| -------------- | ------------------------------------------------------------ |
| create_time    | 数据的创建时间,精确到秒,由数据库在插入数据时取当前系统时间自动生成(扩展字段) |
| token_id       | 该字段的值是将`refresh_token`的值通过MD5加密后存储的.        |
| token          | 存储将`OAuth2RefreshToken.java`对象序列化后的二进制数据.     |
| authentication | 存储将`OAuth2Authentication.java`对象序列化后的二进制数据.   |

##### `oauth_code`

只有当grant_type为"authorization_code"时,该表中才会有数据产生; 其他的grant_type没有使用该表.

主要操作`oauth_code`表的对象是`JdbcAuthorizationCodeServices.java`.

| 字段名         | 字段说明                                                     |
| -------------- | ------------------------------------------------------------ |
| create_time    | 数据的创建时间,精确到秒,由数据库在插入数据时取当前系统时间自动生成(扩展字段) |
| code           | 存储服务端系统生成的`code`的值(未加密).                      |
| authentication | 存储将`AuthorizationRequestHolder.java`对象序列化后的二进制数据. |

#### 配置授权服务

修改`AuthorizationServer`，`ClientDetailsService`和`AuthorizationCodeServices`从数据库读取数据。

```java
@Bean
public AuthorizationCodeServices authorizationCodeServices(DataSource dataSource) {
	return new JdbcAuthorizationCodeServices(dataSource);//设置授权码模式的授权码如何存取
}

@Bean
public ClientDetailsService clientDetailsService(DataSource dataSource) {
    ClientDetailsService clientDetailsService = new JdbcClientDetailsService(dataSource);
    ((JdbcClientDetailsService)clientDetailsService).setPasswordEncoder(passwordEncoder());
    return clientDetailsService;
}
```

配置`tokenStore`，将token保存到数据库

```java
@Bean
public TokenStore tokenStore(DataSource dataSource){
    return new JdbcTokenStore(dataSource);
}
```



## SpringBoot Securit配置属性

- - security.basic.authorize-mode
    要使用权限控制模式.
  - security.basic.enabled
    是否开启基本的鉴权，默认为true
  - security.basic.path
    需要鉴权的path，多个的话以逗号分隔，默认为[/**]
  - security.basic.realm
    HTTP basic realm 的名字，默认为Spring
  - security.enable-csrf
    是否开启cross-site request forgery校验，默认为false.
  - security.filter-order
    Security filter chain的order，默认为0
  - security.headers.cache
    是否开启http头部的cache控制，默认为false.
  - security.headers.content-type
    是否开启X-Content-Type-Options头部，默认为false.
  - security.headers.frame
    是否开启X-Frame-Options头部，默认为false.
  - security.headers.hsts
    指定HTTP Strict Transport Security (HSTS)模式(none, domain, all).
  - security.headers.xss
    是否开启cross-site scripting (XSS) 保护，默认为false.
  - security.ignored
    指定不鉴权的路径，多个的话以逗号分隔.
  - security.oauth2.client.access-token-uri
    指定获取access token的URI.
  - security.oauth2.client.access-token-validity-seconds
    指定access token失效时长.
  - security.oauth2.client.additional-information.[key]
    设定要添加的额外信息.
  - security.oauth2.client.authentication-scheme
    指定传输不记名令牌(bearer token)的方式(form, header, none,query)，默认为header
  - security.oauth2.client.authorities
    指定授予客户端的权限.
  - security.oauth2.client.authorized-grant-types
    指定客户端允许的grant types.
  - security.oauth2.client.auto-approve-scopes
    对客户端自动授权的scope.
  - security.oauth2.client.client-authentication-scheme
    传输authentication credentials的方式(form, header, none, query)，默认为header方式
  - security.oauth2.client.client-id
    指定OAuth2 client ID.
  - security.oauth2.client.client-secret
    指定OAuth2 client secret. 默认是一个随机的secret.
  - security.oauth2.client.grant-type
    指定获取资源的access token的授权类型.
  - security.oauth2.client.id
    指定应用的client ID.
  - security.oauth2.client.pre-established-redirect-uri
    服务端pre-established的跳转URI.
  - security.oauth2.client.refresh-token-validity-seconds
    指定refresh token的有效期.
  - security.oauth2.client.registered-redirect-uri
    指定客户端跳转URI，多个以逗号分隔.
  - security.oauth2.client.resource-ids
    指定客户端相关的资源id，多个以逗号分隔.
  - security.oauth2.client.scope
    client的scope
  - security.oauth2.client.token-name
    指定token的名称
  - security.oauth2.client.use-current-uri
    是否优先使用请求中URI，再使用pre-established的跳转URI. 默认为true
  - security.oauth2.client.user-authorization-uri
    用户跳转去获取access token的URI.
  - security.oauth2.resource.id
    指定resource的唯一标识.
  - security.oauth2.resource.jwt.key-uri
    JWT token的URI. 当key为公钥时，或者value不指定时指定.
  - security.oauth2.resource.jwt.key-value
    JWT token验证的value. 可以是对称加密或者PEMencoded RSA公钥. 可以使用URI作为value.
  - security.oauth2.resource.prefer-token-info
    是否使用token info，默认为true
  - security.oauth2.resource.service-id
    指定service ID，默认为resource.
  - security.oauth2.resource.token-info-uri
    token解码的URI.
  - security.oauth2.resource.token-type
    指定当使用userInfoUri时，发送的token类型.
  - security.oauth2.resource.user-info-uri
    指定user info的URI
  - security.oauth2.sso.filter-order
    如果没有显示提供WebSecurityConfigurerAdapter时指定的Filter order.
  - security.oauth2.sso.login-path
    跳转到SSO的登录路径默认为/login.
  - security.require-ssl
    是否对所有请求开启SSL，默认为false.
  - security.sessions
    指定Session的创建策略(always, never, if_required, stateless).
  - security.user.name
    指定默认的用户名，默认为user.
  - security.user.password
    默认的用户密码.
  - security.user.role
    默认用户的授权角色.



# 完整案例代码

https://gitee.com/li_hy/oauth2-security-demo