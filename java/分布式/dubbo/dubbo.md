# dubbo的背景

随着互联网的发展，网站应用的规模不断扩大，常规的垂直应用架构已无法应对，分布式服务架构以及流动计算架构势在必行，亟需一个治理系统确保架构有条不紊的演进。

![路线图](路线图.png)

**单一应用架构**

当网站流量很小时，只需一个应用，将所有功能都部署在一起，以减少部署节点和成本。此时，用于简化增删改查工作量的数据访问框架（ORM）是关键。

**垂直应用架构**

当访问量逐渐增大，单一应用增加机器带来的加速度越来越小，将应用拆成互不相干的几个应用，以提升效率。此时，用于加速前端页面开发的web框架（MVC）是关键。

**分布式服务架构**

当垂直应用越来越多，应用之间交互不可避免，将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，使前端应用能更快速的响应多变的市场需求。此时，用于提高业务复用及整合的分布式服务框架（RPC）是关键。

**流动计算架构**

当服务越来越多，容量的评估，小服务资源的浪费等问题逐渐显现，此时需要增加一个调度中心基于访问压力实时管理集群容量，提高集群利用率。此时，用于提高机器利用率的作用调度和治理中心（SOA）是关键。

# dubbo的基本需求

![需求](需求.png)

在大规模服务化之前，应用可能只是通过`RMI`或`Hessian`等工具，简单的暴露和引用远程服务，通过配置服务的URL地址进行调用，通过`F5`等硬件进行负载均衡。

**当服务越来越多时，服务URL配置管理变得非常困难，F5硬件负载均衡器的单独压力也越来越大。**此时需要一个服务注册中心，动态的注册和发现服务，使服务的位置透明。并通过在消费方获取服务提供方地址列表，实现软负载均衡和`Failover` 降低对F5硬件负载均衡器的依赖，也能减少部分成本。

**当进一步发展，服务间依赖关系变得错综复杂，甚至分不清哪个应用要在哪个应用之前启动。**这时，需要自动画出应用间的依赖关系图，以帮助清理关系。

**接着，服务的调用量越来越大，服务的容量问题就暴露出来，这个服务需要多少机器支撑？什么时候该加机器？**为了解决这些问题，第一步，要将服务现在每台调用量，响应时间，都统计出来，作为容量规划的参考指标。其次，要可以动态调整权重，在线上，将某台机器的权重一直加大，并在加大的过程中记录响应时间的变化，直到响应时间到达阈值，记录此时的访问量，再以此访问量乘以机器数反推总容量。

# dubbo的架构

![dubbo的架构](dubbo的架构.png)

**节点角色**

| 节点        | 角色说明                               |
| ----------- | -------------------------------------- |
| `Provider`  | 暴露服务的服务提供方                   |
| `Consumer`  | 调用远程服务的服务消息方               |
| `Registry`  | 服务注册与发现的注册中心               |
| `Monitor`   | 统计服务的调用次数和调用事件的监控中心 |
| `Container` | 服务运行容器                           |

**调用关系**

- 0 -- 服务容器负责启动，加载，运行服务提供者。
- 1 -- 服务提供者在启动时，向注册中心注册自己提供的服务。
- 2 -- 服务消费者在启动时，向注册中心订阅自己所需的服务。
- 3 -- 注册中心返回服务提供者地址列表给消费者，如果有变更，注册中心将基于长连接推送变更数据给消费者。
- 4 -- 服务消费者，从提供者地址列表中，基于软负载均衡算法，选一台提供者进行调用，如果调用失败，再选另一台调用。
- 5 -- 服务消费者和提供者，在内存中累积调用次数和调用事件，定时每分钟发送一次统计数据到监控中心。

## dubbo架构的特点：

**连通性**

- 注册中心发现服务地址的注册与查找，相对于目录服务，服务提供者和消费者只在启动时与注册中心交互，注册中心不转发请求，压力较小。
- 监控中心负责统计各服务调用次数，调用事件等。统计先在内存汇总后每分钟一次发送到监控中心服务器，并以报表展示。
- 服务提供者向注册中心注册其提供的服务，并汇报调用事件到监控中心，此时间不包含网络开销。
- 服务消费者向注册中心获取服务提供者地址列表，并根据负载算法直接调用提供者，同时汇报调用时间到监控中心，此时间包含网络开销。
- 注册中心，服务提供者，服务消费者三者之间均为长连接，控制中心除外。
- 注册中心通过长连接感知服务提供者的存在，服务提供者宕机，注册中心将立即推送事件通知消费者。
- 注册中心和监控中心全部宕机，不影响已运行的提供者和消费者，消费者在本地缓存了提供者列表。
- 注册中心和监控中心都是可选的，服务消费者可以直连服务提供者。

**健壮性**

- 监控中心宕掉不影响使用，只是丢失部分采集数据。
- 数据库宕掉后，注册中心仍能通过缓存通过服务列表查询，但不能注册新服务。
-  注册中心对本身可做对等集群，可动态增减节点，任意一台宕掉后，将自动切换到另一台。
- 注册中心全部宕掉后，服务提供者和服务消费者仍能通过本地缓存通讯。
- 服务提供者无状态，任意一台宕掉后，不影响使用。
- 服务提供者全部宕掉后，服务消费者应用将无法使用，并无限次重连等待服务提供者恢复。

**伸缩性**

- 注册中心为对等集群，可动态增加机器部署实例，所有客户端将自动发现新的注册中心。
- 服务提供者无状态，可动态增加机器部署实例，注册中心将推送新的服务提供者信息给消费者。

**升级性**

- 当服务集群规模进一步扩大，带动IT治理结构进一步升级，需要实现动态部署，进行流动计算，现有分布式服务架构不会带来阻力。如下图是未来可能的一种架构

![未来架构](未来架构.png)

**节点角色**

| 节点         | 角色说明                               |
| ------------ | -------------------------------------- |
| `Deployer`   | 自动部署服务的本地代理                 |
| `Repository` | 仓库用于存储服务应用发布包             |
| `Scheduler`  | 调度中心基于访问压力自动增减服务提供者 |
| `Admin`      | 统一管理控制台                         |
| `Registry`   | 服务注册与发现的注册中心               |
| `Monitor`    | 统计服务的调用次数和调用时间的监控中心 |

# dubbo的使用

## 快速启动

dubbo采用全spring配置方式，透明化接入应用，对应用没有任何API侵入，只需用spring加载dubbo的配置即可，dubbo基于spring的`schema扩展`进行加载。

如果不想使用spring配置，也可以通过API的方式进行调用。

### 服务端（服务提供者）

#### 用spring配置声明暴露服务

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
 
    <!-- 提供方应用信息，用于计算依赖关系 -->
    <dubbo:application name="hello-world-app"  />
 
    <!-- 使用multicast广播注册中心暴露服务地址 -->
    <dubbo:registry address="multicast://224.5.6.7:1234" />
 
    <!-- 用dubbo协议在20880端口暴露服务 -->
    <dubbo:protocol name="dubbo" port="20880" />
 
    <!-- 声明需要暴露的服务接口 -->
    <dubbo:service interface="com.alibaba.dubbo.demo.DemoService" ref="demoService" />
 
    <!-- 和本地bean一样实现服务 -->
    <bean id="demoService" class="com.alibaba.dubbo.demo.provider.DemoServiceImpl" />
</beans>
```

#### 加载spring配置

```java
public class Provider {
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"http://10.20.160.198/wiki/display/dubbo/provider.xml"});
        context.start();
        System.in.read(); // 按任意键退出
    }
}
```

### 客户端（服务消费者）

#### 通过spring配置引用远程服务

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
 
    <!-- 消费方应用名，用于计算依赖关系，不是匹配条件，不要与提供方一样 -->
    <dubbo:application name="consumer-of-helloworld-app"  />
 
    <!-- 使用multicast广播注册中心暴露发现服务地址 -->
    <dubbo:registry address="multicast://224.5.6.7:1234" />
 
    <!-- 生成远程服务代理，可以和本地bean一样使用demoService -->
    <dubbo:reference id="demoService" interface="com.alibaba.dubbo.demo.DemoService" />
</beans>
```

#### 加载spring配置，并调用远程服务

```java
public class Consumer {
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"http://10.20.160.198/wiki/display/dubbo/consumer.xml"});
        context.start();
        DemoService demoService = (DemoService)context.getBean("demoService"); // 获取远程服务代理
        String hello = demoService.sayHello("world"); // 执行远程方法
        System.out.println( hello ); // 显示调用结果
    }
}
```

## 多协议支持

dubbo运行配置多协议，在不同服务上支持不同协议或者同一服务上同时支持多种协议。

### 不同服务不同协议

不同协议在性能上适用不同协议进行传输，比如大数据用短连接协议，小数据大并发用长连接协议。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd"> 
    <dubbo:application name="world"  />
    <dubbo:registry id="registry" address="10.20.141.150:9090" username="admin" password="hello1234" />
    <!-- 多协议配置 -->
    <dubbo:protocol name="dubbo" port="20880" />
    <dubbo:protocol name="rmi" port="1099" />
    <!-- 使用dubbo协议暴露服务 -->
    <dubbo:service interface="com.alibaba.hello.api.HelloService" version="1.0.0" ref="helloService" protocol="dubbo" />
    <!-- 使用rmi协议暴露服务 -->
    <dubbo:service interface="com.alibaba.hello.api.DemoService" version="1.0.0" ref="demoService" protocol="rmi" /> 
</beans>
```

### 多协议暴露服务

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
    <dubbo:application name="world"  />
    <dubbo:registry id="registry" address="10.20.141.150:9090" username="admin" password="hello1234" />
    <!-- 多协议配置 -->
    <dubbo:protocol name="dubbo" port="20880" />
    <dubbo:protocol name="hessian" port="8080" />
    <!-- 使用多个协议暴露服务 -->
    <dubbo:service id="helloService" interface="com.alibaba.hello.api.HelloService" version="1.0.0" protocol="dubbo,hessian" />
</beans>
```

## 多注册中心

dubbo支持同一服务向多注册中心同时注册，或者不同服务分别注册到不同的注册中心上去，甚至可以同时引用注册在不同注册中心上的同名服务。另外，注册中心支持自定义扩展。

### 多注册中心注册

比如：服务部署的物理地址不一样，部署在同一个物理地址的服务注册到同一地址的注册中心上。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
    <dubbo:application name="world"  />
    <!-- 多注册中心配置 -->
    <dubbo:registry id="hangzhouRegistry" address="10.20.141.150:9090" />
    <dubbo:registry id="qingdaoRegistry" address="10.20.141.151:9010" default="false" />
    <!-- 向多个注册中心注册 -->
    <dubbo:service interface="com.alibaba.hello.api.HelloService" version="1.0.0" ref="helloService" registry="hangzhouRegistry,qingdaoRegistry" />
</beans>
```

### 不同服务使用不同注册中心

比如有些服务是专门为国际站设计的，有些服务是专门为中文站设计的。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
    <dubbo:application name="world"  />
    <!-- 多注册中心配置 -->
    <dubbo:registry id="chinaRegistry" address="10.20.141.150:9090" />
    <dubbo:registry id="intlRegistry" address="10.20.154.177:9010" default="false" />
    <!-- 向中文站注册中心注册 -->
    <dubbo:service interface="com.alibaba.hello.api.HelloService" version="1.0.0" ref="helloService" registry="chinaRegistry" />
    <!-- 向国际站注册中心注册 -->
    <dubbo:service interface="com.alibaba.hello.api.DemoService" version="1.0.0" ref="demoService" registry="intlRegistry" />
</beans>
```

### 多注册中心引用

比如：需要同时调用中文站和国际站的某个服务，此服务在中文站和国际站均有部署，接口及版本号都一样，但连的数据库不一样。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
    <dubbo:application name="world"  />
    <!-- 多注册中心配置 -->
    <dubbo:registry id="chinaRegistry" address="10.20.141.150:9090" />
    <dubbo:registry id="intlRegistry" address="10.20.154.177:9010" default="false" />
    <!-- 引用中文站服务 -->
    <dubbo:reference id="chinaHelloService" interface="com.alibaba.hello.api.HelloService" version="1.0.0" registry="chinaRegistry" />
    <!-- 引用国际站站服务 -->
    <dubbo:reference id="intlHelloService" interface="com.alibaba.hello.api.HelloService" version="1.0.0" registry="intlRegistry" />
</beans>
```

如果是测试环境临时需要连接两个不同注册中心，可使用竖号分隔多个不同注册中心地址

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd">
    <dubbo:application name="world"  />
    <!-- 多注册中心配置，竖号分隔表示同时连接多个不同注册中心，同一注册中心的多个集群地址用逗号分隔 -->
    <dubbo:registry address="10.20.141.150:9090|10.20.154.177:9010" />
    <!-- 引用服务 -->
    <dubbo:reference id="helloService" interface="com.alibaba.hello.api.HelloService" version="1.0.0" />
</beans>
```

## 启动时检查

dubbo缺省会在启动时检查依赖的服务是否可用，不可用时会抛出异常，阻止spring初始化完成，默认`check="true"`。

可以通过`check="false"`关闭检查，比如测试时有些服务不关心，或者出现了循环依赖，必须有一方先启动。

另外，如果spring容器是懒加载的，或者通过API编程延迟应用服务，必须关闭check，否则服务临时不可用时，会抛出异常，拿到null引用，如果`check="false"`，总是会返回引用，当服务恢复时，能自动连上。

```xml
<!--关闭某个服务的启动时检查 (没有提供者时报错) -->
<dubbo:reference interface="com.foo.BarService" check="false" />
  
 <!--关闭所有服务的启动时检查 (没有提供者时报错) -->
 <dubbo:consumer check="false" />   
     
 <!--关闭注册中心启动时检查 (注册订阅失败时报错) -->  
<dubbo:registry check="false" />
```

**通过`dubbo.properties`**

```properties
dubbo.reference.com.foo.BarService.check=false
dubbo.reference.check=false
dubbo.consumer.check=false
dubbo.registry.check=false
```

**通过-D参数**

```java
java -Ddubbo.reference.com.foo.BarService.check=false
java -Ddubbo.reference.check=false
java -Ddubbo.consumer.check=false 
java -Ddubbo.registry.check=false
```

`dubbo.reference.check=false`，强制改变所有reference的check值，就算配置中有声明，也会被覆盖。

`dubbo.consumer.check=false`，是设置check的缺省值，如果配置中有显示的声明，如`<dubbo:reference check="true">`，不会受影响。

`dubbo.registry.check=false`，前面两个都是指订阅成功，但提供者列表是否为空是否报错，如果注册订阅失败时，也允许启动，需使用此选项，将在后台定时重试。

## 多版本支持

当一个接口实现出现不兼容升级时，可以用版本号过度，版本号不同的服务相互间不引用。

可以按以下的步骤进行版本迁移：

1. 在低压力时间段，先升级一半提供者为新版本；
2. 将所有消费者升级为新版本；
3. 将剩下的一半提供者升级为新版本。

```xml
<!--老版本服务提供者配置 -->
<dubbo:service interface="com.foo.BarService" version="1.0.0" />

<!--新版本服务提供者配置 -->
<dubbo:service interface="com.foo.BarService" version="2.0.0" />
```

```xml
<!--老版本服务消费者配置 -->
<dubbo:reference id="barService" interface="com.foo.BarService" version="1.0.0" />

<!--新版本服务消费者配置 -->
<dubbo:reference id="barService" interface="com.foo.BarService" version="2.0.0" />

<!--如果不需要区分版本，可以按照以下的方式配置(2.20以上版本支持) -->
<dubbo:reference id="barService" interface="com.foo.BarService" version="*" />
```

## 主机绑定

### 查找顺序

缺省主机IP查找顺序：

- 通过`LocalHost.getLocalHost()`获取本地地址。
- 如果是`127.*`等loopback地址，则扫描各网卡，获取网卡IP。

### 主机配置

注册的地址如果获取不正确，比如需要注册公网地址

- 可以在`/etc/hosts`中加入：机器名 公网IP

  ```properties
  test1 205.182.23.201
  ```

- 可以在`dubbo.xml`中加入主机地址的配置

  ```xml
  <dubbo:protocol host="205.182.23.201">
  ```

- 可以在`dubbo.properties`中加入主机地址的配置

  ```properties
  dubbo.protocol.host=205.182.23.201
  ```

### 端口配置

缺省主机端口与协议相关

| 协议       | 端口  |
| ---------- | ----- |
| dubbo      | 20880 |
| rmi        | 1099  |
| http       | 80    |
| hessian    | 80    |
| webservice | 80    |
| memcached  | 11211 |
| redis      | 6379  |

可以按照下面的方式配置端口

- 可以在`dubbo.xml`中加入主机地址的配置

  ```xml
  <dubbo:protocol name="dubbo" port="20880">
  ```

- 可以在`dubbo.properties`中加入主机地址的配置

  ```properties
  dubbo.protocol.dubbo.port=20880
  ```

## 集群容错

在集群调用失败时，dubbo提供了多种容错方案，缺省为`failover`重试。

![集群容错](集群容错.png)

各节点关系：

- `Invoker`是`Provider`的一个可调用`Service`的抽象，`Invoker`封装了`Provider`地址及`Service`接口信息。
- `Directory`代表多个`Invoker`，可以看出`List<Invoker>`，但与`List`不同的是，它的值可能是动态变化的，比如注册中心推送变更。
- `Cluster`将`Directory`中的多个`Invoker`伪装成一个`Invoker`，对上层透明，伪装过程包含了容错逻辑，调用失败后，重试另一个。
- `Router`复制从多个`Invoker`中按路由规则选出子集，比如读写分离，应用隔离等。
- `LoadBalance`复制从多个`Invoker`中选出具体的一个用于本次调用，选的过程包含了负载均衡算法，调用失败后，需要重选。

### 集群容错模式

- Failover Cluster

  失败自动切换，当出现失败，重试其他服务器。通常用于读操作，但重试会带来更长延迟。可通过`retries="2"`（默认2）来设置重试次数（不含第一次）。***如果是写操作需要考虑幂等***。

  ```xml
  <!--服务端-->
  <dubbo:service retries="2" />
  
  <!--客户端-->
  <dubbo:reference retries="2" />
  
  <dubbo:reference>
      <dubbo:method name="findFoo" retries="2" />
  </dubbo:reference>
  ```

- Failfast Cluster

  快速失败，只发起一次调用，失败立即报错，通常用于非幂等性的写操作，比如新增记录。

- Failsafe Cluster

  失败安全，出现异常时直接忽略。通常用于写入审计日志等操作。

- Failback Cluster

  失败自动恢复，后台记录失败请求，定时重发。通常用于消息通知操作。

- Forking Cluster

  并行调用多个服务器，只要一个成功即返回，通常用于实时性要求较高的读操作，但须浪费更多服务资源。可通过`forks="2"`来设置最大并行数。

- Broadcast Cluster

  广播调用所有提供者，逐个调用，任意一台报错则报错。通常用于通知所有提供者更新缓存或日志等本地资源信息。

### 集群模式配置

```xml
<!--服务端-->
<dubbo:service cluster="failsafe" />

<!--客户端-->
<dubbo:reference cluster="failsafe" />
```

## 服务降级

降级的目的是为了保证核心服务可用。

降级可以有几个层面的分类：

- 自动降级和人工降级
- 按功能可以分为读服务降级和写服务降级

### 服务降级的场景

- 对一些非核心服务进行人工降级，在大促之前通过降级开关关闭哪些推荐内容、评价等对主流程没有影响的功能；
- 故障降级，比如调用的远程服务挂了，网络故障、或者RPC服务返回异常。 那么可以直接降级，降级的方案比如设置默认值、采用兜底数据（系统推荐的行为广告挂了，可以提前准备静态页面做返回）等等；
- 限流降级，在秒杀这种流量比较集中并且流量特别大的情况下，因为突发访问量特别大可能会导致系统支撑不了。这个时候可以采用限流来限制访问量。当达到阀值时，后续的请求被降级，比如进入排队页面，比如跳转到错误页（活动太火爆，稍后重试等）。

### 服务降级的实现

通过服务降级功能临时屏蔽某个出错的非关键服务，并定义降级后的返回策略。

向注册中心写入动态配置覆盖规则

```java
RegistryFactory registryFactory = ExtensionLoader.getExtensionLoader(RegistryFactory.class).getAdaptiveExtension();
Registry registry = registryFactory.getRegistry(URL.valueOf("zookeeper://10.20.153.10:2181"));
registry.register(URL.valueOf("override://0.0.0.0/com.foo.BarService?category=configurators&dynamic=false&application=foo&mock=force:return+null"));
```

其中：

- `mock=force:return+null` 表示消费方对该服务的方法调用都直接返回 null 值，不发起远程调用。用来屏蔽不重要服务不可用时对调用方的影响。
- 还可以改为 `mock=fail:return+null` 表示消费方对该服务的方法调用在失败后，再返回 null 值，不抛异常。用来容忍不重要服务不稳定时对调用方的影响。

## 本地伪装

通常用于服务降级，比如某验权服务，当服务提供方全部挂掉后，客户端不抛出异常，而是通过Mock数据返回授权失败。

Mock 是 Stub 的一个子集，便于服务提供方在客户端执行容错逻辑，因经常需要在出现 RpcException (比如网络失败，超时等)时进行容错，而在出现业务异常(比如登录用户名密码错误)时不需要容错，如果用 Stub，可能就需要捕获并依赖 RpcException 类，而用 Mock 就可以不依赖 。

在 spring 配置文件中按以下方式配置

```xml
<dubbo:reference interface="com.foo.BarService" mock="true" />
```

或

```xml
<dubbo:reference interface="com.foo.BarService" mock="com.foo.BarServiceMock" />
```

在工程中提供Mock实现，实现对于接口（需要对哪个接口进行mock就实现哪个），名称必须以Mock结尾

```java
public class BarServiceMock implements BarService {
    public String sayHello(String name) {
        // 你可以伪造容错数据，此方法只在出现RpcException时被执行
        return "容错数据";
    }
}
```

如果服务的消费方经常需要try-catch捕获异常，可考虑改为Mock实现，并在Mock实现中return null。

```xml
<dubbo:reference interface="com.foo.BarService" mock="return null" />
```

### 进阶用法

#### return

使用`return`来返回一个字符串表示的对象，作为Mock的返回值。合法的字符串可以是：

- empty：代表空，基本类型的默认值，或者集合类的空值。
- null：`null`
- true：`true`
- false：`false`
- JSON格式：反序列化JSON所得到的对象

#### throw

使用`throw`来返回一个Exception对象，作为Mock的返回值。

当调用出错时，抛出一个默认的RPCException：

```xml
<dubbo:reference interface="com.foo.BarService" mock="throw" />
```

当调用出错时，抛出指定的Exception：

```xml
<dubbo:reference interface="com.foo.BarService" mock="throw com.foo.MockException" />
```

#### force和fail

在`2.6.6`以上的版本，可以开始在Spring XML配置文件中使用`fail:`和`force:`。

`force:`代表强制使用Mock行为，在这种情况下不会走远程调用。

`fail:`与默认行为一致，只有当远程调用发生错误时才使用Mock行为。

`fail:`和`force:`都支持`throw`或者`return`组合使用。

强制返回指定值：

```xml
<dubbo:reference interface="com.foo.BarService" mock="force:return fake" />
```

强制抛出指定异常：

```xml
<dubbo:reference interface="com.foo.BarService" mock="force:throw com.foo.MockException" />
```

#### 在方法级别配置Mock

Mock 可以在方法级别上指定，假定 `com.foo.BarService` 上有好几个方法，我们可以单独为 `sayHello()` 方法指定 Mock 行为。具体配置如下：

```xml
<dubbo:reference id="demoService" check="false" interface="com.foo.BarService">
    <dubbo:parameter key="sayHello.mock" value="force:return fake"/>
</dubbo:reference>
```

# dubbo配置推荐

## dubbo配置的优先级

配置的覆盖规则：

1. 方法级别配置优于接口级别，最后是全局配置，即小 Scope 优先 
2. Consumer 端配置优于 Provider 配置，优于全局配置，最后是Dubbo 硬编码的配置值

**服务提供方配置通过URL经由注册中心传递给消费方。**

## 在Provider上尽量多配置Consumer端属性

- 作为服务的提供者，比服务使用方更清楚服务性能参数，如调用的超时时间、合理的重试次数等；
- 在Provider配置后，Consumer不配置则会使用Provider的配置值，即Provider配置可以作为Consumer的缺省值。否则Consumer会使用Consumer端的全局设置，这对于Provider是不可控的，并且往往不是不合理的。

在Provider上可以配置的Consumer端属性有：

- `timeout`方法调用超时；
- `retries`失败重试次数，缺省是2；
- `loadbalance`负载均衡算法，缺省是随机`random`。还有轮询`roundrobin`、最不活跃优先`leastactive`等；
- `actives`消费者端，最大并发调用限制，即当Consumer对一个服务的并发调用到上限后，新调用会阻塞直到超时，在方法上配置`dubbo:method`则并发限制针对方法，在接口上配置`dubbo:service`，则并发限制针对服务。

## Provider上配置合理的Provider端属性

Provider上可以配置的Provider端属性有：

- `threads`服务线程池大小
- `executes`一个服务提供者并行执行请求上限，即当Provider对一个服务的并发调用达到上限后，新调用会阻塞，此时Consumer可能会超时。在方法上配置`dubbo:method`则并发限制针对方法，在接口上配置`dubbo:service`，则并发限制针对服务。

## 配置Dubbo缓存文件

提供者列表缓存文件：

```xml
<dubbo:registry file=”${user.home}/output/dubbo.cache” />
```

注意：

- 应用可以根据需要调整缓存文件的路径，保证这个文件不会在发布过程中被清除；
- 如果有多个应用进程，注意不要使用同一个文件，避免内容被覆盖。

该文件会缓存注册中心列表和服务提供者列表。配置缓存文件后，应用重启过程中，若注册中心不可用，应用会从该缓存文件读取服务提供者类别，进一步保证应用可靠性。

## 监控配置

- 使用固定端口暴露服务，而不要使用随机端口

  这样在注册中心推送有延迟的情况下，消费者通过缓存列表也能调用到原地址，保证调用成功。

- 使用 `Dubbo Ops` 监控注册中心上的服务提供方

  使用 `Dubbo Ops` 监控服务在注册中心上的状态，确保注册中心上有该服务的存在。

- 服务提供方，使用 `Dubbo Qos` 的 telnet 或 shell 监控项

  监控服务提供者端口状态：`echo status | nc -i 1 20880 | grep OK | wc -l`，其中的 20880 为服务端口

- 服务消费方，通过将服务强制转型为 EchoService，并调用 `$echo()` 测试该服务的提供者是可用

  如 `assertEqauls(“OK”, ((EchoService)memberService).$echo(“OK”));`

## 不要使用 dubbo.properties 文件配置，推荐使用对应 XML 配置

Dubbo 中所有的配置项都可以配置在 Spring 配置文件中，并且可以针对单个服务配置。

### dubbo.properties 中属性名与 XML 的对应关系

1. 应用名 `dubbo.application.name`

   ```xml
   <dubbo:application name="myalibaba" >
   ```

2. 注册中心地址 `dubbo.registry.address`

   ```xml
   <dubbo:registry address="11.22.33.44:9090" >
   ```

3. 调用超时 `dubbo.service.*.timeout`

   可以在多个配置项设置超时 `timeout`，由上至下覆盖（即上面的优先），其它的参数（`retries`、`loadbalance`、`actives`等）的覆盖策略与 `timeout` 相同。示例如下：

   提供者端特定方法的配置

   ```xml
   <dubbo:service interface="com.alibaba.xxx.XxxService" >
       <dubbo:method name="findPerson" timeout="1000" />
   </dubbo:service>
   ```

   提供者端特定接口的配置

   ```xml
   <dubbo:service interface="com.alibaba.xxx.XxxService" timeout="200" />
   ```

4. 服务提供者协议 `dubbo.service.protocol`、服务的监听端口 `dubbo.service.server.port`

   ```xml
   <dubbo:protocol name="dubbo" port="20880" />
   ```

5. 服务线程池大小 `dubbo.service.max.thread.threads.size`

   ```xml
   <dubbo:protocol threads="100" />
   ```

6. 消费者启动时，没有提供者是否抛异常 `alibaba.intl.commons.dubbo.service.allow.no.provider`

   ```xml
   <dubbo:reference interface="com.alibaba.xxx.XxxService" check="false" />
   ```
