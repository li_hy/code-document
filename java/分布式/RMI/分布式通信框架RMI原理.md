# RPC

RPC（Remote Procedure Call）远程过程调用，一般用来实现部署在不同机器上的系统之间的方法调用，使程序能够像访问本地系统资源一样，通过网络传输去访问远端系统资源。对应客户端来说，传输层使用什么协议、序列化、反序列化都是透明的。

# JAVA RMI

RMI（Remote Method Invocation）远程方法调用，一种用于远程过程调用的应用程序编程接口，是纯java的网络分布式应用系统的核心解决方案之一。

RMI目前使用java远程消息交换协议JRMP（Java Remote Messaging Protocol）进行通信，由于JRMP是专门为java对象定制的，是分布式应用系统的百分百纯java解决方案。

用java RMI开发的应用系统可以部署在任何支持JRE的平台上，由于JRMP是专门为java对象指定的，因此RMI对应非java语言开发的应用系统支持不足，不能与非java语言书写的对象进行通信。

# JAVA RMI示例代码

## 服务端

```java
//服务接口
public interface IHelloService extends Remote {
    String sayHello(String msg) throws RemoteException;
}

//服务实现
public class HelloServiceImpl extends UnicastRemoteObject implements IHelloService {
    protected HelloServiceImpl() throws RemoteException {
        super();
    }

    @Override
    public String sayHello(String msg) throws RemoteException {
        return "hello, " + msg;
    }
}

//服务发布
public class Server {
    public static void main(String[] args) {
        IHelloService  helloService = null;
        try {
            helloService = new HelloServiceImpl();
            LocateRegistry.createRegistry(1099);
            Naming.rebind("rmi://localhost/Hello", helloService);
            System.out.println("服务启动成功");
        } catch (RemoteException e) {
            e.printStackTrace();
        }  catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}

```

## 客户端

```java
//客户端接口
public interface IHelloService extends Remote {
    String sayHello(String msg) throws RemoteException;
}

//客户端调用
public class ClientDemo {
    public static void main(String[] args) {
        try {
            IHelloService helloService = (IHelloService) Naming.lookup("rmi://localhost/Hello");
            System.out.println(helloService.sayHello("lhy"));
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
```

远程对象必须实现`UnicastRemoteObject`，这样才能保证客户端访问获得远程对象时，该远程对象会把自身的一个拷贝以Socket形式传输给客户端，客户端获得的拷贝称为`stub`，而服务端本身已经存在的远程对象称为`skeleton`，此时客户端的`stub`是客户端的一个代理，由于与服务端进行通信，而`skeleton`是服务端的一个代理，用于接收客户端的请求之后调用远程方法来响应客户端的请求。

# JAVA RMI源码分析

## 远程对象发布

![远程对象发布](远程对象发布.png)



## 远程引用层

![远程引用层](远程引用层.png)



## 源码解读

### 发布远程对象

由以上类图可知，需要发布两个远程对象，一个是`RegistryIMPL`，另一个是自定义的RMI实现类对象。

从`HelloServiceImpl`的构造函数开始，调用了父类`unicastRemoteObject`的构造方法

```java
protected UnicastRemoteObject() throws RemoteException
    {
        this(0);
    }
```

此构造方法中调用了私有方法`exportObject()`

```java
protected UnicastRemoteObject(int port) throws RemoteException
    {
        this.port = port;
        exportObject((Remote) this, port);
    }
    
public static Remote exportObject(Remote obj, int port)
        throws RemoteException
    {
    	//创建UncastServerRef对象，对象内有引用了LiveRef(tcp通信)
        return exportObject(obj, new UnicastServerRef(port));
    }
    
private static Remote exportObject(Remote obj, UnicastServerRef sref)
        throws RemoteException
    {
        // if obj extends UnicastRemoteObject, set its ref.
        if (obj instanceof UnicastRemoteObject) {
            ((UnicastRemoteObject) obj).ref = sref;
        }
        return sref.exportObject(obj, null, false);
    }
```

这里做了一个判断，判断服务的实现是不是`UnicastRemoteObject`的子类，如果是，则直接赋值其ref（`RemoteRef`）对象为传入的`UnicastServerRef`对象。反正则调用`UnicastServerRef`的`exportObject()`方法。

```java
IHelloService  helloService = new HelloServiceImpl();
```

因为`HelloServiceImpl`继承了`UnicastRemoteOnject`，所以在服务启动的时候，会通过`UnicastRemoteOnject`的构造方法吧该对象进行发布

```java
public class HelloServiceImpl extends UnicastRemoteObject implements IHelloService {

    protected HelloServiceImpl() throws RemoteException {
        super();
    }
    
    ......
}
```

```java
//UnicastServerRef.java

public Remote exportObject(Remote var1, Object var2, boolean var3) throws RemoteException {
        Class var4 = var1.getClass();

        Remote var5;
        try {
            //创建远程代理类，该代理是对OperationImpl对象的代理，getClinetRef提供的InvocationHandler中提供了TCP连接
            var5 = Util.createProxy(var4, this.getClientRef(), this.forceStubUse);
        } catch (IllegalArgumentException var7) {
            throw new ExportException("remote object implements illegal remote interface", var7);
        }

        if (var5 instanceof RemoteStub) {
            this.setSkeleton(var1);
        }

    	//包装实际对象，并将其暴露在TCP端口上，等待客户端调用
        Target var6 = new Target(var1, this, var5, this.ref.getObjID(), var3);
        this.ref.exportObject(var6);
        this.hashToMethod_Map = (Map)hashToMethod_Maps.get(var4);
        return var5;
    }
```

### 服务端启动Registry服务

```java
LocateRegistry.createRegistry(1099);
```

这段代码服务端创建了一个`RegistryImpl`对象，这里做了一个判断，如果服务端指定的端口是1099并且系统开启了安全管理器，你们就可以在限定的权限集内绕过系统的安全校验。这里纯粹是为了提高效率，真正的逻辑在`this.setup(new UnicastServerRef(var2))`这个方法里面

```java
//RegistryImpl.java

public RegistryImpl(final int var1) throws RemoteException {
        if (var1 == 1099 && System.getSecurityManager() != null) {
            try {
                AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {
                    public Void run() throws RemoteException {
                        LiveRef var1x = new LiveRef(RegistryImpl.id, var1);
                        RegistryImpl.this.setup(new UnicastServerRef(var1x));
                        return null;
                    }
                }, (AccessControlContext)null, new SocketPermission("localhost:" + var1, "listen,accept"));
            } catch (PrivilegedActionException var3) {
                throw (RemoteException)var3.getException();
            }
        } else {
            LiveRef var2 = new LiveRef(id, var1);
            this.setup(new UnicastServerRef(var2));
        }

    }
```

`setup`方法将指向正在初始化的`RegistryImpl`对象的远程引用ref（`RemoteRef`）赋值传入的`UnicastServerRef`对象，这里涉及到向上转型，然后继续执行`UnicastServerRef`的`exprotObject`方法

```java
private void setup(UnicastServerRef var1) throws RemoteException {
        this.ref = var1;
        var1.exportObject(this, (Object)null, true);
    }
```

进入`UnicastServerRef`的`exportOnject()`方法，可以看到这里首先为传入的`RegistryImpl`创建一个代理，这个代理可以推断出就是后面服务于客户端的`RegistryImpl`的`Stub(RegistryImpl_Stub)`对象。然后将`UnicasServerRef`的`skel(skeleton)`对象设置为当前`RegistryImpl`对象。最后用`skeleton`、`stub`、`UnicastServerRef`对象、id和一个boolean值构造了一个`Target`对象，也就是这个`Target`对象基本上包含了全部的信息，等待TCP调用。调用`UnicastServerRef`的ref（`LiveRef`）变量的`exportObject()`方法。

```java
//UnicasServerRef.java

//LiveRef与TCP通信的类
//var1=RegistryImpl
//var2=null
//var3=true
public Remote exportObject(Remote var1, Object var2, boolean var3) throws RemoteException {
        Class var4 = var1.getClass();

        Remote var5;
        try {
            var5 = Util.createProxy(var4, this.getClientRef(), this.forceStubUse);
        } catch (IllegalArgumentException var7) {
            throw new ExportException("remote object implements illegal remote interface", var7);
        }

        if (var5 instanceof RemoteStub) {
            this.setSkeleton(var1);
        }

        Target var6 = new Target(var1, this, var5, this.ref.getObjID(), var3);
        this.ref.exportObject(var6);
        this.hashToMethod_Map = (Map)hashToMethod_Maps.get(var4);
        return var5;
    }
```

到上面为止，看到的都是一些变量的赋值和创建工作，还没有到连接层，这些引用对象将会被`Stub`和`Skeleton`对象使用。

连接层上，追溯`LiveRef`的`exportOnject()`方法，找到`TCPTranspot`的`exportObject()`方法。这个方法做的事就是将上面构造的`Target`对象暴露出去。调用`TCPTransport`的`Listen()`方法创建一个`ServerSocket`，并且启动了一条线程等待客户端的请求。接着调用父类`Transport`的`exportObject()`将`Target`对象存放进`ObjectTable`中。

```java
//TCPTransport.java

public void exportObject(Target var1) throws RemoteException {
        synchronized(this) {
            this.listen();
            ++this.exportCount;
        }

        boolean var2 = false;
        boolean var12 = false;

        try {
            var12 = true;
            super.exportObject(var1);
            var2 = true;
            var12 = false;
        } finally {
            if (var12) {
                if (!var2) {
                    synchronized(this) {
                        this.decrementExportCount();
                    }
                }

            }
        }

        if (!var2) {
            synchronized(this) {
                this.decrementExportCount();
            }
        }

    }
```

到这里，就已将`RegistryImpl`对象创建并且起了服务等待客户端的请求。

### 客户端获取服务端Registry代理

```java
IHelloService helloService = (IHelloService) Naming.lookup("rmi://localhost/Hello");
```

从以上代码追溯到`LocateRegistry`的`getRegistry()`方法。这个方法做的事是通过传入的`host`和`port`构造`RemoteRef`对象，并创建了一个本地代理。这个代理对象其实是`RegistryImpl_Stub`对象。这样客户端便有了服务端的`RegistryImpl`的代理（取决于`ignoreStubClasses`变量）。但注意此时这个代理其实还没有和服务端的`RegistryImpl`对象关联，毕竟是两个VM上的对象，代理和远程`Registry`对象之间是通过socket消息来完成的。

```java
//Naming.java
public static Remote lookup(String name)
        throws NotBoundException,
            java.net.MalformedURLException,
            RemoteException
    {
        ParsedNamingURL parsed = parseURL(name);
        Registry registry = getRegistry(parsed);

        if (parsed.name == null)
            return registry;
        return registry.lookup(parsed.name);
    }
    
//调用
private static Registry getRegistry(ParsedNamingURL parsed)
        throws RemoteException
    {
        return LocateRegistry.getRegistry(parsed.host, parsed.port);
    }

//调用
public static Registry getRegistry(String host, int port)
        throws RemoteException
    {
        return getRegistry(host, port, null);
    }

//调用
public static Registry getRegistry(String host, int port,
                                       RMIClientSocketFactory csf)
        throws RemoteException
    {
    	//获取仓库地址
        Registry registry = null;

        if (port <= 0)
            port = Registry.REGISTRY_PORT;

        if (host == null || host.length() == 0) {
            // If host is blank (as returned by "file:" URL in 1.0.2 used in
            // java.rmi.Naming), try to convert to real local host name so
            // that the RegistryImpl's checkAccess will not fail.
            try {
                host = java.net.InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e) {
                // If that failed, at least try "" (localhost) anyway...
                host = "";
            }
        }

        /*
         * Create a proxy for the registry with the given host, port, and
         * client socket factory.  If the supplied client socket factory is
         * null, then the ref type is a UnicastRef, otherwise the ref type
         * is a UnicastRef2.  If the property
         * java.rmi.server.ignoreStubClasses is true, then the proxy
         * returned is an instance of a dynamic proxy class that implements
         * the Registry interface; otherwise the proxy returned is an
         * instance of the pregenerated stub class for RegistryImpl.
         **/
         //与TCP通信的类
        LiveRef liveRef =
            new LiveRef(new ObjID(ObjID.REGISTRY_ID),
                        new TCPEndpoint(host, port, csf, null),
                        false);
        RemoteRef ref =
            (csf == null) ? new UnicastRef(liveRef) : new UnicastRef2(liveRef);
		//创建远程代理类，并引用liveref，好让动态代理时能进行TCP通信
        return (Registry) Util.createProxy(RegistryImpl.class, ref, false);
    }
```

调用`RegistryImpl_Stub`的ref（`RemoteRef`）对象的`newCall()`方法，将`RegistryImpl_Stub`对象传进去，构造对象的时已将服务器的主机、端口等信息传了进去。`newCall()`做的简单来看就是建立了与远程`RegistryImpl`的`Skeleton`对象的连接（服务端通过`TCPTransport`的`exportObject()`方法等待客户端的请求）

```java
//Naming.java
public static Remote lookup(String name)
        throws NotBoundException,
            java.net.MalformedURLException,
            RemoteException
    {
        ParsedNamingURL parsed = parseURL(name);
        //调用RegistryImpl_Stu实现
        Registry registry = getRegistry(parsed);

        if (parsed.name == null)
            return registry;
        return registry.lookup(parsed.name);
    }
    
//RegistryImpl_Stu.java
//Registry接口实现
public Remote lookup(String var1) throws AccessException, NotBoundException, RemoteException {
        try {
            //调用UnicastRef实现
            RemoteCall var2 = super.ref.newCall(this, operations, 2, 4905912898345647071L);

            try {
                ObjectOutput var3 = var2.getOutputStream();
                var3.writeObject(var1);
            } catch (IOException var18) {
                throw new MarshalException("error marshalling arguments", var18);
            }

            super.ref.invoke(var2);

            Remote var23;
            try {
                ObjectInput var6 = var2.getInputStream();
                var23 = (Remote)var6.readObject();
            } catch (IOException var15) {
                throw new UnmarshalException("error unmarshalling return", var15);
            } catch (ClassNotFoundException var16) {
                throw new UnmarshalException("error unmarshalling return", var16);
            } finally {
                super.ref.done(var2);
            }

            return var23;
        } catch (RuntimeException var19) {
            throw var19;
        } catch (RemoteException var20) {
            throw var20;
        } catch (NotBoundException var21) {
            throw var21;
        } catch (Exception var22) {
            throw new UnexpectedException("undeclared checked exception", var22);
        }
    }
    
//UnicastRef.java
//RemoteRef接口实现
	public RemoteCall newCall(RemoteObject var1, Operation[] var2, int var3, long var4) throws RemoteException {
        clientRefLog.log(Log.BRIEF, "get connection");
        Connection var6 = this.ref.getChannel().newConnection();

        try {
            clientRefLog.log(Log.VERBOSE, "create call context");
            if (clientCallLog.isLoggable(Log.VERBOSE)) {
                this.logClientCall(var1, var2[var3]);
            }

            StreamRemoteCall var7 = new StreamRemoteCall(var6, this.ref.getObjID(), var3, var4);

            try {
                this.marshalCustomCallData(var7.getOutputStream());
            } catch (IOException var9) {
                throw new MarshalException("error marshaling custom call data");
            }

            return var7;
        } catch (RemoteException var10) {
            this.ref.getChannel().free(var6, false);
            throw var10;
        }
    }
```

客户端拥有的是`Registry`对象的代理，而不是真正的位于服务端的`Registry`对象本身，它们位于不同的虚拟机实例之中，无法直接调用，必然是通过消息进行交互的。通过`super.ref.invoke()`追溯到`StreamRemoteCall`的`executeCall()`方法。看似本地调用，但其实很容易从代码中看出来是通过TCP连接发送消息到服务端，由服务端解析并处理调用。

```java
//RegistryImpl_Stu.java
public Remote lookup(String var1) throws AccessException, NotBoundException, RemoteException {
        try {
            RemoteCall var2 = super.ref.newCall(this, operations, 2, 4905912898345647071L);

            try {
                ObjectOutput var3 = var2.getOutputStream();
                var3.writeObject(var1);
            } catch (IOException var18) {
                throw new MarshalException("error marshalling arguments", var18);
            }
            //调用UnicastRef实现
            super.ref.invoke(var2);

            Remote var23;
            try {
                ObjectInput var6 = var2.getInputStream();
                var23 = (Remote)var6.readObject();
            } catch (IOException var15) {
                throw new UnmarshalException("error unmarshalling return", var15);
            } catch (ClassNotFoundException var16) {
                throw new UnmarshalException("error unmarshalling return", var16);
            } finally {
                super.ref.done(var2);
            }

            return var23;
        } catch (RuntimeException var19) {
            throw var19;
        } catch (RemoteException var20) {
            throw var20;
        } catch (NotBoundException var21) {
            throw var21;
        } catch (Exception var22) {
            throw new UnexpectedException("undeclared checked exception", var22);
        }
    }
 
//UnicastRef.java
//RemoteRef接口实现
public void invoke(RemoteCall var1) throws Exception {
        try {
            clientRefLog.log(Log.VERBOSE, "execute call");
            //调用StreamRemoteCall实现
            var1.executeCall();
        } catch (RemoteException var3) {
            clientRefLog.log(Log.BRIEF, "exception: ", var3);
            this.free(var1, false);
            throw var3;
        } catch (Error var4) {
            clientRefLog.log(Log.BRIEF, "error: ", var4);
            this.free(var1, false);
            throw var4;
        } catch (RuntimeException var5) {
            clientRefLog.log(Log.BRIEF, "exception: ", var5);
            this.free(var1, false);
            throw var5;
        } catch (Exception var6) {
            clientRefLog.log(Log.BRIEF, "exception: ", var6);
            this.free(var1, true);
            throw var6;
        }
    }

//StreamRemoteCall.java
//RemoteCall接口实现
public void executeCall() throws Exception {
        DGCAckHandler var2 = null;

        byte var1;
        try {
            if (this.out != null) {
                var2 = this.out.getDGCAckHandler();
            }

            this.releaseOutputStream();
            DataInputStream var3 = new DataInputStream(this.conn.getInputStream());
            byte var4 = var3.readByte();
            if (var4 != 81) {
                if (Transport.transportLog.isLoggable(Log.BRIEF)) {
                    Transport.transportLog.log(Log.BRIEF, "transport return code invalid: " + var4);
                }

                throw new UnmarshalException("Transport return code invalid");
            }

            this.getInputStream();
            var1 = this.in.readByte();
            this.in.readID();
        } catch (UnmarshalException var11) {
            throw var11;
        } catch (IOException var12) {
            throw new UnmarshalException("Error unmarshaling return header", var12);
        } finally {
            if (var2 != null) {
                var2.release();
            }

        }

        switch(var1) {
        case 1:
            return;
        case 2:
            Object var14;
            try {
                var14 = this.in.readObject();
            } catch (Exception var10) {
                throw new UnmarshalException("Error unmarshaling return", var10);
            }

            if (!(var14 instanceof Exception)) {
                throw new UnmarshalException("Return type not Exception");
            } else {
                this.exceptionReceivedFromServer((Exception)var14);
            }
        default:
            if (Transport.transportLog.isLoggable(Log.BRIEF)) {
                Transport.transportLog.log(Log.BRIEF, "return code invalid: " + var1);
            }

            throw new UnmarshalException("Return code invalid");
        }
    }
```

至此，客户端服务查询请求已发出。

### 服务端接收客户端服务查询请求并返回结果

```java
//UnicastServerRef.java
public Remote exportObject(Remote var1, Object var2, boolean var3) throws RemoteException {
        Class var4 = var1.getClass();

        Remote var5;
        try {
            var5 = Util.createProxy(var4, this.getClientRef(), this.forceStubUse);
        } catch (IllegalArgumentException var7) {
            throw new ExportException("remote object implements illegal remote interface", var7);
        }

        if (var5 instanceof RemoteStub) {
            this.setSkeleton(var1);
        }

        Target var6 = new Target(var1, this, var5, this.ref.getObjID(), var3);
    	//处理客户端请求
        this.ref.exportObject(var6);
        this.hashToMethod_Map = (Map)hashToMethod_Maps.get(var4);
        return var5;
    }

//调用
//LiveRef.java
public void exportObject(Target var1) throws RemoteException {
        this.ep.exportObject(var1);
    }

//调用
//TCPEndpoint.java
public void exportObject(Target var1) throws RemoteException {
        this.transport.exportObject(var1);
    }

//TCPTransport.java
//Transport接口实现
public void exportObject(Target var1) throws RemoteException {
        this.transport.exportObject(var1);
    }

//调用
public void exportObject(Target var1) throws RemoteException {
        synchronized(this) {
            this.listen();
            ++this.exportCount;
        }

        boolean var2 = false;
        boolean var12 = false;

        try {
            var12 = true;
            super.exportObject(var1);
            var2 = true;
            var12 = false;
        } finally {
            if (var12) {
                if (!var2) {
                    synchronized(this) {
                        this.decrementExportCount();
                    }
                }

            }
        }

        if (!var2) {
            synchronized(this) {
                this.decrementExportCount();
            }
        }

    }
```

在TCP协议层发起socket监听，并采用多线程循环接收请求，通过线程池来处理socket接收到的请求

```java
//TCPTransport.java
private void listen() throws RemoteException {
        assert Thread.holdsLock(this);

        TCPEndpoint var1 = this.getEndpoint();
        int var2 = var1.getPort();
        if (this.server == null) {
            if (tcpLog.isLoggable(Log.BRIEF)) {
                tcpLog.log(Log.BRIEF, "(port " + var2 + ") create server socket");
            }

            try {
                //在TCP协议层发起socket监听，并采用多线程循环接收请求
                this.server = var1.newServerSocket();
                Thread var3 = (Thread)AccessController.doPrivileged(new NewThreadAction(new TCPTransport.AcceptLoop(this.server), "TCP Accept-" + var2, true));
                var3.start();
            } catch (BindException var4) {
                throw new ExportException("Port already in use: " + var2, var4);
            } catch (IOException var5) {
                throw new ExportException("Listen failed on port: " + var2, var5);
            }
        } else {
            SecurityManager var6 = System.getSecurityManager();
            if (var6 != null) {
                var6.checkListen(var2);
            }
        }

    }


private class AcceptLoop implements Runnable {
        private final ServerSocket serverSocket;
        private long lastExceptionTime = 0L;
        private int recentExceptionCount;

        AcceptLoop(ServerSocket var2) {
            this.serverSocket = var2;
        }

        public void run() {
            try {
                this.executeAcceptLoop();
            } finally {
                try {
                    this.serverSocket.close();
                } catch (IOException var7) {
                    ;
                }

            }

        }

        private void executeAcceptLoop() {
            if (TCPTransport.tcpLog.isLoggable(Log.BRIEF)) {
                TCPTransport.tcpLog.log(Log.BRIEF, "listening on port " + TCPTransport.this.getEndpoint().getPort());
            }

            while(true) {
                Socket var1 = null;

                try {
                    var1 = this.serverSocket.accept();
                    InetAddress var16 = var1.getInetAddress();
                    String var3 = var16 != null ? var16.getHostAddress() : "0.0.0.0";
					//通过线程池来处理socket接收到的请求
                    try {
                        TCPTransport.connectionThreadPool.execute(TCPTransport.this.new ConnectionHandler(var1, var3));
                    } catch (RejectedExecutionException var11) {
                        TCPTransport.closeSocket(var1);
                        TCPTransport.tcpLog.log(Log.BRIEF, "rejected connection from " + var3);
                    }
                } catch (Throwable var15) {
                    Throwable var2 = var15;

                    try {
                        if (this.serverSocket.isClosed()) {
                            return;
                        }

                        try {
                            if (TCPTransport.tcpLog.isLoggable(Level.WARNING)) {
                                TCPTransport.tcpLog.log(Level.WARNING, "accept loop for " + this.serverSocket + " throws", var2);
                            }
                        } catch (Throwable var13) {
                            ;
                        }
                    } finally {
                        if (var1 != null) {
                            TCPTransport.closeSocket(var1);
                        }

                    }

                    if (!(var15 instanceof SecurityException)) {
                        try {
                            TCPEndpoint.shedConnectionCaches();
                        } catch (Throwable var12) {
                            ;
                        }
                    }

                    if (!(var15 instanceof Exception) && !(var15 instanceof OutOfMemoryError) && !(var15 instanceof NoClassDefFoundError)) {
                        if (var15 instanceof Error) {
                            throw (Error)var15;
                        }

                        throw new UndeclaredThrowableException(var15);
                    }

                    if (!this.continueAfterAcceptFailure(var15)) {
                        return;
                    }
                }
            }
        }

        private boolean continueAfterAcceptFailure(Throwable var1) {
            RMIFailureHandler var2 = RMISocketFactory.getFailureHandler();
            if (var2 != null) {
                return var2.failure((Exception)(var1 instanceof Exception ? (Exception)var1 : new InvocationTargetException(var1)));
            } else {
                this.throttleLoopOnException();
                return true;
            }
        }

        private void throttleLoopOnException() {
            long var1 = System.currentTimeMillis();
            if (this.lastExceptionTime != 0L && var1 - this.lastExceptionTime <= 5000L) {
                if (++this.recentExceptionCount >= 10) {
                    try {
                        Thread.sleep(10000L);
                    } catch (InterruptedException var4) {
                        ;
                    }
                }
            } else {
                this.lastExceptionTime = var1;
                this.recentExceptionCount = 0;
            }

        }
    }
```

```java
public void run() {
            Thread var1 = Thread.currentThread();
            String var2 = var1.getName();

            try {
                var1.setName("RMI TCP Connection(" + TCPTransport.connectionCount.incrementAndGet() + ")-" + this.remoteHost);
                AccessController.doPrivileged(() -> {
                    //判断协议（猜测）
                    this.run0();
                    return null;
                }, TCPTransport.NOPERMS_ACC);
            } finally {
                var1.setName(var2);
            }

        }

//调用
private void run0() {
            TCPEndpoint var1 = TCPTransport.this.getEndpoint();
            int var2 = var1.getPort();
            TCPTransport.threadConnectionHandler.set(this);

            try {
                this.socket.setTcpNoDelay(true);
            } catch (Exception var31) {
                ;
            }

            try {
                if (TCPTransport.connectionReadTimeout > 0) {
                    this.socket.setSoTimeout(TCPTransport.connectionReadTimeout);
                }
            } catch (Exception var30) {
                ;
            }

            try {
                InputStream var3 = this.socket.getInputStream();
                Object var4 = var3.markSupported() ? var3 : new BufferedInputStream(var3);
                ((InputStream)var4).mark(4);
                DataInputStream var5 = new DataInputStream((InputStream)var4);
                int var6 = var5.readInt();
                if (var6 == 1347375956) {
                    TCPTransport.tcpLog.log(Log.BRIEF, "decoding HTTP-wrapped call");
                    ((InputStream)var4).reset();

                    try {
                        this.socket = new HttpReceiveSocket(this.socket, (InputStream)var4, (OutputStream)null);
                        this.remoteHost = "0.0.0.0";
                        var3 = this.socket.getInputStream();
                        var4 = new BufferedInputStream(var3);
                        var5 = new DataInputStream((InputStream)var4);
                        var6 = var5.readInt();
                    } catch (IOException var29) {
                        throw new RemoteException("Error HTTP-unwrapping call", var29);
                    }
                }

                short var7 = var5.readShort();
                if (var6 == 1246907721 && var7 == 2) {
                    OutputStream var8 = this.socket.getOutputStream();
                    BufferedOutputStream var9 = new BufferedOutputStream(var8);
                    DataOutputStream var10 = new DataOutputStream(var9);
                    int var11 = this.socket.getPort();
                    if (TCPTransport.tcpLog.isLoggable(Log.BRIEF)) {
                        TCPTransport.tcpLog.log(Log.BRIEF, "accepted socket from [" + this.remoteHost + ":" + var11 + "]");
                    }

                    byte var15 = var5.readByte();
                    TCPEndpoint var12;
                    TCPChannel var13;
                    TCPConnection var14;
                    switch(var15) {
                    case 75:
                        var10.writeByte(78);
                        if (TCPTransport.tcpLog.isLoggable(Log.VERBOSE)) {
                            TCPTransport.tcpLog.log(Log.VERBOSE, "(port " + var2 + ") " + "suggesting " + this.remoteHost + ":" + var11);
                        }

                        var10.writeUTF(this.remoteHost);
                        var10.writeInt(var11);
                        var10.flush();
                        String var16 = var5.readUTF();
                        int var17 = var5.readInt();
                        if (TCPTransport.tcpLog.isLoggable(Log.VERBOSE)) {
                            TCPTransport.tcpLog.log(Log.VERBOSE, "(port " + var2 + ") client using " + var16 + ":" + var17);
                        }

                        var12 = new TCPEndpoint(this.remoteHost, this.socket.getLocalPort(), var1.getClientSocketFactory(), var1.getServerSocketFactory());
                        var13 = new TCPChannel(TCPTransport.this, var12);
                        var14 = new TCPConnection(var13, this.socket, (InputStream)var4, var9);
                            //本例中会走到此处
                        TCPTransport.this.handleMessages(var14, true);
                        return;
                    case 76:
                            ......
                    }
                }
            }
}
    
//调用
void handleMessages(Connection var1, boolean var2) {
        int var3 = this.getEndpoint().getPort();

        try {
            DataInputStream var4 = new DataInputStream(var1.getInputStream());

            do {
                int var5 = var4.read();
                if (var5 == -1) {
                    if (tcpLog.isLoggable(Log.BRIEF)) {
                        tcpLog.log(Log.BRIEF, "(port " + var3 + ") connection closed");
                    }

                    return;
                }

                if (tcpLog.isLoggable(Log.BRIEF)) {
                    tcpLog.log(Log.BRIEF, "(port " + var3 + ") op = " + var5);
                }

                switch(var5) {
                        //最终走到此处
                case 80:
                    StreamRemoteCall var6 = new StreamRemoteCall(var1);
                    if (!this.serviceCall(var6)) {
                        return;
                    }
                    break;
                case 81:
                case 83:
                        ......
                }
            }
        }
}
```

在主要代码中，到`ObjectTable.getTarget()`为止做的事情是从socket流中获取`ObjId`，并通过`ObjId`和`Transport`对象获取`Target`对象，这里的`Target`对象已经是服务端的对象，再借由`Target`的派发器`Dispatcher`，传入参数服务实现和请求对象`RemoteCall`，将请求派发给服务端那个真正提供服务的`RegistryImpl`的`lookUp()`方法，这就是`Skeleton`移交个具体实现的过程，`Skeleton`负责底层的操作。

```java
//Transport.java
public boolean serviceCall(final RemoteCall var1) {
        try {
            ObjID var39;
            try {
                var39 = ObjID.read(var1.getInputStream());
            } catch (IOException var33) {
                throw new MarshalException("unable to read objID", var33);
            }

            Transport var40 = var39.equals(dgcID) ? null : this;
            //此处的Target为服务器对象
            Target var5 = ObjectTable.getTarget(new ObjectEndpoint(var39, var40));
            final Remote var37;
            if (var5 != null && (var37 = var5.getImpl()) != null) {
                final Dispatcher var6 = var5.getDispatcher();
                var5.incrementCallCount();

                boolean var8;
                try {
                    transportLog.log(Log.VERBOSE, "call dispatcher");
                    final AccessControlContext var7 = var5.getAccessControlContext();
                    ClassLoader var41 = var5.getContextClassLoader();
                    ClassLoader var9 = Thread.currentThread().getContextClassLoader();

                    try {
                        setContextClassLoader(var41);
                        currentTransport.set(this);

                        try {
                            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {
                                public Void run() throws IOException {
                                    Transport.this.checkAcceptPermission(var7);
                                    //Target的派发器Dispatcher
                                    var6.dispatch(var37, var1);
                                    return null;
                                }
                            }, var7);
                            return true;
                        } catch (PrivilegedActionException var31) {
                            throw (IOException)var31.getException();
                        }
                    } finally {
                        setContextClassLoader(var9);
                        currentTransport.set((Object)null);
                    }
                } catch (IOException var34) {
                    transportLog.log(Log.BRIEF, "exception thrown by dispatcher: ", var34);
                    var8 = false;
                } finally {
                    var5.decrementCallCount();
                }

                return var8;
            }
```

所以客户通过

```java
IHelloService helloService = (IHelloService) Naming.lookup("rmi://localhost/Hello");
```

先会创建一个`RegistryImpl_Stub`的代理类，通过这个代理类进行socket网络请求，将`lookup`发送到服务端，服务端通过接收到请求后，通过服务端的`RegistryImpl_Stub(Skeleton)`，执行`RegistryImpl`的`lookUp`。而服务端的`RegistryImpl`返回的就是服务端的`HelloServiceImpl`的实现类。

```java
//RegistryImpl.java
public Remote lookup(String var1) throws RemoteException, NotBoundException {
        Hashtable var2 = this.bindings;
        synchronized(this.bindings) {
            //服务端的HelloServiceImpl的实现类
            Remote var3 = (Remote)this.bindings.get(var1);
            if (var3 == null) {
                throw new NotBoundException(var1);
            } else {
                return var3;
            }
        }
    }
```

客户端获取通过`lookUp()`查询获得的客户端`HelloServiceImpl`的`Stub`对象（这一块被`Skeleton`屏蔽了，所以无法跟踪），后续的处理仍然是通过`HelloServiceImpl_Stub`代理对象通过socket网络请求到服务端，通过服务端的`HelloServiceImpl_Stub(Skeleton)`进行代理，将请求通过`Dispatcher`转发到对应的服务端方法获得结果以后再次通过socket把结果返回到客户端。

## RMI做了什么

根据上面的分析，实际上看到的应该是有两个代理类，`RegistryImpl`和`HelloServiceImpl`的代理类。

![RMI时序图](RMI时序图.png)

## 通信原理

![通信原理](通信原理.png)

在RMI Client实施正式的RMI调用前，必须通过`LocateRegistry`或者`Naming`方式到RMI注册表寻找要调用的RMI注册信息。找到RMI事务注册信息后，Client会从RMI注册表获取这个`RMI Remote Service`的`Stub`信息。这个过程成功后，RMI Client才能开始正式的调用过程。

RMI Client正式调用过程也不是由RMI Client直接访问Remote Service，而是由客户端获取的`Stub`作为RMI Client的代理访问Remote Service的代理`Skeleton`，如上图所示的顺序。也就是说真实的请求调用是在`Stub-Skeleton`之间进行的。

**`Registry`并不参与具体的`Stub-Skeleton`的调用过程**，只负责记录哪个服务名使用哪一个`Stub`，并在Remote Client询问时将这个`Stub`拿给Client（如果没有就会报错）。

