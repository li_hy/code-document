# zookeeper的产生

在分布式架构下，当服务越来越多，规模越来越大时，对应的机器数量也越来越大，单靠人工来管理和维护服务及地址的配置信息会越来越困难，单点故障的问题也开始凸显，一旦服务路由或者负载均衡器服务器宕机，依赖它的所有服务均将失效。

此时，需要一个能够动态注册和获取服务信息的地方。来统一管理服务名称和其对应的服务器列表信息，称之为服务配置中心，服务提供者在启动时，将其提供的服务名称、服务器地址注册到服务配置中心，服务消费者通过服务配置中心来获得需要调用的服务的机器列表。通过相应的负载均衡算法，选取其中一台服务器进行调用。当服务器宕机或者下线时，相应的机器需要能够动态地从服务配置中心里面移除，并通知相应的服务消费者，否则服务消费者就有可能因为调用到已经失效服务而发生错误，在这个过程中，服务消费者只有在第一次调用服务时需要查询服务配置中心，然后将查询到的信息缓存到本地，后面的调用直接使用本地缓存的服务地址列表信息，而不需要重新发起请求到服务配置中心去获取相应的服务地址列表，直到服务的地址列表有变更（机器上线或者下线）。这种无中心化的结构解决了之前负载均衡设备所导致的单点故障问题，并且大大减轻了服务配置中心的压力。

需要解决的问题：

- 协议地址维护
- 负载均衡机制
- 服务动态上下线感知

# zookeeper介绍

zookeeper 是一个开源的**分布式协调服务**，由雅虎公司创建，是google chubby 的开源实现。zookeeper 的设计目标是将哪些复杂且容易出错的分布式一致性服务封装起来，构成一个高效可靠的原语集（由若干条指令组成的，完成一定功能的一个过程），并且以一些列简单一用的接口提供给用户使用。

zookeeper是一个典型的分布式数据一致性的解决方案，分布式应用程序可以基于它实现诸如数据发布/订阅、负载均衡、命名服务、分布式协调/通知、集群管理、Master选举、分布式锁和分布式队列等功能。

zookeeper可以保证如下一致性特征：

- 顺序一致性
  - 从同一个客户端发起的事务请求，最终将会严格的按照其发起顺序被应用到zookeeper中去
- 原子性
  - 所有事务请求的处理结果在整个集群中所有机器上的应用情况是一致的，也就是说要买整个集群所有机器都成功应用了某一事务，要么都没有应用，不会出现集群中部分机器应用了该事务，而另一部分没有应用的情况。
- 单一视图（*Single System Image*）
  - 无论客户端连接的是那个zookeeper服务器，看到的服务端数据模型都是一致的。
- 可靠性
  - 一旦服务端成功的应用了一个事务，并完成对客户端的响应，那么该事务所引起的服务端状态变更将会被一直保留下来，除非有另一个事务又对其进行了变更。
- 实时性
  - zookeeper仅仅保证在一定的时间段内，客户端最终一定能够从服务端上读取到最新的数据状态。

# zookeeper设计

1. 防止单点故障

   如果要防止单点故障，势必要做集群。如果集群要满足高性能要求，必须是高性能高可以的技巧。高性能意外这个集群能够分摊客户端的请求流量，高可用意味着集群中的某一个节点宕机以后，不影响整个集群的数据和继续提高服务的可能性。

2. 节点数据一致性

   要满足一个高性能集群，每个节点都能接收到请求，并且每个阶段的数据都必须保持一致，必须要一个leader节点负责协调和数据同步操作（如果集群中没有leader节点，每个阶段都可以接收所有请求，那么集群的数据同步的复杂度非常高）。

3. 选举leader节点

   zookeeper使用基于`paxos`理论所衍生出了的ZAB协议（Zookeeper Atomic Broadcast （zookeeper原子消息广播协议））

4. leader节点和其他节点保证数据一致性

   在分布式系统中，每一个节点虽然都能够保证本身事务操作过程是成功或失败，但却无法直接获取其他分布式节点的操作结果；而zookeeper中leader节点与其他节点的数据必须是强一致的。所有当一个事务操作设计到跨节点时，就需要用到分布式事务。分布式事务的数据一致性协议有2PC协议和3PC协议。zookeeper使用的是改进的2PC协议。

## zookeeper集群

在zookeeper中，客户端会随机连接到zookeeper集群中的一个节点，如果是读请求，就直接从当前节点中读取数据，如果是写请求，那么请求会被转发给leader提交事务，然后leader会广播事务，只有超过半数节点写入成功，那么写请求就会被提交（类2PC事务）。

![zookeeper集群](zookeeper集群.png)

所有事务请求必须由一个全局唯一的服务器来协调处理，这个服务器就是leader，其他的服务器就是follower。leader把客户端的事务请求转化为一个事务proposal（提议），并把这个proposal分发给集群中的所有follower服务器。之后leader需要等待所有follower的反馈，一旦超过半数的follower进行了正确的反馈，那么leader就会再次向所有的follower发送commit消息，要求各个follower节点对前面的一个proposal进行提交。

### 集群角色

#### leader角色

leader服务器是整个zookeeper集群的核心，主要的工作任务有两项

- 事务请求的唯一调度和处理者，保证集群事务处理的顺序性
- 集群内部各服务器的调度者

#### follower角色

follower角色的主要职责是：

- 处理客户端非事务请求，转发事务请求给leader服务器
- 参与事务请求proposal的投票（需要半数以上服务器通过才能通知leader commit数据；leader发起的提案，要求follower投票）
- 参与leader选举的投票

#### observer角色

observer是zookeeper3.3开始引入的一个全新的服务器角色，从字面来理解，该角色充当了观察者的作用：观察zookeeper集群中的最新状态变化并将这些状态变化同步到observer服务器上。

observer的工作原理与follower基本一致，唯一不同在于observer不参与任何形式的投票，包括事务请求proposal的投票和leader选举的投票。

简单来说，observer服务器之提供非事务请求服务，通常用于在不影响集群事务处理能力的前提下提升集群非事务处理能力。

### 集群组成

通常zookeeper是由2n+1台server组成，每个server都知道彼此的存在。对于2n+1台server，只要有n+1台（大多数）server可用，整个系统保持可用。

一个zookeeper集群如果要对外提供可用的服务，那么集群中必须要有过半的机器正常工作并且彼此之间能够正常通信，基于这个特性，如果向搭建一个能够运行F台机器down掉的集群，那么就要部署2×F+1台服务器构成的zookeeper集群。因此3台机器构成的zookeeper集群，能够在挂断一台机器后依然正常工作。一个5台机器集群的服务，能够对2台机器挂掉的情况下进行容灾。如果一台由6台服务构成的集群，同样只能挂掉2台。因此5台和6台在容灾能力上并没有明显优势，反而增加了网络通信负担。

系统启动时，集群中的server会选举出一台server为leader，其它的就作为follower（不考虑observer角色）。

之所以要满足2n+1这个等式，是因为一个节点要成为集群中的leader，需要有超过集群过半数的节点支持，这个涉及到leader选举算法，同时也涉及到事务请求的提交投票。

## ZAB协议

ZAB（Zookeeper Atomic Broadcast）协议是为分布式协调服务zookeeper专门设计的一种支持崩溃恢复的原子广播协议。

在zookeeper中，只有依赖ZAB协议来实现分布式数据一致性，基于该协议，zookeeper实现了一种主备模式的系统架构来保持集群中各个副本之间的数据一致性。

ZAB协议包含两种基本模式，分别是：

- 崩溃恢复
- 原子广播

当整个集群在启动时或者当leader节点出现网络中断、崩溃等情况时，ZAB协议就会进入恢复模式并选举产生新的leader，当leader服务器选举出来后，并且集群中有过半的机器和该leader节点完成数据同步后（同步指的是数据同步，原来保证集群中过半的机器能够和leader服务器的数据状态保持一致），ZAB协议就会退出恢复模式。

当集群中已经有过半的follower节点完成了和leader状态同步以后，那么整个集群就进入了消息广播模式。这个时候，在leader节点正常工作时，启动一台新的服务器加入到集群，那这个服务器会直接进入数据恢复模式，和leader节点进行数据同步，同步完成后即可正常对外提供非事务请求的处理。

### 消息广播的实现原理

消息广播的过程实际上是一个简化版的二阶段提交过程

1. leader接收到消息请求后，将消息赋予一个全局唯一的64位自增id（zxid），通过zxid的大小比较即可实现严格有序这个特性。
2. leader为每个follower准备了一个FIFO队列（通过TCP协议来实现，以实现全局有序）将带有zxid的消息作为一个提案（proposal）分发给所有的follower。
3. 当follower接收到proposal后，先把proposal写到磁盘，写入成功后再向leader回复一个ACK。
4. 当leader接收到合法数量（超过半数节点）的ACK后，leader就会向这些follower发送commit命令，同时会在本地执行该消息。
5. 当follower收到消息的commit命令后，会提交该消息。

![消息广播](消息广播.png)

*leader的投票过程不需要observer的ACK，也就是observer不需要参与投票过程，但是observer必须要同步leader的数据，从而在处理请求的时候保证数据的一致性。*

### 崩溃恢复（数据恢复）

ZAB协议的这个基于原子广播协议的消息广播过程，在正常情况下是没有任何问题的，但是一旦leader节点崩溃，或者由于网络问题导致leader服务器失去了过半的follower节点的联系（leader失去过半follower节点联系可能是leader节点和follower节点之间产生了网络分区，那么此时的leader就不再是合法的leader了），那么就会进入到崩溃恢复模式。在ZAB协议中，为了保证程序的正确运行，整个恢复过程结束后需要选举出一个新的leader。

为了使leader挂了后系统能正常工作，需要解决以下两个问题：

- 已经被处理的消息不能丢失

  当leader收到合法数量follower的ACKs后，就向各个follower广播commit命令，同时也会在本地执行commit并行连接的客户端返回【成功】。但是如果在各个follower在收到commit命令前leader宕机，会导致剩下的服务器并没有执行到这条消息。

  ![崩溃恢复](崩溃恢复.png)

  leader对事务消息发起commit操作，但是该消息在follower1上执行了，但follower2还没有收到commit就已经挂了，而实际上客户端已经收到该事务消息处理成功的回执了。所以在ZAB协议下需要保证所有机器都要执行这个事务消息。

- 被丢弃的消息不能再次出现

  当leader接收到消息请求生成proposal后就挂了，其他follower并没有收到此proposal，因此经过恢复模式重新选举leader后，这条消息是被跳过的。此时，之前挂掉的leader重新启动并注册成follower后，它保留了被跳过消息的proposal状态，与整个系统的状态是不一致的，需要将其删除。

ZAB协议需要满足上面两种情况，就必须要设计一个leader选举算法：能够确保已经被leader提交的事务proposal能够提交，同时丢失已经被跳过的事务proposal。针对这个要求：

- 如果leader选举算法能够保证新选举出来的leader服务器拥有集群中所有机器最高编号（ZXID最大）的事务proposal，那么就可以保证这个新选举处理的leader一定具有已经提交的提案。因为所有提案被commit之前必须有超过半数的follower ACK，即必须有超过半数节点的服务器的事务日志上有该提案的proposal。因此只要有合法数量的节点正常工作，就必然有一个节点保存了所有被commit消息的proposal状态
- 64位zxid中的高32位是epoch编号，每经过一次leader选举产生一个新的leader，新的leader会将epoch号+1，低32位是消息计数器，每接收到一条消息，这个值+1，新leader选举后这个值重置为0。这样设计的好处在于老的leader挂了以后重启，它不会被选举为leader，因为此时它的zxid肯定小于当前新的leader。当老的leader作为follower接入新的leader后，新的leader会让它将所有的拥有旧的epoch号的未被commit的proposal清除。

### ZXID

zxid，也就是事务id。为了保证事务的顺序一致性，zookeeper采用了递增的事务id号（zxid）来标识事务。所有的提议（proposal）都在被提出的时候加上了zxid。

zxid是一个64位的数字，高32位是epoch（ZAB协议通过epoch编号来区分leader周期变化的策略）用来标识leader关系是否改变，每次一个leader被选举出来，它都会有一个新的epoch（原来的epoch+1），标识当前属于哪个leader的统治时期。低32位用于递增计数。

## leader选举

leader选举分为两个过程：

- 启动时选举
- leader崩溃时选举

### 服务器启动时leader选举

每个阶段启动的时候状态都是`LOOKING`，处于观望状态。

进行leader选举，至少需要两台服务器，以3台服务器组成的服务器集群为例。在集群初始化阶段，当有一台服务器server1启动时，本身是无法进行和完成leader选举的，当第二台服务器server2启动时，这两台服务器可以互相通信，每台服务器都试图找到leader，于是进入leader选举过程。

选举过程如下：

1. **每个server发出一个投票**。由于是初始情况，server1和server2都会将自己作为leader服务器来进行投票，每次投票会包含所推举的服务器的myid和ZXID、epoch，使用（myid，ZXID，epoch）来表示，此时server1的投票为（1，0），server2的投票为（2，0），然后各自将这个投票发给集群中的其他服务器。

2. **接收来自各个服务器的投票**。集群的每个服务器收到投票后，首先判断该投票的有效性，如检查是否是本轮投票（epoch）；是否来自LOOKING状态的服务器。

3. **处理投票**。针对每一个投票，服务器都需要将别人的投票和自己的投票进行对比，对比规则如下：

   1. **优先检查ZXID**。ZXID比较大的服务器优先作为leader。
   2. **如果ZXID相同，那么就比较myid**。myid较大的服务器作为leader服务器。

   对应server1而言，它的投票时（1，0），接到server2的投票时（2，0），首先会比较两者的ZXID，均为0，在比较myid，此时server2的myid最大，于是更新自己的投票为（2，0），然后重新投票，对于server2而言，它不需要更新自己的投票，只是再次向集群中所以服务器发出上一次投票信息。

4. **统计投票**。每次投票后，服务器都会统计投票信息，判断是否已经有过半服务器接收到相同的投票信息，对应server1、server2而言，都统计出集群中已经有两台服务器接收了（2，0）的投票信息，此时便认为已经选出的leader。

5. **改变服务器状态**。一旦确定了leader，每个服务器就会更新自己的状态，如果是follower，那么就变更为`FOLLOWING`，如果是leader，就变更为`LEADING`。

### 运行过程中的leader选举

当集群中的leader服务器出现宕机或者不可用的情况时，那么整个集群将无法对外提供服务，而是进入新一轮的leader选举，服务器运行期间的leader选举和启动时的leader选举基本过程是一致的。

1. **变更状态**。leader挂掉后，余下的非observer服务器都会将自己的服务器状态变更为`LOOKING`，然后开始进入leader选举过程。
2. **每个server会发出一个投票**。在运行期间，每个服务器上的ZXID可能不同，此时假定server1的ZXID为123，server3的ZXIS为122；在第一轮投票中，server1和server3都会投自己，产生投票（1，123），（3，122），然后各自将投票发送给集群中所以服务器。接收来自各个服务器的投票。与启动时的过程相同。
3. **处理投票**。与启动时的过程相同，此时server1将会成为leader。
4. **统计投票**。与启动时的过程相同。
5. **改变服务器的状态**。与启动时的过程相同。

# 事件机制

watcher监听机制是zookeeper中非常重要的特性，基于zookeeper上创建的节点，可以对节点绑定监听事件，比如可以监听节点数据变更、节点删除、子节点状态变更等事件，通过这个事件机制，可以给予zookeeper实现分布式锁、集群管理等功能。

watcher特性：当数据发生变化的时候，zookeeper会产生一个watcher事件，并且会发送到客户端。但是客户端只会收到一次通知。如果后续这个节点再次发生变化，那么之前设置watcher的客户端不会再次收到消息（watcher是一次性的操作）。可以通过循环监听去达到永久监听效果。

![watcher机制](watcher机制.png)

zookeeper的watcher机制主要包括客户端线程、客户端watchManager和zookeeper服务器三部分。

客户端在向zookeeper服务器注册watcher的同时，会将watcher对象存储在客户端的watchManger中。当zookeeper服务器端出发watcher事件后，会向客户端发送通知，客户端线程从watchManager中取出对于的watcher对象来执行回调逻辑。

## 如何注册事件

通过三个操作来绑定事件：`getData`，`Exists`，`getChildren`。

凡是事务类型的操作（`create`/`delete`/`setData`），都会触发监听事件。

## watcher事件类型

早zookeeper中，接口类`Watcher`用于表示一个标准的时间处理器，其定义了时间通知相关的逻辑，包含`KeeperState`和`EventType`两个枚举类，分别代表了通知状态和时间类型，同时定义了时间的回调方法`process(WatchedEvent event)`。

`KeeperState`：

- SyncConnected （3） 客户端与服务器处于连接状态
- Disconnected（0） 客户端与服务器处于断开连接状态
- Expireed（-112） 客户端会话失效，通常同时会收到`SessionExpiredException`异常
- AuthFailed（4） 权限验证失败（使用错误的scheme进行权限检查；SASL权限检查失败），通常同时收到`AuthFailedException`异常

`EventType`：

- None（-1） 客户端链接状态发生变化时（成功建立会话；端口连接；会话超时；验证失败），会收到none事件
- NodeCreated（1） 创建节点事件
- NodeDeleted（2） 删除节点事件
- NodeDataChanged（3） 节点数据发生变更事件
- NodeChildrenChanged（4） 子节点创建、删除事件

## watcher特性

- 一次性

  无论是服务端还是客户端，一旦一个watcher被触发，zookeeper都会将其从相应的存储中移除。因此在watcher的使用上需要反复注册。

- 客户端串行执行

  客户端watcher回调的过程是一个串行同步的过程，这可以保证顺序性。

- 轻量

  `WatcherEvent`是zookeeper整个watcher通知机制的最小通知单元，这个数据结构之包含三部分内容：通知状态、事件类、事件类型和节点路径。watcher通知非常简单，只会告诉客户端发生了事件，而不会说明事件的具体内容。

  客户端向服务端注册watcher的时候，并不会把客户端真实的watcher对象传递到服务端，只是在客户端请求中使用了boolean类型属性进行了标记，同时服务端也仅仅只是保存了当前连接的`ServerCnxn`对象。

# zookeeper安装配置

## zookeeper安装

### 单机模式

- 下载[安装包(http://apache.fayea.com/zookeeper/)][http://apache.fayea.com/zookeeper/]
- 使用`tar -zxvf`解压
- 将conf目录下的zoo_sample.cfg文件copy一份重命名为zoo.cfg
- 修改zoo.cfg文件中dataDir目录（存放日志文件存放的路径）

### 集群模式

zookeeper集群中，节点有三种角色，分别是：leader，follower，observer。一个集群至少需要3台zookeeper（2n+1）。

- 修改zoo.cfg文件
  - 修改端口：server.A=B:C:D。其中A是一个数字，表示这是几号服务器；B是这个服务器的IP地址；C是与集群中的leader交换信息的端口；D是选举leader的通信端口。
  - 新建datadir目录，在目录下新建myid文件，编辑文件内容为对应服务器的serverID数据。
    - 必须区别每个服务器的myid文件中的数字不同
    - 所在服务器的zoo.cfg中的server.id与myid中的值一致
    - id的范围是1～255
- 若要使用observer模式，需要增加一台zookeeper
  - 修改zoo.cfg文件
    - 增加 peerType=observer
    - 增加server.A=B:C:D:observer

## zookeeper配置文件

zookeeper的配置文件主要有`zoo.cfg`和`myid`2个文件

### zoo.cfg配置

预装的zoo.cfg下面默认有五个属性，分别是：1.tickTime，2.initLimit，3.syncLimit，4.dataDir，5.clientPort。

- tickTime
  - 通信心跳时间，zookeeper配置中的时间单位；
  - 单位是毫秒，系统默认2000；
  - 客户端与服务器或者服务器与服务器之间维持心跳，也就是每个tickTime时间就会发送一次心跳。通过心跳不仅能够用来监听机器的工作状态，还可以通过心跳来控制Flower跟Leader的通信时间。
- initLimit
  - 集群中的follower与leader之间初始连接时能容忍的最多心跳数（tickTime的数量）。
- syncLimit
  - 集群中flower与leader之间的请求和答应最多能容忍的心跳数。
- dataDir
  - 对应的目录是用来存放myid信息跟一些版本，日志，跟服务器唯一的ID信息等；
  - 默认为`/tmp/zookeeper`，若不修改每次重启服务器会清空数据。
- clientPort
  - 设置客户端连接*zookeeper*服务器的端口。
- server.A=B:C:D:E
  - 用于配置集群
  - A是数字，表示这是几号服务器；
  - B是这个服务器的IP地址；
  - C是与集群中的leader交换信息的端口，默认2888；
  - D是选举leader的通信端口，默认3888；
  - E是`observer`，表示是否是observer服务器。

### myid配置

该文件只有一行内容，对应每台服务器的Server ID 数字。

- 必须区别每个服务器的myid文件中的数字不同；
- 所在服务器的zoo.cfg中的server.id与myid中的值一致；
- id的范围是1～255。

## zookeeper常用命令

1. 启动zookeeper服务：`bin/zkServer.sh start`
2. 查看zookeeper服务状态：`bin/zkServer.sh satus`
3. 停止zookeeper服务：`bin/zkServer.sh stop`
4. 重启zookeeper服务：`bin/zkServer.sh restart`
5. 连接服务器：`bin/zkCli.sh -timeout 0 -r server ip:port`

# zookeeper编程

## zookeeper原生编码

### zookeeper连接Demo

```java
public class ConnectDemo {

    /**
     * 连接zookeeper后状态由NOT_CONNECTED(未连接)变为CONNECTING(连接中)，此时不能进行操作，
     * 需要等状态变为CONNECTED(已连接)后方可进行操作
     */
    private void connect() {
        try {
            ZooKeeper zk = new ZooKeeper("192.168.229.100:2181,192.168.229.101:2181,192.168.229.102:2181", 4000, null);
            System.out.println(zk.getState());  //CONNECTING
            Thread.sleep(2000);
            System.out.println(zk.getState());  //CONNECTED
            zk.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 基于connect存在CONNECTING状态，增加一个watcher进行监听
     * 当状态变为CONNECTED时释放线程锁进行后续操作
     */
    public void connect1(){
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            ZooKeeper zk = new ZooKeeper("192.168.229.100:2181,192.168.229.101:2181,192.168.229.102:2181", 4000, new Watcher() {
                @Override
                public void process(WatchedEvent watchedEvent) {
                    if(watchedEvent.getState()==Event.KeeperState.SyncConnected){
                        //当状态变为CONNECTED时释放线程
                        countDownLatch.countDown();
                    }
                }
            });
            countDownLatch.await();
            //System.out.println(zk.getState());
            //Thread.sleep(2000);
            System.out.println(zk.getState());
            zk.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ConnectDemo().connect();
    }

}
```

### zookeeper操作Demo

**pom**

```xml
<dependency>
     <groupId>org.apache.zookeeper</groupId>
     <artifactId>zookeeper</artifactId>
     <version>3.4.12</version>
</dependency>
```

**demo**

```java
public class OperDemo implements Watcher{

    private CountDownLatch countDownLatch;

    public OperDemo(){
        countDownLatch = new CountDownLatch(1);
    }

    public void oper(){
        try {
            ZooKeeper zk = new ZooKeeper("192.168.229.100:2181," +
                    "192.168.229.101:2181,192.168.229.102:2181", 4000, this::process);
            countDownLatch.await();
            System.out.println("连接状态：" + zk.getState());
            //增加zk-lhy节点
            //create参数(path, data, acl, createMode)
            //path:创建节点路径，需保证父节点已存在
            //data:节点数据
            //acl:权限列表，提供默认的权限OPEN_ACL_UNSAFE(完全开放)、CREATOR_ALL_ACL(创建该znode的连接拥有所有权限)、READ_ACL_UNSAFE(所有客户端都可读)
            //createMode:节点类型，PERSISTEN(持久化节点)、PERSISTENT_SEQUENTIAL(持久化有序节点)、EPHEMERAL(临时节点)、EPHEMERAL_SEQUENTIAL(临时有序节点)
            zk.create("/zk-lhy", "0".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            Thread.sleep(1000);
            zk.exists("/zk-lhy", true);
            //获取节点值；修改节点之前需要获取stat，用于保证同步
            Stat stat = new Stat();
            System.out.println("/zk-lhy原始值:" + new String(zk.getData("/zk-lhy", true, stat)));
            System.out.println("原始版本：" + stat.getVersion());
            //修改节点值
            zk.setData("/zk-lhy", "1".getBytes(), stat.getVersion());
            System.out.println("/zk-lhy修改后的值：" + new String(zk.getData("/zk-lhy", true, stat)));
            System.out.println("修改后版本：" + stat.getVersion());
            //删除节点
            zk.delete("/zk-lhy", stat.getVersion());
            zk.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void process(WatchedEvent watchedEvent) {
        if (watchedEvent.getState() == Event.KeeperState.SyncConnected) {
            //当状态变为CONNECTED时释放线程
            countDownLatch.countDown();
        }
        System.out.println(watchedEvent.getType() + "->" + watchedEvent.getPath());
    }
    public static void main(String[] args) {
        new OperDemo().oper();
    }
}
```

## Curator框架编码

Curator组件包括以下几个组件：

- `Recipes`：通用zookeeper技巧("recipes")的实现. 建立在`curator-framework`之上
- `Framework`：简化zookeeper使用的高级. 增加了很多建立在zookeeper之上的特性. 管理复杂连接处理和重试操作
- `Utilities`：各种工具类
- `Client`：zookeeper本身提供的类的替代者。负责底层的开销以及一些工具
- `Errors`：Curator怎样来处理错误和异常
- `Extensions`：`curator-recipes`包实现了通用的技巧，这些技巧在zookeeper文档中有介绍。为了避免是这个包(package)变得巨大，recipes/applications将会放入一个独立的extension包下。并使用命名规则`curator-x-name`

**pom**

```xml
<dependency>
	<groupId>org.apache.curator</groupId>
    <artifactId>curator-framework</artifactId>
    <version>4.0.0</version>
</dependency>
```

**demo**

```java
public class CuratorDemo {

    public CuratorFramework cf = null;

    public CuratorDemo() {
        //使用流式编程
        cf = CuratorFrameworkFactory.builder()
                //zookeeper服务器地址
                .connectString("192.168.229.100:2181," + "192.168.229.101:2181,192.168.229.102:2181")
                //超时时间
                .sessionTimeoutMs(4000)
                //重试策略：间隔1秒，最大重试3次
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                //命名空间，用于与zookeeper业务隔离
                .namespace("curator").build();
        cf.start();
    }

    public void create(String node, String value) {
        try {
            cf.create()
                //不存在父节点则创建父节点
                .creatingParentsIfNeeded()
                //创建节点类型，PERSISTEN(持久化节点)、PERSISTENT_SEQUENTIAL(持久化有序节点)、EPHEMERAL(临时节点)、EPHEMERAL_SEQUENTIAL(临时有序节点)
                .withMode(CreateMode.PERSISTENT)
                .forPath(node, value.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cf.close();
        }
    }

    public void modify(String node, String value) {
        try {
            Stat stat = new Stat();
            //获取节点值；获取节点状态；修改节点之前需要获取stat，用于保证同步
            byte[] nodeValue = cf.getData().storingStatIn(stat).forPath(node);
            System.out.println("节点" + node + "的初始值为：" + new String(nodeValue) + ";初始状态为：" + stat);
            //修改时需比较stat，用于保证同步
            cf.setData().withVersion(stat.getVersion()).forPath(node, value.getBytes());
            cf.getData().storingStatIn(stat).forPath(node);
            System.out.println("节点" + node + "的当前值为：" + new String(nodeValue) + ";当前状态为：" + stat);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cf.close();
        }
    }

    public void delete(String node) {
        try {
            //有子节点则先删除子节点
            cf.delete().deletingChildrenIfNeeded().forPath(node);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cf.close();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //可理会根节点直接创建子节点
        //new CuratorDemo().create("/lhy/app", "xyz");

        //new CuratorDemo().modify("/lhy/app", "xxx");
        //可理会子节点直接删除根节点
        new CuratorDemo().delete("/lhy");

    }
}
```

## zookeeper实现分布式锁

 **基于zookeeper的节点唯一性实现分布式锁**

多个客户端同时创建一个名为locker的节点，只能有一个客户端创建成功，其余客户端将监听此节点的删除事件，当节点被删除时监听的客户端将再次争抢创建节点，未争抢到的继续监听。从而实现分布式锁。

![分布式锁一](C:\Users\lhy\Documents\md\java\分布式\zookeeper\分布式锁一.png)

但此方法会产生`惊群效应（羊群效应）`，即在节点状态改变时会产生大量的事件变更发送给所有的监听客户端，在访问客户端比较多的情况下不建议使用此方法。

 **基于zookeeper临时有序节点实现的分布式锁**

每个客户端对某个功能加锁时，在zookeeper上的与该功能对应的指定节点的目录下，每个客户端生成一个唯一的临时有序节点。判断是否获取锁的方式很简单，只需要判断有序节点中序号最小的一个。而未获得锁的客户端只要监听比自己小的节点。当释放锁的时候，只需将这个临时节点删除即可。同时，其可以避免服务宕机导致的锁无法释放，而产生的死锁问题。

![分布式锁二](分布式锁二.png)

以下代码都是基于此种思想实现。

### 原生实现

需要实现`java.util.concurrent.locks.Lock`与`org.apache.zookeeper.Watcher`接口。

```java
public class DistributedLockDemo implements Lock, Watcher {

    private ZooKeeper zk = null;
    //定义根节点
    private final static String ROOT_LOCK = "/lock";
    //等待前一个锁
    private String wait_lock;
    //当前锁
    private String current_lock;

    private CountDownLatch countDownLatch;

    public DistributedLockDemo() {
        try {
            zk = new ZooKeeper("localhost:2181", 4000, this);
            //判断根节点是否存在
            Stat stat = zk.exists(ROOT_LOCK, false);
            if (stat == null) {
                //创建开放持久化节点
                zk.create(ROOT_LOCK, "0".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean tryLock() {
        try {
            //创建临时有序节点
            current_lock = zk.create(ROOT_LOCK + "/", "0".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
            System.out.println(Thread.currentThread().getName() + ":" + current_lock + ",尝试竞争锁");
            //获取根节点下的所有子节点
            List<String> childrens = zk.getChildren(ROOT_LOCK, false);
            //对子节点进行排序
            SortedSet<String> sortedSet = new TreeSet();
            for (String children : childrens) {
                sortedSet.add(ROOT_LOCK + "/" + children);
            }
            //获取当前所有子节点中最小的节点
            String firestNode = sortedSet.first();
            //获取比当前锁小的节点
            SortedSet<String> lessThenMe = ((TreeSet<String>)sortedSet).headSet(current_lock);
            //如果当前锁等于子节点中最小的节点则获得锁
            if (current_lock.equals(firestNode)) {
                return true;
            }
            if (!lessThenMe.isEmpty()) {
                //获取比当前节点小的最后一个节点，设置给wait_lock
                wait_lock = lessThenMe.last();
            }
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void lock() {
        //获得锁成功
        if (this.tryLock()) {
            System.out.println(Thread.currentThread().getName() + ":" + current_lock + "--获得锁成功");
            return;
        }
        //没有获得锁，继续等待
        waitForLock(wait_lock);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public void unlock() {
        System.out.println(Thread.currentThread().getName() + ":释放锁" + current_lock);
        try {
            zk.delete(current_lock, -1);
            current_lock = null;
            zk.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Condition newCondition() {
        return null;
    }

    @Override
    public void process(WatchedEvent watchedEvent) {
        if (this.countDownLatch != null) {
            this.countDownLatch.countDown();
        }
    }

    private boolean waitForLock(String prev) {
        try {
            //监听当前节点的上一个节点
            Stat stat = zk.exists(prev, true);
            if (stat != null) {
                System.out.println(Thread.currentThread().getName() + ":等待锁" + prev + "释放锁");
                countDownLatch = new CountDownLatch(1);
                countDownLatch.await();
                //当前线程是最小节点则获得锁
                if (nodeIsFirest()) {
                    System.out.println(Thread.currentThread().getName() + ":获得锁成功");
                }
            }
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
    
    /***
     * 判断当前节点是否是最小节点
     * @return
     * @throws KeeperException
     * @throws InterruptedException
     */
    private boolean nodeIsFirest() throws KeeperException, InterruptedException {
        //获取根节点下的所有子节点
        List<String> childrens = zk.getChildren(ROOT_LOCK, false);
        SortedSet<String> sortedSet = new TreeSet();
        for (String children : childrens) {
            sortedSet.add(ROOT_LOCK + "/" + children);
        }
        //获取当前所有子节点中最小的节点
        String firestNode = sortedSet.first();
        //获取比当前锁小的节点
        SortedSet<String> lessThenMe = ((TreeSet<String>) sortedSet).headSet(current_lock);
        //如果当前锁等于子节点中最小的节点则获得锁
        if (current_lock.equals(firestNode)) {
            return true;
        }
        return false;
    }
}
```

同时开启10个线程模拟并发

```java
public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    countDownLatch.await();
                    DistributedLockDemo demo = new DistributedLockDemo();
                    demo.lock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },"Thread-"+i).start();
            countDownLatch.countDown();
        }
        try {
            //阻塞线程，等待释放锁
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

运行结果

![原生实现分布式锁运行结果](原生实现分布式锁运行结果.png)

在zkCli中释放当前锁，可以看到后续线程会逐个获得锁

![后续线程获得锁](后续线程获得锁.png)

### Curator框架实现

`InterProcessMutex`:全局可重入锁。客户端都可以请求锁，并且同一个客户端在拥有锁的同时，可以多次获取，不会被阻塞。客户端在拥有锁的同时，可以多次获取，不会被阻塞。

`InterProcessSemaphoreMutex`：不可重入锁。和`InterProcessMutex`相比，就是少了`Reentrant`的功能，也就意味着它不能在同一个线程中重入。使用方法与``InterProcessMutex``类似。

`InterProcessReadWriteLock`：可重入读写锁。类似JDK的`ReentrantReadWriteLock`. 一个读写锁管理一对相关的锁。 一个负责读操作，另外一个负责写操作。读操作在写锁没被使用时可同时由多个进程使用，而写锁使用时不允许读 (阻塞)。 此锁是可重入的。一个拥有写锁的线程可重入读锁，但是读锁却不能进入写锁。 这也意味着写锁可以降级成读锁， 比如请求写锁 —>读锁 —->释放写锁。从读锁升级成写锁是不成的。

以下使用`InterProcessMutex`实现。

**pom**

```xml
<dependency>
	<groupId>org.apache.curator</groupId>
	<artifactId>curator-recipes</artifactId>
	<version>4.0.0</version>
</dependency>
```

**demo**

```java
public class CuratorLockDemo {
    //定义根节点
    private final static String ROOT_LOCK = "/curatorLock";
    //zookeeper服务器地址
    private final static String ZOOKEEPER_PATH = "localhost:2181";

    private CuratorFramework client;
    private InterProcessMutex lock;


    public CuratorLockDemo() {
        client = CuratorFrameworkFactory.builder().connectString(ZOOKEEPER_PATH).sessionTimeoutMs(40000).retryPolicy(new ExponentialBackoffRetry(1000, 3)).namespace("curator").build();
        client.start();

        lock = new InterProcessMutex(client, ROOT_LOCK);
    }

    public void lock() {
        try {
            lock.acquire();

            System.out.println(Thread.currentThread().getName() + "获得锁:");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unlock() {
        try {
            lock.release();
            System.out.println(Thread.currentThread().getName() + "释放锁" + client.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CountDownLatch down = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    down.await();
                    System.out.println(Thread.currentThread().getName() + "开始获取锁");
                    CuratorLockDemo demo = new CuratorLockDemo();
                    demo.lock();
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    demo.unlock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "Thread" + i).start();
            down.countDown();
        }
    }
}
```

## zookeeper实现注册中心

服务端将服务注册到registrys节点下，客户端从registrys节点下获取fuw对应的服务地址，根据不同的算法选择某一个地址进行业务通信。

![注册中心](注册中心.png)

### 服务端

**zookeeper相关类**

```java
//存储zookeeper配置信息
public class ZkConfig {

    //zookeeper服务器地址
    public final static String CONNECTION_STR = "192.168.229.100:2181";

    //zookeeper目录
    public final static String ZK_REGISTER_PATH = "/registrys";
}
```

```java
//注册中心接口
public interface IRegisterCenter {

    /**
     * 注册服务名称和服务地址
     * @param serviceName
     * @param serviceAddress
     */
    void register(String serviceName, String serviceAddress);
}
```

```java
//注册中心实现
public class RegisterCenterImpl implements IRegisterCenter {

    private CuratorFramework cf;

    {
        cf = CuratorFrameworkFactory.builder()
                .connectString(ZkConfig.CONNECTION_STR)
                .sessionTimeoutMs(4000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 10))
                .build();
        cf.start();
    }

    @Override
    public void register(String serviceName, String serviceAddress) {
        //注册对应的服务
        String servicePath = ZkConfig.ZK_REGISTER_PATH + "/" + serviceName;
        try {
            //判断服务节点是否存在，不存在则创建
            if (cf.checkExists().forPath(servicePath) == null) {
                cf.create().creatingParentsIfNeeded()
                        .withMode(CreateMode.PERSISTENT)
                        .forPath(servicePath, "0".getBytes());
            }

            //服务地址节点
            String addressPath = servicePath+"/"+serviceAddress;
            //增加临时服务地址节点，服务地址宕机会自动删除该节点
            String node = cf.create()
                    .withMode(CreateMode.EPHEMERAL)
                    .forPath(addressPath,"0".getBytes());
            System.out.println("服务注册成功："+node);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

**注解信息**

```java
//用于注入服务接口
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RpcAnnotion {

    /**
     * 对外发布的服务的接口地址
     * @return
     */
    Class<?> value();

    /**
     * 调用的版本
     * @return
     */
    String version() default "";
}
```

**RPC相关类

```java
//服务接口
public interface IHelloService {

    String say(String msg);
}
```

```java
//rpc请求封装类
public class RpcRequest implements Serializable{

    private static final long serialVersionUID = -4704099409329272099L;

    private String className;
    private String methodName;
    private String version;
    private Object[] parameters;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }

}
```

```java
//用于绑定服务，发布服务
public class RPCServer {

    private static final ExecutorService executor = Executors.newCachedThreadPool();

    private IRegisterCenter registerCenter;
    private String serviceAddress;

    private Map<String, Object> handlerMap = new HashMap<>();

    public RPCServer(IRegisterCenter registerCenter, String serviceAddress) {
        this.registerCenter = registerCenter;
        this.serviceAddress = serviceAddress;
    }

    /**
     * 绑定服务名称和服务对象
     *
     * @param services
     */
    public void bind(Object... services) {
        for (Object service : services) {
            //获取服务的注解信息
            RpcAnnotion annotion = service.getClass().getAnnotation(RpcAnnotion.class);
            //注解值即服务名称
            String serviceName = annotion.value().getName();
            //获取服务版本号
            String version = annotion.version();
            if (version != null && !version.equals("")) {
                serviceName = serviceName + "V" + version;
            }
            //绑定服务接口名称对应的服务
            handlerMap.put(serviceName, service);
        }
    }

    /**
     * 发布服务监听
     */
    public void publisher() {
        ServerSocket serverSocket = null;

        try {
            String[] addrs = serviceAddress.split(":");
            //启动服务监听，端口使用serviceAddress中的端口
            serverSocket = new ServerSocket(Integer.parseInt(addrs[1]));

            for (String interfaceName : handlerMap.keySet()) {
                registerCenter.register(interfaceName, serviceAddress);
                System.out.println(interfaceName + "服务注册成功，地址为：" + serviceAddress);
            }
            //循环监听
            while (true){
                //监听服务
                Socket socket = serverSocket.accept();
                //通过线程池去处理请求
                executor.execute(new ProcessHandler(socket, handlerMap));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(serverSocket!=null){
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```

```java
//处理客户端请求
public class ProcessHandler implements Runnable {

    private Socket socket;
    private Map<String, Object> handlerMap;

    public ProcessHandler(Socket socket, Map<String, Object> handlerMap) {
        this.socket = socket;
        this.handlerMap = handlerMap;
    }

    @Override
    public void run() {
        ObjectInputStream is = null;
        ObjectOutputStream oos = null;
        try {
            //获取客户端输入流
            is = new ObjectInputStream(socket.getInputStream());
            //反序列化远程传输的请求对象
            RpcRequest request = (RpcRequest) is.readObject();
            //通过反射调用本地方法
            Object result = invoke(request);

            //通过输出流将结果返回给客户的
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(result);
            oos.flush();
            oos.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 通过反射调用服务
     * @param request
     * @return
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private Object invoke(RpcRequest request) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //获取参数
        Object[] args = request.getParameters();
        //获取参数类型
        Class<?>[] types = new Class[args.length];
        for (int i = 0; i < args.length; i++) {
            types[i] = args[i].getClass();
        }
        //获取服务名称
        String serviceName = request.getClassName();
        //获取服务版本
        String version = request.getVersion();
        if (version != null && !version.equals("")) {
            serviceName += "V" + version;
        }
        //从handlerMap中根据客户端请求的地址获取相应的服务
        Object service = handlerMap.get(serviceName);
        //获取调用的服务的方法
        Method method = service.getClass().getMethod(request.getMethodName(), types);
        //反射执行方法,返回结果
        return method.invoke(service, args);
    }
}
```

**服务实现类**

```java
@RpcAnnotion(IHelloService.class)
public class HelloServiceImpl implements IHelloService {
    @Override
    public String say(String msg) {
        return "this is default node:"+msg;
    }
}

//客户端调用不同版本的实现
@RpcAnnotion(value = IHelloService.class, version = "1")
public class HelloServiceImplV1 implements IHelloService {
    @Override
    public String say(String msg) {
        return "this is version1 node:"+msg;
    }
}

//客户端调用不同版本的实现
@RpcAnnotion(value = IHelloService.class, version = "2")
public class HelloServiceImplV2 implements IHelloService{
    @Override
    public String say(String msg) {
        return "this is version2 node:"+msg;
    }
}

//服务端负载均衡模拟的实现
@RpcAnnotion(IHelloService.class)
public class HelloServiceImplLB1 implements IHelloService {
    @Override
    public String say(String msg) {
        return "this is LB1 node:"+msg;
    }
}

//服务端负载均衡模拟的实现
@RpcAnnotion(IHelloService.class)
public class HelloServiceImplLB2 implements IHelloService {
    @Override
    public String say(String msg) {
        return "this is LB2 node:"+msg;
    }
}
```

**服务端启动类**

```java
public class ServerDemo {
    public static void main(String[] args) throws IOException {
        //注册多个版本
        IHelloService service = new HelloServiceImpl();
        IHelloService service1 = new HelloServiceImplV1();
        IHelloService service2 = new HelloServiceImplV2();

        IRegisterCenter registerCenter = new RegisterCenterImpl();

        RPCServer server = new RPCServer(registerCenter, "127.0.0.1:10000");
        server.bind(service, service1, service2);
        server.publisher();
        System.in.read();

    }
}
```

```java
//模拟负载均衡，同时开启2个不同端口的服务
public class LBServerDemo1 {

    public static void main(String[] args) {
        IHelloService service = new HelloServiceImplLB1();
        IRegisterCenter registerCenter = new RegisterCenterImpl();
        RPCServer rpcServer = new RPCServer(registerCenter, "127.0.0.1:10001");
        rpcServer.bind(service);
        rpcServer.publisher();
    }
}

public class LBServerDemo2 {

    public static void main(String[] args) {
        IHelloService service = new HelloServiceImplLB2();
        IRegisterCenter registerCenter = new RegisterCenterImpl();
        RPCServer rpcServer = new RPCServer(registerCenter, "127.0.0.1:10002");
        rpcServer.bind(service);
        rpcServer.publisher();
    }
}
```



### 客户端

**zookeeper相关类**

```java
//zookeeper配置信息
public class ZkConfig {

    //zookeeper服务器地址
    public final static String CONNECTION_STR = "192.168.229.100:2181";

    //zookeeper目录
    public final static String ZK_REGISTER_PATH = "/registrys";
}
```

```java
//服务发现接口
public interface IServiceDiscovery {

    /**
     * 根据请求的服务地址,获得对应的调用地址
     * @param serviceName
     * @return
     */
    String discover(String serviceName);
}
```

```java
public class ServiceDiscoveryImpl implements IServiceDiscovery {

    //服务地址列表
    private List<String> repos = new ArrayList<>();

    //zookeeper地址
    private String address;

    private CuratorFramework cf;

    public ServiceDiscoveryImpl(String address) {
        this.address = address;

        cf = CuratorFrameworkFactory.builder()
                .connectString(address)
                .sessionTimeoutMs(4000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 10))
                .build();
        cf.start();
    }

    @Override
    public String discover(String serviceName) {
        //服务节点
        String path = ZkConfig.ZK_REGISTER_PATH + "/" + serviceName;

        try {
            repos = cf.getChildren().forPath(path);
        } catch (Exception e) {
            throw new RuntimeException("获取子节点异常");
        }
        //动态发现服务节点的变化
        registerWatcher(path);

        //负责均衡机制
        ILoadBalance lb = new RandomLoadBalance();

        //返回调用的服务地址
        return lb.selectHost(repos);
    }

    private void registerWatcher(String path) {
        //监听当前节点与子节点的变化
        PathChildrenCache childrenCache = new PathChildrenCache(cf, path, true);

        PathChildrenCacheListener listener = new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                repos = cf.getChildren().forPath(path);
            }
        };

        childrenCache.getListenable().addListener(listener);
        try {
            childrenCache.start();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("注册PathChild Watch 异常" + e);
        }
    }
}
```

**RPC相关类**

```java
//服务接口
public interface IHelloService {

    String say(String msg);
}
```

```java
//传输对象（用于组装请求的相关信息）
public class RpcRequest implements Serializable{

    private static final long serialVersionUID = -4704099409329272099L;

    private String className;
    private String methodName;
    private String version;
    private Object[] parameters;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
```

```java
//客户端远程代理
public class RpcClientProxy {

    private IServiceDiscovery serviceDiscovery;

    public RpcClientProxy(IServiceDiscovery serviceDiscovery) {
        this.serviceDiscovery = serviceDiscovery;
    }

    /**
     * 创建客户端的远程代理。通过远程代理进行访问
     *
     * @param interfaceCls
     * @param <T>
     * @return
     */
    public <T> T clientProxy(final Class<T> interfaceCls, String version) {
        //使用到了动态代理。
        return (T) Proxy.newProxyInstance(interfaceCls.getClassLoader(), new Class[]{interfaceCls}, new RemoteInvocationHandler(serviceDiscovery, version));
    }


}
```

```java
public class RemoteInvocationHandler implements InvocationHandler {
    private IServiceDiscovery serviceDiscovery;

    private String version;

    public RemoteInvocationHandler(IServiceDiscovery serviceDiscovery,String version) {
        this.serviceDiscovery=serviceDiscovery;
        this.version=version;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //组装请求
        RpcRequest request=new RpcRequest();
        request.setClassName(method.getDeclaringClass().getName());
        request.setMethodName(method.getName());
        request.setParameters(args);
        request.setVersion(version);

        //根据接口名称得到对应的服务地址
        String serviceAddress=serviceDiscovery.discover(request.getClassName());
        //通过tcp传输协议进行传输
        TCPTransport tcpTransport=new TCPTransport(serviceAddress);
        //发送请求
        return tcpTransport.send(request);
    }

}
```



```java
//TCP传输信息
public class TCPTransport {

    private String serviceAddress;

    public TCPTransport(String serviceAddress) {
        this.serviceAddress=serviceAddress;
    }

    //创建一个socket连接
    private Socket newSocket(){
        System.out.println("创建一个新的连接");
        Socket socket;
        try{
            String[] arrs=serviceAddress.split(":");
            socket=new Socket(arrs[0],Integer.parseInt(arrs[1]));
            return socket;
        }catch (Exception e){
            throw new RuntimeException("连接建立失败");
        }
    }

    public Object send(RpcRequest request){
        Socket socket=null;
        try {
            socket = newSocket();
            //获取输出流，将客户端需要调用的远程方法参数request发送给
            ObjectOutputStream outputStream=new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(request);
            outputStream.flush();
            //获取输入流，得到服务端的返回结果
            ObjectInputStream inputStream=new ObjectInputStream(socket.getInputStream());
            Object result=inputStream.readObject();
            inputStream.close();
            outputStream.close();
            return result;

        }catch (Exception e ){
            e.printStackTrace();
            throw new RuntimeException("发起远程调用异常:",e);
        }finally {
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
```

**负载均衡相关类**

```java
//负载均衡接口
public interface ILoadBalance {

    /**
     * 通过各种自定义算法选择服务，实现负载均衡
     * @param repos
     * @return
     */
    String selectHost(List<String> repos);
}
```

```java
//负载均衡抽象实现，过滤无服务和只有一个地址的情况
public abstract class AbstractLoadBalance implements ILoadBalance {

    /**
     * 过滤无服务和只有一个地址的情况
     * @param repos
     * @return
     */
    @Override
    public String selectHost(List<String> repos) {
        if(repos==null||repos.size()==0){
            return null;
        }
        if(repos.size()==1){
            return repos.get(0);
        }
        return doSelect(repos);
    }

    /**
     * 自定义算法选择服务
     * @param repos
     * @return
     */
    protected abstract String doSelect(List<String> repos);
}
```

```java
//负载均衡随机数算法
public class RandomLoadBalance extends AbstractLoadBalance {

    @Override
    protected String doSelect(List<String> repos) {
        int len = repos.size();
        Random random = new Random();
        return repos.get(random.nextInt(len));
    }
}
```

**客户端启动类**

```java
//访问不同版本的服务
public class ClientDemo {

    public static void main(String[] args) throws InterruptedException {
        IServiceDiscovery serviceDiscovery = new ServiceDiscoveryImpl(ZkConfig.CONNECTION_STR);
        RpcClientProxy rpcClientProxy = new RpcClientProxy(serviceDiscovery);

       IHelloService hello = rpcClientProxy.clientProxy(IHelloService.class, null);
        System.out.println(hello.say("lhy"));

        IHelloService helloV1 = rpcClientProxy.clientProxy(IHelloService.class, "1");
        System.out.println(helloV1.say("lhy"));

        IHelloService helloV2 = rpcClientProxy.clientProxy(IHelloService.class, "2");
        System.out.println(helloV2.say("lhy"));
    }
}
```

```java
//测试负载均衡
public class LBClientDemo {

    public static void main(String[] args) throws InterruptedException {
        IServiceDiscovery serviceDiscovery = new ServiceDiscoveryImpl(ZkConfig.CONNECTION_STR);
        RpcClientProxy rpcClientProxy = new RpcClientProxy(serviceDiscovery);

        for (int i = 0; i < 10; i++) {
            IHelloService hello = rpcClientProxy.clientProxy(IHelloService.class, null);
            System.out.println(hello.say("lhy"));
            Thread.sleep(1000);
        }
    }


}
```

***注意，以上Demo如果zookeeper、服务端、客户端都在本地的话，在客户端连接操作后断开zookeeper，zookeeper会将服务端也断开。***