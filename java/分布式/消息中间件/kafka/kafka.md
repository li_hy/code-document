# Kafka简介

## 什么是Kafka

Kafka是一款分布式消息发布订阅系统，具有高性能、高吞吐量的特点而被广泛应用于大数据传输场景。是有Linkedln公司开发，使用Scala语言编写，之后成为Apache基金会的一个顶级项目。Kafka提供了类似JMS的特性，但是在设计和实现上是完全不同的，而且也不是JMS规范的实现。

## Kafka产生的背景

Kafka作为一个消息系统，早期设计的目的是做Linkedln的活动流（Activity Stream）和运营数据处理管道（Pipeline）。活动流数据是所有的网站对用户的使用情况做分析的时候要用到的最常规的部分，活动数据包括页面的访问量（PV）、被查看内容方面的信息以及搜索内容。这种数据通常的处理方式是先把各种活动以日志的形式写入某种文件，然后周期性的对这些文件进行统计分析。运营数据指的是服务器的性能数据（CPU、IO使用率、请求时间、服务日志等）。

## Kafka的应用场景

由于Kafka具有更好的吞吐量、内置分区、冗余及容错性的优点（kafka每秒可以处理几十万消息），让Kafka成为了一个很好的大规模消息处理应用的解决方案。

在企业级应用上，主要会应用于如下几个方面：

- 行为追踪

  Kafka可以用于跟踪用户浏览页面、搜索及其他行为。通过发布-订阅模式实时记录到对应的topic中，通过后端大数据平台接入处理分析，并做更进一步的实时处理和监控。

- 日志收集

  日志收集方面，有很多比较优秀的产品，比如Apache Flume。很多公司是一Kafka代理日志聚合。日志聚合表示从服务器上收集日志文件，然后放到一个集中的平台（文件服务器）进行处理。在实际应用开发中，应用程序的log都会输出到本地的磁盘上，排查问题的话通过Linux命令来查询，如果应用程序组成了负载均衡集群，并且集群的机器有几十台以上，你们想通过日志快速定位到问题就是很麻烦的事了。所有一般都会做一个日志统一收集平台管理log日志用来快速查询重要应用的问题。所有很多公司都是把应用日志集中到Kafka上，然后分别导入到`elasticsearch`和`HDFS`上，用来做实时检索分析和离线统计数据备份等。而且Kafka本身又提供了很好的API来集成日志并做日志收集。

![1544685296155](日志收集.png)

# Kafka架构

一个典型的Kafka集群包含若干`Producer`（可以是应用节点产生的消息，也可以是通过`Flume`收集日志产生的事件），若干个`broker`（Kafka支持水平扩展）、若干个`Consumer Group`，以及一个`zookeeper`集群。Kafka通过`zookeeper`管理集群配置及服务协同。

`Producer`使用push模式将消息发布到`broker`，`consumer`通过监听使用pull模式从`broker`订阅并消费消息。

多个`broker`协同工作，`producer`和`consumer`部署在各个产业逻辑中。三者通过`zookeeper`管理协调请求和转发。这样就组成了一个高性能的分布式消息发布和订阅系统。

![架构](架构.png)

Kafka与其他MQ中间件不同的是，`producer`发送消息到`broker`的过程是push，而`consumer`从`broker`消费消息的过程是pull，主动去拉数据，而不是`broker`把数据主动发送给`consumer`。

# Kafka的安装部署

## 安装

在[官网][http://kafka.apache.org/]下载压缩包，直接解压即可。需要JDK环境。

## Kafka目录结构

- `/bin` 操作Kafka的可执行脚本

- `/bin/window` windows系统下Kafka的可执行文件

  注意：在windows下启动Kafka可能会报”找不到或无法加载主类“的错误。解决方法是在Kafka安装目录中找到`bin\windows`目录中的`kafka-run-class.bat`找到

  ```bash
  set COMMAND=%JAVA% %KAFKA_HEAP_OPTS% %KAFKA_JVM_PERFORMANCE_OPTS% %KAFKA_JMX_OPTS% %KAFKA_LOG4J_OPTS% -cp %CLASSPATH% %KAFKA_OPTS% %*
  ```

  修改为

  ```bash
  set COMMAND=%JAVA% %KAFKA_HEAP_OPTS% %KAFKA_JVM_PERFORMANCE_OPTS% %KAFKA_JMX_OPTS% %KAFKA_LOG4J_OPTS% -cp "%CLASSPATH%" %KAFKA_OPTS% %*
  ```

  即给`%CLASSPATH%`加上引号。

- `/config` 配置文件

- `/libs` 依赖库目录

- `/logs` 日志数据目录

## 启动/停止Kafka

- 需要先启动`zookeeper`，如果没有搭建`zookeeper`环境，可以直接运行Kafka内嵌的`zookeeper`

  启动命令：

  ```bash
  bin/zookeeper-server-start.sh config/zookeeper.properties &
  ```

- 启动Kafka

  ```bash
  bin/kafka-server-start.sh ｛-daemon (后台启动)｝ config/server.properties &
  ```

- 停止Kafka

  ```bash
  bin/kafka-server-stop.sh config/server.properties
  ```

## Kafka的基本操作

### 创建topic

```bash
./kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
```

`replication-factor`表示该topic需要在不同的broker中保存几份，这里设置成1，表示在两个broker中保持两份。

`partitions`分区数

### 查看topic

```bash
./kafka-topics.sh --list --zookeeper localhost:2181
```

### 查看topic属性

```bash
./kafka-topics.sh --describe --zookeeper localhost:2181 --topic test
```

### 发送消息

```bash
./kafka-console-producer.sh --broker-list localhost:9092 --topic test
```

### 消费消息

```bash
./kafka-console-consumer.sh –bootstrap-server localhost:9092 --topic test --from-beginning
```

## 安装集群环境

修改`server.properties`配置：

- 修改`server'properties.broker.id=0`每个broker的id不可重复

- 修改`server.properties` 修改为本机IP

  ```properties
  advertised.listeners=PLAINTEXT://192.168.11.153:9092
  ```

  当Kafka broker启动时，会在ZK上注册自己的IP和端口号，客户端就通过这个IP和端口号来连接。

## Kafka JAVA Demo

### 发送端Demo

```java
public class KafkaProducerDemo extends Thread {

    private final KafkaProducer<Integer, String> producer;
    private final String topic;
    private final boolean isAysnc;

    public KafkaProducerDemo(String topic, boolean isAysnc) {
        this.topic = topic;
        this.isAysnc = isAysnc;

        Properties properties = new Properties();
        //建立与kafka集群连接，可多个。格式为ip1:port1,ip2:port2,...
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //客户端编号，用于追踪请求源
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaProducerDemo");
        //生产者发送消息后的确认类型，0：不等待确认；1：等待leader写到local log
        properties.put(ProducerConfig.ACKS_CONFIG, "-1");
        //key的序列化实现类
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.IntegerSerializer");
        //value的序列化实现类
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<Integer, String>(properties);
    }

    @Override
    public void run() {
        int num = 0;
        while (num < 20) {
            String message = "message_" + num;
            System.out.println("send message:" + message);

            if (isAysnc) {  //异步发送
                producer.send(new ProducerRecord<Integer, String>(topic, message), new Callback() {

                    //回调方法
                    @Override
                    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                        if (recordMetadata != null) {
                            System.out.println("async-offset:" + recordMetadata.offset()
                                    + ";partition" + recordMetadata.partition());
                        }
                    }
                });
            } else {    //同步发送
                try {
                    RecordMetadata recordMetadata = producer.send(new ProducerRecord<Integer, String>(topic, message)).get();
                    System.out.println("sync-offset:" + recordMetadata.offset()
                            + ";partition:" + recordMetadata.partition());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            num++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new KafkaProducerDemo("test", false).start();
    }
}
```

### 消费端Demo

```java
public class KafkaConsumerDemo extends Thread {
    private final KafkaConsumer consumer;

    public KafkaConsumerDemo(String topic) {
        Properties properties = new Properties();
        //建立与kafka集群连接，可多个。格式为ip1:port1,ip2:port2,...
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //消费组编号，同一个消费组协调消费同一组topic
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumerDemo");
        //自动提交
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        //key的反序列化实现类
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.IntegerDeserializer");
        //value的反序列化实现类
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        //从最早的消息开始消费
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        consumer = new KafkaConsumer(properties);
        consumer.subscribe(Collections.singletonList(topic));
    }

    @Override
    public void run() {
        while (true) {
            //去brock上主动拉数据
            ConsumerRecords<Integer, String> consumerRecords = consumer.poll(Duration.ofSeconds(1));
            for (ConsumerRecord record : consumerRecords) {
                System.out.println("message recive:" + record.value());
                //consumer.commitAsync();
            }
        }
    }

    public static void main(String[] args) {
        new KafkaConsumerDemo("test").start();
    }
}
```

### 配置信息分析

#### 发送端的可选配置信息分析

##### `acks`

`acks`配置表示`producer`发送消息到`broker`上以后的确认值。有三个可选项：

- 0：表示`producer`不需要等待`broker`的消息确认。这个选项时延最小但同时风险最大（因为当server宕机时，数据将会丢失）。
- 1：表示`producer`只需要获得Kafka集群中的leader节点确认即可，这个选择时延较小同时确保了leader节点确认接收成功。
- all（-1）：需要ISR中所有的`Replica`给予接收确认，速度最慢，安全性最高，但是由于ISR可能会缩小到仅包含一个`Replica`，所有设置参数为all并不能一定避免数据丢失。

##### `batch.size`

生产者发送多个消息到`broker`上的同一个分区时，为了减少网络请求带来的性能开销，通过批量的方式来提交消息，可以通过这个参数来控制批量提交的字节数大小，默认大小是16384byte，也就是16KB，意味着当一批消息大小达到指定的`batch.size`的时候会统一发送。

##### `linger.ms`

`producer`默认会把两次发送时间间隔内收集到的所有`requests`进行一次集合然后再发送，以此提高吞吐量，而`linger.ms`就是为每次发送到`broker`的请求增加一下delay，以此来聚合更多的message请求。这个有点像TCP里面的Nagle算法（在TCP协议的传输中，为了减少大量小数据包的发送，采用了Nagle算法，也就是基于小包的等-停协议）。

`batch.size`和`linger.ms`这两个参数是Kafka性能优化的关键参数，这两者的作用是一样的，如果两个都配置了，只要满足其中一个要求，就会发送请求到`broker`上。

##### `max.request.size`

设置请求的数据的最大字节数，为了防止发生较大的数据包影响到吞吐量，默认值为1MB。

#### 消费端的可选配置分析

##### `group.id`

`consumer group`是Kafka提供的可扩展且具有容错性的消费者机制。既然是一个组，那么组内必然可以有多个消费者或消费者实例（`consumer instance`）,它们共享一个公共的ID，即`group ID`。组内的所有消费者协调在一起来消费订阅主题（`subscribed topics`）的所有分区（`partition`）。当然，每个分区只能由同一个消费组内的一个`consumer`来消费。

![group](group.png)

如图所示，分别有3个消费者，属于2个不同的`group`，那么对于`firstTopic`这个topic来说，这2组的消费者都能同时消费者更topic中的消息，对应此时的架构来说，这个`firstTopic`就类似`ActiveMQ`中的topic概念。

如果3个消费者都属于同一个`group`，那么此时`firstTopic`就是一个`queue`的概念。

##### `auto.offset.reset`

这个参数是针对新的`groupid`中的消费者而言的，当有新`groupid`的消费者来消费指定的topic时，对于该参数的配置，会有不同的语义：

- `auto.offset.reset=latest`：新的消费者将会从其他消费者最后消费的`offset`处开始消费topic下的消息；
- `auto.offset.reset=earliest`：新的消费者会从该topic最早的消息开始消费；
- `auto.offset.reset=none`：新的消费者加入后，由于之前不存在`offset`，会直接抛出异常。

##### `max.poll.records`

此色或者限制每次调用poll返回的消息数，这样可以更容易的预测每次poll间隔要处理的最大值。通过调整此值，可以减少poll间隔。

# Kafka原理

## Topic & Partition

### Topic

在Kafka中，`topic`是一个存储消息的逻辑概念，可以认为是一个消息集合。每条消息发送到Kafka集群的消息都有一个类别。物理上来说，不同的`topic`的消息是分开存储的，每个`topic`可以有多个生产者向它发送消息，也可以有多个消费者去消费其中的消息。

![topic示例](topic示例.png)

### Partition

每个`topic`可以划分多个分区（每个`topic`至少有一个分区），同一`topic`下的不同分区包含的消息是不同的。每个消息在被添加到分区时，都会被分配一个`offset`（偏移量），它是消息在此分区中的唯一编号，Kafka通过`offset`保证消息在分区内的顺序，`offset`的顺序不跨分区，即Kafka只保证在同一个分区内的消息是有序的。

![partition示例](partition示例.png)

图中对于名为`test`的`topic`做了3个分区，分别是p0，p1，p2。

每一条消息发送到`broker`时，会根据`partition`的规则选择存储到哪一个`partition`。如果`partition`规则设合理，那么所有的消息会均匀的分别在不同的`partition`中，这样就有点类似数据库的分库分表的概念，把数据做个分片处理。

**`partition`一般为`consumer`的整数倍，如此可以平衡消费消息。如果`consumer`数大于`partition`数则多出的`consumer`无法消费到消息；如果`consumer`数小于`partition`数而且不是整数倍，则有部分`consumer`将多消费多出的`partition`中的消息。**

### Topic & Partition 的存储

`partition`是以文件的形式存储在文件系统中，比如创建一个名为`firstTopic`的topic，其中有3个`partition`，那么在Kafka的数据目录（`/tmp/kafka-log`）中就有3个目录，`firstTopic-0～3`，命名规则是`<topic_name>-<partition_id>`。

创建`partition`的命令如下：

```bash
./kafka-topic.sh --create --zookeeper loclahost:2181 --replication-factor 1 --partitions 3 --topic firstTopic
```

## 消息分发原理

### Kafka消息分发策略

消息是Kafka中最基本的数据单元。在Kafka中，一条消息有key、value两部分构成，在发送一条消息时，可以指定这个key，那么producer会根据key和partition机制来判断当前这条消息应该发送并存储到哪个`partition`中。可以根据需要进行扩展producer的partition机制。

### 代码Demo

在发送端增加分片算法的实现类

```java
/**
 * 自定义分片算法
 */
public class MyPartition implements Partitioner {

    private Random random = new Random();

    /**
     * 根据自定义算法将消息存入对应的分片
     * @param topic
     * @param key
     * @param keyBytes
     * @param value
     * @param valueBytes
     * @param cluster
     * @return
     */
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        //获得分区列表
        List<PartitionInfo> partitionInfos = cluster.partitionsForTopic(topic);
        int partitonNum = 0;
        if (key == null) {
            //key为空则使用随机算法
            partitonNum = random.nextInt(partitionInfos.size());
        } else {
            //key不为空则使用取模算法
            partitonNum = Math.abs((key.hashCode()) % partitionInfos.size());
        }
        System.out.println("消息存储：【key：" + key + "|value:" + value + "|paratition:" + partitonNum);
        return partitonNum;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
```

在前面的发送端Demon的构造方法中增加自定义分片算法的配置

```java
//自定义分片算法实现类
        properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, "priv.lhy.MyPartition");
```

### 消息默认的分发机制

默认情况下，Kafka采用的是`hash取模`的分区算法。如果key为null，则会随机分配一个分区。这个随机是在这个参数`metadata.amx.age.ms`的时间范围内随机选择一个/对应这个时间段内，如果key为null，则只会发送到唯一的分区。这个值默认情况下是10分钟更新一次。

关于`metadata`，简单理解就是`topic/partition`和`broker`的映射关系。每一个`topic`的每一个`partition`，需要知道对应的`broker`列表是什么，`leader`是谁，`follower`是谁。这些信息都是存储在`metadata`这个类里面。

### 消费端如何消费指定的分区

```java
//消费指定分区的时候，不需要再订阅 
//kafkaConsumer.subscribe(Collections.singletonList(topic)); 
//消费指定的分区 
TopicPartition topicPartition = new TopicPartition(topic,0); kafkaConsumer.assign(Arrays.asList(topicPartition));
```

通过以上代码可以消费指定该topic下的0号分区，但是其他分区的数据就无法接收。

## 消息消费原理

在实际生产过程中，每个`topic`都会有多个`partition`，多个`partition`的好处在于，一方面能够对`broker`上的数据进行分片，有效减少了消息的容量，从而提升IO性能。

另一方面，为了提高消费端的消费能力，一般会通过多个consumer去消费同一个`topic`，也就是消费端的负载均衡机制。

Kafka存在`consumer group`的概念，也就是`group.id`一样的consumer，这些consumer属于一个`consumer group`，组内的所有消费者协调在一起消费订阅主题的所有分区。每一个分区也只能由同一个消费组内的consumer来消费。

![消费原理](消费原理.png)

如图所示，3个分区，3个消费者。这3个消费者会分别消费`test`这个`topic`的3个分区，也就是每个consumer消费一个`partition`。

### 分区分配策略

在Kafka中，存在两种分区分配策略，一种是`Range`（范围，默认策略），另一种是`RoundRobin`（轮询）。通过`partition.assignment.stratgy`参数来设置。

#### `Range strategy`（范围分区）

`Range`策略是对每个主题而言的，首先对同一个主题里面的分区安装序号进行排序，并对消费者按照字母顺序进行排序。假设有10个分区，3个消费者，排完序的分区是：0，1，2，3，4，5，6，7，8，9；消费者线程排完序是：C1-0，C2-0，C3-0。然后将partition的个数除以消费者线程的总数来决定每个消费者线程消费几个分区。在本例中10/3=3余1，那么消费者线程C1-0将会多消费一个分区，最后分区分配的结果是：

- C1-0 消费0，1，2，3分区
- C2-0 消费4，5，6分区
- C3-0消费7，8，9分区

假设有11个分区，那么分区分配结果是：

- C1-0 消费0，1，2，3分区
- C2-0 消费4，5，6，7分区
- C3-0 消费8，9，10分区

假设有2个主题（T1、T2），分别有10个分区，最后的分区分配结果是：

- C1-0 消费T1主题的 0，1，2，3分区以及T2主题的 0，1，2，3分区
- C2-0 消费T1主题的 4，5，6分区以及T2主题的 4，5，6分区
- C3-0 消费T1主题的 7，8，9分区以及T2主题的7，8，9分区

***可以看出C1-0消费者线程比其他消费者线程多消费了2个分区，这就是`Range stategy`的一个很明显的弊端。***

#### `RoundRobin stategy`（轮询分区）

轮询分区策略是把所有`partition`和所有consumer线程都列出来，然后按照`hashcode`进行排序。最后通过轮询算法分配`patition`给消费线程。如果所有consumer实例的订阅是相同的，那么`partition`会均匀分布。

在上面的例子中，按照`hashCode`排序完的`topic-partitions`组依次为T1-5，T1-3，T1-10，T1-8，T1-2，T1-1，T1-4，T1-7，T1-6，T1-9；消费者线程排序为C1-0，C2-0，C3-0。最后分区分配结果为：

- C1-0 消费 T1-5，T1-8，T1-4，T1-9 分区
- C2-0 消费 T1-3，T1-2，T1-7 分区
- C3-0 消费 T1-10，T1-1，T1-6 分区

使用轮询分区策略必须满足2个条件：

- 每个主题的消费者实例具有相同数量的流（`kafka-streams'中的'num.stream.threads`）
- 每个消费者订阅的主题必须是相同的

### `rebalance`机制

在出现以下3种情况时，Kafka会进行一次分区分配操作，也就是Kafka consumer的`rebalance`：

- 同一个`consumer group`内新增了消费者；
- 消费者离开当前所属的`consumer group`，比如主动停机或宕机；
- `topic`新增了分区（分区数量发生了变化）。

Kafka consumer的`reblance`机制规定了一个consumer group下的所有consumer如何达到一致来分配订阅topic的每个分区。而具体如何执行分区策略，就是上述的两种内置的分区策略。Kafka对于分配策略提供了可插拔的实现方式，也就是说，除了以上2种，可以创建自己的分配机制。

#### `coordinator`

Kafka提供`coordinator`来执行对应consumer group的管理。

当consumer group的第一个consumer启动时，会和Kafka server确定谁是group中的`coordinator`。之后该group内的所有成员都会和该`coordinator`进行协调通信。

#### 如何确定`coordinator`

消费者向Kafka集群中的任意一个`broker`发送一个`GroupCoordinatorRequest`请求，服务端会返回一个负载最小的`broker`节点的id，并将该`broker`设置为`coordinator`。

##### `JoinGroup`过程

在rebalance之前，需要保证`coordinator`是已经确定好了的，整个rebalance的过程分为2个步骤：`Join`和`Sync`。

`Join`：表示加入到`consumer group`中。

在这一步中，所有的成员都会向`coordinator`发送`JoinGroup`请求，一旦所有成员都发送了`JoinGroup`请求，那么`coordinator`会选择一个`consumer`担任`leader`角色，并把组成员信息和订阅信息发送消费者。

![join过程](join过程.png)

- `protocol_metadata`：序列化后消费者的订阅信息
- `leader_id`：消费组中的消费者，`coordinator`会选择一个作为`leader`，对应的就是`member_id`
- `member_metadata`：对应消费者的订阅信息
- `members`：`consumer group`中全部的消费者
- `generation_id`：年代信息，类似于`zookeeper`的`epoch`，对于每一轮`rebalance`，`generation_id`都会递增。主要用来保护`consumer group`，隔离无效的`offset`提交（也就是上一轮的consumer成员无法提交`offset`到新的`consumer group`中）

##### `Synchronizing Group State`阶段

完成分区分配之后，就进入了`Synchronizing Group State`阶段，主要逻辑是向`GroupCoordinator`发送`SyncGroupRequest`请求，并且处理`SyncGroupResponse`响应。简单来说，就是`leader`将消费者对应的`partition`分配方案同步给`consumer group`中所有consumer。

![sync过程](sync过程.png)

每个消费者都会向`coordinator`发送`syncgroup`请求，不过只要`leader`节点会发送分配方案。当`leader`把方案发给`coordinator`以后，`coordinator`会把结果设置到`SyncGroupResponse`中。这样所有成员都知道自己应该消费那个分区。

***`consumer group`的分区分配方案是在客户端执行的。***Kafka将这个权利下放给客户端主要是因为这样做可以有更好的灵活性。

## 消息的存储策略

### `offset`

每个`topic`可以划分多个分区（每个`topic`至少有一个分区），同一`topic`下的不同分区包含的消息是不同的。每个消息在被添加到分区时，都会被分配一个`offset`（偏移量），它是消息在此分区中的唯一编号。

Kafka通过`offset`保证消息在分区内的顺序。`offset`的顺序不跨分区，即Kafka只保证在同一个分区内的消息是有序的。

对于应用层的消费来说，每次消费一个消息并且提交以后，会保存当前消费到的最近的一个`offset`。

![offset保存](offset保存.png)

#### `offset`的维护

在Kafka中，提供了一个`__consumer_offsets_*`的一个`topic`，把`offset`信息写入到这个`topic`中。`__consumer_offsets_*`保存每个`consumer group`某一时刻提交的`offset`信息。

`__consumer_offsets_*`默认有50个分区。

#### `offset`储存消息位置的计算

用消息的`groupid`的`hashCode`与`__consumer_offsets_*`总数取模，得到的值即当前`consumer_group`的位移信息储存的位置。

```java
Math.abs("groupid".hashCode()) % groupMetadataTopi cPartitionCount;
```

可以执行以下命令查看当前`consumer_group`中的位移信息：

```bash
#0.11.0.0之后版本（含）
sh kafka-simple-consumer-shell.sh --topic __consumer_offsets --partition 35 --broker-list localhost:9092 --formatter "kafka.coordinator.group.GroupMetadataManager\$OffsetsMessageFormatter"
```

### 消息的保存路径

Kafka是使用日志文件的方式来保存生产者和发送者的消息，每条消息都有一个`offset`值来表示它在分区中的偏移量。Kafka中存储的一般都是海量的消息数据，为了避免日志文件过大，Log并不是直接对应在一个磁盘上的日志文件，而是对应磁盘上的一个目录，这个目录的命名规则是`<topic_name>_<partition_id>`。比如创建一个名为`firstTopic`的topic，其中有3个partition，那么在Kafka的数据目录（`/tmp/kafka-log`）中就有3个目录：`firstTopic-0`、`firstTopic-1`、`firstTopic-2`。

### 多个分区在集群中的分配

如果对于一个topic，在集群中创建多个partition，那么partition应如何分配：

- 将所有n个broker和代分配的i个partition排序
- 将第i个partition分配到第（i mod n）个broker上

![partition分配](partition分配.png)

### 消息写入的性能

现在大部分企业仍然使用的是机械结构的磁盘，如果把消息以随机的方式写入到磁盘，那么磁盘首先要做的就是寻址（也就是定位到数据所在的物理地址，在磁盘上就要找到对应的柱面、磁头以及对应的扇区）。这个过程相对内存来说会消耗大量时间，为了规避随机读写带来的时间消耗，Kafka采用顺序写的方式存储数据。即使这样，频繁的I/O操作仍然会造成磁盘的性能瓶颈，所有Kafka还有一个性能策略：`零拷贝`。

#### 零拷贝

消息从发送到落地保存，`broker`维护的消息日志本身就是文件目录，每个文件都是二进制保存，生产者和消费者使用相同的格式来处理。在消费者获取消息时，服务器先从硬盘读取数据到内存，然后把内存中的数据原封不动的通过socket发送给消费者。

![零拷贝1](零拷贝1.png)

操作的步骤：

- 操作系统将数据从磁盘读入到内核空间的页缓存
- 应用程序将数据从内核空间读入到用户空间缓存中
- 应用程序将数据写回到内核空间的socket缓存中
- 存储系统将数据从socket缓冲区复制到网卡缓冲区，以便将数据经网络发出

这个过程涉及到4次上下文切换以及4次数据复制，并且有2次复制操作是由CPU完成。但是这个过程中，数据完全没有进行变化，仅仅是从磁盘复制到网卡缓冲区。

通过`零拷贝`技术，可以去掉这些没必要的数据复制操作，同时又会减少上下文切换次数。现代的Unix操作系统提供一个优化的代码路径，用于将数据从页缓存传输到socket；在Linux中，是通过`sendfile`系统调用来完成的。Java提供了访问这个系统调用的方法：`FileChannel.transferTo`。

![零拷贝2](零拷贝2.png)

使用`sendfile`，只需要一次拷贝就行，允许操作系统将数据直接从页缓存发送到网络上。所有者这个优化的路径中，只有最后一步将数据拷贝到网卡缓存中是需要的。

### 消息的存储原理

在Kafka的日志存储路径下可以看到对应`partition`的日志文件

![日志文件](日志文件.png)

Kafka是通过分段的方式将Log分为多个`LogSegment`，`LogSegment`是一个逻辑上的概念，一个`LogSegment`对应磁盘上的一个日志文件和一个索引文件，其中日志文件时用力记录消息的，索引文件时用力保存消息的索引。

#### `LogSegment`

假设Kafka已`partition`为最小存储单位，那么可以想象当Kafka producer不断发送消息，比如会引起`partition`文件的无限扩张，这样对于消息文件的维护以及被消费消息的清理带来非常大的挑战，所有Kafka以`segemnt`为单位又把`partition`进行细分。每个`partition`相当于一个巨型文件被平均分配到多个大小相等的`segment`数据文件中（每个`segment`文件中的消息不一定相等），这种特性方便已经被消费消息的清理，提高磁盘的利用率。

日志文件的相关配置：

```properties
#日志文件保存路径
log.dirs=/tmp/kafka-logs

#日志分段大小（默认1G）
log.segment.bytes=1073741824

#日志文件达到多大时删除日志
log.retention.bytes=1073741824

#多长时间后删除日志（默认7天）
log.retention.hours=168

#日志清理程序检测是否有符合删除条件的日志的频率
log.retention.check.interval.ms=300000
```



#### segment中index和log的对应关系

首先将日志分段大小调小

```properties
#日志分段大小为1M
log.segment.bytes=1048576
```

批量插入数据，可看到日志分段的效果

![1545205727508](日志分段.png)

`segment file`由2大部分组成，分别为`index file`和`data file`，此2个文件一一对应，成对出现，后缀为`.index`和`.log`，分别表示为segment索引文件和数据文件。

segment文件命名规则：`partition`全局的第一个`segment`从0开始，后续每个`segment`文件名为上一个`segment`文件最后一条消息的`offset`值+1，数值最大为64位long大小，20位数字字符长度，没有数字用0填充。

可以通过以下命令查看Kafka消息日志的内容

```bash
sh kafka-run-class.sh kafka.tools.DumpLogSegments --files /tmp/kafka-logs/firstTopic-0/00000000000000000000.log --print-data-log
```

输出结果为：

![1545206351020](日志分段内容.png)

第一个log文件的最后一个`offset`为49060，所有下一个segment的文件命名为：00000000000000049061.log，对应的index为00000000000000049061。index。

Kafka为了提高查找消息的性能，为每一个日志文件添加了2个索引文件：`OffsetIndex`和`TimeIndex`，分别对应`*.index`和`*.timeindex`。

使用以下命令可以查看索引内容：

```bash
#查看index索引文件
sh kafka-run-class.sh kafka.tools.DumpLogSegments --files /tmp/kafka-logs/firstTopic-0/00000000000000000000.index --print-data-log

#查看timeindex索引文件
sh kafka-run-class.sh kafka.tools.DumpLogSegments --files /tmp/kafka-logs/firstTopic-0/00000000000000000000.timeindex --print-data-log
```



`*.index`文件内容为：

![1545206857821](index文件内容.png)

`*.timeindex`文件内容为：

![1545207006842](timeindex文件内容.png)

`timeindex`所有文件映射的是时间戳和相对`offset`。

![1545207171992](索引与日志.png)

如图所示，`index`中存储了索引以及物理偏移量；`log`存储了消息的内容；索引文件的元数据执行对应数据文件中message的物理偏移地址。

以[4053, 80899]为例，在log文件中，对应的是第4053条记录，物理偏移量（`position`）为80899。`position`是`ByteBuffer`的指针位置。

#### 在partition中如何通过offset查找message

1. 根据offset的值，查找segment段中的index索引文件。由于索引文件命名是以上一个文件的最后一个offset+1进行命名的，所以使用二分查找算法能够根据offset快速定位到指定的索引文件。
2. 找到索引文件后，根据offset进行定位，找到索引文件中的符合范围的索引（Kafka采用稀疏索引的方式来提高查找性能）。
3. 得到position以后，再到对应的log文件中，从position处开始查找offset对应的消息，将每条消息的offset与目标offset进行比较，直到找到消息。

比如要查找offset=2490这条消息，那么先找到00000000000000000000.index，然后找到[2487，49111]这个索引，再到log文件中根据49111这个position开始查找，比较每条消息的offset是否大于等于2490，最后查找到对应的消息以后返回。

#### Log文件的消息内容分析

在log文件中，一条消息会包含很多的字段

```
offset: 46241 position: 983768 CreateTime: 1545205650761 isvalid: true keysize: -1 valuesize: 13 magic: 2 compresscodec: NONE producerId: -1 producerEpoch: -1 sequence: -1 isTransactional: false headerKeys: [] payload: message_46241
```

- `offset`：消息偏移量
- `position`：物理偏移量
- `CreateTime`：创建时间
- `keysize`：key的大小
- `valuesize`：value的大小
- `compresscodec`：压缩编码
- `payload`：消息的具体内容

#### 日志的清除策略

日志的清理策略有2个：

- 根据消息的保留时间，当消息在Kafka中保持的时间超过了指导实践，就会触发清理过程
- 根据topic存储的数据大小，当topic所占的日志文件大小大于一定的阈值，则可以开始删除最旧的消息。

Kafka会启动一个后台线程，定期检查是否存在可以删除的消息。

通过`log.retention.bytes`和`log.retention.hours`这2个参数来设置，当其中任意一个达到要求，都会执行删除。

#### 日志压缩策略

Kafka通过日志压缩（Log Compaction）功能可以有效的减少日志文件的大小，缓解磁盘紧张的情况。

在很多实际场景中，消息的key和value的值之间的对应关系是不断变化的，就像数据库中的数据会不断被修改一样，消费者只关心key对应的最新的value。因此可以开启Kafka的日志压缩功能，服务端会在后台启动Cleaner线程池，定期将相同的key进行合并，只保留最新的value值。

日志的压缩原理

![1545208972863](压缩原理.png)

## `partition`的高可用副本机制

Kafka的美国`topic`都可以分为多个`partition`，并且多个`partition`会均匀分布在集群的各个节点下。虽然这种方式能够有效的对数据进行分片，但是对于每个`partition`来说，都是单点的，当其中一个`partition`不可用的时候，那么这部分消息就无法消费。所有Kafka为了提高`partition`的可靠性而提供了副本的概念（`Replica`），通过副本机制来实现冗余备份。

每个分区可以有多个副本，并且在副本集合中会存在一个leader的副本，所有的读写请求都是由leader副本来进行处理。剩余的其他副本都作为follower副本，follower副本会从leader副本同步消息日志。类似zookeeper中leader和follower的概念，但是具体的实现方式有比较大的差异。可以认为，副本集会存在一主多从的关系。

一般情况下，同一个分区的多个副本会被均匀分配到集群中的不同broker上，当leader副本所在的broker出现故障后，可以重新选举新的leader副本继续对外提供服务。通过这样的副本机制来提高Kafka集群的可用性。

### 副本分配算法

- 将所有N个broker和带分配的i个partition排序；
- 将第i个partition分配到第（i mod n）个broker上；
- 将第i个partition的第j个副本分配到第（（i+j） mod n）个broker上。

创建副本的命令

```bash
./kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 2 --partitions 3 --topic secondTopic
```

`replication-factor`参数表示创建几个副本

在`/tmp/kafka-log`路径下可以看到对于的topic的副本信息。

![1545270708584](副本示例.png)

在zookeeper上获取对应分区的信息，使用以下命令获取`secondTopic`第一分区的状态信息：

```bash
get /brokers/topics/secondTopic/partitions/1/state
```

结果为：

```json
{"controller_epoch":12, "leader":0, "version":1, "leader_epoch":0, "isr":[0,1]}
```

`leader`表示当前分区的leader是哪个`broker-id`。下图中绿色线条的表示该分区中的leader节点，其他节点就是follower。

![1545271150848](leader.png)

Kafka提供了数据复制算法，如果leader发生故障或挂掉，一个新leader会被选举并接受客户端的消息。Kafka确保从同步副本列表中选举一个副本为leader；leader负责维护和跟踪`ISR`（`in-Sync replicas`，副本同步队列）中所有follower的状态。当producer发送一条消息到broker后，leader写入消息并复制到所有follower。消息提交之后才被成功复制到所有的同步副本。

### 副本数据同步的实现

需要注意，不要把zookeeper的leader和follower的同步机制和Kafka副本的同步机制搞混。虽然从思想层面来说是一样的，但是原理层面的实现是完全不同的。

#### Kafka副本机制中的几个概念

Kafka分区下有可能有很多个副本（replica）用于实现冗余，从而进一步实现高可用。副本根据角色的不同可分为3类：

- leader副本：响应clients端读写请求的副本
- follower副本：被动的备份leader副本中的数据，不能响应clients端读写请求
- ISR副本：包含了leader副本和所有与leader副本保持同步的follower副本

每个Kafka副本对象（是所有副本，而不只是leader副本）都有2个重要的属性：LEO和HW。

- LEO：日志末端位移（`Log End Offset`），记录了该副本底层日志中***下一条消息***的位移值。也就是说，如果LEO=10，表示该副本保持了10条消息，位移范围是[0, 9]。另外，leader LEO和follower LEO的更新是有区别的。
- HW：水位值（`HighWaterMark`），对于同一个副本对象而言，其HW值不会大于LEO值。小于等于HW值的所有消息都被认为是“已备份”的（replicated）。leader副本和follower副本的HW更新是有区别的。

#### 副本协同机制

消息的读写操作都会由leader节点来接收和处理，follower副本值负责同步数据以及当leader副本所在的broker挂了以后，会从follower副本中选取新的leader。

![1545272519472](副本协同.png)

写请求首先由leader副本处理，之后follower副本会从leader上拉取写入的消息，这个过程会有一定的延迟，导致follower副本中保存的消息略少于leader副本，但是只要没有超出阈值都可以容忍。但是当一个follower副本出现异常，比如宕机、网络断开等原因长时间没有同步到消息，那这时leader就会把它剔除，Kafka通过ISR集合来维护分区副本的信息。

#### ISR

ISR表示目前可用且消息量与leader相差不多的副本集合，这时整个副本集合的一个子集。具体来说，ISR集合中的副本必须满足2个条件：

- 副本所在节点必须维持着与zookeeper的连接；

- 副本最后一条消息的offset与leader副本的最后一条消息的offset之间的差值不能超过指定的阈值（`replica.lag.time.max.ms`）。

  `replica.lag.time.max.ms`：如果该follower在此时间间隔内一直没有追上过leader的所有消息，则该follower就会被剔除ISR列表。

ISR数据保存在zookeeper的`/brokers/topics/<topic>/partitions/<partitionId>/state`节点中。

#### HW & LEO

HW和LEO这2个参数跟ISR集合紧密关联。

HW标记了一个特殊的offset，当消费者处理消息的时候，只能拉到HW之前的消息，HW之后的消息对消费者来说是不可见的。也就是说，取partition对应ISR中最小的LEO作为HW。consumer最多只能消费到HW所在的位置。每个replica都有HW，leader和follower各种维护更新自己的HW状态。一条消息只有被ISR里的所有follower都从leader复制过去才会被认为已提交，这样就避免了部分数据被写进了leader，还没来得及被任何follower复制就宕机而造成数据丢失（consumer无法消费这些数据）。而对于producer而言，可以选择是否等待消息commit，这可以通过`acks`来设置。这种机制确保了只有ISR有一个或以上的follower，一条被commit的消息就不会丢失。

#### 数据的同步过程

数据的特别过程需要解决：

- 如何传播消息；
- 在向消息发送端返回`ack`之前要保证多少个replica已经接收到这个消息。

producer在发布消息到某个partition时，先通过zookeeper找到该partition的leader，无论该topic的`replication factor`为多少（该partition有多少个replica），producer只将该消息发送到该partition的leader。leader会将该消息写入其本地log。每个follower都从leader pull数据。这种方式上，follower存储的数据顺序与leader保持一致。follower在收到该消息并写入其log后，向leader发送`ack`，一旦leader收到了ISR中的所有replica的`ack`，该消息就被认为已经commit了，leader将增加HW并向producer发送`ack`。



初始状态下，leader和follower的HW和LEO都是0，leader副本会保持remote LEO，表示所有follower LEO，也会被初始化为0，这个时候，producer没有发送消息。follower会不断的向leader发送fetch请求，但是因为没有数据，这个请求会被leader寄存，当在指定时间之后会强制完成请求，这个时间配置是`replica.fetch.wait.max.ms`，如果在指定时间内producer有消息发送过来，那么Kafka会唤醒fetch请求，让leader继续处理。

![1545274879526](消息传递初始状态.png)

这里会分两种情况

- 第一种是leader处理完producer请求之后，follower发送一个fetch请求过来；
- 第二种是follower阻塞在leader指定时间之内，leader副本收到producer的请求。

这2种情况下处理方式是不一样的。

##### follower的fetch请求是在leader处理消息以后执行的

- 生产者发送一条消息

  leader处理完producer请求之后，follower发送一个fetch请求过来，此时的状态如图所示：

  ![1545275726002](数据同步1-1.png)

  leader副本收到请求以后，会做几件事：

  - 把消息追加到log文件，同时更新leader副本的LEO。

  - 尝试更新leader的HW值。

    这个时候由于follower副本还没有发送fetch请求，那么leader的remote LEO仍然是0，leader会比较自己的LEO以及remote LEO的值，发现最小值是0，与HW的值相同，所以不会更新HW。

- follower fetch消息

  ![1545288638678](数据同步1-2.png)

  follower发送fetch请求，leader副本的处理逻辑是：

  - 读取log数据、更新remote LEO=0（follower还没有写入这条消息，这个值是根据follower的fetch请求中的offset来确定的）；
  - 尝试更新HW，因为这个时候LEO和remote LEO还是不一致的，所以仍然HW=0；
  - 把消息内容和当前分区的HW值发送给follower副本。

- follower副本收到response以后

  - 将消息写入到本地log，同时更新follower的LEO；
  - 更新follower HW，本地的LEO和leader返回的HW进行比较取小的值，所以仍然是0；

  第一次交互结束以后，HW仍然还是0，这个值会在下一次follower发起fetch请求时被更新。

  ![1545298689272](数据同步1-3.png)

- follower发第二次fetch请求，leader收到请求以后

  - 读取log数据；
  - 更新remote LEO=1，因为这次fetch携带的offset是1；
  - 更新当前分区的HW，这个时候leader LEO和remote LEO都是1，所以HW的值也更新为1；
  - 把数据和当前分区的HW值返回给follower副本，这个时候如果没有数据，则返回为空。

- follower副本收到response以后

  - 如果有数据则写本地日志，并且更新LEO；
  - 更新follower的HW值。

至此数据的同步完成，意味着消费端能够消费offset=0这条消息。

##### follower的fetch请求是直接从阻塞过程中触发

当leader副本暂时没有数据过来，follower的fetch会被阻塞，直到等待超时或者leader接收到新的数据。当leader收到请求以后会唤醒处于阻塞的fetch请求。处理过程基本与上面一致：

- leader将消息写入本地之人，更新leader的LEO
- 唤醒follower的fetch请求
- 更新HW

##### 数据丢失问题

Kafka使用HW和LEO的方式来实现副本数据的同步，本身是一个好的设计，但是在特定背景下会存在数据丢失的问题。

在`min.insync.replicas=1`（必须应答成功写入的副本最小数等于1，当且仅当`acks`参数设置为-1（需要所有副本确认）时此参数才生效）时，延迟一轮fetch RPC更新HW值的设计使得follower HW值是异步延迟更新的，倘若在这个过程中leader发送变更，那么成为新leader的follower的HW值就有可能是过期的，使得client端认为是成功提交的消息被删除。

![1545369761479](数据丢失.png)

##### 数据丢失的解决方案

在Kafka0.11.0.0版本之后，提供了一个新的解决方案，使用`leader epoch`来解决数据丢失问题，`leader epoch`时间上是一对值（epoch，offset），epoch表示leader的版本号，从0开始，当leader变更过1此时epoch就会加1，而offset则对应于该epoch版本的leader写入第一条消息的位移。比如（0，0）；（1，50），表示的一个leader从fooset=0开始写消息，一共写了50条，第二个leader版本号是1，从50条处开始写消息。这个信息保存在对应分区的本地磁盘文件中，文件名为：`/tmp/kafka-log/topic/leader-epoch-checkpoint`。

leader broker会保存一个缓存，并定期写入到一个checkpoint文件中。当leader写log时会尝试更新整个缓存（如果这个leader首次写消息，则会在缓存中增加一个条目了否则就不做更新）。而每次副本重新成为leader时会查询这部分缓存，获取出对应leader版本的offset。

![1545376674653](解决数据丢失.png)

##### 所有replica不工作的情况

在ISR中至少有一个follower是，Kafka可以确保已经commit的数据不丢失，但如果某个partition的所有replica都宕机了，就无法保证数据不丢失，此时有2种解决方案：

1. 等待ISR中任一个replica恢复，并选其作为leader；
2. 选择第一个恢复过来的replica（不一定是ISR中的）作为leader（一个broker中有多个partition的replica）。

这就需要在可用性和一致性当中做出一个简单的折中：

- 如果一定要等到ISR中的replica恢复，那不可以的时间就可能会相对较长。而且如果ISR中的所有replica都无法恢复，或者数据都丢失了，这个partition将永远不可用；
- 选择的一个恢复的replica作为leader，而这个replica不是ISR中的replica，那即是它并不保证已经包含了所有已commit的消息，它也会成为leader而作为consumer的数据源。Kafka默认就是使用这个方案。

#### ISR的设计原理

在所有的分布式存储中，冗余备份是一种常见的设计方式，而常用的模式有同步复制和异步复制。

按照Kafka这个副本模型来说：

- 如果采用同步复制，那么需要要求所有能工作的follower副本都复制完，这条消息才会被认为提交成功，一旦有一个follower副本出现故障，就会导致HW无法完成递增，消息就无法提交，消费者就获取不到消息。这种情况下，故障的follower副本就会拖慢整个系统的性能，甚至导致系统不可用；
- 如果采用异步复制，leader副本收到生产者推送的消息后，就认为此消息提交成功，follower副本则异步从leader副本同步。这种设计虽然避免了同步复制的问题，但是假设所有follower副本的同步速度都比较慢，它们保持的消息量远远落后于leader副本，而此时leader副本所在的broker突然宕机，则会重新选举新的leader副本，而新的leader副本中没有原来leader副本的消息。这将出现消息的丢失。

Kafka权衡了同步和异步的两种策略，采用ISR集合，巧妙解决了2种方案的缺陷：当follower副本延迟过高，leader副本则会把该follower副本剔除出ISR集合，消息依然可以快速提交；当leader副本所在的broker宕机，会优先将ISR集合中follower副本选举为leader，新leader副本包含了HW之前的全部消息，这样就避免了消息的丢失。

# 如何设置合适的分区数

Kafka的高性能体现在：

- 采用操作系统层面的页缓存来缓存数据；
- 日志采用顺序写入以及零拷贝的方式提升IO性能；
- partition可以水平分区，能够把一个topic拆分成多个分区；
- 发送端和消费端都可以采用并行的方式来消费分区中的消息。

Kafka的分区数并不是越大越好：

- 首先Kafka采用的是批量发送，批量发送的消息都缓存在内存当中，如果分区数过多则内存占用过高；
- 其次大量的分区需要对应数量的消费者来消费，生产环境中一般采用多线程来消费，这样会产生大量的上下文切换；
- 再次大量的分区需要打开大量的文件句柄来进行操作；
- 最后大量的分区将会产生大量的副本，如果broker相对较小的话，一个broker上将会有大量的分区leader。如果某个broker宕机，则其中的所有leader分区所对应的副本都需要重新选举leader，这也会产生大量的资源消耗。

Kafka的分区数一般根据以下情况来确定：

- 硬件资源 

  一个topic一个分区的情况下，针对目标硬件资源进行压测获取发送端和消费端的TPS值

- 消息大小

  峰值情况下的消息大小

- 目标吞吐量

  希望达到的吞吐量，用吞吐量除以压测的TPS可以得到理论的分区数和消费者数