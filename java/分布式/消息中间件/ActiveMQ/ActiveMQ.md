# 消息中间件的初步认识

## 什么是消息中间件

信息中间件是指利用高效可靠的消息传递机制进行平台无关的数据交流，并基于数据通信来进行分布式系统的集成。通过提供信息传递和消息排队模型，可以自啊分布式架构下扩展进程之间的通信。

## 消息中间件能做什么

信息中间件主要解决的就是分布式系统之间消息传递的问题，它能够屏蔽各种平台以及协议之间的特性，实现应用程序之间的协同。

以电商平台的注册功能为例，用户注册这一服务，不单单只是insert一条数据到数据库里就结束，还需要发生激活邮件、发送新人红包或积分、发生营销短信等一系列操作；假如这里面的每一个操作都需要消耗1秒，那么整个注册过程就需要耗时4秒才能响应给用户。

![注册流程](注册流程.png)

但是从注册这个服务可以看到，每一个子操作都是相对独立的，同时，基于领域划分以后，发送激活邮件、发送营销短信、赠送积分及红包否属于不同的子域。所有可以对这些字操作进行异步化执行，类似于多线程并行处理。

若使用多线程实现异步化，消息的持久化、消息的重发这些条件，多线程并不能满足所有需要借助一些开源中间件来解决。而分布式消息队列就是一个非常好的解决办法。

![分布式注册流程](分布式注册流程.png)

通过引入分布式队列，能够大大提升程序的处理效率，而且还解决了各个模块之间的耦合问题。

*这个是分布式消息队列的第一个解决场景`异步处理`。*

又比如在电商平台的秒杀场景下，流量会非常大。通过消息队列的方式可以很好的缓解高流量的问题。可以通过分布式消息队列来实现流量整形。

![秒杀](秒杀.png)

- 用户提交的请求先写入到消息队列。消息队列是有长度的，如果消息队列长度超过指定长度，直接抛弃。
- 秒杀的具体核心处理业务接收消息队列中消息进行处理，这里的消息处理能力取决于消费端本身的吞吐量。

消息中间件还有更多应用场景，比如在弱一致性事务模型中，可以采用分布式消息队列实现最大能力通知方式来实现数据的最终一致性等等。

# ActiveMQ

ActiveMQ是完全基于JMS规范实现的一个消息中间件产品。是Apache开源基金会研发的消息中间件。ActiveMQ主要应用在分布式系统架构中，帮助构建高可用、高性能、可伸缩的企业级面向消息服务的系统。

## JMS规范

JMS（Java Message Service（Java消息服务））是java平台中关于面向消息中间件的API，用于在两个应用程序之间，或者分布式系统中发送消息，进行异步通信。

 JMS是一个与具体平台无关的API，绝大多数MOM（Message Oriented Middleware（面向消息中间件））提供商都对JMS提供了支持。

JMS规范的目的是为了使得java应用程序能够访问现有MOM系统，形成一套统一的标准规范，解决不同消息中间件之间的协作问题。在创建JMS规范时，设计者希望能够结合现有的消息传送的精髓，比如：

- 不同的消息传送模式或域，如点对点消息传送和发布/订阅 消息传送
- 提供接收同步和异步消息的工具
- 对可靠消息传送的支持
- 常见消息格式，如流、文本、字节

JMS的体系结构

![JMS体系结构](JMS体系结构.png)

## ActiveMQ特性

- 多语言和协议编写客户端
  - 语言：java/C/C++/C#/Ruby/Perl/Python/PHP
  - 应用协议：openwire/stomp/REST/ws/notification/XMPP/AMQP
- 完全支持jms1.1和J2ee1.4规范
- 对spring的支持，AciveMQ可以很容易内嵌到spring模块中。

## ActiveMQ编码Demo

### 点对点模式

消息生产者demo

```java
//创建工厂
ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616");
Connection connection=null;
try {
	//创建连接
    connection = connectionFactory.createConnection();
    connection.start();
    //创建session
    Session session = connection.createSession(true,Session.AUTO_ACKNOWLEDGE);
    //创建投递队列
    Destination destination = session.createQueue("myQueue");
    //创建消息生产者
    MessageProducer producer = session.createProducer(destination);
    //创建消息
    //TextMessage message = session.createTextMessage("hello world");
    //发送消息
    //producer.send(message);
    for (int i=0;i<10;i++){
    	TextMessage message = session.createTextMessage("hello world"+i);
        producer.send(message);
    }
    session.commit();
    session.close();
} catch (JMSException e) {
    e.printStackTrace();
}finally {
    if(connection!=null){
    	try {
        	connection.close();
        } catch (JMSException e) {
             e.printStackTrace();
        }
	}
}
```

消息消费者demo

```java
ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616");
Connection connection = null;
try {
 	//创建连接
	connection = connectionFactory.createConnection();
    connection.start();
    //创建session
    Session session = connection.createSession(true,Session.AUTO_ACKNOWLEDGE);
    //创建接收队列
    Destination destination = session.createQueue("myQueue");
    //创建接收者
    MessageConsumer consumer = session.createConsumer(destination);
    //接收消息
    TextMessage message = (TextMessage) consumer.receive();
    System.out.println(message.getText());
    session.commit();
    session.close();
} catch (JMSException e) {
	e.printStackTrace();
}finally {
    if(connection!=null){
    	try {
        	connection.close();
        } catch (JMSException e) {
             e.printStackTrace();
        }
	}
}
```

上述方法在接收完消息之后就会结束，可使用listener方式监听

```java
ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616");
Connection connection = null;
try {
	connection = connectionFactory.createConnection();
    connection.start();
    final Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
	Destination destination = session.createQueue("myQueue");
    MessageConsumer consumer = session.createConsumer(destination);
    //创建消息监听
    MessageListener listener = new MessageListener() {
    	@Override
        public void onMessage(Message message) {
        	try {
            	System.out.println(((TextMessage)message).getText());
                session.commit();
             } catch (JMSException e) {
                e.printStackTrace();
             }
         }
     };
     while (true){
         consumer.setMessageListener(listener);
     }
} catch (JMSException e) {
	e.printStackTrace();
}finally {
    if(connection!=null){
         try {
              connection.close();
         } catch (JMSException e) {
              e.printStackTrace();
         }
    }
}
```



### 发布订阅模式

消息生产者

```java
ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616");
Connection connection = null;
try {
	connection = connectionFactory.createConnection();
     connection.start();
     Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
	//设置主题
     Destination destination = session.createTopic("myTopic");
     MessageProducer producer = session.createProducer(destination);
     //消息持久化，防止宕机丢失
     producer.setDeliveryMode(DeliveryMode.PERSISTENT);
     for (int i = 0; i < 10; i++) {
     	TextMessage message = session.createTextMessage("发布时间：" + new Date() + ";内容：topic测试" + i);
         producer.send(message);
     }
     session.commit();
     session.close();
} catch (JMSException e) {
     e.printStackTrace();
} finally {
     if (connection != null) {
     	try {
         	connection.close();
         } catch (JMSException e) {
             e.printStackTrace();
         }
	}
}
```

消息消费者demo

```java
ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616");
Connection connection = null;
try {
	connection = connectionFactory.createConnection();
	connection.start();
    Session session = connection.createSession(true,Session.AUTO_ACKNOWLEDGE);
    Topic destination = session.createTopic("myTopic");
    MessageConsumer consumer = session.createConsumer(destination);
    TextMessage message = (TextMessage) consumer.receive();
    System.out.println(message.getText());
    session.commit();
    session.close();
} catch (JMSException e) {
    e.printStackTrace();
}finally {
    if(connection!=null){
    	try {
         	connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
	}
}
```

## MOM

MOM是面向消息的中间件，使用消息传送提供者来协调消息传送操作。MOM需要提供API和管理工具。客户端使用API调用，把消息发送到由提供者管理的目的地。在发送消息之后，客户端会继续执行其他工作，并且在接收方收到这个消息确认之前，提供者一直保留该消息。

![MOM](MOM.png)

MOM的特点：

- 消息异步接收，发送者不需要等待消息接收者响应
- 消息可靠接收，确保消息在中间件可靠保存。只有接收方收到后才删除消息。

java消息传送服务规范最初的开发目的是为了使java应用程序能够访问现有MOM系统。引入该规范之后，已被许多现有的MOM供应商采用并且已经凭借自身的功能实现为异步消息传送系统。



## JMS基本功能

### 消息传递域

JMS规范中定义了两种消息传递域：

- 点对点（point-to-point）消息传递域
- 发布/订阅（publish/subscribe）消息传递域

类似于通过QQ聊天的时候，在群里发消息和给其中一个用户私聊消息。在群里发消息，所有群成员都能收到消息；私聊消息只有被私聊的用户能收到消息。

#### 点对点消息传递域

- 每个消息只能有一个消费者
- 消费的生产者和消费者之间没有时间上的相关性。无论消费者在生产者发送消息的时候是否处于运行状态，都可以提取消息。

![点对点](点对点.png)

#### 发布订阅消息传递域

- 每个消息可以有多个消费者
- 生产者和消费者之间有时间上的相关性。订阅一个主题的消费者只能消费自它订阅之后发布的消息。JMS规范运行客户创建持久订阅，这在一定程度上降低了时间上的相关性要求。持久订阅允许消费者消费它在未处于激活状态时发送的消息。

![发布订阅](发布订阅.png)

### 消息结构组成

JMS消息由几部分组成：消息头、属性、消息体

#### 消息头

消息头（header）包含消息的识别信息和路由信息，消息头包含一些标准的属性：

- `JMSDestination` 消息发送的目的地，queue或者topic
- `JMSDeliveryMode` 传送模式，持久模式或非持久模式
- `JMSPriority` 消息优先级（优先级分为10个级别，从0（最低）到9（最高））。如果不设定优先级，默认级别是4。需要注意的是，JMS provider并不一定保证按照优先级的顺序提交消息。
- `JMSMessageID` 唯一识别每个消息的标识

#### 属性

按类型可以分为应用设置的属性，标准属性和消息中间件定义的属性。

- 应用程序设置和添加的属性

  ```java
  //在发送端订阅消息属性
  Message.setStringProperty("key", "value");
  ```

  ```java
  //在客户端获取消息属性
  Enumeration enumeration = message.getPropertyNames(); 
  while (enumeration.hasMoreElements()) { 
      String name = enumeration.nextElement().toString(); 
      System.out.println("name:" + name + ":" + message.getStringProperty(name)); 
  }
  ```

- JMS定义的属性

  使用“JMSX”作为属性名的前缀，通过以下代码可以返回所有连接支持的JMSX属性的名字

  ```java
  Enumeration names = connection.getMetaData().getJMSXPropertyNames();
  while (names.hasMoreElements()) {
      String name = (String)names.nextElement();
      System.out.println(name);
  }
  ```

- JMS provider特定的属性

  - `JMSXUserId` 发送消息的用户身份

  - `JMSAppId`： 发送消息的应用程序的身份

  - `jmsxDeliveryCount`： 尝试发送消息的次数

  - `JMSXGroupId`： 该消息所属的消息组的身份

  - `JMSXGroupSeq`： 该消息在消息组中的序号

  - `JMSXProducerTxID`： 生成该消息的事物的身份

  - `JMSXConsumerTxID`： 使用该消息的事物的身份

  - `JMSXRcvTimestamp`： JMS 将消息发送给客户的时间

#### 消息体

即需要传递的消息内容，JMS API定义了5种消息体格式，可以使用不同形式发送接收数据，并可以兼容现有的消息格式。

| 消息格式 | 描述 |
| ------------- | ----------------------------------------------------------- |
| TextMessage   | java.lang.String 对象，如xml文件内容                        |
| MapMessage    | 名/值对的集合，名是String对象，值类型可以是java任何基本类型 |
| BytesMessage  | 字节流                                                      |
| StreamMessage | java中的输入输出流                                          |
| ObjectMessage | java中的可序列化对象                                        |
| Message       | 没有消息体，只有消息头和属性                                |

绝大部分时候，只需要基于消息体进行构造。

### 持久订阅

与QQ类似，当QQ退出后，在下次登录时仍能收到离线的消息。

持久订阅与此类似，持久订阅有两个特点：

- 持久订阅者和非持久订阅者针对的Domain是Pub/Sub，而不是P2P
- 当Broker发布消息给订阅者时，如果订阅者处于未激活状态，持久订阅者可以收到消息，而非持久订阅者则收不到消息。

当持久订阅处于未激活状态时，Broker需要为持久订阅者保存消息，如果持久订阅者订阅的消息太多则会溢出。

持久订阅时，客户端向JMS服务器注册一个自己身份的ID，当这个客户端处于离线时，JMS Provider会为这个ID保存所有发送到主题的消息，当客户再次连接到JMS Provider时，会根据自己的ID得到所有当自己处于离线时发送到主题的消息。

这份身份ID，在代码中的体现就是connection的ClientID。连接的ClientID必须是唯一的，订阅者的名称在同一个连接内必须唯一，这样才能确定连接的是订阅者。

### JMS消息的可靠性机制

理论上来说，需要保证消息中间件上的消息只有被消费者确认过以后才会被签收，相对于寄快递，手机人没有收到快递，就认为这个包裹还是属于待签收状态，这样才能保证包裹能够安全到达收件人手里。

消息的消费通常包含3个阶段：客户接收消息；客户处理消息；消息被确认。

JMS的会话可分为事务性会话和非事务性会话。

JMS Session接口提供了`commit`和`rollback`方法。事务提交意味着生产的所有消息被发送，消费的所有消息被确认；事务回滚意味着生产的所有消息被销毁，消费的所有消息被恢复并重新提交，除非已经过期。事务性的会话总是牵涉到事务处理中，`commit`或`rollbcak`方法一旦被调用，一个事务就结束了，而另一个事务被开始。关闭事务性会话将回滚其中的事务。

*在事务型会话中*，在事务状态下进行发送操作，消息并未真正投递到中间件，而只有进行session.commit操作之后，消息才会发送到中间件，再转发到适当的消费者进行处理。如果是调用rollback操作，则表明当前事务期间内所发送的消息都取消掉。通过在创建session的时候使用true or false来决定当前的会话时事务性还是非事务性。

```java
connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
```

在事务性会话中，消息的确认是自动进行，也就是通过`session.commit()`以后，消息会自动确认。

***必须保证发送端和接受度都是事务性会话。***

*在非事务性会话中*，消息何时被确认取决于创建会话时的应答模式（acknowledgement mode），有3个可选项：

- Session.AUTO_ACKNOWLEDGE

  当客户端成功的从`receive`方法返回的时候，或者从`MessageListenner.onMessage`方法成功返回的时候，会话自动确认客户收到消息。

- Session.CLIENT_ACKNOWLEDGE

  客户通过调用消息的`acknowledge`方法确认消息。

  在这种模式中，确认是在会话层上进行的，确认一个被消费的消息将自动确认所有一杯会话消费的消息。如：一个消息消费者消费了10个消息，然后确认了第五个消息，那么0～5的消息都会被确认。

- Session.DUPS_ACKNOWLEDGE

  消息延迟确认。指定消息提供者在消息接收者没有确认发送时重新发送消息，这种模式不在乎接收者收到重复的消息。

### 消息的持久化存储

消息的持久化存储也是保证可靠性最重要的机制之一，也就是消息发送到Broker上以后，如果broker出现故障宕机，那么存储在broker上的消息不应该丢失。

可以通过以下代码来设置消息发送端的持久化和非持久化特性

```java
MessageProducer producer = session.createProducer(destination);
//消息持久化
producer.setDeliveryMode(DeliveryMode.PERSISTENT);
//消息非持久化
producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
```

- 对应非持久化的消息，JMS provider不会将它存到文件/数据库等文档的存储介质中。也就是说非持久消息驻留在内存中，如果JMS provider宕机，你们内存中的非持久消息会丢失。
- 对应持久消息，消息提供者会使用存储-转发机制，先将消息存储到稳定介质中，等消息发送成功后再删除。如果JMS provider挂掉，那么这些未送达的消息不会丢失，JMS provider恢复后，会重新读取这些消息，并传送给对应的消费者。

## 持久化消息和非持久化消息的发送策略

### 消息同步发送和异步发送

ActiveMQ支持同步、异步两种发送模式将消息发送到broker上。

同步发送过程中，发送者发送一条消息会阻塞，直到broker反馈一个确认消息，表示消息已经被broker处理。这个机制提供了消息的安全性保障，但是由于是阻塞的操作，会影响到客户端消息发送的性能。

异步发送的过程中，发送者不需要等待borker提供反馈，所有性能相对较高，但是可能会出现消息丢失的情况。所以使用异步发送的前提是在某些情况下允许出现数据丢失的情况。

***默认情况下，非持久化消息是异步发送的，持久化消息并且是在非事务模式下是同步发送的。***

但是在开启事务的情况下，消息都是异步发送。由于异步发送的效率会比同步发送性能更高。***所以在发送持久化消息的时候，尽量去开启事务会话。***

除了持久化消息和非持久化消息的同步和异步特性以为，还可以通过以下几种风水来色或者异步发送：

- 在url中配置

  ```java
  ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616?jms.useAsyncSend=true");
  ```

- 使用API设置

  ```java
  ((ActiveMQConnectionFactory) connectionFactory).setUseAsyncSend(true);
  ```

  或

  ```java
  ((ActiveMQConnection)connection).setUseAsyncSend(true);
  ```


消息发送的流程图

![发送流程](发送流程.png)

*ActiveMQ发送同步或异步消息时调用的都是异步发送方法，只是同步发送时会阻塞等待返回信息。*

```java
//同步发送和异步发送的区别（源码）
public Object request(Object command, int timeout) throws IOException {
FutureResponse response = asyncRequest(command, null);
return response.getResult(timeout); // 从future方法阻塞等待返回
}
```



### ProducerWindowSize的含义

producer每发送一个消息，统计一下发送的字节数，当字节数达到`ProducerWindowSize`值时，需要等待broker的确认，才能继续发送。

主要用来约束在异步发送时producer端允许积压的（尚未ACK）的消息大小，且只对异步发送有意义。每次发送消息之后，都将会导致`memoryUsage`大小增加（+`message.size`），当broker返回`producerAck`时，`memoryUsage`尺寸减少（`producerAck.size`，此size表示先前发送消息的大小）。

可以通过以下方式设置：

- 在`brokerUrl`中设置

  ```java
  ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616?jms.producer.windowSize=xxxxxx");
  ```

  这种设置将会对所有的producer生效。

- 在`destinationUri`中设置

  ```java
  Destination destination = session.createQueue("myQueue?producer.windowSize=xxxxxx");
  ```

  此参数只会对使用此Destination实例的producer有效，将会覆盖`brokerUrl`中的`producerWindowSize`值。

***注意：`ProducerWindowSize`值越大，意味着消耗Client端的内存就越大。***

## 持久化消息和非持久化消息的存储原理

正常情况下，非持久化消息是存储在内存中的，持久化消息是存储在文件中的。能够存储的最大消息数据在`${ActiveMQ_HOME}/conf/activemq.xml`文件中的`systemUsage`节点。

```xml
<systemUsage>
    <systemUsage>
        <memoryUsage>
            <!--该子标记设置整个ActiveMQ节点的“可用内存限制”。这个值不能超过ActiveMQ本身设置的最大内存大小。其中的percentOfJvmHeap属性表示百分比。占用70%的堆内存-->
            <memoryUsage percentOfJvmHeap="70" />
        </memoryUsage>
        <storeUsage>
            <!--该标记设置整个ActiveMQ节点，用于存储“持久化消息”的“可用磁盘空间”。该子标记的limit属性必须要进行设置-->
            <storeUsage limit="100 gb"/>
        </storeUsage>
        <tempUsage>
        	<!--一旦ActiveMQ服务节点存储的消息达到了memoryUsage的限制，非持久化消息就会被转储到 temp store区域，虽说非持久化消息不进行持久化存储，但是ActiveMQ为了防止“数据洪峰”出现时非持久化消息大量堆积致使内存耗尽的情况出现，还是会将非持久化消息写入到磁盘的临时区域——temp store。这个子标记就是为了设置这个temp store区域的“可用磁盘空间限制”-->
        	<tempUsage limit="50 gb"/>
        </tempUsage>
    </systemUsage>
</systemUsage>
```

从上面的配置得到一个结论：当非持久化消息堆积到一定程度的时候（也就是内存超过指定的设置阈值时），ActiveMQ会将内存中的非持久化消息写入到临时文件，以便腾出内存。但它和持久化消息的区别是，重启之后，持久化消息会从文件中恢复，非持久化的临时文件会直接删除。

消息持久化对于可靠消息传递来说是一种比较好的方法，即使发送者和接收者不是同时在线或者消息中心在发送者发送消息后宕机了，在消息中心重启后仍然可以将消息发送出去。消息持久性的原理很简单，就是在发送消息出去后，消息中心首先将消息存储在本地文件、内存或者远程数据库，然后把消息发送给接收者，发送成功后再把消息从存储中删除，失败则继续尝试。

### 持久化存储支持类型

ActiveMQ支持多种不同的持久化方式，主要有以下几种，不过，无论使用哪种持久化方式，消息的存储逻辑都是一致的。

- `KahaDB`存储（默认存储方式）
- `JDBC`存储
- `Memory`存储
- `LevelDB`存储
- `JDBC With ActiveMQ Journal`

#### `KahaDB`存储

`KahaDB`是目前默认的存储方式，可用于任何场景，提供了性能和恢复能力。消息存储使用一个事务日志和仅仅用一个索引文件来存储它所有的地址。

`KahaDB`是一个专门针对消息持久化的解决方案，它对典型的消息使用模式进行了优化。在`KahaDB`中，数据被追加到`data logs`中。当不再需要log文件中的数据的时候，log文件会被丢弃。

**`KahaDB`的配置方式**

```xml
<persistenceAdapter>
	<kahaDB directory="${activemq.data}/kahadb"/>
</persistenceAdapter>
```

**`KahaDB`的存储原理**

在`data/kahadb`目录下，会生成四个文件

- `db.data`：消息的索引文件，本质是B-Tree，使用B-Tree作为索引指向`db-*.log`里面存储的消息
- `db.redo`：用来进行消息恢复
- `db-*.log`：存储消息内容。新的数据以APPEND的方式追加到日志文件末尾。属于顺序写入，因此消息存储是比较快的。默认是32M，达到阈值会自动递增。
- `lock`：文件锁，表示当前获得`Kahadb`读写权限的broker

#### `JDBC`存储

使用JDBC持久化方式，数据库会创建3个表：

- `activemq_msg`：消息表，queue和topic都存在这个表中
- `activemq_acks`：存储持久订阅的信息和最后一个持久订阅接收的消息ID
- `activemq_locks`：锁表，用来确保某一时刻只能有一个`ActiveMQ broker`实例来访问数据库

**JDBC存储配置**

```xml
<persistenceAdapter>
	<jdbcPersistenceAdapter dataSource="# MySQL-DS " createTablesOnStartup="true" />
</persistenceAdapter>
```

`dotaSource`指定持久化数据库的bean，`createTavlesOnStartup`指示符在启动的时候创建数据表，默认是true，这样每次启动都会去创建数据表，一般是第一次启动的时候设置为true，之后改成false。

mysql持久化bean配置

```xml
<bean id="Mysql-DS" class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close">
    <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
    <property name="url" value="jdbc:mysql://192.168.11.156:3306/activemq?
    relaxAutoCommit=true"/>
    <property name="username" value="root"/>
    <property name="password" value="root"/>
</bean>
```

需要添加jar包

![jar包](jar包.png)

#### `LevelDB`存储

`LevelDB`持久化性能高于`KahaDB`，虽然目前默认的持久化方式仍然是`KahaDB`。并且，在ActiveMQ5.9版本提供了基于`LevelDB`和`Zookeeper`的数据复制方式，用于`Master-slave`方式的首选数据复制方案。

*不过，据ActiveMQ官网对LevelDB的表述：LevelDB官方不建议使用及不再支持，推荐使用KahaDB*

**`LevelDB`存储配置**

```xml
<persistenceAdapter>
	<levelDBdirectory="activemq-data"/>
</persistenceAdapter>
```

#### `Memory`存储

基于内存的消息存储，内存消息存储主要是存储所有的非持久化的消息在内存中。`persistent="false"`表示不色或者持久化存储，直接存储到内存中。

```xml
<beans>
    <broker brokerName="test-broker" persistent="false"
    xmlns="http://activemq.apache.org/schema/core">
        <transportConnectors>
        	<transportConnector uri="tcp://localhost:61635"/>
        </transportConnectors> 
    </broker>
</beans>
```

#### `JDBC Message store with ActiveMQ Journal`

这种方式克服了`JDBC Store`的不足，JDBC每次消息都需要去写库和读库。

`ActiveMQ Journal`使用高速缓存写入技术，大大提高了性能。

当消费者的消费速度能够及时跟上生产者消息的生产速度时，journal文件能够大大减少需要写入到DB中的消息。例如：生产者生产了1000条消息，这1000条消息会保存到journal文件，如果消费者的消费速度很快的情况下，在journal文件还没有同步到DB之前，消费者已经消费了90%以上的消息，你们这个时候只需要同步剩余的10%的消息到DB。

如果消费者的消费速度很慢，这个时候journal文件可以使消息以批量方式写入到DB。

```xml
<persistenceFactory>
	<journalPersistenceAdapterFactory dataSource="#Mysql-DS" dataDirectory="activemqdata"/>
</persistenceFactory>
```

## 消费端消费

消息消费流程图

![消费流程](消费流程.png)

消费端可以用两种方法来接收消息，一种是使用同步阻塞的`MessageConsumer.receive`方法，另一种是使用消息监听器`MessageListener`。这里需要注意的是，在同一个session下，这两者不能同时工作，也就是说不能针对不同消息采用不同的接收方式，否则会抛出异常。

至于为什么这么做，最大的原因还是在事务性会话中，两种消费模式的事务不好管控。

```java
//消费端消费消息（源码）
public Message receive() throws JMSException {
    checkClosed();
    checkMessageListener(); //检查receive和MessageListener是否同时配置在当前的会话中
    sendPullCommand(0); //如果PrefetchSizeSize为0并且unconsumerMessage为空，则发起pull命令
    MessageDispatch md = dequeue(-1); //从unconsumerMessage出队列获取消息
    if (md == null) {
    return null;
    }
    beforeMessageIsConsumed(md);
    afterMessageIsConsumed(md, false); //发送ack给到broker
    return createActiveMQMessage(md);//获取消息并返回
}
```

### 消费端的`PrefetchSize`

ActiveMQ的consumer端也有窗口机制，通过`prefetchSize`就可以设置窗口大小。不同类型的队列，`prefetchSize`的默认值也是不一样的。

- 持久化队列和非持久化队列的默认值为1000
- 持久化topic默认值为100
- 非持久化队列的默认值为`Short.Max_value-1`

消费端会根据`prefetchSize`的大小批量获取数据，比如默认值是1000，你们消费端会预先加载1000条数据到本地内存中。

在ActiveMQ中可以通过一下几种方式设置`prefetchSize`大小：

- 在连接工厂连接URL里设置

  ```java
  //设置所有类型队列每次最大取50条
  ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616?jms.prefetchPolicy.all=50");
  //设置queue每次最大取50条
  ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616?jms.prefetchPolicy.queuePrefetch=50");
  //设置topic每次最大取50条
  ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616?jms.prefetchPolicy.topicPrefetch=50");
  ```

- 在创建队列时设置

  ```java
  //特定队列每次预取10条消息
  Destination destination = session.createQueue("myQueue?consumer.prefetchSize=10");
  ```

### `optimizeAcknowledge`

ActiveMQ提供了`optimizeAcknowledge`来优化确认，它表示是否开启“优化ACK”，只有在为true的情况下，`prefechSize`以及`optimizeAknowledgeTimeout`参数才会有意义。

优化确认一方面可以减轻client负担（不需要频繁的确认消息）、减少通信开销，另一方面由于延迟了确认（默认ack了`0.65*prefetchSize`个消息才确认），broker再次发送消息时又可以批量发送。

如果只是开启了`prefetchSize`，每条消息都去确认的话，broker在收到确认后也只是发送一条消息，并不是批量发布，当然有人可以通过设置`DUPS_OK_ACK`来手动延迟确认。

在`brokerUrl`指定`optimizeACK`选项

```java
ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://192.168.229.103:61616?jms.optimizeAcknowledge=true&jms.optimizeAcknowledgeTimeOut=10000");
```

**注意，如果`optimizeAcknowledge`为true，那么`prefetchSize`必须大于0，当`prefetchSize`=0的时候，表示consumer通过PULL方式从broker获取消息，broker将不会主动push消息给consumer，直到consumer发送`PullCommand`时；当`prefetchSize`>0时，就开启了broker push模式，此后只要当consumer消费且ACK了一定的消息之后，会立即push给consumer多条消息。**

`potimizeAcknowledge`和`prefetchSize`两者协同工作，通过批量获取消息、延迟批量确认，来达到一个高效的消息消费模式。它比仅减少了客户端在获取消息时的阻塞次数，还能减少每次获取消息时的网络通信开销。

需要注意的是，如果消费端的消费速度比较高，通过这两者组合是能大大提升consumer的性能。如果consumer的消费性能本身就比较慢，色或者比较大的`prefetchSize`反而不能有效的达到提升消费性能的目的。因为过大的`prefetchSize`不利于consumer端消息的负载均衡。因为通常情况下，会部署多个consumer节点来提升消费端的消费性能。

这个优化方案还会存在另一个潜在风险，当消费被消费之后还没有来得及确认时，client端发送故障，你们这些消息就有可能会被重新发送给其他consumer，你们这种风险就需要client端能够容忍“重复”消息。

### 消息的确认过程

#### `ACK_MODE`

消息确认有四种`ACK_MODE`，分别是：

- `AUTO_ACKNOWLEDGE` = 1 ：自动确认
- `CLIENT_ACKNOWLEDGE` = 2 ：客户端手动确认
- `DUPS_OK_ACKNOWLEDGE` = 3 ：自动批量确认
- `SESSION_TRANSACTED` = 0 ：事务提交并确认

虽然client端知道了ACK模式，但是在client与broker在交换ACK指令的时候，还需要告知`ACK_TYPE`，`ACK_TYPE`表示此确认指令的类型，不同的`ACK_TYPE`将传递着消息的状态，broker可以根据不同的`ACK_TYPE`对消息进行不同的操作。

#### `ACK_TYPE`

- `DELIVERED_ACK_TYPE` = 0 ：消息“已接收”，但尚未处理结束
- `STANDARD_ACK_TYPE` = 2 ：“标准”类型，通常表示为消息“处理成功”，broker端可以删除消息
- `POSION_ACK_TYPE` = 1 ：消息“错误”，通常表示“抛弃”此消息，比如消息重发多次后，都无法正确处理时，消息将会被删除或者DLQ（死信队列）
- `REDELIVERED_ACK_TYPE` = 3：消息需“重发”，比如consumer处理消息时抛出了异常，broker稍后会重新发送此消息
- `INDIVIDUAL_ACK_TYPE` = 4：表示只确认“单条消息”，无论在任何ACK_MODE下
- `UNMATCHED_ACK_TYPE` = 5：在topic中，如果一条消息在转发给“订阅者”时，发现此消·息不符合Selector过滤条件，你们此消息将不会转发给订阅者，消息将会被存储引擎删除（相当于在broker上确认了消息）。

client端在不同的ACK模式时，将意味着在不同的时机发送ACK指令，每个ACK Command中会包含ACK_TYPE，你们broker端就可以根据ACK_TYPE来决定此消息的后续操作。

### 消息的重发机制

在正常情况下，有以下几种情况会导致消息重新发送：

- 在事务性会话中，没有调用`session.commit`确认消息或调用`session.rollback`回滚消息；
- 在非事务性会话中，ACK模式为`CLIENT_ACKNOWLEDGE`的情况下，没有调用`acknowledge`或者调用了`recover`方法；

一个消息被`redelivedred`超过默认的最大重发次数（默认6次）时，消费端会给broker发送一个`poison ack`，表示这个消息有问题，告诉broker不要再发了。这个时候broker会把这个消息放到DLQ（死信队列）。

### 死信队列

ActiveMQ中默认的死信队列是ActiveMQ.DLQ，如果没有特别的配置，有毒的消息都会被发送到这个队列。默认情况下，如果持久消息过期以后，也会被送到DLQ中。

#### 死信队列配置策略

缺省所以队列的思想消息都被发送到同一个缺省死信队列，不便于管理，可以通过`individualDeadLetterStrategy`或`sharedDeadLetterStrategy`策略来进行修改

```xml
<destinationPolicy> 
    	<policyMap> 
            <policyEntries> 
                <policyEntry topic=">" > 
                    <pendingMessageLimitStrategy> 
                        <constantPendingMessageLimitStrategy limit="1000"/> 
                    </pendingMessageLimitStrategy> 
                </policyEntry> 
                <!-- “>”表示对所有队列生效，如果需要设置指定队列，则直接写队列名称 -->
                <policyEntry queue=">">
                <deadLetterStrategy> 
                    <!--queuePrefix:设置死信队列前缀 
					useQueueForQueueMessage 设置队列保存到死信。--> 
                    <individualDeadLetterStrategy queuePrefix="DLQ." useQueueForQueueMessages="true"/> 
                    </deadLetterStrategy> 
                </policyEntry> 
            </policyEntries> 
    </policyMap> 
</destinationPolicy>
```

#### 自动丢弃过期消息

```xml
<deadLetterStrategy> 
    <sharedDeadLetterStrategy processExpired="false" /> 
</deadLetterStrategy>
```

#### 死信队列的再次消费

当定位到消息不能消费的原因后，就可以在解决掉这个问题之后，再次消费死相队列中的消息。因为死信队列仍然是一个队列。

## ActiveMQ静态网络配置

### 配置说明

修改ActiveMQ服务器的activemq.xml，增加如下配置

```xml
<networkConnectors> 
    <networkConnector uri="static://(tcp://192.168.11.153:61616,tcp://192.168.11.154:61616)"/> 
</networkConnectors>
```

两个brokers通过一个static的协议来进行网络连接。一个consumer连接到broker B的一个地址上，当producer在broker A上一相同的地址发送消息时，此消息会被转移到broker B上，也就是说broker A会转发消息到broker B上。

### 消息回流

从5.6版本开始，在`destinationPolicy`上新增了一个选项`replayWhenNoConsumers`属性，这个属性可以用来解决当broker A上有需要转发的消息但是没有消费者时，把消息回流到它原始的broker。同时把`enableAudit`设置为false，为了防止消息回流后背当作重复消息而不被分发。

在activemq.xml中进如下配置，分别在两台服务器都配置，即可完成消息回流处理。

```xml
<policyEntry queue=">" enableAudit="false"> 
    <networkBridgeFilterFactory> 
        <conditionalNetworkBridgeFilterFactory replayWhenNoConsumers="true"/> 
    </networkBridgeFilterFactory> 
</policyEntry>
```

## 动态网络连接

ActiveMQ使用`Multicast`协议将一个Service和其他的broker的Service连接起来。`Multicast`能够自动发现其他broker，从而替代了使用static功能列表brokers。

配置格式如下：

```xml
<broker xmlns="http://activemq.apache.org/schema/core" brokerName="multicast" dataDirectory="${activemq.base}/data">
    <networkConnectors>
        <networkConnector name="default-nc" uri="multicast://default"/>
    </networkConnectors>
    <transportConnectors>
        <transportConnector name="openwire" uri="tcp://localhost:61616" discoveryUri="multicast://default"/>
    </transportConnectors>
</broker>
```

说明：

-  uri=“multicast://default”中的default是activemq默认的ip，默认动态的寻找地址
-  “discoveryUri”是指在transport中用multicast的default的地址传递
-  “uri”指动态寻找可利用的地址
-  如何防止自动的寻找地址？
  -   名称为openwire的transport，移除discoveryUri=”multicast://default”即可。传输链接用默认的名称openwire来配置broker的tcp多点链接，这将允许其它broker能够自动发现和链接到可用的broker中。
  -   名称为“default-nc”的networkConnector，注释掉或者删除即可。ActiveMQ默认的networkConnector基于multicast协议的链接的默认名称是default-nc，而且自动的去发现其他broker。去停止这种行为，只需要注销或者删除掉default-nc网络链接。
  -   使brokerName的名字唯一，可以唯一识别Broker的实例，默认是localhost。

## 基于zookeeper+levelDB的HA集群搭建

ActiveMQ5.9以后退出的机遇zookeeper的master/slave主从实现。虽然ActiveMQ不建议使用LevelDB作为存储（主要原因是社区的主要精力都集中在kahadb的维护上，包括bug修复等），但实际上在很多公司仍然采用了zookeeper+levelDB的高可用集群方案。而实际推荐的方案，仍然是基于kahaDB的文件共享以及JDBC的方式来实现。

### 配置

在三台机器上安装ActiveMQ，通过桑实例组成集群。

修改配置：

- `directory`：表示levelDB所在的主工作目录
- `replicas`：表示总的节点数。比如集群有3个节点，且最多允许一个阶段出现故障 ，你们这个值可以设置为2，也可以设置为3.业务计算公式为（replicas/2）+ 1。如果设置为4，就表示不允许4个节点的任何一个阶段出错。
- `bind`：当前的节点为master时，会根据绑定好的地址和端口来进行主从复制协议
- `zkAddress`：zk的地址
- `hostname`：本机IP
- `sync`：在认为消息被消费完成前，同步信息所存储的策略

## ActiveMQ的优缺点

ActiveMQ采用消息推送方式，所有最适合的场景是默认消息都可以在短时间内被消费。数据量越大，查找和消费消息就越慢，消息积压程度与消息速度成反比。

### 缺点

- 吞吐量低。由于ActiveMQ需要建立索引，导致吞吐量下降。这是无法克服的缺点，只有使用完全符合JMS规范的消息中间件，就要接受这个级别的TPS
- 无分片功能。这个一个功能缺失，JMS并没有规定消息中间件的集群、分片机制。而由于ActiveMQ是伪企业级开发设计的消息中间件，初衷并不是为了处理海量消息和高并发请求。如果一台服务器不能承受更多消息，则需要横向拆分。ActvieMQ官方不提供分片机制，需要自己实现。

### 适用场景

对TPS要求比较低的系统，可以使用ActiveMQ来实现，一方面比较简单，能够快速上手开发，另一方面可控性也比较好，还有比较好的监控机制和界面。

### 不适用的场景

消息量巨大的场景。ActiveMQ不支持消息自动分配机制，如果消息量巨大，导致一台服务器不能处理全部消息，就需要自己开发消息分片功能。