# RabbitMQ简介

## AMQP协议

AMQP协议（Advanced Message Queuing Protocol），一个提供统一消息服务的应用层标准高级消息队列协议，是应用层协议的一个开放标准，为面向消息的中间件设计。基于此协议的客户端和消息中间件可传递消息，并不受客户端/中间件产品、不同的开发语言等条件的限制。

AMQP的实现有：`RabbitMQ`、 `OpenAMQ`、 `Apache Qpid`、 `Redhat Enterprise MRG`、 `AMQP Infrastructure`、 `ØMQ`、` Zyre`等。

## RabbitMQ的特性

RabbitMQ使用`Erlang`语言编写，使用`Mnesia`数据库存储消息。具有以下特点：

- 可靠性（Reliability）

  RabbitMQ使用一些机制来保证可靠性，如持久化、传输确认、发布确认。

- 灵活的路由（Flexible Routing）

  在消息进入队列之前，通过`Exchange`来路由消息。对应典型的路由功能，RabbitMQ提供了一些内置的`Exchange`来实现；针对更复杂的路由功能，可以将多个`Exchange`绑定在一起，通过插件机制实现自己的`Exchanage`。

- 消息集群（Clustering）

  多个RabbitMQ服务器可以组成一个集群，形成一个逻辑Broker。

- 高可用（Highly Available Queues）

  队列可以在集群中的机器上进行镜像，使得部分节点出问题时队列仍可用。

- 多种协议（Multi-protocol）

  RabbitMQ支持多种消息队列协议，比如`AMQP`、`STOMP`、`MQTT`等。

- 多语言客户端（Many Clients）

  RabbitMQ几乎支持所有常用语言，比如`java`，`.net`，`Ruby`，`PHP`，`C#`，`javaScript`等.

- 管理界面（Management UI）

  RabbitMQ提供了一个易用的用户管理界面，使得用户可以监控和管理消息、集群中的节点。

- 插件机制（Plugin System）

  RabbitMQ提供了许多插件，以实现从多方面扩展，当然也可以编写自己的插件。

# 工作模型

![1545979750497](工作模型.png)

| 概念         | 说明                                                         |
| ------------ | ------------------------------------------------------------ |
| Broker       | RabbitMQ的实体服务器，提供一种传输服务，维护一条从生产者到消费者的传输线路，保证消息数据能按照指定的方式传输。 |
| Exchange     | 消息交换机。指定消息按照上面规则路由到哪个队列Queue上。**Exchange交换机没有自己运行的进程，只是一个队列的地址列表。** |
| Queue        | 消息队列。消息的载体，每条消息都会被投送到一个或多个队列中。 |
| Binding      | 绑定。将Exchange和Queue安装某种路由规则绑定起来。            |
| Routing Key  | 路由关键字。Exchange根据Routing Key进行消息投递。定义绑定时指定的关键字称为Binding Key。 |
| Virtual Host | 虚拟主机。一个Broker可以有多个虚拟主机，用作不同用户的权限分离。一个虚拟主机持有一组Exchange、Queue和Binding。 |
| Producer     | 消息生产者。将消息投递到对应的Exchange上。一般是独立的程序。 |
| Consumer     | 消息消费者。消息的接收者，一般是独立的程序。                 |
| Connection   | Producer和Consumer与Broker之间的TCP长连接。                  |
| Channel      | 消息通道，也称信道。在客户端的每个连接里可以建立多个Channel，每个Channel代表一个会话任务。在RabbitMQ Java Client API中，channel上定义了大量的编程接口。 |

## 交换机类型

RabbitMQ有四种交换机类型：`direct`，`topic`，`fanout`，`headers`。常用的是`direct`，`topic`，`fanout`这三种。

### Direct  Exchange 直连交换机

直连类型的交换机与一个队列绑定时，需要指定一个明确的`binding key`。

发送消息到直连类型的交换机时，只有`rounting key`和`binding key`完全匹配时，绑定的队列才能收到消息。

```java
//只有队列1能收到消息
channel.basicPublish("DIRECT_EXCHANGE", "key1", null, msg.getBytes());
```

![1546046548620](direct exchange.png)

### Topic Exchange 主题交换机

主题类型的交换机与一个队列绑定时，可以指定按模式匹配的`routing key`。

通配符有两个：`*`代表匹配一个单词；`#`代表匹配零个或多个单词。单词与单词之间用`.`隔开。

发送消息到主题类型的交换机时，`routing key`符合`binding key`的模式时，绑定的队列才能收到消息。

```java
// 只有队列1能收到消息
channel.basicPublish("TOPIC_EXCHANGE", "sh.abc", null, msg.getBytes());
// 队列2和队列3能收到消息
channel.basicPublish("TOPIC_EXCHANGE", "bj.book", null, msg.getBytes());
// 只有队列4能收到消息
channel.basicPublish("TOPIC_EXCHANGE", "abc.def.food", null, msg.getBytes());
```

![1546047321050](topic exchange.png)

### Fanout  Exchange  广播交换机

广播类型的交换机与一个队列绑定时，不需要指定`binding key`。

当相信发送到广播类型的交换机时，不需要指定`routing key`，所有与之绑定的队列都能收到消息。

```java
// 4个队列都会收到消息
channel.basicPublish("FANOUT_EXCHANGE", "", null, msg.getBytes());
```

![1546047567984](fanout exchange.png)

### Headers Exchange  头交换机

类似topic exchange，但是不处理路由键，而是根据发送的消息内容中的`headers`属性进行匹配。

在绑定`queue`和`exchange`时指定一组键值对；当消息发送到RabbitMQ时会取到该消息的`headers`与`Exchange`绑定时指定的键值对进行匹配；`headers`属性是一个键值对，可以是Hashtable，键值对的值可以是任何类型。而`fanout`，`direct`，`topic `的路由键都需要要字符串形式的。

此交换机有个重要参数：”x-match”，用来设置匹配规则：

- x-match = all ：表示所有的键值对都匹配才能接受到消息
- x-match = any ：表示只要有键值对匹配就能接受到消息

## java API 编程Demo

### pom.xml引入依赖

```xml
<dependency>
    <groupId>com.rabbitmq</groupId>
    <artifactId>amqp-client</artifactId>
    <version>4.1.0</version>
</dependency>
```

### 生产者

```java
public class MyProducer {
    private final static String QUEUE_NAME = "ORIGIN_QUEUE";
    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        // 连接IP
        factory.setHost("127.0.0.1");
        // 连接端口
        factory.setPort(5672);
        // 虚拟机
        factory.setVirtualHost("/");
        // 用户
        factory.setUsername("guest");
        factory.setPassword("guest");
        // 建立连接
        Connection conn = factory.newConnection();
        // 创建消息通道
        Channel channel = conn.createChannel();
        String msg = "Hello world, Rabbit MQ";
        // 声明队列
        // String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 发送消息（发送到默认交换机AMQP Default，Direct）
        // 如果有一个队列名称跟Routing Key相等，那么消息会路由到这个队列
        // String exchange, String routingKey, BasicProperties props, byte[] body
        channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
        channel.close();
        conn.close();
    }
}
```

### 消费者

```java
public class MyConsumer {
    private final static String QUEUE_NAME = "ORIGIN_QUEUE";
    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        // 连接IP
        factory.setHost("127.0.0.1");
        // 默认监听端口
        factory.setPort(5672);
        // 虚拟机
        factory.setVirtualHost("/");
        // 设置访问的用户
        factory.setUsername("guest");
        factory.setPassword("guest");
        // 建立连接
        Connection conn = factory.newConnection();
        // 创建消息通道
        Channel channel = conn.createChannel();
        //声明交换机
        //String exchange, String type, boolean durable, boolean autoDelete, Map<String, Object> arguments
        channel.exchangeDeclare("", "direct", false, false, null);
        // 声明队列
        // String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" Waiting for message....");
        // 创建消费者
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
            AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body, "UTF-8");
                System.out.println("Received message : '" + msg + "'");
            }
        };
        // 开始获取消息
        // String queue, boolean autoAck, Consumer callback
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
```

### 参数说明

#### 声明交换机

```java
//String exchange, String type, boolean durable, boolean autoDelete, Map<String, Object> arguments
 channel.exchangeDeclare("", "direct", false, false, null);
```

一般在消费端声明交换机

- String exchange ：交换机名称，`“”`为默认交换机
- String type : 交换机类型，`direct`，`topic`，`fanout`，`headers`中的一种
- boolean durable ：是否持久化，表示交换机在服务器重启后是否还存在
- boolean autoDelete ：是否自动删除，表示在交换机无人使用时是否删除
- Map<String, Object> arguments ：交换机的其他参数设置

#### 声明队列

```java
// String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
channel.queueDeclare(QUEUE_NAME, false, false, false, null);
```

- String queue ：队列名称

- boolean durable ：是否持久化，表示队列在服务器重启后是否还存在

- boolean exclusive ：是否排他性队列

  排他性队列只能在声明它的connection中使用（另一个连接无法声明一个同样的排他队列，而且只区别connection而不是channel，从一个连接创建的不同通道可以同时访问某一个排他性队列），连接断开时自动删除（不管是否设置了持久化）

- boolean autoDelete ：是否自动删除，表示至少有一个消费者连接到这个队列，当所有与这个队列连接的消费者都断开时，队列会自动删除

- Map<String, Object> arguments ：队列的其他属性，例如x-message-ttl、 x-expires、 x-max-length、 x-maxlength-
  bytes、 x-dead-letter-exchange、 x-dead-letter-routing-key、 x-max-priority

#### 发送消息

```java
// String exchange, String routingKey, BasicProperties props, byte[] body
channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
```

- String exchange ：交换机名称，`“”`为默认交换机

- String routingKey ：绑定关键字，用来确定发送到哪个队列

- BasicProperties props ：消息属性，全部共14个，比较重要的有

  | 参数                        | 说明                            |
  | --------------------------- | ------------------------------- |
  | Map<String, Object> headers | 消息的其他自定义参数            |
  | Integer deliveryMode        | 等于2时表示持久化，其他表示瞬态 |
  | Integer priority            | 消息的优先级                    |
  | String correlationId        | 关联ID，方便RPC相应的请求关联   |
  | String replyTo              | 回调队列                        |
  | String expiration           | TTL，消息过期时间，单位毫秒     |

- byte[] body ：消息正文

## 进阶知识

### 过期时间TTL（Time To Live）

#### 消息的过期时间

有2种设置方式：

- 通过队列属性设置消息过期时间：

  ```java
  Map<String, Object> argss = new HashMap<String, Object>();
  argss.put("x-message-ttl",6000);
  channel.queueDeclare("TEST_TTL_QUEUE", false, false, false, argss);
  ```

- 设置单条消息的过期时间：

  ```java
  AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
      .deliveryMode(2) // 持久化消息
      .contentEncoding("UTF-8")
      .expiration("10000") // TTL
      .build();
  channel.basicPublish("", "TEST_TTL_QUEUE", properties, msg.getBytes());
  ```

#### 队列的过期时间

队列的过期时间决定了在没有任何消费者以后，队列可以存活多久。

```java
Map<String, Object> argss = new HashMap<String, Object>();
argss.put("x-message-ttl",6000);
channel.queueDeclare("TEST_TTL_QUEUE", false, false, false, argss);
```

### 死信队列DLX（Dead Letter Exchange）

消息有3种情况会进入死信交换机：

- (NACK || Reject) && requeue == false
- 消息过期
- 队列达到最大长度（先入队的消息会被发送到DLX）

可以设置一个死信队列（Dead Letter Queue）与DLX绑定，这样就可以存储Dead Letter，消费者可以监听这个队列取走消息。

```java
Map<String,Object> arguments = new HashMap<String,Object>();
arguments.put("x-dead-letter-exchange","DLX_EXCHANGE");
// 指定了这个队列的死信交换机
channel.queueDeclare("TEST_DLX_QUEUE", false, false, false, arguments);
// 声明死信交换机
channel.exchangeDeclare("DLX_EXCHANGE","topic", false, false, false, null);
// 声明死信队列
channel.queueDeclare("DLX_QUEUE", false, false, false, null);
// 绑定
channel.queueBind("DLX_QUEUE","DLX_EXCHANGE","#");
```

### 优先级队列

优先级高的消息可以优先被消费，但是只有消息堆积（消息的发送速度大于消费者的消费速度）的情况下优先级才有意义。

```java
//设置一个队列的最大优先级
Map<String, Object> argss = new HashMap<String, Object>();
argss.put("x-max-priority",10); // 队列最大优先级
channel.queueDeclare("ORIGIN_QUEUE", false, false, false, argss);
```

```java
//发送消息时指定消息当前的优先级
AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
.priority(5) // 消息优先级
.build();
channel.basicPublish("", "ORIGIN_QUEUE", properties, msg.getBytes());
```

### 延迟队列

RabbitMQ本身不支持延迟队列。可以使用TTL结合DLX的方式来实现消息的延迟投递，即把DLX跟某个队列绑定，到了指定时间，消息过期后，就会从DLX路由到这个队列，消费者可以从这个队列取走消息。

另一种方式使用`rabbitmq-delaye-message-exchange`插件。

将需要发送的信息保存在数据库，使用任务调度系统扫描然后发送也可以实现。

### RPC

原理：服务端处理消息后，把响应消息发送到一个响应队列，客户端再从相应队列取到结果。

其中的问题是Client收到消息后，怎么知道应答消息是回复哪一条消息的。所有必须有一个唯一ID来关联，就是`correlationId`。

![1546052613715](RPC.png)

### 服务端流控（Flow Control）

RabbitMQ会在启动时检测机器的物理内存数值，默认当MQ占用40%以上内存时，MQ会主动抛出一个内存警告并阻塞所有连接。可以通过修改`rabbitmq.config`文件来调整内存阈值，默认值是0.4

```properties
{vm_memory_high_watermark, 0.4}
```

默认情况下，如果剩余磁盘空间在1GB以下，RabbitMQ主动阻塞所有的生产者。这个阈值也是可调的

```properties
{vm_memory_high_watermark, {absolute, "1024M"}}
```

***注意：队列长度只在消费堆积的情况下有意义，而且会删除先入队的消息，不能实现服务端限流。***

### 消费端限流

在`AutoACK`为false的情况下，如果一定数目的消息（通过基于consumer或者channel设置`Qos`的值）未被确认前，不消费新的消息。

```java
channel.basicQos(2); // 如果超过2条消息没有发送ACK，当前消费者不再接受队列消息
channel.basicConsume(QUEUE_NAME, false, consumer);
```

# RabbitMQ可靠性投递

首先需要明确，效率与可靠性是无法兼得的，如果要保证每一个环节都成功，势必会对消息的收发效率造成影响。如果是一些业务实时一致性要求不是特别高的场合，可以牺牲一些可靠性来换取效率。

![1546063665665](可靠性投递.png)

- ①代表消息从生产者发送到Exchange；
- ②代表消息从Exchange路由到Queue；
- ③代表消息在Queue中存储；
- ④代表消费者订阅Queue并消费消息。

## 确保消息发送到RabbitMQ服务器

可能因为网络或者Broker的问题导致①失败,而生产者是无法知道消息是否正确发送到Broker的。

有两种解决方案，第一种是`Transaction`（事务）模式，第二种是`Confirm`（确认）模式。

首先通过`channel.txSelect`方法开启事务。如果事务提交成功，则消息一定到达了RabbitMQ中，如果在事务提交执行之前由于RabbitMQ异常崩溃或者其他原因抛出异常，这时可以将其捕获，进而通过执行`channel.txRollback`方法来实现事务回滚。使用事务机制会“吸干”RabbitMQ的性能，一般不建议使用。

生产者通过调用`channel.confirmSelect`方法（即`Confirm.Select`命令）将信道设置为confirm模式。一旦消息被投递到所有匹配的队列之后，RabbitMQ就会发送一个确认（`Basic.Ack`）给生产者（包含消息的唯一ID），这就使得生产者知晓消息已经正确到达了目的地。

## 确保消息路由到正确的队列

可能因为路由关键字错误，或者队列不存在，或者队列名称错误导致②失败。

使用`mandatory`参数和`ReturnListener`，可以实现消息无法路由的时候返回给生产者。

另一种方式就是使用备份交换机（`alternate-exchange`），无法路由的消息会发送到这个交换机上。

```java
Map<String,Object> arguments = new HashMap<String,Object>();
arguments.put("alternate-exchange","ALTERNATE_EXCHANGE"); // 指定交换机的备份交换机
channel.exchangeDeclare("TEST_EXCHANGE","topic", false, false, false, arguments);
```

## 确保消息在队列正确的存储

可能因为系统宕机、重启、关闭等情况导致存储在队列的消息丢失，即③出现问题。

解决方案：

- 队列持久化

  ```java
  // String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
  channel.queueDeclare(QUEUE_NAME, true, false, false, null);
  ```

- 交换机持久化

  ```java
  // String exchange, boolean durable
  channel.exchangeDeclare("MY_EXCHANGE","true");
  ```

- 消息持久化

  ```java
  AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
  .deliveryMode(2) // 2代表持久化，其他代表瞬态
  .build();
  channel.basicPublish("", QUEUE_NAME, properties, msg.getBytes());
  ```

- 集群、镜像队列

  参见集群章节

## 确保消息从队列正确的投递到消费者

如果消费者收到消息后未来得及及时处理就发送异常，或者在处理过程中发送异常，会导致④失败。

为了保证消息从队列可靠的到达消费者，RabbitMQ提供了消息确认机制（Message acknowledgement）。消费者在订阅队列时，可以指定`autoAck`参数，当`autoAck`等于false时，RabbitMQ会等待消费者显式的回复确认信号后才从队列中移除消息（当`autoAck`等于true时，RabbitMQ不会等待消费端消费完消息再移除消息，即消费者收到消息后RabbitMQ就会移除消息）。

```java
//args：String queue, boolean autoAck, Consumer callback
//参数：队列名称，是否自动确认，回调函数
channel.basicConsume(QUEUE_NAME, true, consumer);

//手动确认，在回调函数中确认
channel.basicAck(envelope.getDeliveryTag(), true);
```



如果消息消费失败，也可以调用`Basic.Reject`或者`Basic.Nack`来拒绝当前消息而不是确认。如果`requeue`参数设置为true，可以把这条消息重新存入队列，以便发给下一个消费者（只有一个消费者的时候，这种方式可能会出现无限循环重复消费的情况，可以投递到新的队列中，或者只打印异常日志）。

```java
 //args:long deliveryTag, boolean requeue
//发送标签，重回队列（false时直接丢弃，相对于告诉队列可以直接删除）
//拒绝tag = envelope.getDeliveryTag()的消息并重新放入队列
channel.basicReject(envelope.getDeliveryTag(), true);

//args: long deliveryTag, boolean multiple, boolean requeue
//参数： 发送标签，是否批量拒绝，是否重回队列
//拒绝tag <= envelope.getDeliveryTag()的消息并直接删除消息
channel.basicNack(envelope.getDeliveryTag(), true, false);
```



## 消费者回调

消费者处理了消息以后，可以在发送一条消息给生产者，或者调用生产者的API，告知消息处理完毕。例如：二代支付中异步通信的回执，多次交互。

## 补偿机制

对于一定时间没有得到响应的消息，可以设置一个定时重发的机制，但要控制次数，比如最多重发3次，否则会造成消息堆积。例如：ATM存款未得到响应时发送5次确认；ATM取款未得到响应时，发送5次冲正。

## 消息幂等性

服务端是没有这种控制的，只能在消费端控制。

消息重复可能会有2个原因：

- 生产者的问题，环节①重复发送消息，比如在开启了Confirm模式但未收到确认；
- 环节④由于消费者为未送ACK或者其他原因，消息重复投递。

对于重复发送的消息，可以对每一条消息生成一个唯一的业务ID，通过日志或者建表来做重复控制。

## 消息的顺序性

消息的顺序性指的是消费者消费的顺序跟生产者产生消息的顺序是一致的。在RabbitMQ中，一个队列有多个消费者时，由于不同的消费者消费消息的速度是不一样的，顺序无法保证。只有一个队列只有一个消费者的情况下RabbitMQ才能保证顺序性（消息是先进先出）。

可以为消息增加全集唯一ID，根据ID的顺序来处理消息。

# 高可用架构

![1546069019297](高可用架构.png)

## RabbitMQ集群

集群主要用于实现高可用与负载均衡。

RabbitMQ通过`/var/lib/rabbitmq/.erlang.cookie`来验证身份，需要在所有节点上保持一致。

集群有两种节点类型：一种是磁盘节点，一种是内存节点。集群中至少需要一个磁盘节点以实现元数据的持久化，未指定类型的情况下，默认为磁盘节点。

集群通过25672端口两两通信。

需要注意的是RabbitMQ集群无法搭建在广域网上，除非使用`federation`或者`shovel`等插件。

集群的配置步骤：

- 配置`hosts`
- 同步`erlang.cookie`
- 加入集群

## RabbitMQ镜像队列

集群方式下，队列和消息是无法在节点之间同步的，因此需要使用RabbitMQ的镜像队列机制进行同步。

| 操作方式    | 命令或步骤                                                   |
| ----------- | ------------------------------------------------------------ |
| rabbitmqctl | rabbitmqctl set_policy ha-all "^ha." "{""ha-mode"":""all""}" |
| HTTP API    | PUT /api/policies/%2f/ha-all {"pattern":"^ha.", "definition":{"ha-mode":"all"}} |
| Web UI      | Navigate to Admin > Policies > Add / update a policy
Name输入：mirror_image Pattern输入：^（代表匹配所有） Definition点击 HA mode，右边输入：all |

![1546069784783](配置镜像队列.png)

## 网络分区

RabbitMQ对网络延迟分成敏感，为了保证数据一致性和性能，在出现网络故障时，集群节点会出现分区。

### 如何监控网络分区

- 客户端出现异常
- 可以在Web管理界面查看
- 通过服务器命令`rabbitmqctl cluster_status`查看
- 通过HTTP API：` /api/nodes`查看
- 通过log日志查看

### 如何判断网络分区

RabbitMQ通过对`net_ticktime`进行检测来判定是否出现网络分区。如果在`net_ticktime`指定的时间内一个节点无法与其他节点进行通信，则判定该节点已分区。

### 网络分区之后的恢复

在高版本的RabbitMQ中，通过配置`rabbitmq.config`中配置`cluster_partion_handing`参数进行自动恢复。

自动回复的策略：

- ignore 默认，发生分区时不做任何动作，等待人工干预
- `pause_minority`
  - 分区发生开始时(net_tick_timeout)，关闭“少数派”分区中的节点（“少数派”分区是指小于等于集群中一半节点数）
  - 分区判定之后，启动关闭的节点
  - 对等分区的情况处理得不够优雅
- `pause-if-all-down`
  - 分区之后，与列表中的任何节点不能内部通信时会关闭应用
  - 难点：受信节点的选择
  - 多分区的处理比autoheal优雅
- `autoheal`
  - 分区判定之后会挑选获胜分区
  - 重启非获胜分区中的节点
  - 如何挑选获胜分区
    - 客户端连接数
    - 分区内节点数
    - 节点名称字典顺序

也可以人工介入重启不被信任的服务器来进行恢复。

挑选信任分区的注意点：

- 有磁盘节点的分区
- 节点数最多的分区
- 队列数最多的分区
- 客户端连接数最多的分区

重启命令

```bash
rabbitmqctl stop_app / rabbitmqctl start_app（推荐）
#或者
rabbitmqctl stop / rabbit-server -detached
```

重启的顺序：

- 停止其他分区中的所有节点，然后再启动这些节点，如果此时还有网络分区的告警，则再重启信任分区中的节点以去除警告
- 关闭整个集群的节点，然后再启动每一个节点，这里需要确保启动的第一个节点在信任的分区之中