# idea快捷键

#### **1. IDEA内存优化**

\IntelliJ IDEA 9\bin\idea.exe.vmoptions

------------------------------------------

Xms64m

-Xmx256m

-XX:MaxPermSize=92m

-ea

-server

-Dsun.awt.keepWorkingSetOnMinimize=true

#### **2、查询快捷键 **

查找类  ‘CTRL+N’   

查找文件 ‘CTRL+SHIFT+N’

查找类中的方法或变量  ‘CTRL+SHIFT+ALT+N’         

找变量的来源   `CIRL+B`   

找所有的子类 `CTRL+ALT+B `    

找变量的类  `CTRL+SHIFT+B`         

定位行  `CTRL+G`         

在当前窗口查找文本  `CTRL+F`   

在指定窗口查找文本  `CTRL+SHIFT+F`         

在当前窗口替换文本 `CTRL+R`    

在指定窗口替换文本`CTRL+SHIFT+R`      

查找修改的文件 `ALT+SHIFT+C`          

最近打开的文件`CTRL+E`              

向下查找关键字出现位置  `F3 `       

向上一个关键字出现位置 `SHIFT+F3`             

查找变量来源`F4 `      

选中的字符查找工程出现的地方 `CTRL+ALT+F7`    

弹出显示查找内容`CTRL+SHIFT+O`

#### **3、SVN 管理**

把SVN库添加到IDEA中 SETTING ->  VERSION CONTROL -> VCS = SVBVERSION

#### **4、自动代码 **

导入包,自动修正 `ALT+ENTER`      

格式化代码`CTRL+ALT+L`       

自动缩进`CTRL+ALT+I` 

优化导入的类和包`CTRL+ALT+O`      

生成代码(如GET,SET方法,构造函数等) `ALT+INSERT `         

最近更改的代码   `CTRL+E `   

最近更改的代码` ALT+SHIFT+C`

自动补全代码  `CTRL+SHIFT+SPACE`      

代码提示  `CTRL+空格`

 类名或接口名提示  `CTRL+ALT+SPACE `        

方法参数提示` CTRL+P`        

自动代码`CTRL+J  `       

把选中的代码放在 TRY{} IF{} ELSE{} 里`CTRL+ALT+T`

#### **5、复制快捷方式**

拷贝文件快捷方式`F5`        

复制行`CTRL+D`        

剪切,删除行`CTRL+X`  

可以复制多个文本`CTRL+SHIFT+V`

#### **6、高亮**

选中的文字,高亮显示 上下跳到下一个或者上一个`CTRL+F`

高亮错误或警告快速定位`F2或SHIFT+F2`     

高亮显示多个关键字`CTRL+SHIFT+F7`

#### **7、其他快捷方式**

大小写切换`CIRL+U`            

倒退`CTRL+Z`

向前`CTRL+SHIFT+Z `

资源管理器打开文件夹`CTRL+ALT+F12`

查找文件所在目录位置`ALT+F1`

竖编辑模式`SHIFT+ALT+INSERT`

注释// `CTRL+/`

注释/\*...*/ `CTRL+SHIFT+/ `

选中代码，连续按会 有其他效果`CTRL+W`

快速打开光标处的类或方法`CTRL+B `

切换代码视图`ALT+ ←/→ `

返回上次编辑的位置`CTRL+ALT ←/→ `

在方法间快速移动定位`ALT+ ↑/↓ `

重构-重命名`SHIFT+F6`

显示类结构图`CTRL+H`

显示注释文档`CTRL+Q `

快速打开或隐藏工程面板`ALT+1 `

代码 向上/下移动 `CTRL+SHIFT+UP/DOWN`

光标跳转到第一行或最后一行下 `CTRL+UP/DOWN`

光标返回编辑框 `ESC `

光标返回编辑框,关闭无用的窗口 `SHIFT+ESC `

帮助千万别按,很卡! `F1`

非常重要下班都用 `CTRL+F4`

#### **8、重要的设置**

不编译某个MODULES的方法，但在视图上还是有显示

SETTINGS -> COMPILER -> EXCLUDES ->不编译某个MODULES，并且不显示在视图上MODULES SETTINGS -> (选择你的MODULE) -> SOURCES -> EXCLUDED -> 整个工程文件夹

#### **9、IDEA编码设置3步曲**

FILE -> SETTINGS -> FILE ENCODINGS -> IDE ENCODING

FILE -> SETTINGS -> FILE ENCODINGS -> DEFAULT ENCODING FOR PROPERTIES FILES

FILE -> SETTINGS -> COMPILER -> JAVA COMPILER -> ADDITIONAL COMMAND LINE PARAMETERS加上参数 -ENCODING UTF-8 编译GROOVY文件的时候如果不加，STRING S = "中文"; 这样的GROOVY文件编译不过去.

#### **10、编译中添加其他类型文件比如 \*.TXT *.INI**

FILE -> SETTINGS -> RESOURCE PATTERNS

改变编辑文本字体大小FILE -> SETTINGS -> EDITOR COLORS & FONTS -> FONT -> SIZE

#### **11、修改智能提示快捷键**

FILE -> SETTINGS -> KEYMAP -> MAIN MENU -> CODE -> COMPLETE CODE -> BASIC

#### **12、显示文件过滤**

FILE -> SETTINGS -> FILE TYPES -> IGNORE FILES...

下边是我过滤的类型,区分大小写的CVS;SCCS;RCS;rcs;.DS_Store;.svn;.pyc;.pyo;\*.pyc;\*.pyo;.git;\*.hprof;_svn;.sbas;.IJI.\*;vssver.scc;vssver2.scc;.\*;\*.iml;\*.ipr;\*.iws;\*.ids

#### **13、在PROJECT窗口中快速定位,编辑窗口中的文件 **

在编辑的所选文件按ALT+F1, 然后选择PROJECT VIEW