# SPI机制

SPI（Service provider interface）是JDK内置的一种服务提供发现机制，目前市面上有很多框架都是用它来做服务的扩展发现，如JDBC，日志框架等等。

简单来说，SPI是一种动态替换发现的机制。例如我们定义了一个规范，需要第三方厂商去实现，那么对于我们应用来说，只需要集成对应厂商的插件，即可以完成对应规范的实现机制，形成一种***插拔式的扩展手段***。

# SPI流程

![SPI流程](SPI流程.png)

SPI 实际上是***“基于接口的编程＋策略模式＋配置文件”组合实现的动态加载机制***。

实现SPI，需要安装SPI本身定义的规范来进行配置，SPI规范如下：

1. 需要在`classpath`下创建目录，目录名必须为：`META-INF/services`
2. 在该目录下创建一个properties文件，该文件需要满足：
   - 文件名必须是扩展的接口的全路径名称；
   - 文件内部描述的是该扩展接口的所有实现类；
   - 文件的编码格式是UTF-8
3. 通过`java.util.ServiceLoader`的加载机制来发现。

# SPI示例

项目结构如下：

![项目结构](项目结构.png)

1. **创建规范模块`SPI-interface`，编写规范接口，并打包发布**

   ```java
   public interface DatabaseDriver {
   
       String connect(String host);
   }
   ```

2. **创建实现模块`SPI-impl`，引用规范，并编写实现类，打包发布**

   ```java
   public class MysqlDriver implements DatabaseDriver {
       @Override
       public String connect(String host) {
           return "mysql connect:" + host;
       }
   }
   ```

3. **创建应用模块`SPI-test`，引用规范和实现，使用`java.util.ServiceLoader`获取实现类**

   ```java
   public class App {
       public static void main(String[] args) {
           ServiceLoader<DatabaseDriver> drivers = ServiceLoader.load(DatabaseDriver.class);
           for (DatabaseDriver driver : drivers) {
               System.out.println(driver.connect("localhost"));
           }
       }
   }
   ```

   输出结果:

   ![spi输出](spi输出.png)

# SPI的实际应用

SPI在很多地方都有应用，可参考最常用的`java.sql.Driver`。JDK官方提供了`java.sql.Driver`这个驱动扩展点，但并没有对应的Driver实现。

已连接Mysql为例，需要添加`mysql-connecor-java`依赖。可以在这个jar包中找到SPI的配置信息。所有`java.sql.Driver`是由各个数据库厂商自行实现的。

![mysql-spi](mysql-spi.png)

除此之外，在spring的包中也可以看到相应的痕迹。

# SPI的缺点

- JDK标准的SPI会一次性加载实例化扩展点的所有实现。如果在`META-INF/services`下的文件里面加了N个实现类，那么JDK启动的时候会一次性全部加载。如果有的扩展点实现初始化很耗时或者有些实现类并没有用到，那么会浪费资源。
- 如果扩展点加载失败，会导致调用方报错，而这个错误很难定位到是这个原因。

