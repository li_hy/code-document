

# 注解

## 启动类注解

`@SpringBootApplication` 包含 `@EnableAutoConfiguration` + `@ComponentScan` + `@Configuration`

- `Configuration`：等同于spring的XML配置文件

- `@componemtScan`：组件扫描，自动发现和装配Bean
- `EnableAutoConfiguration`：自动装配

## springMVC注解

`@RestController`等同于 `@Controller` + `@ResponseBody`

- `@ResponseBody`：表示该方法的返回结果直接写入HTTP response body
-  `@Controller`：用于定义控制器，通常方法需要配合注解`@ResponseMapping`

`PostRequest`等同于`@RequestMapping(method = RequestMethod.POST)`

`GetRequest`等同于`@RequestMapping(method = RequestMethod.GET)`

`RequestMapping`：提供路由信息，辅助URL到Controller中的具体函数的映射

`@RequestParam`：用在方法参数前

`@PathVariable`：路径变量，参数与`@RequestMapping`中的路径变量名匹配

`@RequestBody`：将HTTP请求正文插入方法中，适用方法在传递对象，需要指定HTTP头的`content-Type`为`application/json`

`@RequestHeader`：用于映射控制器参数以请求头值，例如cookie、token等

## springboot json注解

JavaBean序列化Json,性能:Jackson->FastJson->Gson->Json-Lib，springboot默认使用Jackson。

`@JsonProperty`：作用在属性上，为json key指定一个别名

`@JsonIgnore`：作用在属性上，忽略此属性

`@JsonIgnoreProperties`：作用在类上，忽略一组属性

`@JsonFormat`：用于格式化时间，有4个参数

- shap：表示序列化后的一种类型，有`JsonFormat.Shape.STRING`，`JsonFormat.Shape.NUMBER`等
- pattern：表示日期格式
- timezone：默认是GMT，中国需要用`GMT+8`
- locale：根据位置序列化，例如`zh_CN`等

`@JsonView`：作用在类或属性上，用来定义一个序列化组

`@JsonSerialize`：指定一个实现类来自定义序列化，类必须实现`JsonSerializer`接口

## 配件注解

`@Component`：把普通POJO实例化到spring容器中

`@PropertySource`：指定自定义配置文件路径

`@ConfigurationProperties` ：将类中所有属性和配置文件中相关的配置进行绑定，可使用value或prefix指定配置文件中的前缀

`@Value`：从配置文件在获取值

`@ConfigurationProperties`和`@Value`对比

|                                 | @ConfigurationProperties                           | @Value     |
| ------------------------------- | -------------------------------------------------- | ---------- |
| 功能                            | 根据配置文件中设置的属性，批量注入属性值           | 单个指定值 |
| 松散语法                        | 支持（lastName可以写作last-name，last_name）       | 不支持     |
| SpEL（spring表达式）            | 不支持                                             | 支持       |
| JSR303数据校验                  | 支持（可以通过@Email等注解校验属性值是否符合要求） | 不支持     |
| 复杂类型封装（MAP、List等类型） | 支持                                               | 不支持     |

## 异常注解

`@ControllerAdvice`：设置全局异常

`@RestControllerAdvice`：`@ControllerAdvice` + `@ResponseBody`

`@ExceptionHandler`： 指定需要拦截的异常类型



# 目录结构

## 目录

- `src/main/java`：存放源代码
- `src/main/resources`
  - `static`：存放静态文件，css、js、image等
  - `templates`：存放静态页面文件，jsp、html、tpl等
  - `config`：存放配置文件，application.properties等
  - `resources`：存放配置文件、脚本等

若静态资源文件不放在`resources`、`static`、`public`这几个文件夹中：

1. 可以引入模板引擎，并用controller跳转

   ```xml
   <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-thymeleaf</artifactId>
   </dependency>
   ```

   

2. 可以增加springboot查找静态资源文件的路径

   ```properties
   #默认查找路径，可在后面增加自定义路径，如classpath:/templates/
   spring.resources.static-locations = classpath:/META-INF/resources/,classpath:/resources/,classpath:/static/,classpath:/public/ 
   ```

   

## 资源文件的加载顺序

springboot默认的查找顺序为：

`META-INF/resources` -> `resources` -> `static` -> `public`

如果都找不到相应的资源则报错

# 配置文件

springboot常见的配置文件有`xml`，`yml`，`properties`，`json`

YAML（Yet Another Markup Language），写 YAML 要比写 XML 快得多(无需关注标签或引号)，使用空格 Space 缩进表示分层，不同层次之间的缩进可以使用不同的空格数目

注意：key后面的冒号，后面一定要跟一个空格,树状结构

application.properties示例

```properties
server.port=8090  
server.session-timeout=30  
server.tomcat.max-threads=0  
server.tomcat.uri-encoding=UTF-8
```

application.yml示例

```yaml
server:  
    port: 8090  
    session-timeout: 30  
    tomcat.max-threads: 0  
    tomcat.uri-encoding: UTF-8
```

[配置文件示例(https://docs.spring.io/spring-boot/docs/2.1.0.BUILD-SNAPSHOT/reference/htmlsingle/#common-application-properties)][https://docs.spring.io/spring-boot/docs/2.1.0.BUILD-SNAPSHOT/reference/htmlsingle/#common-application-properties]

## 加载配置文件

1. 通过注解`@value`配置

   1. Controller配置`@PropertySource({"classpath:resource.properties"})`
   2. 增加属性 `@Value("${test.name}") private String name;`

2. 通过实体类配置文件

   1. 添加`@Component`注解

   2. 使用`@PropertySource`注解指定配置文件位置

   3. 使用`@ConfigurationProperties`注解设置相关属性

   4. 必须通过注入IOC对象Resource进来，才能在类中使用获取的配置文件值

## 常见问题

- 配置文件注入失败`Could not resolve placeholder`
  - 根据springboot启动流程，会有自动扫描包没有扫描到相关注解，默认Spring框架实现会从声明@ComponentScan所在的类的package进行扫描，来自动注入，因此启动类最好放在根路径下面，或者指定扫描包范围
- 注入bean的方式，属性名称和配置文件里面的key一一对应，就用加@Value 这个注解，如果不一样，就要加@value("${XXX}")
- 如果`META-INF/resources` 、 `resources` 、 `static` 、 `public`中有与自定义属性相同的key，会被spring优先获取而无法获取自定义属性值

# 热部署

springboot结合dev-tool工具

## 依赖包

```xml
<dependency>  
     <groupId>org.springframework.boot</groupId>  
     <artifactId>spring-boot-devtools</artifactId>  
     <optional>true</optional>  
 </dependency>
```

## 不被热部署的文件

- `/META-INF/maven`，`META-INF/resources`，`/resources`，`/static`，`/public`，`/template`

- 指定文件不进行热部署

  ```properties
  #指定修改static，public下文件不重启
  spring.devtools.restart.exclude=static/**,public/**
  ```

- 手工触发重启（改代码不重启，通过一个文本去控制）

  ```properties
  #指定热部署触发器为trigger.txt，修改trigger.txt则重启
  spring.devtools.restart.trigger-file=trigger.txt
  ```

**PS：生产环境不要开启热部署；使用`java-jar`启动不会进行热部署。**

# 过滤器、监听器、拦截器

- 监听器：listener是servlet规范中定义的一种特殊类。用于监听`servletContext`、`HttpSession`和`servletRequest`等域对象的创建和销毁事件。监听域对象的属性发生修改的事件。用于在事件发生前、发生后做一些必要的处理。其主要可用于以下方面：1、统计在线人数和在线用户2、系统启动时加载初始化信息3、统计网站访问量4、记录用户访问路径。
- 过滤器：Filter是Servlet技术中最实用的技术，Web开发人员通过Filter技术，对web服务器管理的所有web资源：例如Jsp, Servlet, 静态图片文件或静态 html 文件等进行拦截，从而实现一些特殊的功能。例如实现URL级别的权限访问控制、过滤敏感词汇、压缩响应信息等一些高级功能。它主要用于对用户请求进行预处理，也可以对`HttpServletResponse`进行后处理。使用Filter的完整流程：Filter对用户请求进行预处理，接着将请求交给Servlet进行处理并生成响应，最后Filter再对服务器响应进行后处理。
- 拦截器：Interceptor 在AOP（Aspect-Oriented Programming）中用于在某个方法或字段被访问之前，进行拦截然后在之前或之后加入某些操作。比如日志，安全等。一般拦截器方法都是通过动态代理的方式实现。可以通过它来进行权限验证，或者判断用户是否登陆，或者是像12306 判断当前时间是否是购票时间。

## 过滤器

### spring启动默认加载的Filter

- `characterEncodingFilter`：设置编码
- `hiddenHttpMethodFilter`：html中form表单只支持`GET`与`POST`请求，而`DELETE`、`PUT`等method并不支持，spring3添加了一个过滤器，可以将这些请求转换为标准的http方法，使得支持`GET`、`POST`、`PUT`与`DELETE`请求
- `httpPutFormContentFilter`：获取put表单参数值，并将之传递到Controller中标注了method为`RequestMethod.put`的方法中
- `requestContextFilter`：将当前请求暴露到当前线程

### Filter优先级

- `Ordered.HIGHEST_PRECEDENCE`：最高优先级，值为`Integer.MIN_VALUE`
- `Ordered.LOWEST_PRECEDENCE`：最低优先级，值为`Integer.MAX_VALUE`

值越低意味着优先级越高

自定义Filter需要避免和默认Filter优先级一样，不然会冲突

### 自定义Filter

- 使用`FilterRegistrationBean`注册自定义Filter

  ```java
  @Configuration
  public class FilterConfig {
  
  @Bean
  public FilterRegistrationBean registrationBean() {
          FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new CustomerFilter());
          filterRegistrationBean.addUrlPatterns("/manager/*");
          return filterRegistrationBean;
      }
  }
  ```

- 使用`@WebFilter`标记一个类为Filter

  ```java
  @WebFilter(filterName = "accessFilter", urlPatterns = "/manager/*")
  public class AccessFilter implements Filter {
  	//TODO
  }
  ```

PS：使用`@WebFilter`需要在启动类中增加`@ServletComponentScan`；并且不要在Filter类上加`@Component`注解，否则会造成加载两次Filter，并且`urlPatterns`失效，会过滤全路径。

## 监听器

使用`@WebListener`来创建监听器

```java
@WebListener
public class RequestListener implements ServletRequestListener {
    //TODO
}
```

## 拦截器

SpringBoot2.X 新版本配置拦截器 只要实现`WebMvcConfigurer`接口，自定义拦截器实现`HandlerInterceptor`接口

```java
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AccessInterceptor()).addPathPatterns("/admin/**");
    }
}
```

```java
public class AccessInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("this is accessInterceptor preHandle");
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        System.out.println("this is accessInterceptor postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        System.out.println("this is accessInterceptor afterCompletion");
    }
}
```

- `preHandle`：调用Controller某个方法之前调研
- `postHandle`：Controller之后，视图渲染之前调用，如果控制器Controller出现了异常，则不会执行此方法
- `afterCompletion`：不管有没有异常，这个`afterCompletion`都会被调用，用于资源清理

### 拦截器执行顺序

按照注册顺序进行拦截，先注册，先被拦截。先注册的`preHandle`先被调用，然后是后注册的`preHandle`，之后是后注册的`postHandle`，最后是先注册的`postHandle`。

## 过滤器与拦截器对比

- Filter是基于函数回调 `doFilter()`，而Interceptor则是基于AOP思想
- Filter在只在Servlet前后起作用，而Interceptor够深入到方法前后、异常抛出前后等
- Filter依赖于Servlet容器即web应用中，而Interceptor不依赖于Servlet容器所以可以运行在多种环境
- 在接口调用的生命周期里，Interceptor可以被多次调用，而Filter只能在容器初始化时调用一次

## 过滤器与拦截器的执行顺序

过滤前->拦截前->action执行->拦截后->过滤后

# 异常处理

使用`@ControllerAdvice`+ `@ExceptionHandler` 注解方式实现全局异常处理

```java
@RestControllerAdvice
public class MyExceptionHandler {
    /**
    * 捕获全局异常,处理所有不可知的异常
    */
	@ExceptionHandler(value = Exception.class)
    public Object handleException(Exception e, HttpServletRequest request) {
        //TODO
    }
    /**
    * 处理自定义异常
    */
    @ExceptionHandler(value = MyException.class)
    public Object handleMyException(MyException e, HttpServletRequest request){
    	//TODO
    }
}
```

# redis

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```



```properties
#redis的ip地址  
spring.redis.hostName=127.0.0.1
#数据库，默认为0
spring.redis.database=0
#端口号  
spring.redis.port=6379
#如果有密码  
spring.redis.password=
#客户端超时时间单位是毫秒 默认是2000 
spring.redis.timeout=10000 
```



```java
stringRedisTemplate.opsForValue();//操作字符串
stringRedisTemplate.opsForHash();//操作hash
stringRedisTemplate.opsForList();//操作list
stringRedisTemplate.opsForSet();//操作set
stringRedisTemplate.opsForZSet();//操作有序set
```



```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    /**
     * stringRedisTemplate基本操作
     */
    public void redis(){
        stringRedisTemplate.opsForValue().set("key", "value",60*5, TimeUnit.SECONDS);//向redis里存入数据和设置缓存时间(5分钟)
        stringRedisTemplate.boundValueOps("key").increment(-1);//val做-1操作
        stringRedisTemplate.opsForValue().get("key");//根据key获取缓存中的val
        stringRedisTemplate.boundValueOps("key").increment(1);//val +1
        stringRedisTemplate.getExpire("key");//根据key获取过期时间
        stringRedisTemplate.getExpire("key",TimeUnit.SECONDS);//根据key获取过期时间并换算成指定单位
        stringRedisTemplate.delete("key");//根据key删除缓存
        stringRedisTemplate.hasKey("key");//检查key是否存在，返回boolean值
        stringRedisTemplate.opsForSet().add("key", "1","2","3");//向指定key中存放set集合
        stringRedisTemplate.expire("key",1000 , TimeUnit.MILLISECONDS);//设置过期时间
        stringRedisTemplate.opsForSet().isMember("key", "1");//根据key查看集合中是否存在指定数据
        stringRedisTemplate.opsForSet().members("key");//根据key获取set集合

    }
}
```

