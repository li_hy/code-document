## @ComponentScan

启用组件扫描
相当于Spring context命名空间的`<context:component-scan/>`。

## @component、@Repository、@Service、@Controller

把普通pojo实例化到spring容器中，
相当于配置文件中的 `<bean id="" class=""/>`。

@Repository、@Service、@Controller是拥有特殊语义的注释，和 @Component 是等效的，但是从注释类的命名上，很容易看出这 3 个注释分别和持久层、业务层和控制层（Web 层）相对应。 

@Component不推荐使用。

## @Named

@Named和Spring的@Component功能相同。

@Named可以有值，如果没有值生成的Bean名称默认和类名相同。

## @Configuration

等价于XML中配置beans，相当于Ioc容器。

它的某个方法头上如果注册了@Bean，就会作为这个Spring容器中的Bean，与xml中配置的bean意思一样。

## @Value

- 为了简化从properties里取配置。
  @value需要参数，这里参数可以是两种形式：
  1、@Value("#{configProperties['t1.msgname']}")这种形式的配置中有“configProperties”，其实它指定的是配置文件的加载对象。

  ```xml
  <bean id="configProperties" class="org.springframework.beans.factory.config.PropertiesFactoryBean">  
      <property name="locations">  
          <list>  
              <value>classpath:/config/t1.properties</value>  
          </list>  
      </property>  
  </bean> 
  ```

- 2、@Value("${t1.msgname}")这种形式不需要指定具体加载对象，这时候需要一个关键的对象来完成PreferencesPlaceholderConfigurer

  ```xml
  <bean id="propertyConfigurer" class="org.springframework.beans.factory.config.PreferencesPlaceholderConfigurer">  
      <property name="location">  
      <value>config/t1.properties</value>  
      </property>  
  </bean> 
  ```



## @Scope

配置bean的作用域，默认是单例模式，即@Scope("singleton")。

singleton：单例，即容器里只有一个实例对象。

prototype：多对象，每一次请求都会产生一个新的bean实例，Spring不无法对一个prototype bean的整个生命周期负责，容器在初始化、配置、装饰或者是装配完一个prototype实例后，将它交给客户端，由程序员负责销毁该对象，不管何种作用域，容器都会调用所有对象的初始化生命周期回调方法，而对prototype而言，任何配置好的析构生命周期回调方法都将不会被调用。

request：对每一次HTTP请求都会产生一个新的bean，同时该bean仅在当前HTTP request内有效。

## @PostConstruct、 @PreDestory

实现初始化和销毁bean之前进行的操作，只能有一个方法可以用此注释进行注释，方法不能有参数，返回值必需是void,方法需要是非静态的。

@PostConstruct：在构造方法和init方法（如果有的话）之间得到调用，且只会执行一次。

@PreDestory：注解的方法在destory()方法调用后得到执行。

Spring 容器中的 Bean 是有生命周期的，Spring 允许在 Bean 在初始化完成后以及 Bean 销毁前执行特定的操作，常用的设定方式有以下三种：

- 通过实现 `InitializingBean/DisposableBean`接口来定制初始化之后/销毁之前的操作方法；
- 通过 `<bean>` 元素的` init-method/destroy-method` 属性指定初始化之后/销毁之前调用的操作方法；
- 在指定方法上加上@PostConstruct或@PreDestroy注解来制定该方法是在初始化之后还是销毁之前调用。

但他们之前并不等价。即使3个方法都用上了，也有先后顺序。

Constructor > @PostConstruct >InitializingBean > init-method

## @Primary

自动装配时当出现多个Bean候选者时，被注解为@Primary的Bean将作为首选者，否则将抛出异常。

## @Lazy(true)

用于指定该Bean是否取消预初始化，用于注解类，延迟初始化。

## @Autowired

对类成员变量、方法及构造函数进行标注，完成自动装配的工作。 通过 @Autowired的使用来消除 set ，get方法。

@Autowired默认先按byType，如果发现找到多个bean，则，又按照byName方式比对，如果还有多个，则报出异常。

- 可以手动指定按byName方式注入，使用@Qualifier。
- 如果要允许null 值，可以设置它的required属性为false。

## @Resource

- @Autowired与@Resource都可以用来装配bean. 都可以写在字段上,或写在setter方法上。
- @Autowired默认按类型装配（这个注解是属与spring的）。
- @Resource 是JDK1.6支持的注解，是jsr330规范，默认按照名称进行装配，名称可以通过name属性进行指定，如果没有指定name属性，当注解写在字段上时，默认取字段名，按照名称查找，如果注解写在setter方法上默认取属性名进行装配。当找不到与名称匹配的bean时才按照类型进行装配。但是需要注意的是，如果name属性一旦指定，就只会按照名称进行装配。

## @Qualifier

限定描述符除了能根据名字进行注入，更能进行更细粒度的控制如何选择候选者

## @Inject

使用@Inject需要引用javax.inject.jar，它与Spring没有关系，是jsr330规范。

与@Autowired有互换性

## @Async

通过在方法上设置@Async注解，可使得方法被异步调用。也就是说调用者会在调用时立即返回，而被调用方法的实际执行是交给Spring的TaskExecutor来完成。

## @Singleton

在类上加上这个注解，可以实现一个单例类，不需要自己手动编写单例实现类。

## @Valid、@Validated

Spring Validation验证框架对参数的验证机制提供了@Validated（Spring's JSR-303规范，是标准JSR-303的一个变种），javax提供了@Valid（标准JSR-303规范），配合BindingResult可以直接提供参数验证结果。

在检验Controller的入参是否符合规范时，使用@Validated或者@Valid在基本验证功能上没有太多区别。但是在分组、注解地方、嵌套验证等功能上两个有所不同：

- @Validated：用在方法入参上无法单独提供嵌套验证功能。不能用在成员属性（字段）上，也无法提示框架进行嵌套验证。能配合嵌套验证注解@Valid进行嵌套验证。
- @Valid：用在方法入参上无法单独提供嵌套验证功能。能够用在成员属性（字段）上，提示验证框架进行嵌套验证。能配合嵌套验证注解@Valid进行嵌套验证。

## @RequestMapping

用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径；用于方法上，表示在类的父路径下追加方法上注解中的地址将会访问到该方法，此处需注意@RequestMapping用在类上可以没有，但是用在方法上必须有。

## @PathVariable

用来获取请求路径（url ）中的动态参数。

## @ResponseBody

用于将Controller的方法返回的对象，通过适当的HttpMessageConverter转换为指定格式后，写入到Response对象的body数据区。 

## @RequestBody

将 HTTP 请求正文插入方法中，使用适合的 HttpMessageConverter 将请求体写入某个对象。

有个默认属性required,默认是true,当body里没内容时抛异常。

**注意：需要使用ajax方式请求时，必需使用POST方式提交参数，不能用Fiddler去模拟post请求。**

## @RequestParam

作用是提取和解析请求中的参数。@RequestParam支持类型转换，类型转换目前支持所有的基本Java类型。

## @PathVariable、@RequestHeader、@CookieValue、@RequestParam、 @RequestBody、@SessionAttributes、 @ModelAttribute

@PathVariable：处理requet uri部分,当使用@RequestMapping URI template 样式映射时， 即someUrl/{paramId}, 这时的paramId可通过 @Pathvariable注解绑定它传过来的值到方法的参数上。

@RequestHeader，@CookieValue: 处理request header部分的注解。

@RequestParam,  @RequestBody: 处理request body部分的注解。

@SessionAttributes,@ModelAttribute：处理attribute类型是注解。

## @CrossOrigin

解决跨域访问的问题，在Spring4.2以上的版本可直接使用。在类上或方法上添加该注解。

## @ResponseStatus

@ResponseStatus用于修饰一个类或者一个方法，修饰一个类的时候，一般修饰的是一个异常类,当处理器的方法被调用时，@ResponseStatus指定的code和reason会被返回给前端。value属性是http状态码，比如404，500等。reason是错误信息。

当修改类或方法时，只要该类得到调用，那么value和reason都会被添加到response里。

## @RestController

@RestController = @Controller + @ResponseBody。

是2个注解的合并效果，即指定了该controller是组件，又指定方法返回的是String或json类型数据，不会解释成jsp页面，注定不够灵活，如果一个Controller即有SpringMVC返回视图的方法，又有返回json数据的方法即使用@RestController太死板。

灵活的作法是：定义controller的时候，直接使用@Controller，如果需要返回json可以直接在方法中添加@ResponseBody。

## @ControllerAdvice

把@ControllerAdvice注解内部使用@ExceptionHandler、@InitBinder、@ModelAttribute注解的方法应用到所有的 @RequestMapping注解的方法。非常简单，不过只有当使用@ExceptionHandler最有用，另外两个用处不大。

## @Retention、@Target、@Document、@Inherited

元注解，指注解的注解。

@Retention: 定义注解的保留策略

@Target：定义注解的作用目标

@Document：说明该注解将被包含在javadoc中

@Inherited：说明子类可以继承父类中的该注解

## @GetMapping、@PostMapping

@GetMapping(value = "page")等价于@RequestMapping(value = "page", method = RequestMethod.GET)

@PostMapping(value = "page")等价于@RequestMapping(value = "page", method = RequestMethod.POST)

## @profile

是spring提供的一个用来标明当前运行环境的注解。

在spring使用DI来依赖注入的时候，能够根据当前制定的运行环境来注入相应的bean。最常见的就是使用不同的DataSource。

如果使用的是main函数进行真正的开发、测试和上线时，我们需要设置一下运行参数：-Dspring.profile.active=...

如果是在web的后台项目中,可通过xml的方式进行设置：

```xml
<context-param>  
  <param-name>spring.profiles.active</param-name>  
  <param-value>DOUBLEUPMINT</param-value>  
</context-param>  
```