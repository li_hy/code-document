# spring架包

**spring-aop**

*Proxy-based AOP support*

基于代理的AOP支持

**spring-aspects**

*AspectJ based aspects*

集成AspectJ

**spring-beans**

*Beans support, including Groovy*

beans的支持，包括Groovy

**spring-context**

*Application context runtime, including scheduling and remoting abstractions*

应用程序上下文运行时，包括调度和远程抽象，即IoC容器

**spring-context-support**

*Support classes for integrating common third-party libraries into a Spring application context*

支持将公共第三方库集成到Spring应用程序上下文中的类


**spring-core**

*Core utilities, used by many other Spring modules*

核心实用程序，由其他Spring模块使用

**spring-expression**

*Spring Expression Language (SpEL)*

spring表达式基础

**spring-instrument**

*Instrumentation agent for JVM bootstrapping*

提供一些类级的工具支持和ClassLoader级的实现，用于服务器

**spring-instrument-tomcat**

*Instrumentation agent for Tomcat*

针对tomcat的instrument实现

**spring-jdbc**

*JDBC support package, including DataSource setup and JDBC access support*

JDBC支持包, 包括数据源设置和JDBC访问支持

**spring-jms**

*JMS support package, including helper classes to send/receive JMS messages*

java消息服务,JMS支持包，包括用于发送/接收JMS消息的帮助类

**spring-messaging**

*Support for messaging architectures and protocols*

支持消息传递体系结构和协议, 用于构建基于消息的应用程序

**spring orm**

*Object/Relational Mapping, including JPA and Hibernate support*

对象关系映射, 集成orm框架, 包括JPA和Hibernate支持

**spring-oxm**

*Object/XML Mapping*

对象/ XML映射

**spring-test**

*Support for unit testing and integration testing Spring components*

支持单元测试和集成测试Spring组件

**spring-tx**

*Transaction infrastructure, including DAO support and JCA integration*

事务基础架构, 包括DAO支持和JCA集成

**spring-web**

*Foundational web support, including web client and web-based remoting*

基础web支持, 包括web客户端和基于web的远程访问

**spring-webmvc**

*HTTP-based Model-View-Controller and REST endpoints for Servlet stacks*

mvc实现

**spring-webmvc-portlet**

*MVC implementation to be used in a Portlet environment*

在Portlet环境中使用的MVC实现

**spring-websocket**

*WebSocket and SockJS infrastructure, including STOMP messaging support*

WebSocket和SockJS基础设施，包括STOMP消息支持

## spring架包关系 

![image](https://docs.spring.io/spring/docs/4.3.18.BUILD-SNAPSHOT/spring-framework-reference/htmlsingle/images/spring-overview.png)

图中将spring分为5个部分：core、aop、data access、web、test，图中每个圆角矩形都对应一个jar，如果在maven中配置，所有这些jar的“groupId”都是“org.springframework”，每个jar有一个不同的“artifactId”。

## 各架包依赖关系 

### 1. core部分

```    mermaid
graph LR

spring-core--- A[commons-logging > commons-logging]

spring-beans --- B[spring-core]

spring-expression --- C[spring-core]

spring-context --- D[spring-core]

spring-context --- E[spring-beans]

spring-context --- F[spring-aop]

spring-context --- G[spring-expression]
```

*因为spring-core依赖了commons-logging，而其他模块都依赖了spring-core，所以整个spring框架都依赖了commons-logging，如果有自己的日志实现如log4j，可以排除对commons-logging的依赖，没有日志实现而排除了commons-logging依赖，编译报错*

### 2. AOP部分 

```mermaid
graph LR

spring-aop --- A[spring-core]

spring-aop --- B[spring-beans]

spring-aop --- C[aopalliance > aopalliance]

spring-aspects ---D[org.aspectj > aspectjweaver]

spring-instrument

spring-instrument-tomcat
```



### 3.data access部分 

```mermaid
graph LR

spring-jdbc --- A[spring-core]

spring-jdbc --- B[spring-beans]

spring-jdbc --- C[spring-tx]

spring-tx --- D[spring-core]

spring-tx --- E[spring-beans]

spring-orm --- F[spring-core]

spring-orm --- G[spring-beans]

spring-orm --- H[spring-jdbc]

spring-orm --- I[spring-tx]

spring-oxm --- J[spring-core]

spring-oxm --- K[spring-beans]

spring-jms --- L[spring-core]

spring-jms --- M[spring-beans]

spring-jms --- N[spring-aop]

spring-jms --- O[spring-tx]

spring-jms --- P[spring-context]
```

### 4. web部分

```mermaid
graph LR

spring-web --- A[spring-core]

spring-web --- B[spring-beans]

spring-web --- C[spring-aop]

spring-web --- D[spring-context]

spring-webmvc --- E[spring-core]

spring-webmvc --- F[spring-beans]

spring-webmvc --- G[spring-web]

spring-webmvc --- H[spring-expression]

spring-webmvc --- I[spring-context]

spring-webmvc-portlet --- J[spring-core]

spring-webmvc-portlet --- K[spring-beans]

spring-webmvc-portlet --- L[spring-web]

spring-webmvc-portlet --- M[spring-webmvc]

spring-webmvc-portlet --- N[spring-context]

spring-websocket --- O[spring-core]

spring-websocket --- P[spring-web]

spring-websocket --- Q[spring-context]
```

### test部分

```mermaid
graph LR
spring-text --- D[spring-core]
```



### 6. support部分

```mermaid
graph LR
spring-context-support --- A[spring-core]

spring-context-support --- B[spring-beans]

spring-context-support --- C[spring-context]
```



### 7. message部分

```mermaid
graph LR
spring-messaging --- A[spring-core]

spring-messaging --- B[spring-beans]

spring-messaging --- C[spring-context]
```

