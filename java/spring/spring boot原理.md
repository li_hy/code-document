# Spring注解驱动

## spring 1.x

提供了IOC功能，通过配置文件来管理bean，会产生大量的配置文件。

> 已提供了`@ManagedResource`, `@ManagedAttribute` , `@ManagedOperation`等注解

```xml
<bean id="demoService" class="priv.lhy.mvc.DemoService"/>
```

```java
public static void main(String[] args) {
        ApplicationContext context =
                new FileSystemXmlApplicationContext("classpath:applicationContext.xml");
        System.out.println(context.getBean(DemoService.class));
        System.out.println(context.getBean("demoService"));
    }
```

## spring 2.x

引入了`@Required`，`@Repository`，`@Aspect`等注解

spring 2.5是一个重要版本，引入了`@Componet`，`@Service`，`@Controller`，`@RequestMapping`等注解

```xml
<context:component-scan base-package="priv.lhy.spring"/>
```

```java
public static void main(String[] args) {
        ApplicationContext context =
                new FileSystemXmlApplicationContext("classpath:applicationContext.xml");
        System.out.println(context.getBean(DemoV2Service.class));

    }
```

## spring 3.x

引入了`@COnfiguration` 去xml化

> 核心目的是使用更加便捷的方式把bean对象加载到spring IOC容器中

```java
@Configuration  //相等于applicationContext.xml
public class BeanConfiguration {
    @Bean   //相等于 <bean id="demoV3Service" class="..."/>
    public DemoV3Service getDemoV3Service(){
        return new DemoV3Service();
    }

    /**
     * 依赖注入，demoV3Service必须为被spring托管的bean
     * @param demoV3Service
     * @return
     */
    @Bean
    public DemoRelyService getDemoRelyService(DemoV3Service demoV3Service){
        DemoRelyService demoRelyService = new DemoRelyService();
        demoRelyService.setDemoV3Service(demoV3Service);
        return demoRelyService;
    }
}

public class DemoRelyService {
    private DemoV3Service demoV3Service;

    public DemoV3Service getDemoV3Service() {
        return demoV3Service;
    }

    public void setDemoV3Service(DemoV3Service demoV3Service) {
        this.demoV3Service = demoV3Service;
    }
}


public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(BeanConfiguration.class);
        System.out.println(context.getBean(DemoV3Service.class));
    }
}
```



引入了`@EnableXXX`注解来启动一个模块，把相关组件的bean自动装配到IOC容器中。

以`scheduled`为例：

在为使用`@EnableScheduling`之前，需要配置xml文件

```xml
<task:annotation-driven scheduler="scheduler"/>
<task:scheduler id="scheduler" pool-size="5"/>
```

spring在启动时扫描到`task:annotation-driven`后会用`AnnotationDrivenBeanDefinitionParser`类加载task

```java
//......
else {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(
                "org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor");
        builder.getRawBeanDefinition().setSource(source);
        String scheduler = element.getAttribute("scheduler");
        if (StringUtils.hasText(scheduler)) {
            builder.addPropertyReference("scheduler", scheduler);
        }
        registerPostProcessor(parserContext, builder, TaskManagementConfigUtils.SCHEDULED_ANNOTATION_PROCESSOR_BEAN_NAME);
    }
//......
```

使用`@EnableScheduling`之后

`@EnableScheduling`注解中引入了`SchedulingConfiguration.class`，`SchedulingConfiguration`类中加载了`ScheduledAnnotationBeanPostProcessor`类

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(SchedulingConfiguration.class)
@Documented
public @interface EnableScheduling {

}


@Configuration
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class SchedulingConfiguration {

	@Bean(name = TaskManagementConfigUtils.SCHEDULED_ANNOTATION_PROCESSOR_BEAN_NAME)
	@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
	public ScheduledAnnotationBeanPostProcessor scheduledAnnotationProcessor() {
		return new ScheduledAnnotationBeanPostProcessor();
	}

}
```

## spring 4.x

@Conditional 条件化装配

```java
@Configuration
public class DemoConfiguration {
    /**
    * 在注入方法上加@Conditional注解可以根据条件来决定是否加载bean
    */
    @Conditional(MyConditional.class)
    @Bean
    public DemoService demoService(){
        return new DemoService();
    }
}


/**
* 实现Condition接口，在mathces方法中增加判断逻辑
*/
public class MyConditional implements Condition {
    /**
     *
     * @param context   判断条件能使用的上下文环境
     * @param metadata  注解所在位置的注释信息
     * @return
     */
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        //TODO 判断逻辑
        if (1 == 1) {
            return true;
        } else {
            return false;
        }
    }
}
```

## spring 5.x

引入的 `@Indexed`，为注解添加索引，以提升应用启动性能。

当工程打包为 JAR 或在 IDE 工具中重新构建后，`METE-INF/spring.components` 文件将自动生成。换言之，该文件在编译时生成。当 Spring 应用上下文执行 @CompoentScan 扫描时，`METE-INF/spring.components` 将被 `CandidateComponentsIndexLoader` 读取并加载，转化为 `CandidateComponentsIndex` 对象，进而 `@CompoentScan` 不再扫描指定的 package，而是读取 `CandidateComponentsIndex` 对象，从而达到提升性能的目的。



## spring IOC bean的装载

- xml
- configuration
- enable
- 自动装配

# spring boot 特性

## 自动装配 EnableAutoConfiguration

spring动态bean的装载

- `ImportSelector`
- `Registator`

### ImportSelector

```java
/**
* 定义注解，导入importSelector类
*/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(DemoImportSelector.class)
public @interface EnableConfiguration {
}


/**
* 实现ImportSelector接口，返还加载bean的配置类路径（也可以是类的路径）
*/
public class DemoImportSelector implements ImportSelector {
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[] {
            Demo1Configuration.class.getName(),
            // 也可以是类路径
            // Demo1Service.class.getName()
            Demo2Configuration.class.getName()
        } ;
    }
}


@EnableConfiguration
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        System.out.println(context.getBean("demo1Service"));
        System.out.println(context.getBean("demo2Service"));
    }
}
```

#### 源码分析

`@SpringBootApplication`注解中包含`@EnableAutoConfiguration`

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = { @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
		@Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
public @interface SpringBootApplication{
    ......
}
```

`@EnableAutoConfiguration`注解中导入了`AutoConfigurationImportSelector`

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@AutoConfigurationPackage
@Import(AutoConfigurationImportSelector.class)
public @interface EnableAutoConfiguration {
    ......
}
```

`AutoConfigurationImportSelector`实现了`DeferredImportSelector`继而实现了`ImportSelector`

```java
public class AutoConfigurationImportSelector implements DeferredImportSelector, BeanClassLoaderAware, ResourceLoaderAware, BeanFactoryAware, EnvironmentAware, Ordered{
    
    ......
    /**
    * 获取配置
    */
    @Override
	public String[] selectImports(AnnotationMetadata annotationMetadata) {
		if (!isEnabled(annotationMetadata)) {
			return NO_IMPORTS;
		}
		AutoConfigurationMetadata autoConfigurationMetadata = AutoConfigurationMetadataLoader
				.loadMetadata(this.beanClassLoader);
		AutoConfigurationEntry autoConfigurationEntry = getAutoConfigurationEntry(autoConfigurationMetadata,
				annotationMetadata);
        //返回所有扫描到的配置类路径
		return StringUtils.toStringArray(autoConfigurationEntry.getConfigurations());
	}
    
    /**
    * 自动配置实体类
    */
    protected AutoConfigurationEntry getAutoConfigurationEntry(AutoConfigurationMetadata autoConfigurationMetadata,
			AnnotationMetadata annotationMetadata) {
		if (!isEnabled(annotationMetadata)) {
			return EMPTY_ENTRY;
		}
        //属性
		AnnotationAttributes attributes = getAttributes(annotationMetadata);
		//配置类集合
        List<String> configurations = getCandidateConfigurations(annotationMetadata, attributes);
		//去重
        configurations = removeDuplicates(configurations);
		//排查类集合
        Set<String> exclusions = getExclusions(annotationMetadata, attributes);
		//检查
        checkExcludedClasses(configurations, exclusions);
		//去掉排除类
        configurations.removeAll(exclusions);
		//过滤
        configurations = filter(configurations, autoConfigurationMetadata);
		//释放事件
        fireAutoConfigurationImportEvents(configurations, exclusions);
		return new AutoConfigurationEntry(configurations, exclusions);
	}
    
    ......
    /**
    * 配置类集合，扫描所有META-INF/spring.factories文件
    */
    protected List<String> getCandidateConfigurations(AnnotationMetadata metadata, AnnotationAttributes attributes) {
        //调用SpringFactoriesLoader获取配置类
		List<String> configurations = SpringFactoriesLoader.loadFactoryNames(getSpringFactoriesLoaderFactoryClass(),
				getBeanClassLoader());
		Assert.notEmpty(configurations, "No auto configuration classes found in META-INF/spring.factories. If you "
				+ "are using a custom packaging, make sure that file is correct.");
		return configurations;
	}
    
}
```

```java
//spring-core--5.x 下 SpringFactoriesLoader.java
public final class SpringFactoriesLoader {

	/**
	 * The location to look for factories.
	 * <p>Can be present in multiple JAR files.
	 * 自动装配扫描路径
	 */
	public static final String FACTORIES_RESOURCE_LOCATION = "META-INF/spring.factories";

    ......
    
    public static List<String> loadFactoryNames(Class<?> factoryType, @Nullable ClassLoader classLoader) {
		String factoryTypeName = factoryType.getName();
		return loadSpringFactories(classLoader).getOrDefault(factoryTypeName, Collections.emptyList());
	}

	private static Map<String, List<String>> loadSpringFactories(@Nullable ClassLoader classLoader) {
		MultiValueMap<String, String> result = cache.get(classLoader);
		if (result != null) {
			return result;
		}

		try {
			Enumeration<URL> urls = (classLoader != null ?
					classLoader.getResources(FACTORIES_RESOURCE_LOCATION) :
					ClassLoader.getSystemResources(FACTORIES_RESOURCE_LOCATION));
			result = new LinkedMultiValueMap<>();
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				UrlResource resource = new UrlResource(url);
				Properties properties = PropertiesLoaderUtils.loadProperties(resource);
				for (Map.Entry<?, ?> entry : properties.entrySet()) {
					String factoryTypeName = ((String) entry.getKey()).trim();
					for (String factoryImplementationName : StringUtils.commaDelimitedListToStringArray((String) entry.getValue())) {
						result.add(factoryTypeName, factoryImplementationName.trim());
					}
				}
			}
			cache.put(classLoader, result);
			return result;
		}
		catch (IOException ex) {
			throw new IllegalArgumentException("Unable to load factories from location [" +
					FACTORIES_RESOURCE_LOCATION + "]", ex);
		}
	}
    
    ......
}

```







## 启动依赖 Starter



## 监控 Actuator



## 客户端 Spring boot CLI