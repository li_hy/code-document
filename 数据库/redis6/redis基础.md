

# redis基本数据类型

`String`，`Hash`，`Set`，`List`，`Zset`，`Hyperloglog`，`Geo`，`Streams`。

redis是KV数据库，最外层是通过hashtable实现的，每个键值对都是一个`dictEntry`，通过指针指向key的存储结构和value的存储结构，next存储了指向下一个键值对的指针。

```c
typedef struct dicEntry {
    void *key;	//key 关键字定义
    union {
        void *val;	//value定义
        uint64_t u64;
        int64_t s64;
        double d;
    } v;
    struct dictEntry *next;	//指向下一个键值对
} dicEntry;
```

五大常用数据类型的键值对中的value都是通过`redisObject`来存储的

```c
typedef struct redisObject {
    unsigned type:4;	//对象的类型，包括OBJ_STRING、OBJ_LIST、OBJ_SET、OBJ_ZSET
    unsigned encoding:4; //具体的数据结构
    unsigned lru:LRU_BITS;	//24位，对象最后一次被命令程序访问的时间
    int refcount;	//引用计数。当refcount为0时表示该对象已经不被任何对象引用，则可以进行垃圾回收
    void *ptr; //指向对象时间的数据结构
} robj;
```



## String 字符串

可以用来存储`int(整形)`，`float(单精度浮点数)`,`string(zfc)`。

redis对外的string命令有3种不同的编码：

- `int`，存储8个字节的长整型（long，2^63-1）
- `embstr`，代表embstr格式的SDS，存储小于44个字节的字符串
- `raw`，存储大于44个字节的字符串

### SDS

reids实现了一种字符串类型--`SDS(Simple Dynamic String)`简单动态字符串，SDS又有多种结构：`sdshdr5`，`sdshdr8`，`sdshdr16`，`sdshdr32`，`sdshdr64`，用于存储不同长度的字符串，分别代码2^5=32byte， 2^8=256byte， 2^16=65536byte=64kB， 2^32=4GB。

```c
struct __attribute__ ((__packed)) sdshdr8 {
    uint8_t len;	//当前字符数组长度
    uint8_t alloc;	//当前字符数组共分配的内存大小
    unsigned char flags;//当前字符数组的属性：sdshdr8或sdshdr16等
    char buf[];	//字符串正真的值
};
```

因为C语言没有字符串类型，只能用字符数组char[]实现：

- 使用字符数组必须先给目标变量分配足够的空间，否则可能溢出
- 如果要获取字符串长度，必须遍历字符数组，时间复杂度为O(n)
- 字符串长度的变更会对字符数组做内存重分配
- 通过从字符串开始到结尾碰到的第一个`\0`来标记字符串的结束，不能保存图片、音频、视频、压缩文件等二进制内容，会被截断

SDS的特点：

- 不要担心内存溢出，如果需要会对SDS进行扩容
- 获取字符串长度的时间复杂度为O(1)，因为定义了len属性
- 通过`空间预分配(sdsMakeRoomFor)`和`惰性空间释放`，防止多次重分配内存
- 判断是否结束的标志是`len`属性，可以包含`\0`（SDS也是以`\0`结尾，这样就可以使用C语言中的函数库操作字符串的函数）

### embstr和raw

`embstr`的使用只分配遗产内存空间（因为RedisObject和SDS是连续的），而`raw`需要分配连词内存空间（分别是RedisObject和SDS分配空间）。

`embstr`和`raw`相比，`embstr`的好处在于创建时少分配一次空间，删除时少释放一次空间，以及对象的所有数据连在一起，寻找方便。

`embstr`的坏处也很明显，如果字符串的长度增加需要重新分配内存时，整个RedisObject和SDS都需要重新分配空间，因此`embstr`实现为只读（这种编码的内容是不能修改的）。

`int`和`embstr`转化为`raw`：

- `int`数据不再是整形，转化为`raw`
- `int`大小超过long的范围（2^63-1），转化为`embstr`
- `embstr`长度超过44个字节，转化为`raw`
- `embstr`对象进行修改时，转化为`raw`

redis内部编码的转换在redis写入数据时完成，且转换过程不可逆，只能从小内存编码向大内存编码转换（不包括重新set）

### 应用场景

#### 缓存

存储热点数据（网站首页，热点新闻等），显著提升访问速度

#### 分布式数据共享

分布式session等

#### 分布式锁

string类型的setnx方法，只有不存在是才能添加成功，返回true

#### 全局ID

INT类型，INCRBY方法，利用原子性

#### 计数器

INT类型，INCR方法

例如文章的阅读量，微博点赞数，运行一段的颜色，先写入redis再定时同步到数据库

#### 限流

INT类型，INCR方法

以访问者的IP和其他信息作为可以，访问一次增加一次计数，超过次数则返回false

## Hash 哈希

`hash`用来存储多个无序的键值对，最大存储数量2^32-1（40亿左右）。

redis所有的KV本身就是键值对，用`dictEntry`实现，是外层的哈希，这里是内层的哈希。

`hash`的value只能是字符串，不能嵌套其他类型

`hash`和`string`的主要区别：

- 把所有相关的值聚集到一个key中，节省内存空间
- 只是有一个key，减少key冲突
- 当需要批量获取值的时候，只需要使用一个命令，减少内存/IO/CPU的消耗

`hash`不适合的场景：

- Field不能单独设置过期时间
- 需要考虑数据量分别的问题（Field非常多的时候，无法分表到多个节点）

`hash`底层使用两种数据结构实现：

- `ziplist`：OBJ_ENCODING-ZIPLIST（压缩列表）
- `hashtable`：OBJ_ENCODING_HT（哈希表）

### ziplist

`ziplist`是一个经过特殊编码的，由连续内存块组成的双向链表；它不存储指向上一个链表节点和指向下一个链表节点的指针，而是存储上一个节点长度和当前节点长度，这样读写可能会慢一些，但是可以节省内存，是一种时间换空间。

当`hash`同时满足以下两个条件的时候，使用`ziplist`编码：

- 哈希对象保存的键值对数量小于512个
- 所有的键值对的键和值的字符串长度都小于64byte

如果超过这两个阈值的任何一个，存储结构就会转化成`hashtable`

### hashtable（dict）

在redis中，`hashtable`被称为`字典（dictionary）`，redis的KV结构是通过`dictEntry`来实现的，在`hashtable`中又对`dictEntry`进行了多层封装。从最底层到最高层`dictEntry`--`dictht`--`dict`

```c
/**
* dictEntry
*/
typedef struct dictEntry {
    void *key;
    union {
        void *val;
        uint64_t u64;
        uint64_t s64;
        double d;
    } v;
    struct dictEntry *next;
} dictEntry;

/**
* dictht
*/
typedef dictht {
    dicEntry **table;	//哈希表数组
    unsigned long size;	//哈希表大小
    unsigned long sizemask;	//掩码大学，用于计算索引值。总是等于size-1
    unsigned long used;	//已有节点数
} dictht;


/**
* dict
*/
typedef struct dict {
    dictType *type;	//字典类型
    void *privdata;	//私有数据
    dictht ht[2];	//一个字典有2个哈希表
    long rehashidx;	//rehash索引
    unsigned long iterators;	//当前正在使用的迭代器数量
} dict;
```

redis的哈市默认使用的是ht[0]，ht[1]不会初始化和分配空间。

哈希表`dictht`是用链地址法来解决碰撞问题的，在这种情况下哈希表的性能取决于它的大小（size属性）和它所保存的节点的数量（user属性）之间的比率：

- 比率在1:1时（一个哈希表ht只存储一个节点entry），哈希表性能最好
- 如果节点数量比哈希表的大小要大很多的话（比例用`ratio`表示，5表示平均一个ht存储5个entry），那么哈希表会退化成多个链表，哈希表本身的性能优势就不再存在

#### rehash

如果单个哈希表的节点数量过多，哈希表的大小就需要扩容，redis中这种操作叫做`rehash`。

`rehash`的步骤：

- 为ht[1]哈希表分配空间，ht[1]的大小为第一个大于等于ht[0].userd*2的2的N次幂。比如已经使用10000，那就是16384
- 将所有的ht[0]上的节点rehash到ht[1]上，重新计算hash值和索引，然后放入指定的位置
- 当ht[0]全部迁移到ht[1]之后，释放ht[0]的空间，将ht[1]设置为ht[0]，并创建新的ht[1]，为下次rehash做准备

#### 负载因子

```c
static int dict_can_resize = 1; //是否需要扩容
static unsigned int dict_force_resize_ratio = 5; //扩容因子
```

### 应用场景

string可以做的事，hash都可以做

#### 存储对象类型的数据

比如对象或者一张表的数据，比string节省了更多的空间，也更加便于集中管理

#### 购物车

key：用户id；field：商品id；value：商品数量

+1：hincr

-1：hdecr

删除：hincrby key field -1

全选：hgetall

商品数：hlen

## List 列表

存储有序的字符串（从左到右），元素可以重复，最大存储数量为2^32-1（40亿左右）

早期版本中，数据量较小是用`ziplist`存储，达到临界值是转换为`linkedlist`进行存储，分别对应`OBJ_ENCODING_ZIPLIST`和`OBJ_ENCODING_LINKEDLIST`。

3.2版本后统一用`quicklist`来存储。`quicklist`存储了一个双向链表，每个节点都是一个`ziplist`，所以是`ziplist`和`linkedlist`的结合体。

```c
typedef struct quicklist {
    quicklistNode *head;	//指向双向列表的表头
    quicklistNode *tail;	//指向双向列表的表尾
    unsigned long count;	//所有的ziplist中一共存了多少个元素
    unsigned long len;		//双向链表的长度，node的数量
    int fill : QL_FILL_BITS;	//ziplist最大大小，对应list-max-ziplist-size
    unsigned int compress : QL_COMP_BITS;	//压缩深度，对应list-compress-depth
    unsigned int bookmark_count : QL_BM_BITS;	//4位，bookmarks数组的大小
    quicklistBookmark bookmarks[];	//bookmarks是一个可选字段，quicklist重新分配内存空间时使用，不使用是不占空间
} quicklist;

typedef struct quicklistNode {
    struct quicklistNode *prev;	//指向前一个节点
    struct quicklistNode *next;	//指向后一个节点
    unsigned char *zl;	//指向实际的ziplist
    unsigned int sz;	//当前ziplist占用多少字节
    unsigned int count;	//当前ziplist中存储了多少个元素，占16bit，最大65536个
    unsigned int encoding : 2;	//是否采用LZF压缩缩放压缩节点；RAW==1,LZF==2
    unsigned int container : 2;	//存储结构；NONE==1,ZIPLIST==2
    unsigned int recompress : 1;	//当前ziplist是不是已经被解压出来做临时使用
    unsigned int attempted_compress : 1;	//测试用
    unsigned int extra : 10;	//预留
} quicklistNode;
```

### 应用场景

#### 列表

例如用户的消息列表，网站的公告列表，活动列表，博客的文章列表，评论列表等

#### 队列/栈

当作分布式环境的队列/栈使用。

list提供了两个阻塞的弹出操作：`blpop`和`brpop`，可以设置超时时间（单位秒）

## Set 集合

set存储string类型的无序集合，最大存储数量2^32-1

redis用`inset`或`hashtable`存储set。如果元素都是整数类型，就用`inset`存储，否则用`hashtable`存储，如果元素个数超过512个（配置`set-max-intset-entries 512`），也会使用`hashtable`存储

### 应用场景

#### 抽奖

随机获取元素：spop myset

#### 点赞、签到、打卡

#### 商品标签

#### 商品筛选

差集：sdiff set1 set2

交集：sinter set1 set2

并集：sunion set1 set2

#### 用户关注、推荐模型

## Zset 有序集合

储存有序的元素，每个元素有个`score`，按照`score`从小到大排序，若`score`相同时则按照key的ASCII吗排序。

默认使用ziplist编码，在ziplist内部按照score排序递增来存储，插入时移动之后的数据。若元素数量大于等于128个（配置文件`zset-max-ziplist-entries 128`），或者任一member长度大于等于64字节（配置文件`zset-max-ziplist-value 64`）则使用skiplist+dict存储。

### skiplist 跳表

有序链表结构

![1613800077830](1613800077830.png)

skiplist结构

![1613801134806](1613801134806.png)

skiplist比普通有序链表多了层的概念，每个节点存储时会随机存在某一层；查询时会先查高层再查低层，因此不需要与链表中每个节点逐个进行比较，需要比较的节点大概只有原来的一半。

### 应用场景

应用于顺序会动态变化的列表

#### 排行榜

例如热榜、热搜

## 其他数据结构

### BitMaps

bitmaps是在字符串类型上定义的位操作。一个字节由8个二进制位组成。支持按位与、按位或等操作。

因为bit非常节省空间（1M=8388698bit），可以用来做大数据量的统计

#### 应用场景

##### 用户访问统计

##### 在线用户统计

### Hyperloglogs

提供了一种不太精确的基数统计方法，用来统计一个集合中不重复的元素个数。比如统计网站的UV，应用的日活、月活，存在一定的误差。

### Geo

存储地理位置的经纬度，可以方便的计算地理位置的距离，获取指定范围内的地理位置集合等。

### Streams

5.0推出的数据类型，支持多播的可持久化的消息队列，用于实现发布订阅功能，借鉴了kafka的设计。

# redis原理

## redis为什么快

总结起来主要有三点：

- 纯内存操作
- 请求处理单线程
- 多路复用机制

### 内存

redis是KV结构的内存数据库，时间复杂度是O(1)。

### 单线程

redis处理客户端的请求时单线程的。4.0以后还引入了一些线程处理其他事务，例如清理脏数据、无用链接的释放、大key的删除。

把处理请求的主线程设置为单线程的好处：

- 没有创建线程、销毁线程带来的消耗
- 避免了上下文切换导致的CPU消耗
- 避免了线程之间带来的竞争问题，例如加锁释放锁等等

CPU不是redis的瓶颈，redis的瓶颈最有可能是机器内存或者网络带宽。因为请求处理是单线程的，不要在生产环境允许长命令，如`keys`，`flushall`，`flushdb`等，否则会导致请求被阻塞。

### I/O多路复用

- 多路指的是多个TCP连接（Socket或Channel）

- 复用指的是复用一个或多个线程

多路复用的基本原理是不再由应用程序来监视连接，而是由内核替应用程序监视文件描述符（FD）。

客户端在操作时会产生具有不同事件类型的socket，在服务端I/O多路复用程序会把消息放入队列中，然后通过文件事件分配器转发到不同的事件处理器中。

![1613804046225](1613804046225.png)

多路复用有多种实现，以select为例，当用户进程调用了多路复用器，进程会被阻塞。内核会监视多路复用器负责的所有socket，当任何一个socket的数据准备好了，多路复用器就会返回。这时候也会进程再调用read操作，把数据从内核缓冲区拷贝到用户空间。

![1613805000683](1613805000683.png)

I/O多路复用的特点就是通过一种机制让一个进程能同时等待多个文件描述符，而这些文件描述符其中的任意一个进入读就绪（readable）状态，select()函数就可以返还。

多路复用需要操作系统的支持，redis的多路复用提供了`select`，`epoll`，`evport`，`kqueue`几种

- `evport`由solaris系统内核提供支持
- `epoll`由linux系统内核提供支持
- `kqueue`由mac系统提供支持
- `select`是posix提供的，一般的操作系统都有支持（保底方案）

## redis事务

redis的单个命令是原子性的，如果涉及到多个命令时就需要依赖redis的事务功能来实现。

redis的事务由3个特点：

- 按进入队列的顺序执行
- 不会受到其他客户端的请求影响
- 事务不能嵌套，多个multi命令效果一样

事务涉及到的命令：

- `multi`：开启事务
- `exec`：执行事务
- `discard`：取消事务
- `watch`：监视

通过`multi`命令开启事务，在`multi`执行后客户端可以继续向服务器发送任意多条命令，这些命令不会被立即执行，而是被放到一个队列中；当`exec`命令被调用时，所有队列中的命令才会被执行；如果中途不想执行事务可以调用`discard`命令清空队列，放弃执行；为防止事务过程中某个key的值被其他客户端请求修改，可以使用`watch`监视一个或多个key；如果开启事务之后至少有一个被监视的key在`exec`执行之前被修改了，那么整个事务都会被取消（key提前过期除外）；可以用`unwatch`取消。

### 事务执行可能遇到的问题

- 在执行exec之前发生错误，比如入队的命令存在语法错误，包括参数数量、参数名等（编译器错误）。

此种情况事务会拒绝执行，也就是队列中的所有命令都不会执行

- 在执行exec之后发生的错误，比如对string使用了hash命令，参数正确但数据类型错误（运行时错误）

此种情况错误命令之前的命令会被执行

redis的事务机制不能保证数据的一致

为什么不回滚

- redis命令只会因为错误的语法而失败，也就是说，从实用性角度来说，失败的命令是由代码错误造成的，而这些错误应该在开发的过程中被发现，而不应该出现在生产环境中
- 因为不需要对回滚进行支持，所以redis的内部可以保持简单且快速

## Lua脚本

lua脚本是一种轻量级脚本语言，用C语言编写，跟数据的存储过程类似

```shell
redis.call(command, key [param1, param2...])
```

- `command`：reids命令，包括set、个体、del等
- `key`：被操作的键
- `param1, param2...`：代表给key的参数

lua脚本可以放在文件中，然后执行这个文件

lua脚本可以用`script load`命令缓存到服务端，生成一个摘要码；使用`evalsha`加摘要码执行脚本

使用lua脚本需要注意脚本超时。redis的指令执行本身是单线程的，在执行lua脚本时会阻塞其他请求；可以设置脚本的超时时间，默认5秒（配置文件`lua-time-limit 5000`）。超过5秒其他客户端命令不会等待，而是直接返回“busy”错误。可以使用`script kill`终止脚本的执行。

不是所有脚本都可以kill，如果当前脚本对redis的数据进行了修改（set、del等），那么就不能通过`script kill`命令终止，只会返回“unkillable”错误，因为这样就违背了脚本原子性的目标。这种情况只能通过`shutdown nosave`命令来关闭redis。

`shutdown nosave`与`shutdown`的区别在于不会进行持久化，意味着发生在上一次快照后的数据库修改都会丢失。

## 持久化

reids提供了两种持久化方案：RDB快照（redis database）；AOF（append only file）

### RDB

RDB是redis默认的持久化方案（如果开启了AOF则优先用AOF）。当满足一定条件的时候会把当前内存中的数据写入磁盘，生成一个快照文件`dump.rdb`。redis重启会通过加载`dump.rdb`文件恢复数据。

#### RDB触发

##### 配置规则触发

redis.conf中定义了触发把数据保存到磁盘的处罚频率，如果不需要RDB方案，注释save或者配置成空字符串

```shell
save 900 1		#900秒内至少有一个key被修改（包括添加）
save 300 10		#300秒内至少有10个key被修改
save 60 1000	#60秒内至少有10000个key被修改
```

以上配置之间不冲突，满足任意一个都会触发

##### shutdown触发

关闭服务时触发，保证服务器正常关闭

##### flushall触发

rdb文件为空，无意义

#### 手动触发

redis提供2条保存快照的命令

- `save`：在生成快照时会阻塞当前redis。数据多时会造成长时间阻塞，生产环境不建议使用
- `bgsave`：后台异步进行快照操作，可以同时响应客户端请求。redis进程执行fork操作创建子进程，RDB持久化过程由子进程负责，完成后自动结束。不记录fork之后产生的数据。阻塞只发生在fork阶段，一般时间很短

#### RDB的优势和劣势

**优势**

- RDB是一个非常紧凑的文件，保存了redis在某个时间点上的数据集，非常适合用于进行备份和恢复
- 生成RDB文件的时候，redis主进程会fork()一个子进程来处理所有保存工作，主进程不需要进行任何磁盘IO操作
- RDB在恢复大数据集时的速度比AOF的回复速度要快

**劣势**

- RDB方式数据没办法做到实时持久化/秒级持久化。因为bgsave每次都要执行fork操作创建子进程，频繁执行成本过高
- 在一定间隔时间做一次备份，redis意外down掉就会丢失最后一次快照之后的所有修改

### AOF

AOF采用日志的形式来记录每个写操作，并追加到文件中，默认不开启。redis重启时会根据日志文件的内容把写指令从前到后执行一次以完成数据的恢复

AOF配置

```shell
appendonly no	#开关，修改为yes即开启
appendfilename "appendonly.aof"	#文件名
```

由于操作系统的缓存机制，AOF数据并没有正真写入硬盘，而是进入例如系统的硬盘缓存，可配置`appendfsync ieverysec`来设置写入频率：

- `no`：不执行fsync，由操作系统保证数据同步到磁盘，速度最快，但是不太安全
- `always`：每次写入都执行fsync，效率很低
- `everysec`：每秒执行一次sfync，可能会丢失1秒数据，默认选项，兼顾安全性和效率

当AOF文件的大小超过所设定的阈值时，redis会启动重写机制来对AOF文件的内容进行压缩，只保留可以恢复数据的最小指令集。也可以使用`bgrewriteaof`命令来手动压缩。

AOF文件重新并不是对原文件进行重新整理，而是直接读取服务器现有的键值对，然后用一条命令去代替之前记录这个键值对的多条命令，生成一个新的文件去替换原来的AOF文件。

```shell
#aof重写触发机制
auto-aof-rewrite-percentage 100	#当aof文件大小超过上一次重写文件大小的百分之多少进行重写，默认100即2倍
auto-aof-rewrite-min-size 64mb #允许重写的最小aof文件大小，避免达到百分百但尺寸仍然很小的情况，默认64M
```

当子进程在执行AOF重写时，主进程需要执行三个工作：

- 处理命令请求
- 将写命令追加到现有的AOF文件中
- 将写命令追加到AOF重写缓存中

这样可以解决在AOF重写时AOF文件被更改的问题

#### AOF的优势和劣势

**优点**

- AOF持久化的方法提供了多种同步频率，即使是一默认的同步频率每秒一次，redis最多也就丢失1秒的数据

**缺点**

- 对于具有相同数据的redis，AOF文件通常会比RDF文件体积更大
- 虽然AOF提供了多种同步的频率，在高并发的情况下，RDB比AOF具有更好的性能保证

## 内存回收

### 过期策略

**立即过期（主动淘汰）**

每个设置过期时间的key都需要创建一个定时器，到期立即清除。该策略可以立即清除过期的数据，对内存很友好；但会占用大量的CPU资源去处理过期的数据，从而影响缓存的响应时间和吞吐量

**惰性过期（被动淘汰）**

当访问一个key时才会判断该key是否过期，过期则清除。该策略最大化的节省CPU资源，却对内存非常不友好。极端情况可能出现大量的过期key没有再次被访问，从而不会被清除，占用大量内存

**定期过期**

每隔一段时间会扫描一定数量的数据库的expires字典中一定数量的key，并清除其中已过期的key。该策略是前两者的一个折中方案。通过调整定时扫描的时间间隔和每次扫描的限定耗时，可以在不同情况下使用CPU和内存资源达到最优的平衡效果

redis中同时使用了惰性过期和定期过期两种策略，并不是实时的清除过期key

### 淘汰策略

redis的内存淘汰策略是指当内存使用达到最大内存极限时，需要使用淘汰算法来决定清理掉那些数据，以保证新数据的存入

```shell
#最大内存设置
#maxmemory <bytes>
```

如果不设置maxmemory或者设置为0，32位系统最多使用3GB内存，64位系统不限制内存

三种淘汰策略：

-  LRU（Least Recently Used）最近最少使用，判断最近被使用的时间，目前最远的数据优先被淘汰
-  LFU（Least Frequently Used）最不常用，按照使用频率删除，4.0版本新增
- random 随机删除

| 策略            | 含义                                                         |
| --------------- | ------------------------------------------------------------ |
| volatile-lru    | 根据LRU算法删除设置了超时属性（expire）的键，直到腾出足够的内存，如果没有可删除对象则退到noeviction策略 |
| allkeys-lru     | 根据LRU算法删除，不管数据有没有设置超时属性，直到腾出足够内存 |
| volatile-lfu    | 在带有过期时间的键中选择最不常用的                           |
| allkeys-lfu     | 在带有过期时间的键中选择最不常用的，不管数据有没有设置超时属性 |
| volatile-random | 在带有过期时间的键中随机选择                                 |
| allkeys-random  | 是随机删除所有键，直到腾出足够内存位置                       |
| volatile-ttl    | 根据键值对象的ttl属性删除最近将要过期数据，如果没有则退到noeviction策略 |
| noeviction      | 么默认策略，不会删除任何数据，拒绝所有写入操作并返回客户端错误信息，此时redis只响应读操作 |

## 发布订阅模式

redis可以通过list的rpush和blpop实现消息队列（队尾进队头出），没有任何元素可以弹出的时候连接会被阻塞。但是基于list实现的消息队列不支持一对多的消息分发，相对于只有一个消费者。

如果要实现一对多的消息分发，可以使用redis的发布订阅功能。

#### 订阅频道

消息的生产者和消费者是不同的客户端，连接到同一个redis服务。redis通过`channel（频道）`将两者关联起来（在rabbitMQ中是`queue`，kafka中是`topic`）。

订阅者可以订阅一个或多个`channel`，消息的发布者可以给指定的`channel`发布消息。只要有消息到达了`channel`，所有订阅了这个`channel`的订阅者都会受到这条消息。

```shell
#订阅频道（可以订阅多个）
subscribe channel-1 channel-2 channel-3

#发布消息（不支持一次向多个频道发送消息）
publish channel-l hello

#取消订阅(不能在redis-cli中使用，因为redis-cli运行订阅后客户端处于阻塞模式)
unsubscribe channel-1
```

### 按规则（pattern）订阅频道

支持`?`和`*`占位符，`?`代表一个字符，`*`代表0个或多个字符

考虑到性能和持久化的因素，不建议使用redis的发布订阅功能来实现MQ。redis的一些内部机制用到了发布订阅功能。

# redis实战

## redis客户端

### 客户端和服务端通信

redis默认监听6379端口，可以通过TCP方式建立连接。

服务端约定了一种特殊的消息格式，每个命令都是以`\r\n`结尾，叫`Redis Serialization Protocol(RESP，redis序列化协议)`，发送和接收消息都需要按这种格式编码。

使用redis-cli连接redis，执行`set name ccc`和`get name`命令，对其进行抓包可以看到

![image-20210221123328638](image-20210221123328638.png)

```shell
#redis命令
set name ccc

#发送数据包
*3\r\n$3\r\nset\r\n$4\r\nname\r\n$3\r\nccc\r\n
```



![image-20210221123600933](image-20210221123600933.png)

![image-20210221123623780](image-20210221123623780.png)

```shell
#redis命令
get name

#发送数据包
*2\r\n$3\r\nget\r\n$4\r\nname\r\n

#接收数据包
$3\r\nccc\r\n
```

原理就是把命令、参数和长度用`\r\n`连接起来，以`set name ccc`为例：

- `*3`表示3个参数
- `$3`表示第一个参数的长度是3
- `set`第一个参数（redis的set命令）
- `$4`表示第二个参数的长度是4
- `name`第二个参数（key）
- `$3`表示第三个参数的长度是3
- `ccc`第三个参数（value）



### 常用客户端

官方推荐的java客户端有3个：`Jedis`，`Redisson`和`Luttuce`

- jedis：体积小，但功能很完善
- lettuce：高级客户端，支持线程安全、异步、反应式编程、支持集群、哨兵、pipeline、编解码
- redisson：基于redis服务实现的java分布式可扩展的数据结构

spring操作redis提供了一个模板方法`RedisTemplate`，是对其他现有的客户端再次封装；并定义了一个连接工厂接口`RedisConnectionFactory`，这个接口有很多实现，例如：`JedisConnectionFactory`，`JredisConnectionFactory`，`LettuceConnectionFactory`，`SrpConnectionFactory`。

spring boot 2.x版本之前，`RedisTemplate`默认使用Jedis，2.x版本之后默认使用Lettuce。

#### Jedis

jedis是最常用的客户端，但在多线程下使用一个连接是线程不安全的。可以使用连接池为美国请求创建不同的连接，Jedis的连接池有三种实现：`JedisPool`，`ShardedJedisPool`，`JedisSentinelPool`。

#### Lettuce



#### Redisson





## 数据一致性

## 高并发问题