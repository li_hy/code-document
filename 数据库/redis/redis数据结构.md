

# redis数据结构

reids可以存储键与5种不同数据结构类型之间的映射，分别为

- string（字符串）
- list（列表）
- set（集合）
- hash（散列）
- zset（有序集合）

## 字符串类型

redis中最基本的数据类型，能存储任何形式的字符串，包括二进制数据。一个字符类型键允许存储的最大容量是512M。

### 内部数据结构

string类型通过int、SDS（simple dynamic string）作为结构存储。int用来存放整型数据，sds存放字节/字符串和浮点型数据。

### 操作

- 可对整个字符串或字符串的其中一部分执行操作；

- 对整数和浮点数执行自增（increment）或自减（decrement）操作。

## 列表类型

列表类型（list）可以存储一个有序的字符串列表。

列表类型内部使用双向链表实现，使用向列表两端添加元素的时间复杂度为O(1)，获取越接近两端的元素速度就越快。

### 内部数据结构

redis3.2之前，list类型的value对象内部以linkedlist或者ziplist来实现，当list的元素个数和单个元素的长度比较小的时候，redis会采用ziplist（压缩列表）来实现用以减少内存占用。否则就会采用linkedlist（双向链表）结构。

这两种存储方式都有优缺点：

- 双向链表在链表两端进行push和pop操作，在插入阶段上复杂度比较低，但是内存开销比较大；

- ziplist存储在一段连续的内存上，所以存储效率很高，但是插入和删除都需要频繁申请和释放内存。

redis3.2之后，采用quicklist来存储list，列表的底层都由quicklist实现。

quicklist仍然是一个双向链表，只是列表的每个阶段都是一个ziplist，其实就是linkedlist和ziplist的结合，quicklist中每个节点ziplist都能够存储多个数据元素。

### 操作

- 从链表的两端推入（push）或弹出元素（pop）；
- 根据偏移量对链表进行修剪（trim）；
- 读取单个或多个元素；
- 根据值查找或者移除元素。

## hash类型

包含键值对的无序散列表；整体可看作一个对象，每一个field-value相对于对象的属性和属性值。

### 数据结构

hash提供两种结果来存储，一种是hashtable，另一种是ziplist。数据量小的时候用ziplist。

hash表分为三层：

- dicEntry		
  - 管理一个key-value，同时保留同一个桶中相邻元素的指针，用来维护hash桶内部链
- dictht              
  - 实现一个hash表会使用一个buckets存放dicEntry的地址，一般情况下通过hash(key)%len得到的值就是buckets的索引，这个值决定了要将此dictEntry节点放入buckets的哪个索引里，这个buckets实际上就是hash表
- dict                 
  - dictht实际上就是hash表的核心，但是只要一个dictht还不够，比如rehash、遍历hash等操作，所以redis定义了一个叫dict的结构以支持字典的各种操作，当dictht需要扩容/缩容时，用来管理dictht的迁移

### 操作

- 添加、获取、移除单个键值对；
- 获取所有键值对。

## 集合类型

集合类型中，每个元素都是不同的，也就是不能有充分数据，同时集合类型中的数据是无序的。一个集合类型键可以存储至多2^32-1个。

集合类型和列表类型最大的区别是有序性和唯一性。

集合类型的常用操作是向集合中插入或删除元素、判断某个元素是否存在，由于集合类型在redis内部是使用的值为空的散列表（hash table），所以这些操作的时间复杂度都是O(1)。

### 内部数据结构

set在底层数据结构以intset或者hashtable来存储。当set中只包含整数型的元素时，采用intset来存储，否则采用hashtable存储，但是对于set来说，该hashtable的calue值为null，通过key来存储元素。

### 操作

- 添加、获取、移除单个元素；
- 检测一个元素是否存在与集合中；
- 计算交集、并集、差集；
- 从集合里随机获取元素。

## 有序集合类型

和上文所说的集合类型的区别就是多了有序的功能。

在集合类型的基础上，有序集合类型为集合中的每个元素都关联了一个分数，这使得我们不仅可以完成插入、删除和判断元素是否存在等级和类型支持的操作，还能获得分数最高（或最低）的前N个元素、获得指定分数范围内的元素等与分数有关的操作。虽然集合中每个元素都是不同的，但是他们的分数却可以相同。

### 数据结构

zset类型的数据结构比较复杂，内部是以ziplist或者skiplist+hashtable来实现，最核心的一个结构就是skiplist，也就是跳跃表。

![跳跃表结构](跳跃表结构.png)

### 操作

- 添加、获取、移除单个元素；
- 根据分值范围（range）或者成员来获取元素

