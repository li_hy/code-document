MyBatis工作流程

MyBatis启动时首先需要去解析配置文件，包括全局配置文件和映射器配置文件，里面包含了如何控制MyBatis的行为和对数据库下达的指令，也就是SQL信息。将其解析成一个`Configuration`对象。

接着就是对数据库接口的操作，接口位于应用程序和数据库之间，代表跟数据库之间的一次连接，即`SqlSession`对象。

要获得`SqlSession`对象，必须有一个会话工厂`SqlSessionFactory`，`SqlSessionFactory`里必须包含所有的配置信息，通过`SqlSessionFactoryBuidler`来创建工厂类。

`SqlSession`持有一个`Executor`对象，用来封装对数据库的操作。在执行器`Executor`执行query或者update操作时会创建一系列的对象来处理参数（`ParameterHandler`）、执行SQL、处理结果集（`ResultSetHandler`），MyBaits将其封装成一个对象`StatementHandler`。

![mybatis工作流程](mybatis工作流程.png)

# MyBaits架构分层与模块划分

![MyBaits架构](MyBaits架构.png)

## 接口层

接口层的核心对象是`SqlSession`，它是上层应用和MyBaits的桥梁，`SqlSession`上定义了非常多的对数据库的操作方法。接口层在接收到调用请求时会调用核心处理层的相应模块来完成具体的数据库操作。

## 核心处理层

核心处理层主要作用：

- 把接口中传入的参数解析并且映射成JDBC 类型
- 解析xml 文件中的SQL 语句，包括插入参数，和动态SQL 的生成
- 执行SQL 语句
- 处理结果集，并映射成Java 对象

插件也属于核心层，这是由它的工作方式和拦截的对象决定的。

## 基础支撑层

基础支持层主要是一些抽取出来的通用的功能（实现复用），用来支持核心处理层的功能。比如数据源、缓存、日志、xml 解析、反射、IO、事务等等这些功能。

## MyBatis核心对象

| 对象             | 相关对象                                                     | 作用                                                         |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Configuration    | MapperRegistry、 TypeAliasRegistry、 TypeHandlerRegistry     | 包含了MyBatis 的所有的配置信息                               |
| SqlSession       | SqlSessionFactory、 DefaultSqlSession                        | 对操作数据库的增删改查的API 进行了封装，提供给应用层使用     |
| Executor         | BaseExecutor、 SimpleExecutor、 BatchExecutor、 ReuseExecutor | MyBatis 执行器，是MyBatis 调度的核心，负责SQL 语句的生成和查询缓存的维护 |
| StatementHandler | BaseStatementHandler、 SimpleStatementHandler、 PreparedStatementHandler 、CallableStatementHandler | 封装了JDBC Statement 操作，负责对JDBC statement 的操作，如设置参数、将Statement 结果集转换成List 集合 |
| ParameterHandler | DefaultParameterHandler                                      | 把用户传递的参数转换成JDBC Statement 所需要的参数            |
| ResultSetHandler | DefaultResultSetHandler                                      | 把JDBC 返回的ResultSet 结果集对象转换成List 类型的集合       |
| MapperProxy      | MapperProxyFactory                                           | 代理对象，用于代理Mapper 接口方法                            |
| MappedStatement  | SqlSource、 BoundSql                                         | MappedStatement 维护了一条<select                            |



# 源码解读

## 配置解析过程

配置解析的过程全部只解析了两种文件。一个是mybatis-config.xml 全局配置文件。另外就是可能有很多个的Mapper.xml 文件，也包括在Mapper 接口类上面定义的注解。

```java
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
```

首先new 了一个`SqlSessionFactoryBuilder`，非常明显的建造者模式，它里面定义了很多个build 方法的重载，最终返回的是一个`SqlSessionFactory `对象（单例模式）。

第二步是调用另一个`build()`方法，返回`DefaultSqlSessionFactory`。

```java
public SqlSessionFactory build(InputStream inputStream, String environment, Properties properties) {
    try {
      XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, environment, properties);
        //调用parser.parse()方法，返回一个Configuration类
      return build(parser.parse());
    } catch (Exception e) {
      throw ExceptionFactory.wrapException("Error building SqlSession.", e);
    } finally {
      ErrorContext.instance().reset();
      try {
        inputStream.close();
      } catch (IOException e) {
        // Intentionally ignore. Prefer previous error.
      }
    }
}

//返回DefaultSqlSessionFactory
public SqlSessionFactory build(Configuration config) {
    return new DefaultSqlSessionFactory(config);
  }
```

这里面创建了一个`XMLConfigBuilder` 对象（`Configuration `对象也是这个时候创建的）。

### XMLConfigBuilder

`XMLConfigBuilder `是抽象类`BaseBuilder `的一个子类，专门用来解析全局配置文
件，针对不同的构建目标还有其他的一些子类，比如：

- `XMLMapperBuilder`：解析Mapper 映射器
- `XMLStatementBuilder`：解析增删改查标签

这里有两步，第一步是调用`parser.parse()`方法，它会返回一个`Configuration`类。配置文件里面所有的信息都会放在Configuration 里面。

```java
public Configuration parse() {
    //检查是否已解析过
    if (parsed) {
      throw new BuilderException("Each XMLConfigBuilder can only be used once.");
    }
    parsed = true;
    //解析配置文件
    parseConfiguration(parser.evalNode("/configuration"));
    return configuration;
}

//解析xml配置文件，xml节点顺序不能改变
private void parseConfiguration(XNode root) {
    try {
      //issue #117 read properties first
      propertiesElement(root.evalNode("properties"));
      Properties settings = settingsAsProperties(root.evalNode("settings"));
      loadCustomVfs(settings);
      loadCustomLogImpl(settings);
      typeAliasesElement(root.evalNode("typeAliases"));
      pluginElement(root.evalNode("plugins"));
      objectFactoryElement(root.evalNode("objectFactory"));
      objectWrapperFactoryElement(root.evalNode("objectWrapperFactory"));
      reflectorFactoryElement(root.evalNode("reflectorFactory"));
      settingsElement(settings);
      // read it after objectFactory and objectWrapperFactory issue #631
      environmentsElement(root.evalNode("environments"));
      databaseIdProviderElement(root.evalNode("databaseIdProvider"));
      typeHandlerElement(root.evalNode("typeHandlers"));
      mapperElement(root.evalNode("mappers"));
    } catch (Exception e) {
      throw new BuilderException("Error parsing SQL Mapper Configuration. Cause: " + e, e);
    }
}
```

#### propertiesElement()

解析`<properties>`标签，读取我们引入的外部配置文件。这里面又有两种类型，一种是放在`resource `目录下的，是相对路径，一种是写的绝对路径的。解析的最终结果就是把所有的配置信息放到名为defaults 的Properties 对象里面，最后把XPathParser 和Configuration 的Properties 属性都设置成填充后的Properties对象。

#### settingsAsProperties()

将`<settings>`标签解析成了一个Properties 对象。

#### loadCustomVfs(settings)

获取Vitual File System 的自定义实现类，比如要读取本地文件，或者FTP 远程文件的时候，就可以用到自定义的VFS 类。根据`<settings>`标签里面的`<vfsImpl>`标签，生成了一个抽象类VFS 的子类，并且赋值到Configuration中。

#### loadCustomLogImpl(settings)

根据`<logImpl>`标签获取日志的实现类，可以用到很多的日志的方案，包括LOG4J，LOG4J2，SLF4J 等等。这里生成了一个Log 接口的实现类，并且赋值到Configuration 中。

#### typeAliasesElement()

解析`<typeAliases>`标签，有两种定义方式，一种是直接定义一个类的别名，一种就是指定一个包，那么这个package 下面所有的类的名字就会成为这个类全路径的别名。类的别名和类的关系，会放在一个`TypeAliasRegistry `对象里面。

#### pluginElement()

解析`<plugins>`标签，比如Pagehelper 的翻页插件，或者自定义的插件。`<plugins>`标签里面只有`<plugin>`标签，`<plugin>`标签里面只有`<property>`标签。

标签解析完以后，会生成一个Interceptor 对象，并且添加到Configuration 的`InterceptorChain `属性里面，它是一个List。

#### bjectFactoryElement()、objectWrapperFactoryElement()

`<objectFactory>` 和`<objectWrapperFactory>` 这两个标签， 分别生成`ObjectFactory `、`ObjectWrapperFactory `对象，同样设置到Configuration 的属性里面。

#### reflectorFactoryElement()

解析`<reflectorFactory> `标签，生成`ReflectorFactory `对象

#### settingsElement(settings)

对`<settings>`标签里面所有子标签的处理，在`settingsAsProperties()`里已经把子标签全部转换成了Properties 对象，所以在这里处理Properties 对象就可以了。

二级标签里面有很多的配置，比如二级缓存，延迟加载，自动生成主键等等。MyBatis的所有`setting`的值都是在这赋值的，可以在这里查看默认值。所有的值，都会赋值到Configuration 的属性里面去。

#### environmentsElement()

解析`<environments>`标签，一个environment 就是对应一个数据源，这里会根据配置的`<transactionManager>`创建一个事务工厂，根据`<dataSource>`标签创建一个数据源，最后把这两个对象设置成Environment 对象的属性，放到Configuration 里面。

#### databaseIdProviderElement()

解析`<databaseIdProvider>` 标签，生成`DatabaseIdProvider `对象（用来支持不同厂商的数据库）。

#### typeHandlerElement()

跟TypeAlias 一样，TypeHandler 有两种配置方式，一种是单独配置一个类，一种是指定一个package。最后得到的是JavaType 和JdbcType，以及用来做相互映射的TypeHandler 之间的映射关系。最后存放在TypeHandlerRegistry 对象里面。

#### mapperElement()

解析`<mappers>`标签，将接口与接口代理工厂保存到`MapperRegistry`中。

##### 判断

判断是不是接口，只有接口才解析；然后判断是不是已经注册了，单个Mapper重复注册会抛出异常。

##### 注册

`mapperElement()`最后会调用

```java
XMLMapperBuilder mapperParser = new XMLMapperBuilder(inputStream, configuration, resource, configuration.getSqlFragments());
mapperParser.parse();
```

`XMLMapperBuilder.parse()`方法，是对Mapper 映射器的解析。里面有两个方法：

- `configurationElement()`：解析所有的子标签， 其中`buildStatementFromContext()`最终获得`MappedStatement `对象。
- `bindMapperForNamespace()`：把namespace（接口类型）和工厂类绑定起来。

无论是按package 扫描，还是按接口扫描，最后都会调用到`MapperRegistry `的`addMapper()`方法。

`MapperRegistry `里面维护的其实是一个Map 容器，存储接口和代理工厂的映射关系。

##### 处理注解

除了映射器文件，在这里也会去解析Mapper 接口方法上的注解。在`addMapper()`方法里面创建了一个`MapperAnnotationBuilder`

```java
MapperAnnotationBuilder parser = new MapperAnnotationBuilder(config, type);
parser.parse();
```

```java
public void parse() {
    String resource = type.toString();
    if (!configuration.isResourceLoaded(resource)) {
      loadXmlResource();
      configuration.addLoadedResource(resource);
      assistant.setCurrentNamespace(type.getName());
      //处理@CacheNamespace
      parseCache();
      //处理@CacheNamespaceRef
      parseCacheRef();
      Method[] methods = type.getMethods();
      for (Method method : methods) {
        try {
          // issue #237
          if (!method.isBridge()) {
            //处理@Options，@SelectKey，@ResultMap等
            parseStatement(method);
          }
        } catch (IncompleteElementException e) {
          configuration.addIncompleteMethod(new MethodResolver(this, method));
        }
      }
    }
    parsePendingMethods();
}
```

`parseCache()` 和`parseCacheRef() `方法其实是对`@CacheNamespace` 和`@CacheNamespaceRef` 这两个注解的处理。

`parseStatement()`方法里面的各种`getAnnotation()`，都是对注解的解析，比如`@Options`，`@SelectKey`，`@ResultMap` 等等。

最后同样会解析成`MappedStatement `对象，也就是说在XML 中配置，和使用注解配置，最后起到一样的效果。

##### 收尾

如果注册没有完成，还要从Map 里面remove 掉。

```java
// MapperRegistry.java
finally {
    if (!loadCompleted) {
    	knownMappers.remove(type);
}
```

最后，`MapperRegistry `也会放到`Configuration `里面去。

### 总结

在配置解析过程中，主要完成了config 配置文件、Mapper 文件、Mapper 接口上的注解的解析。得到了一个最重要的对象Configuration，这里面存放了全部的配置信息，它在属性里面还有各种各样的容器。最后，返回了一个`DefaultSqlSessionFactory`，里面持有了`Configuration `的实例。

## 会话创建过程

在跟数据库的每一次连接时， 都需要创建一个会话， 使用openSession()方法来创建。

```java
//DefaultSqlSessionFactory.java
public SqlSession openSession() {
    return openSessionFromDataSource(configuration.getDefaultExecutorType(), null, false);
}

private SqlSession openSessionFromDataSource(ExecutorType execType, TransactionIsolationLevel level, boolean autoCommit) {
    Transaction tx = null;
    try {
      final Environment environment = configuration.getEnvironment();
      //TransactionFactory有2个实现类JdbcTransactionFactory，ManagedTransactionFactory
      final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
      tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);
      final Executor executor = configuration.newExecutor(tx, execType);
      //最终返回DefaultSqlSession，属性包括Configuration、Executor 对象
      return new DefaultSqlSession(configuration, executor, autoCommit);
    } catch (Exception e) {
      closeTransaction(tx); // may have fetched a connection so lets call close()
      throw ExceptionFactory.wrapException("Error opening session.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
}
```

这个会话里面，需要包含一个`Executor `用来执行SQL。`Executor `又要指定事务类型和执行器的类型。

先从`Configuration `里面拿到`Enviroment`，`Enviroment `里面就有事务工厂。

### 创建Transaction

| 属性    | 产生工厂类                | 产生事务           |
| ------- | ------------------------- | ------------------ |
| JDBC    | JdbcTransactionFactory    | JdbcTransaction    |
| MANAGED | ManagedTransactionFactory | ManagedTransaction |

如果配置的是JDBC，则会使用Connection 对象的commit()、rollback()、close()管理事务。

如果配置成MANAGED，会把事务交给容器来管理，比如JBOSS，Weblogic。

如果是Spring + MyBatis ， 则没有必要配置， 可以直接在applicationContext.xml 里面配置数据源和事务管理器，覆盖MyBatis 的配置。

### 创建Executor

```java
//Configuration.java
public Executor newExecutor(Transaction transaction, ExecutorType executorType) {
    executorType = executorType == null ? defaultExecutorType : executorType;
    executorType = executorType == null ? ExecutorType.SIMPLE : executorType;
    Executor executor;
    if (ExecutorType.BATCH == executorType) {
      executor = new BatchExecutor(this, transaction);
    } else if (ExecutorType.REUSE == executorType) {
      executor = new ReuseExecutor(this, transaction);
    } else {
      executor = new SimpleExecutor(this, transaction);
    }
    //启用缓存
    if (cacheEnabled) {
      executor = new CachingExecutor(executor);
    }
    //调用插件对executor进行包装
    executor = (Executor) interceptorChain.pluginAll(executor);
    return executor;
}
```



Executor 的基本类型有三种：`SIMPLE`、`BATCH`、`REUSE`，默认是`SIMPLE`（`settingsElement()`读取默认值），他们都继承了抽象类`BaseExecutor`。

![Executor类图](Executor类图.png)

- `SimpleExecutor`

  每执行一次update 或select，就开启一个Statement 对象，用完立刻关闭Statement 对象

- `ReuseExecutor`

  执行update 或select，以sql 作为key 查找Statement 对象，存在就使用，不存在就创建，用完后，不关闭Statement 对象，而是放置于Map 内，供下一次使用。简言之，就是重复使用Statement 对象。

- `BatchExecutor`

  执行update（没有select，JDBC 批处理不支持select），将所有sql 都添加到批处理中（`addBatch()`），等待统一执行（`executeBatch()`），它缓存了多个Statement 对象，每个Statement 对象都是`addBatch()`完毕后，等待逐一执行`executeBatch()`批处理。与JDBC 批处理相同。

如果配置了`cacheEnabled=ture`，会用装饰器模式对executor 进行包装：`new CachingExecutor(executor)`。

包装完毕后，会执行：

```java
executor = (Executor) interceptorChain.pluginAll(executor);
```

此处会对executor 进行包装。

最终返回`DefaultSqlSession`，属性包括Configuration、Executor 对象。

### 总结

创建会话的过程，最终获得了一个`DefaultSqlSession`，里面包含了一个`Executor`，它是SQL 的执行者。

## 获得Mapper对象

之前已经有一个`DefaultSqlSession `，必须找到Mapper.xml 里面定义的`Statement ID`，才能执行对应的SQL 语句。

找到Statement ID 有两种方式：

- 是直接调用session 的方法，在参数里面传入Statement ID

  ```java
  Blog blog = (Blog) session.selectOne("com.test.mapper.BlogMapper.selectBlogById", 1);
  ```

  - 属于硬编码，修改复杂
  - 如果参数传入错误，在编译阶段也是不会报错的，不利于预先发现问题

- 定义一个接口，然后再调用Mapper 接口的方法

  ```java
  BlogMapper mapper = session.getMapper(BlogMapper.class);
  ```

  - 由于接口名称跟Mapper.xml 的namespace 是对应的，接口的方法跟statement ID 也都是对应的，所以根据方法就能找到对应的要执行的SQL。

`DefaultSqlSession `的`getMapper()`方法，调用了`Configuration `的`getMapper()`方法。

```java
//DefaultSqlSession.java
public <T> T getMapper(Class<T> type) {
    return configuration.getMapper(type, this);
}
```

`Configuration `的`getMapper()`方法，又调用了`MapperRegistry `的`getMapper()`方法。

```java
//Configuration.java
public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    return mapperRegistry.getMapper(type, sqlSession);
}
```

在解析mapper 标签和Mapper.xml 的时候已经把接口类型和类型对应的`MapperProxyFactory `放到了一个Map 中。获取Mapper 代理对象，实际上是从Map 中获取对应的工厂类后，调用`mapperProxyFactory.newInstance(sqlSession)`创建对象

```java
//MapperRegistry.java
public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    final MapperProxyFactory<T> mapperProxyFactory = (MapperProxyFactory<T>) knownMappers.get(type);
    if (mapperProxyFactory == null) {
      throw new BindingException("Type " + type + " is not known to the MapperRegistry.");
    }
    try {
      return mapperProxyFactory.newInstance(sqlSession);
    } catch (Exception e) {
      throw new BindingException("Error getting mapper instance. Cause: " + e, e);
    }
}
```

最终通过代理模式返回代理对象

```java
//MapperProxyFactory.java
protected T newInstance(MapperProxy<T> mapperProxy) {
    return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
}

public T newInstance(SqlSession sqlSession) {
    final MapperProxy<T> mapperProxy = new MapperProxy<>(sqlSession, mapperInterface, methodCache);
    return newInstance(mapperProxy);
}
```

### JDK动态代理和MyBatis用到的JDK动态代理的区别

#### JDK动态代理

JDK 动态代理代理，在实现了`InvocationHandler `的代理类里面，需要传入一个被代理对象的实现类。

![JDK动态代理](JDK动态代理.png)

#### MyBatis的动态代理

只需要根据接口类型+方法的名称，就可以找到Statement ID 了，所以不需要实现类。在MapperProxy
里面直接执行逻辑（也就是执行SQL）就可以。

![MyBatis的动态代理](MyBatis的动态代理.png)

### 总结

获得Mapper 对象的过程，实质上是获取了一个`MapperProxy `的代理对象。`MapperProxy `中有`sqlSession`、`mapperInterface`、`methodCache`。

## 执行SQL

由于所有的Mapper 都是`MapperProxy `代理对象，所以任意的方法都是执行`MapperProxy` 的`invoke()`方法。

### MapperProxy.invoke()

```java
// MapperProxy.java
public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
      //不执行Object方法
      if (Object.class.equals(method.getDeclaringClass())) {
        return method.invoke(this, args);
      } else if (isDefaultMethod(method)) {//java8接口中的default方法不处理
        return invokeDefaultMethod(proxy, method, args);
      }
    } catch (Throwable t) {
      throw ExceptionUtil.unwrapThrowable(t);
    }
    // 获取缓存，保存了方法签名和接口方法的关系
    final MapperMethod mapperMethod = cachedMapperMethod(method);
    return mapperMethod.execute(sqlSession, args);
}

private MapperMethod cachedMapperMethod(Method method) {
    return methodCache.computeIfAbsent(method, k -> new MapperMethod(mapperInterface, method, sqlSession.getConfiguration()));
}
```

首先判断是否需要去执行SQL，还是直接执行方法。Object 本身的方法和Java 8 中接口的默认方法不需要去执行SQL。

然后获取缓存。这里加入缓存是为了提升`MapperMethod `的获取速度。

Map 的`computeIfAbsent()`方法：只有key 不存在或者value 为null 的时候才调用`mappingFunction()`。

### MapperMethod.execute()

`MapperMethod` 里面主要有两个属性， 一个是`SqlCommand `， 一个是`MethodSignature`，这两个都是`MapperMethod `的内部类。另外`MapperMethod` `定义了多个execute()方法。

根据不同的type 和返回类型：

- 调用`convertArgsToSqlCommandParam()`将参数转换为SQL 的参数。
- 调用`sqlSession `的`insert()`、`update()`、`delete()`、`selectOne ()`方法

### DefaultSqlSession.selectOne()

`selectOne()`最终也是调用了`selectList()`。

```java
//DefaultSqlSession.java
public <T> T selectOne(String statement) {
    return this.selectOne(statement, null);
}

@Override
public <T> T selectOne(String statement, Object parameter) {
    // Popular vote was to return null on 0 results and throw exception on too many.
    List<T> list = this.selectList(statement, parameter);
    if (list.size() == 1) {
      return list.get(0);
    } else if (list.size() > 1) {
      throw new TooManyResultsException("Expected one result (or null) to be returned by selectOne(), but found: " + list.size());
    } else {
      return null;
    }
}
```

在`SelectList()`中，先根据command name（Statement ID）从Configuration中拿到`MappedStatement`，这个ms 上面有xml 中配置的所有属性，包括id、statementType、sqlSource、useCache、入参、出参等等。

然后执行了Executor 的query()方法。

```java
public <E> List<E> selectList(String statement, Object parameter, RowBounds rowBounds) {
    try {
      MappedStatement ms = configuration.getMappedStatement(statement);
      return executor.query(ms, wrapCollection(parameter), rowBounds, Executor.NO_RESULT_HANDLER);
    } catch (Exception e) {
      throw ExceptionFactory.wrapException("Error querying database.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
}
```

如果启用了二级缓存，就会先调用`CachingExecutor `的`query()`方法，里面有缓存相关的操作，然后才是再调用基本类型的执行器，比如默认的`SimpleExecutor`。

在没有开启二级缓存的情况下，先会走到`BaseExecutor `的`query()`方法（否则会先走到`CachingExecutor`）。

### BaseExecutor.query()

```java
// BaseExecutor.java
public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException {
    BoundSql boundSql = ms.getBoundSql(parameter);
    //创建缓存key
    CacheKey key = createCacheKey(ms, parameter, rowBounds, boundSql);
    return query(ms, parameter, rowBounds, resultHandler, key, boundSql);
}

@SuppressWarnings("unchecked")
  @Override
  public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    ErrorContext.instance().resource(ms.getResource()).activity("executing a query").object(ms.getId());
    if (closed) {
      throw new ExecutorException("Executor was closed.");
    }
    //queryStack 用于记录查询栈，防止递归查询重复处理缓存
    if (queryStack == 0 && ms.isFlushCacheRequired()) {
      	//清空一级缓存
        clearLocalCache();
    }
    List<E> list;
    try {
      queryStack++;
      list = resultHandler == null ? (List<E>) localCache.getObject(key) : null;
      if (list != null) {
        handleLocallyCachedOutputParameters(ms, key, parameter, boundSql);
      } else {
        //查询数据库
        list = queryFromDatabase(ms, parameter, rowBounds, resultHandler, key, boundSql);
      }
    } finally {
      queryStack--;
    }
    if (queryStack == 0) {
      //从缓存读取
      for (DeferredLoad deferredLoad : deferredLoads) {
        deferredLoad.load();
      }
      // issue #601
      deferredLoads.clear();
      //配置文件中设置LocalCacheScope=STATEMENT则清空缓存
      if (configuration.getLocalCacheScope() == LocalCacheScope.STATEMENT) {
        // issue #482
        clearLocalCache();
      }
    }
    return list;
}
```

#### 创建CacheKey

从`Configuration `中获取`MappedStatement`， 然后从`BoundSql` 中获取SQL 信息，创建`CacheKey`。这个`CacheKey `就是缓存的Key。

然后再调用另一个`query()`方法。

#### 清空本地缓存

`queryStack `用于记录查询栈，防止递归查询重复处理缓存。

`flushCache=true` 的时候，会先清理本地缓存（一级缓存）：`clearLocalCache()`

如果没有缓存，会从数据库查询：`queryFromDatabase()`

如果`LocalCacheScope == STATEMENT`，会清理本地缓存。

#### 从数据库查询

先在缓存用占位符占位。执行查询后，移除占位符，放入数据。

执行`Executor `的`doQuery()`；默认是`SimpleExecutor`。

#### SimpleExecutor.doQuery()

##### 创建StatementHandler

```java
// SimpleExecutor.java
public <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
    Statement stmt = null;
    try {
      Configuration configuration = ms.getConfiguration();
      StatementHandler handler = configuration.newStatementHandler(wrapper, ms, parameter, rowBounds, resultHandler, boundSql);
      stmt = prepareStatement(handler, ms.getStatementLog());
      return handler.query(stmt, resultHandler);
    } finally {
      closeStatement(stmt);
    }
}
```

在`configuration.newStatementHandler()`中，new 一个`StatementHandler`，先得到`RoutingStatementHandler`。

```java
//Configuration.java
public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
    StatementHandler statementHandler = new RoutingStatementHandler(executor, mappedStatement, parameterObject, rowBounds, resultHandler, boundSql);
    statementHandler = (StatementHandler) interceptorChain.pluginAll(statementHandler);
    return statementHandler;
  }
```

``RoutingStatementHandler` 里面没有任何的实现， 是用来创建基本的`StatementHandler `的。这里会根据`MappedStatement `里面的`statementType `决定`StatementHandler `的类型。默认是`PREPARED `（ `STATEMENT `、`PREPARED `、`CALLABLE`）。

```java
//RoutingStatementHandler.java
public RoutingStatementHandler(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {

    switch (ms.getStatementType()) {
      case STATEMENT:
        delegate = new SimpleStatementHandler(executor, ms, parameter, rowBounds, resultHandler, boundSql);
        break;
      case PREPARED:
        delegate = new PreparedStatementHandler(executor, ms, parameter, rowBounds, resultHandler, boundSql);
        break;
      case CALLABLE:
        delegate = new CallableStatementHandler(executor, ms, parameter, rowBounds, resultHandler, boundSql);
        break;
      default:
        throw new ExecutorException("Unknown statement type: " + ms.getStatementType());
    }

}
```

`StatementHandler `里面包含了处理参数的`ParameterHandler` 和处理结果集的`ResultSetHandler`。

这两个对象都是在上面new 的时候由父类`BaseStatementHandler`创建的。

```java
//PreparedStatementHandler.java
public PreparedStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
    super(executor, mappedStatement, parameter, rowBounds, resultHandler, boundSql);
  }
```

```java
//BaseStatementHandler.java
protected BaseStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
    this.configuration = mappedStatement.getConfiguration();
    this.executor = executor;
    this.mappedStatement = mappedStatement;
    this.rowBounds = rowBounds;

    this.typeHandlerRegistry = configuration.getTypeHandlerRegistry();
    this.objectFactory = configuration.getObjectFactory();

    if (boundSql == null) { // issue #435, get the key before calculating the statement
      generateKeys(parameterObject);
      boundSql = mappedStatement.getBoundSql(parameterObject);
    }

    this.boundSql = boundSql;
	//创建parameterHandler
    this.parameterHandler = configuration.newParameterHandler(mappedStatement, parameterObject, boundSql);
    //创建resultSetHandler
    this.resultSetHandler = configuration.newResultSetHandler(executor, mappedStatement, rowBounds, parameterHandler, resultHandler, boundSql);
}
```

这三个对象都是可以被插件拦截的四大对象之一，所以在创建之后都要用拦截器进行包装的方法。

```java
//Configuration.java
public ParameterHandler newParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
    ParameterHandler parameterHandler = mappedStatement.getLang().createParameterHandler(mappedStatement, parameterObject, boundSql);
    //使用拦截器包装
    parameterHandler = (ParameterHandler) interceptorChain.pluginAll(parameterHandler);
    return parameterHandler;
  }

  public ResultSetHandler newResultSetHandler(Executor executor, MappedStatement mappedStatement, RowBounds rowBounds, ParameterHandler parameterHandler,
      ResultHandler resultHandler, BoundSql boundSql) {
    ResultSetHandler resultSetHandler = new DefaultResultSetHandler(executor, mappedStatement, parameterHandler, resultHandler, boundSql, rowBounds);
    //使用拦截器包装
    resultSetHandler = (ResultSetHandler) interceptorChain.pluginAll(resultSetHandler);
    return resultSetHandler;
  }

  public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
    StatementHandler statementHandler = new RoutingStatementHandler(executor, mappedStatement, parameterObject, rowBounds, resultHandler, boundSql);
    //使用拦截器包装
    statementHandler = (StatementHandler) interceptorChain.pluginAll(statementHandler);
    return statementHandler;
}

  public Executor newExecutor(Transaction transaction) {
    return newExecutor(transaction, defaultExecutorType);
  }

  public Executor newExecutor(Transaction transaction, ExecutorType executorType) {
    executorType = executorType == null ? defaultExecutorType : executorType;
    executorType = executorType == null ? ExecutorType.SIMPLE : executorType;
    Executor executor;
    if (ExecutorType.BATCH == executorType) {
      executor = new BatchExecutor(this, transaction);
    } else if (ExecutorType.REUSE == executorType) {
      executor = new ReuseExecutor(this, transaction);
    } else {
      executor = new SimpleExecutor(this, transaction);
    }
    if (cacheEnabled) {
      executor = new CachingExecutor(executor);
    }
    //使用拦截器包装
    executor = (Executor) interceptorChain.pluginAll(executor);
    return executor;
  }
```

##### 创建Statement

用new 出来的`StatementHandler `创建`Statement `对象——`prepareStatement()`方法对语句进行预编译，处理参数。

```java
// SimpleExecutor.java
private Statement prepareStatement(StatementHandler handler, Log statementLog) throws SQLException {
    Statement stmt;
    Connection connection = getConnection(statementLog);
    stmt = handler.prepare(connection, transaction.getTimeout());
    //预编译
    handler.parameterize(stmt);
    return stmt;
}
```

##### 执行的StatementHandler 的query()方法

`RoutingStatementHandler `的`query()`方法。

delegate 委派，最终执行`PreparedStatementHandler `的`query()`方法。

```java
//RoutingStatementHandler.java
public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
    return delegate.query(statement, resultHandler);
  }
```

```java
//PreparedStatementHandler.java
public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
    PreparedStatement ps = (PreparedStatement) statement;
    ps.execute();
    return resultSetHandler.handleResultSets(ps);
  }
```

##### 执行PreparedStatement 的execute()方法

这里就是JDBC 包中的`PreparedStatement `的执行。

##### ResultSetHandler 处理结果集

`ResultSetHandler `只有一个实现类： `DefaultResultSetHandler` 。也就是执行`DefaultResultSetHandler `的`handleResultSets ()`方法。

```java
//DefaultResultSetHandler.java
public List<Object> handleResultSets(Statement stmt) throws SQLException {
    ErrorContext.instance().activity("handling results").object(mappedStatement.getId());

    final List<Object> multipleResults = new ArrayList<>();

    int resultSetCount = 0;
    //获取第一个结果集
    ResultSetWrapper rsw = getFirstResultSet(stmt);

    List<ResultMap> resultMaps = mappedStatement.getResultMaps();
    int resultMapCount = resultMaps.size();
    validateResultMapsCount(rsw, resultMapCount);
    while (rsw != null && resultMapCount > resultSetCount) {
      ResultMap resultMap = resultMaps.get(resultSetCount);
      handleResultSet(rsw, resultMap, multipleResults, null);
      rsw = getNextResultSet(stmt);
      cleanUpAfterHandlingResultSet();
      resultSetCount++;
    }

    //获取多个结果集
    String[] resultSets = mappedStatement.getResultSets();
    if (resultSets != null) {
      while (rsw != null && resultSetCount < resultSets.length) {
        ResultMapping parentMapping = nextResultMaps.get(resultSets[resultSetCount]);
        if (parentMapping != null) {
          String nestedResultMapId = parentMapping.getNestedResultMapId();
          ResultMap resultMap = configuration.getResultMap(nestedResultMapId);
          handleResultSet(rsw, resultMap, null, parentMapping);
        }
        rsw = getNextResultSet(stmt);
        cleanUpAfterHandlingResultSet();
        resultSetCount++;
      }
    }

    return collapseSingleResultList(multipleResults);
}


private void handleResultSet(ResultSetWrapper rsw, ResultMap resultMap, List<Object> multipleResults, ResultMapping parentMapping) throws SQLException {
    try {
      if (parentMapping != null) {
        handleRowValues(rsw, resultMap, null, RowBounds.DEFAULT, parentMapping);
      } else {
        if (resultHandler == null) {
          DefaultResultHandler defaultResultHandler = new DefaultResultHandler(objectFactory);
          handleRowValues(rsw, resultMap, defaultResultHandler, rowBounds, null);
          multipleResults.add(defaultResultHandler.getResultList());
        } else {
          handleRowValues(rsw, resultMap, resultHandler, rowBounds, null);
        }
      }
    } finally {
      // issue #228 (close resultsets)
      closeResultSet(rsw.getResultSet());
    }
  }
```

首先会先拿到第一个结果集，如果没有配置一个查询返回多个结果集的情况，一般只有一个结果集。如果不走下面一个while 循环，就是执行一次。

然后会调用`handleResultSet()`方法。

# MyBatis插件

Mybaits的插件使用代理模式对拦截的对象进行修改，通过责任链模式将插件形成一个链路，实现层层拦截。

## MyBatis可拦截的对象及方法

- `Executor`：上层的对象，SQL 执行全过程，包括组装参数，组装结果集返回和执行SQL 过程

  | 可拦截的方法    | 方法作用                                                     |
  | --------------- | ------------------------------------------------------------ |
  | update          | 执行update、insert、delete 操作                              |
  | query           | 执行query 操作                                               |
  | flushStatements | 在commit 的时候自动调用，SimpleExecutor、ReuseExecutor、BatchExecutor 处理不同 |
  | commit          | 提交事务                                                     |
  | rollback        | 事务回滚                                                     |
  | getTransaction  | 获取事务                                                     |
  | close           | 结束（关闭）事务                                             |
  | isClosed        | 判断事务是否关闭                                             |

- `StatementHandler`：执行SQL 的过程，最常用的拦截对象

  | 可拦截的方法 | 方法作用                          |
  | ------------ | --------------------------------- |
  | prepare      | （BaseSatementHandler）SQL 预编译 |
  | parameterize | 设置参数                          |
  | batch        | 批处理                            |
  | update       | 增删改操作                        |
  | query        | 查询操作                          |

- `ParameterHandler`：SQL 参数组装的过程

  | 可拦截的方法       | 方法作用 |
  | ------------------ | -------- |
  | getParameterObject | 获取参数 |
  | setParameters      | 设置参数 |

- `ResultSetHandler`：返回结果的组装

  | 可拦截的方法           | 方法作用         |
  | ---------------------- | ---------------- |
  | handleResultSets       | 处理结果集       |
  | handleOutputParameters | 处理存储过程出参 |

## 插件编写与注册

（基于spring-mybatis）运行自定义的插件，需要3 步，以PageHelper 为例：

- 编写自定义的插件类

  - 实现Interceptor 接口

  - 添加@Intercepts({@Signature()})，指定拦截的对象和方法、方法参数方法名称+参数类型，构成了方法的签名，决定了能够拦截到哪个方法

  - 实现接口的3 个方法

    ```java
    // 用于覆盖被拦截对象的原有方法（在调用代理对象Plugin 的invoke()方法时被调用）
    Object intercept(Invocation invocation) throws Throwable;
    // target 是被拦截对象，这个方法的作用是给被拦截对象生成一个代理对象，并返回它
    Object plugin(Object target);
    // 设置参数
    void setProperties(Properties properties);
    ```

- 插件注册，在mybatis-config.xml 中注册插件

- 插件登记

  - MyBatis 启动时扫描`<plugins>` 标签， 注册到`Configuration `对象的`InterceptorChain `中。`property `里面的参数，会调用`setProperties()`方法处理。

## 插件代理和拦截的实现

### 四大核心对象创建代理的时间

- `Executor `是`openSession()` 的时候创建的
- `StatementHandler `是`SimpleExecutor.doQuery()`创建的
- `StatementHandler `里面包含了处理参数的`ParameterHandler `和处理结果集的`ResultSetHandler `的创建，创建之后即调用`InterceptorChain.pluginAll()`，返回层层代理后的对象

### 代理的调用顺序

多个插件的情况下，代理是可以被继续代理的；代理的的调用顺序与代理的定义顺序相反

![代理的调用顺序](代理的调用顺序.png)

### 代理对象

代理对象是在`Plugin`类中创建的，在`wrap`方法中对原始对象进行了代理。在重写的`plugin() `方法里面可以直接调用`return Plugin.wrap(target, this);`返回代理对象。

```java
//Plugin.java
public static Object wrap(Object target, Interceptor interceptor) {
    Map<Class<?>, Set<Method>> signatureMap = getSignatureMap(interceptor);
    Class<?> type = target.getClass();
    Class<?>[] interfaces = getAllInterfaces(type, signatureMap);
    if (interfaces.length > 0) {
      return Proxy.newProxyInstance(
          type.getClassLoader(),
          interfaces,
          new Plugin(target, interceptor, signatureMap));
    }
    return target;
}
```

最后调用Plugin 的`invoke()`方法。它先调用了拦截器的`intercept()`方法。可以通过`invocation.proceed()`调用到被代理对象被拦截的方法。

```java
//Plugin.java
public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
      Set<Method> methods = signatureMap.get(method.getDeclaringClass());
      if (methods != null && methods.contains(method)) {
        return interceptor.intercept(new Invocation(target, method, args));
      }
      return method.invoke(target, args);
    } catch (Exception e) {
      throw ExceptionUtil.unwrapThrowable(e);
    }
}
```

```java
//被拦截对象的相关信息
public class Invocation {

  private final Object target;	//被拦截对象
  private final Method method;	//被拦截的方法
  private final Object[] args;	//被拦截的方法参数

  public Invocation(Object target, Method method, Object[] args) {
    this.target = target;
    this.method = method;
    this.args = args;
  }

  public Object getTarget() {
    return target;
  }

  public Method getMethod() {
    return method;
  }

  public Object[] getArgs() {
    return args;
  }

  //调用被拦截方法
  public Object proceed() throws InvocationTargetException, IllegalAccessException {
    return method.invoke(target, args);
  }

}
```

### 插件调用流程

![插件调用流程](插件调用流程.png)

| 对象           | 作用                                                      |
| -------------- | --------------------------------------------------------- |
| Interceptor    | 自定义插件需要实现接口，实现4 个方法                      |
| InterceptChain | 配置的插件解析后会保存在Configuration 的InterceptChain 中 |
| Plugin         | 用来创建代理对象，包装四大对象                            |
| Invocation     | 对被代理类进行包装，可以调用proceed()调用到被拦截的方法   |

## 应用场景

- 水平分表
  - 对query update 方法进行拦截；在接口上添加注解，通过反射获取接口注解，根据注解上配置的参数进行分表，修改原SQL，例如id 取模，按月分表
- 数据加解密
  - update——加密；query——解密；获得入参和返回值
- 菜单权限控制
  - 对query 方法进行拦截；在方法上添加注解，根据权限配置，以及用户登录信息，在SQL 上加上权限过滤条件

# Mybaits与Spring整合

## 关键配置

以传统的xml配置为例，使用配置类`@configuration`效果与之相同。

引入Mybatis和Spring整合包（mybatis 的版本和mybatis-spring 的版本有兼容关系）

```xml
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis-spring</artifactId>
    <version>2.0.0</version>
</dependency>
```

在spring的`applicationContext.xml`里配置`SqlSessionFactoryBean`，用来创建会话，其中还有知道全局配置文件和mapper映射器文件路径。

```xml
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <property name="configLocation" value="classpath:mybatis-config.xml"></property>
    <property name="mapperLocations" value="classpath:mapper/*.xml"></property>
    <property name="dataSource" ref="dataSource"/>
</bean>
```

在applicationContext.xml 配置需要扫描Mapper 接口的路径。

在Mybatis 里面有几种方式，第一种是配置一个`MapperScannerConfigurer`

```xml
<bean id="mapperScanner" class="org.mybatis.spring.mapper.MapperScannerConfigurer">
	<property name="basePackage" value="com.test.crud.dao"/>
</bean>
```

第二种是配置一个`<scan>`标签：

```xml
<mybatis-spring:scan base-package="com.gupaoedu.crud.dao"/>
```

还有一种就是直接用@MapperScan 注解，比如在Spring Boot 的启动类上加上一个注解：

```java
@SpringBootApplication
@MapperScan("com.test.crud.dao")
public class MybaitsApp {
    public static void main(String[] args) {
    	SpringApplication.run(MybaitsApp.class, args);
    }
}
```

## 创建会话工厂

Spring 对MyBatis 的对象进行了管理，但是并不会替换MyBatis 的核心对象。也就意味着：MyBatis.jar 包中的`SqlSessionFactory`、`SqlSession`、`MapperProxy `这些都会用到。而mybatis-spring.jar 里面的类只是做了一些包装或者桥梁的工作。

Spring 的配置文件中配置的`SqlSessionFactoryBean`

![SqlSessionFactoryBean](SqlSessionFactoryBean.png)

它实现了`InitializingBean `接口，所以要实现`afterPropertiesSet()`方法，这个方法会在bean 的属性值设置完的时候被调用。

```java
public interface InitializingBean {
	void afterPropertiesSet() throws Exception;
}
```

另外它实现了`FactoryBean `接口，所以它初始化的时候，实际上是调用`getObject()`方法，它里面调用的也是`afterPropertiesSet()`方法。

```java
public SqlSessionFactory getObject() throws Exception {
    if (this.sqlSessionFactory == null) {
      afterPropertiesSet();
    }

    return this.sqlSessionFactory;
}
```

在afterPropertiesSet()方法里面：第一步是一些标签属性的检查，接下来调用了`buildSqlSessionFactory()`方法。

在`buildSqlSessionFactory()`方法中然后定义了一个`Configuration`，叫做`targetConfiguration`。

```java
final Configuration targetConfiguration;
```

判断`Configuration `对象是否已经存在，也就是是否已经解析过。如果已经有对象，就覆盖一下属性。

如果Configuration 不存在，但是配置了`configLocation `属性，就根据`mybatis-config.xml` 的文件路径，构建一个`xmlConfigBuilder `对象。之后如果`xmlConfigBuilder `不为空，调用`xmlConfigBuilder.parse()`去解析配置文件

`Configuration `对象不存在，`configLocation `路径也没有，使用默认属性去构建去给`configurationProperties `赋值。

```java
XMLConfigBuilder xmlConfigBuilder = null;
//判断Configuration对象是否已经存在
if (this.configuration != null) {
  targetConfiguration = this.configuration;
  if (targetConfiguration.getVariables() == null) {
    targetConfiguration.setVariables(this.configurationProperties);
  } else if (this.configurationProperties != null) {
    targetConfiguration.getVariables().putAll(this.configurationProperties);
  }
} else if (this.configLocation != null) {
  //Configuration不存在，但是配置了configLocation 属性，根据mybatis-config.xml 的文件路径，构建一个xmlConfigBuilder 对象
  xmlConfigBuilder = new XMLConfigBuilder(this.configLocation.getInputStream(), null, this.configurationProperties);
  targetConfiguration = xmlConfigBuilder.getConfiguration();
} else {
  LOGGER.debug(() -> "Property 'configuration' or 'configLocation' not specified, using default MyBatis Configuration");
//Configuration 对象不存在，configLocation 路径也没有，使用默认属性去构建去给configurationProperties 赋值。
  targetConfiguration = new Configuration();
  Optional.ofNullable(this.configurationProperties).ifPresent(targetConfiguration::setVariables);
}

/**
*......
*/
//如果xmlConfigBuilder 不为空，调用xmlConfigBuilder.parse()去解析配置文件
if (xmlConfigBuilder != null) {
  try {
    xmlConfigBuilder.parse();
    LOGGER.debug(() -> "Parsed configuration file: '" + this.configLocation + "'");
  } catch (Exception ex) {
    throw new NestedIOException("Failed to parse config resource: " + this.configLocation, ex);
  } finally {
    ErrorContext.instance().reset();
  }
}
```

没有明确指定事务工厂， 默认使用`SpringManagedTransactionFactory` 。

```java
targetConfiguration.setEnvironment(new Environment(this.environment,
    this.transactionFactory == null ? new SpringManagedTransactionFactory() : this.transactionFactory,
    this.dataSource));
```

把接口和对应的`MapperProxyFactory `注册到`MapperRegistry `中

```java
xmlMapperBuilder.parse();
```

最后调用`sqlSessionFactoryBuilder.build()` 返回了一个`DefaultSqlSessionFactory`。

```java
return this.sqlSessionFactoryBuilder.build(targetConfiguration);
```

在这里就完成了第一步，根据配置文件获得一个工厂类，它是单例的，会在后面用来创建`SqlSession`。

| 接口                                | 方法                                | 作用                              |
| ----------------------------------- | ----------------------------------- | --------------------------------- |
| FactoryBean                         | getObject()                         | 返回由FactoryBean 创建的Bean 实例 |
| InitializingBean                    | afterPropertiesSet()                | bean 属性初始化完成后添加操作     |
| BeanDefinitionRegistryPostProcessor | postProcessBeanDefinitionRegistry() | 注入BeanDefination 时添加操作     |

## 创建SqlSession

在Spring 里面，不是直接使用`DefaultSqlSession `的，而是对它进行了一个封装，这个`SqlSession `的实现类就是`SqlSessionTemplate`。`DefaultSqlSession`是线程不安全的，而`SqlSessionTemplate `是线程安全的。

`SqlSessionTemplate `里面有`DefaultSqlSession `的所有的方法：`selectOne()`、`selectList()`、`insert()`、`update()`、`delete()`，不过它都是通过一个代理对象实现的。这个代理对象在构造方法里面通过一个代理类创建。

```java
public SqlSessionTemplate(SqlSessionFactory sqlSessionFactory, ExecutorType executorType,
      PersistenceExceptionTranslator exceptionTranslator) {

    notNull(sqlSessionFactory, "Property 'sqlSessionFactory' is required");
    notNull(executorType, "Property 'executorType' is required");

    this.sqlSessionFactory = sqlSessionFactory;
    this.executorType = executorType;
    this.exceptionTranslator = exceptionTranslator;
    this.sqlSessionProxy = (SqlSession) newProxyInstance(
        SqlSessionFactory.class.getClassLoader(),
        new Class[] { SqlSession.class },
        new SqlSessionInterceptor());
}
```

所有的方法都会先走到内部代理类`SqlSessionInterceptor `的`invoke()`方法。

首先会使用工厂类、执行器类型、异常解析器创建一个`sqlSession`，然后再调用`sqlSession `的实现类，实际上就是在这里调用了`DefaultSqlSession `的方法。

```java
private class SqlSessionInterceptor implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      SqlSession sqlSession = getSqlSession(
          SqlSessionTemplate.this.sqlSessionFactory,
          SqlSessionTemplate.this.executorType,
          SqlSessionTemplate.this.exceptionTranslator);
      try {
        Object result = method.invoke(sqlSession, args);
          /**
          *......
          */
}
```

## 获取SqlSessionTemplate

如果不用注入的方式，MyBatis 提供了一个`SqlSessionDaoSupport`，里面持有一个`SqlSessionTemplate `对象，并且提供了一个`getSqlSession()`方法，可以获得一个`SqlSessionTemplate`。

```java
public abstract class SqlSessionDaoSupport extends DaoSupport {
    private SqlSessionTemplate sqlSessionTemplate;
    public SqlSession getSqlSession() {
    	return this.sqlSessionTemplate;
    }
    /**
    * ......
    */
```

## 接口扫描注册

在`applicationContext.xml` 里面配置了一个`MapperScannerConfigurer`。实现了`BeanDefinitionRegistryPostProcessor `接口，`BeanDefinitionRegistryPostProcessor `是`BeanFactoryPostProcessor `的子类，可以通过编码的方式修改、新增或者删除某些Bean 的定义。

![MapperScannerConfigurer](MapperScannerConfigurer.png)

只需要重写`postProcessBeanDefinitionRegistry()`方法，在这里面操作Bean就可以了。

```java
public interface BeanDefinitionRegistryPostProcessor extends BeanFactoryPostProcessor {
	void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException;
}
```

```java
public class MapperScannerConfigurer implements BeanDefinitionRegistryPostProcessor, InitializingBean, ApplicationContextAware, BeanNameAware {
    /**
    * ......
    */
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) {
        if (this.processPropertyPlaceHolders) {
          processPropertyPlaceHolders();
        }

        ClassPathMapperScanner scanner = new ClassPathMapperScanner(registry);
        scanner.setAddToConfig(this.addToConfig);
        scanner.setAnnotationClass(this.annotationClass);
        scanner.setMarkerInterface(this.markerInterface);
        scanner.setSqlSessionFactory(this.sqlSessionFactory);
        scanner.setSqlSessionTemplate(this.sqlSessionTemplate);
        scanner.setSqlSessionFactoryBeanName(this.sqlSessionFactoryBeanName);
        scanner.setSqlSessionTemplateBeanName(this.sqlSessionTemplateBeanName);
        scanner.setResourceLoader(this.applicationContext);
        scanner.setBeanNameGenerator(this.nameGenerator);
        scanner.registerFilters();
        //调用子类doScan方法
        scanner.scan(StringUtils.tokenizeToStringArray(this.basePackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS));
  	}
    /**
    * ......
    */
```

`ClassPathBeanDefinitionScanner`的子类`ClassPathMapperScanner`覆盖了`doScan()` 方法， 在`doScan()` 中调用了`processBeanDefinitions`

```java
//ClassPathMapperScanner.java
public Set<BeanDefinitionHolder> doScan(String... basePackages) {
    //先调用父类的doScan()扫描所有的接口。
    Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);
    if (beanDefinitions.isEmpty()) {
    	LOGGER.warn(() -> "No MyBatis mapper was found in '" + Arrays.toString(basePackages) + "' package. Please check your configuration.");
    } else {
    	processBeanDefinitions(beanDefinitions);
    }
    return beanDefinitions;
}
```

先调用父类的`doScan()`扫描所有的接口。

`processBeanDefinitions `方法里面，在注册`beanDefinitions `的时候，`BeanClass`被改为`MapperFactoryBean`。

```java
private void processBeanDefinitions(Set<BeanDefinitionHolder> beanDefinitions) {
    GenericBeanDefinition definition;
    for (BeanDefinitionHolder holder : beanDefinitions) {
    definition = (GenericBeanDefinition) holder.getBeanDefinition();
    String beanClassName = definition.getBeanClassName();
    LOGGER.debug(() -> "Creating MapperFactoryBean with name '" + holder.getBeanName()
    + "' and '" + beanClassName + "' mapperInterface");
    // the mapper interface is the original class of the bean
    // but, the actual class of the bean is MapperFactoryBean
    definition.getConstructorArgumentValues().addGenericArgumentValue(beanClassName); //issue #59
    definition.setBeanClass(this.mapperFactoryBean.getClass());
    /**
    * ......
    */
```

`MapperFactoryBean `继承了`SqlSessionDaoSupport `， 可以拿到`SqlSessionTemplate`。

## 接口注入使用

使用Mapper 的时候，只需要在加了Service 注解的类里面使用@Autowired注入Mapper 接口就好了。

```java
@Service
public class EmployeeService {
    @Autowired
    EmployeeMapper employeeMapper;
    
    public List<Employee> getAll() {
    	return employeeMapper.selectByMap(null);
    }
}
```

Spring 在启动的时候需要去实例化`EmployeeService`。

`EmployeeService `依赖了`EmployeeMapper `接口（是`EmployeeService `的一个属性）。

Spring 会根据Mapper 的名字从`BeanFactory `中获取它的`BeanDefination`，再从`BeanDefination `中获取`BeanClass `， `EmployeeMapper `对应的`BeanClass `是`MapperFactoryBean`。

`MapperFactoryBean`，实现了`FactoryBean `接口，同样是调用`getObject()`方法。

```java
// MapperFactoryBean.java
public T getObject() throws Exception {
	return getSqlSession().getMapper(this.mapperInterface);
}
```

因为`MapperFactoryBean `继承了`SqlSessionDaoSupport `， 所以这个`getSqlSession()`就是调用父类的方法，返回`SqlSessionTemplate`。

```java
// SqlSessionDaoSupport.java
public SqlSession getSqlSession() {
	return this.sqlSessionTemplate;
}
```

`SqlSessionTemplate `的`getMapper()`方法，里面又有两个方法：

```java
// SqlSessionTemplate.java
public <T> T getMapper(Class<T> type) {
	return getConfiguration().getMapper(type, this);
}

public Configuration getConfiguration() {
	return this.sqlSessionFactory.getConfiguration();
}
```

通过`DefaultSqlSessionFactory`，返回全部配置`Configuration`：

```java
// DefaultSqlSessionFactory.java
public Configuration getConfiguration() {
	return configuration;
}
```

通过工厂类`MapperProxyFactory `获得一个`MapperProxy `代理对象。

```java
// Configuration.java
public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
	return mapperRegistry.getMapper(type, sqlSession);
}
```

也就是说，注入到Service 层的接口，实际上还是一个`MapperProxy `代理对象。所以最后调用Mapper 接口的方法，也是执行`MapperProxy `的`invoke()`方法。

| 对象                             | 生命周期                                                     |
| -------------------------------- | ------------------------------------------------------------ |
| SqlSessionTemplate               | Spring 中SqlSession 的替代品，是线程安全的，通过代理的方式调用DefaultSqlSession 的方法 |
| SqlSessionInterceptor （内部类） | 代理对象，用来代理DefaultSqlSession，在SqlSessionTemplate 中使用 |
| SqlSessionDaoSupport             | 用于获取SqlSessionTemplate，只要继承它即可                   |
| MapperFactoryBean                | 注册到IOC 容器中替换接口类，继承了SqlSessionDaoSupport 用来获取SqlSessionTemplate，因为注入接口的时候，就会调用它的getObject()方法 |
| SqlSessionHolder                 | 控制SqlSession 和事务                                        |