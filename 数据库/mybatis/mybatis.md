# JDBC方式访问数据库

## JDBC方式访问数据库的过程

- 注册驱动
- 通过`DriverManager`获取一个`Connection`
- 通过`Connection`创建一个`Statement`对象
- 通过`Statement`的`execute()`方法执行SQL，返回`ResultSet`结果集
- 通过`ResultSet`获取数据，转换成POJO对象
- 关闭数据库相关资源，包括`ResultSet`，`Statement`，`Connection`，关闭顺序和打开顺序相反

```java
// 注册JDBC 驱动
Class.forName("com.mysql.jdbc.Driver");
// 打开连接
conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
// 执行查询
stmt = conn.createStatement();
String sql= "SELECT bid, name, author_id FROM blog";
ResultSet rs = stmt.executeQuery(sql);
// 获取结果集
while(rs.next()){
    int bid = rs.getInt("bid");
    String name = rs.getString("name");
    String authorId = rs.getString("author_id");
}
```

## JDBC方式的缺点

- 代码冗长
- 需要管理连接资源
- 业务逻辑与数据处理耦合
- 结果集处理代码重复

# JDBC工具

## Dbutils

DbUtils解决的最核心的问题就是结果集的映射，可以把ResultSet封装成JavaBean。

DbUtils提供了一个`QueryRunner`类，，它对数据库的增删改查的方法进行了封装，操作数据库可以直接使用它提供的方法。

在`QueryRunner `的构造函数里面，我们又可以传入一个数据源，比如使用`Hikari`连接池，这样就不需要再去写各种创建和释放连接的代码了。

在DbUtils 里面提供了一系列的支持泛型的`ResultSetHandler`，只要在DAO 层调用`QueryRunner `的查询方法，传入这个Handler，它就可以自动把结果集转换成实体类Bean 或者List 或者Map。

```java
queryRunner = new QueryRunner(dataSource);
String sql = "select * from blog";
List<BlogDto> list = queryRunner.query(sql, new BeanListHandler<>(BlogDto.class));
```



## Spring JDBC

Spring 也对原生的JDBC 进行了封装，提供了一个模板方法`JdbcTemplate`，来简化对数据库的操作。

使用Spring JDBC不再需要去关心资源管理问题，对于结果集的处理，Spring JDBC 也提供了一个`RowMapper `接口，可以把结果集转换成Java 对象。

```java
public class EmployeeRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        Employee employee = new Employee();
        employee.setEmpId(resultSet.getInt("emp_id"));
        employee.setEmpName(resultSet.getString("emp_name"));
        employee.setEmail(resultSet.getString("emial"));
        return employee;
    }
}
```

## JDBC工具的优点

- 无论是QueryRunner 还是JdbcTemplate，都可以传入一个数据源进行初始化，也就是资源管理这一部分，可以交给专门的数据源组件去做，不用手动创建和关闭；
- 对操作数据的增删改查的方法进行了封装；
- 可以映射结果集，无论是映射成List、Map 还是实体类。

## JDBC工具的缺点

- SQL 语句都是写死在代码里面的，依旧存在硬编码的问题；
- 参数只能按固定位置的顺序传入（数组），它是通过占位符去替换的，不能自动映射；
- 在方法里面，可以把结果集映射成实体类，但是不能直接把实体类映射成数据库的记录（没有自动生成SQL 的功能）；
- 查询没有缓存的功能。

# Hibernate

一个老牌的ORM框架。

hibernate操作流程：

- 为实体类建立一些hbm 的xml 映射文件（或者类似于@Table 的这样的注解）

  ```xml
  <hibernate-mapping>
          <class name="cn.gupaoedu.vo.User" table="user">
          <id name="id">
          	<generator class="native"/>
          </id>
          <property name="password"/>
          <property name="cellphone"/>
          <property name="username"/>
      </class>
  </hibernate-mapping>
  ```

- 通过Hibernate 提供（session）的增删改查的方法来操作对象。

  ```java
  //创建对象
  User user = new User();
  user.setPassword("123456");
  user.setCellphone("18166669999");
  user.setUsername("qingshan");
  //获取加载配置管理类
  Configuration configuration = new Configuration();
  //不给参数就默认加载hibernate.cfg.xml 文件，
  configuration.configure();
  //创建Session 工厂对象
  SessionFactory factory = configuration.buildSessionFactory();
  //得到Session 对象
  Session session = factory.openSession();
  //使用Hibernate 操作数据库，都要开启事务,得到事务对象
  Transaction transaction = session.getTransaction();
  //开启事务
  transaction.begin();
  //把对象添加到数据库中
  session.save(user);
  //提交事务
  transaction.commit();
  //关闭Session
  session.close();
  ```

Hibernate 的框架会自动帮我们生成SQL语句（可以屏蔽数据库的差异），自动进行映射。

Hibernate 在业务复杂的项目中使用也存在一些问题：

- 比如使用get()、save() 、update()对象的这种方式，实际操作的是所有字段，没有办法指定部分字段，换句话说就是不够灵活。
- 这种自动生成SQL 的方式，如果我们要去做一些优化的话，是非常困难的，也就是说可能会出现性能比较差的问题。
- 不支持动态SQL（比如分表中的表名变化，以及条件、参数）。

# MyBatis

MyBatis是一个“半自动化”的ORM框架。“半自动化”是相对于Hibernate 的全自动化来说的，也就是说它的封装程度没有Hibernate 那么高，不会自动生成全部的SQL 语句，主要解决的是SQL 和对象的映射问题。

在MyBatis 里面，SQL 和代码是分离的，所以会写SQL 基本上就会用MyBatis，没有额外的学习成本。

MyBatis的核心特性：

- 使用连接池对连接进行管理
- SQL和代码分离，集中管理
- 结果集映射
- 参数映射和动态SQL
- 重复SQL的提取
- 缓存管理
- 插件机制

MyBatis的操作流程：

- 创建全局配置文件`mybaits-config.xml`，里面是对MyBatis 的核心行为的控制
- 创建映射文件`Mapper.xml`，通常来说一张表对应一个，在这个里面配置增删改查的SQL 语句，以及参数和返回的结果集的映射关系。
- 创建会话`SqlSession`，。SqlSession 是工厂类根据全局配置文件创建的。
- 执行SQL
  - 通过`SqlSession`接口的方法传入`StatementID`来执行SQL（存在硬编码，不推荐）
  - 定义一个Mapper接口，与Mapper.xml里面的`namespace`对应，方法也跟`StatementID`一一对应

```java
String resource = "mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
SqlSession session = sqlSessionFactory.openSession();
try {
    BlogMapper mapper = session.getMapper(BlogMapper.class);
    Blog blog = mapper.selectBlogById(1);
    System.out.println(blog);
} finally {
	session.close();
}
```

# MyBatis核心对象

- `SqlSessionFactoryBuiler`：用来构建`SqlSessionFactory`
  - `SqlSessionFactory`只需要一个，使用只要构建了一个`SqlSessionFactory`，它的使命就完成了。使用`SqlSessionFactoryBuilder`的**生命周期只存在于方法的局部**。
- `SqlSessionFactory`：用来创建`SqlSession`
  - 每次应用程序访问数据库都需要创建一个会话，因此一直有创建会话的需要，使用`SqlSessionFactory`应该存在于**应用的整个生命周期中（作用域是应用作用域）**。
  - 创建`SqlSession`只需要一个实例来做，否则会产生很多混乱并浪费资源，所以要**采用单例模式**。
- `SqlSession`：一个会话
  - 因为不是线程安全的，不能在线程间共享，所以在请求开始的时候创建一个`SqlSession`对象，在请求结束或者说方法执行完毕时要及时关闭（**作用域是一次请求或者操作**）。
- `Mapper`：是从`SqlSession`中获取的（实际上是一个代理对象）
  - 它的作用是发生SQL来操作数据库的数据，**作用域应该在一个`SqlSession`事物方法之内**。

| 对象                      | 生命周期                     |
| ------------------------- | ---------------------------- |
| SqlSessionFactoryBuild    | 方法局部（method）           |
| SqlSessionFactory（单例） | 应用级别（application）      |
| SqlSession                | 请求和操作（request/method） |
| Mapper                    | 方法（method）               |

# MyBatis核心配置

## config文件

[config文件的详细配置(http://www.mybatis.org/mybatis-3/zh/configuration.html)][http://www.mybatis.org/mybatis-3/zh/configuration.html]

### 一级标签

configuration 是整个配置文件的根标签，实际上也对应着MyBatis 里面最重要的配置类Configuration。它贯穿MyBatis 执行流程的每一个环节。

注意：MyBatis 全局配置文件顺序是固定的，否则启动的时候会报错。

#### properties

用来配置参数信息，比如最常见的数据库连接信息。

为了避免直接把参数写死在xml 配置文件中，我们可以把这些参数单独放在properties 文件中，用properties 标签引入进来，然后在xml 配置文件中用`${}`引用就可以了。

可以用resource 引用应用里面的相对路径，也可以用url 指定本地服务器或者网络的绝对路径。

将配置独立出来的好处：

- 利于多处引用，维护简单；
- 把配置文件放在外部，避免修改后重新编译打包，只需要重启应用；
- 程序和配置分离，提升数据的安全性，比如生产环境的密码只有运维人员掌握。

#### settings

这是 MyBatis 中极为重要的调整设置，它们会改变 MyBatis 的运行时行为。

| 设置名                    | 描述                                                         | 有效值                                                       | 默认值                                                  |
| ------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------- |
| cacheEnabled              | 全局地开启或关闭配置文件中的所有映射器已经配置的任何缓存。   | true \| false                                                | true                                                    |
| lazyLoadingEnabled        | 延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。 特定关联关系中可通过设置 `fetchType`属性来覆盖该项的开关状态。 | true \| false                                                | false                                                   |
| aggressiveLazyLoading     | 当开启时，任何方法的调用都会加载该对象的所有属性。 否则，每个属性会按需加载（参考 `lazyLoadTriggerMethods`)。 | true \| false                                                | false （在 3.4.1 及之前的版本默认值为 true）            |
| defaultExecutorType       | 配置默认的执行器。SIMPLE 就是普通的执行器；REUSE 执行器会重用预处理语句（prepared statements）； BATCH 执行器将重用语句并执行批量更新。 | SIMPLE REUSE BATCH                                           | SIMPLE                                                  |
| lazyLoadTriggerMethods    | 指定哪个对象的方法触发一次延迟加载。                         | 用逗号分隔的方法列表。                                       | equals, clone, hashCode, toString                       |
| localCacheScope           | MyBatis 利用本地缓存机制（Local Cache）防止循环引用（circular references）和加速重复嵌套查询。 默认值为 SESSION，这种情况下会缓存一个会话中执行的所有查询。 若设置值为 STATEMENT，本地会话仅用在语句执行上，对相同 SqlSession 的不同调用将不会共享数据。 | SESSION \| STATEMENT                                         | SESSION                                                 |
| logImpl                   | 指定 MyBatis 所用日志的具体实现，未指定时将自动查找。        | SLF4J \| LOG4J \| LOG4J2 \| JDK_LOGGING \| COMMONS_LOGGING \| STDOUT_LOGGING \| NO_LOGGING | 未设置                                                  |
| multipleResultSetsEnabled | 是否允许单一语句返回多结果集（需要驱动支持）。               | true \| false                                                | true                                                    |
| useColumnLabel            | 使用列标签代替列名。不同的驱动在这方面会有不同的表现，具体可参考相关驱动文档或通过测试这两种不同的模式来观察所用驱动的结果。 | true \| false                                                | true                                                    |
| useGeneratedKeys          | 允许 JDBC 支持自动生成主键，需要驱动支持。 如果设置为 true 则这个设置强制使用自动生成主键，尽管一些驱动不能支持但仍可正常工作（比如 Derby）。 | true \| false                                                | False                                                   |
| autoMappingBehavior       | 指定 MyBatis 应如何自动映射列到字段或属性。 NONE 表示取消自动映射；PARTIAL 只会自动映射没有定义嵌套结果集映射的结果集。 FULL 会自动映射任意复杂的结果集（无论是否嵌套）。 | NONE, PARTIAL, FULL                                          | PARTIAL                                                 |
| defaultStatementTimeout   | 设置超时时间，它决定驱动等待数据库响应的秒数。               | 任意正整数                                                   | 未设置 (null)                                           |
| defaultFetchSize          | 为驱动的结果集获取数量（fetchSize）设置一个提示值。此参数只可以在查询设置中被覆盖。为了防止从数据库查询出来的结果过多。 | 任意正整数                                                   | 未设置 (null)                                           |
| safeRowBoundsEnabled      | 允许在嵌套语句中使用分页（RowBounds）。如果允许使用则设置为 false。 | true \| false                                                | False                                                   |
| safeResultHandlerEnabled  | 允许在嵌套语句中使用分页（ResultHandler）。如果允许使用则设置为 false。 | true \| false                                                | True                                                    |
| mapUnderscoreToCamelCase  | 是否开启自动驼峰命名规则（camel case）映射，即从经典数据库列名 A_COLUMN 到经典 Java 属性名 aColumn 的类似映射。 | true \| false                                                | False                                                   |
| jdbcTypeForNull           | 当没有为参数提供特定的 JDBC 类型时，为空值指定 JDBC 类型。 某些驱动需要指定列的 JDBC 类型，多数情况直接用一般类型即可，比如 NULL、VARCHAR 或 OTHER。 | JdbcType 常量，常用值：NULL, VARCHAR 或 OTHER。              | OTHER                                                   |
| defaultScriptingLanguage  | 指定动态 SQL 生成的默认语言。                                | 一个类型别名或完全限定类名。                                 | org.apache.ibatis. scripting.xmltags. XMLLanguageDriver |
| callSettersOnNulls        | 指定当结果集中值为 null 的时候是否调用映射对象的 setter（map 对象时为 put）方法，这在依赖于 Map.keySet() 或 null 值初始化的时候比较有用。注意基本类型（int、boolean 等）是不能设置成 null 的。 | true \| false                                                | false                                                   |
| returnInstanceForEmptyRow | 当返回行的所有列都是空时，MyBatis默认返回 `null`。 当开启这个设置时，MyBatis会返回一个空实例。 请注意，它也适用于嵌套的结果集 （如集合或关联）。（新增于 3.4.2） | true \| false                                                | false                                                   |
| logPrefix                 | 指定 MyBatis 增加到日志名称的前缀。                          | 任何字符串                                                   | 未设置                                                  |
| vfsImpl                   | 指定 VFS 的实现                                              | 自定义 VFS 的实现的类全限定名，以逗号分隔。                  | 未设置                                                  |
| useActualParamName        | 允许使用方法签名中的名称作为语句参数名称。 为了使用该特性，你的项目必须采用 Java 8 编译，并且加上 `-parameters` 选项。（新增于 3.4.1） | true \| false                                                | true                                                    |
| configurationFactory      | 指定一个提供 `Configuration` 实例的类。 这个被返回的 Configuration 实例用来加载被反序列化对象的延迟加载属性值。 这个类必须包含一个签名为`static Configuration getConfiguration()` 的方法。（新增于 3.2.3） | 类型别名或者全类名.                                          | 未设置                                                  |

一个配置完整的 settings 元素的示例如下：

```xml
<settings>
  <setting name="cacheEnabled" value="true"/>
  <setting name="lazyLoadingEnabled" value="true"/>
  <setting name="multipleResultSetsEnabled" value="true"/>
  <setting name="useColumnLabel" value="true"/>
  <setting name="useGeneratedKeys" value="false"/>
  <setting name="autoMappingBehavior" value="PARTIAL"/>
  <setting name="autoMappingUnknownColumnBehavior" value="WARNING"/>
  <setting name="defaultExecutorType" value="SIMPLE"/>
  <setting name="defaultStatementTimeout" value="25"/>
  <setting name="defaultFetchSize" value="100"/>
  <setting name="safeRowBoundsEnabled" value="false"/>
  <setting name="mapUnderscoreToCamelCase" value="false"/>
  <setting name="localCacheScope" value="SESSION"/>
  <setting name="jdbcTypeForNull" value="OTHER"/>
  <setting name="lazyLoadTriggerMethods" value="equals,clone,hashCode,toString"/>
</settings>
```

#### typeAliases

TypeAlias 是类型的别名，跟Linux 系统里面的alias 一样，主要用来简化全路径类名的拼写。

可以为自己的Bean 创建别名，既可以指定单个类，也可以指定一个package，自动转换。

MyBatis 里面有系统预先定义好的类型别名，在TypeAliasRegistry 中。

#### typeHandlers

由于Java 类型和数据库的JDBC 类型不是一一对应的（比如String 与varchar），所以把Java 对象转换为数据库的值，和把数据库的值转换成Java 对象，需要经过一定的转换，这两个方向的转换就要用到TypeHandler。

MyBatis 内置了很多TypeHandler（在type 包下），它们全部全部注册在`TypeHandlerRegistry `中，他们都继承了抽象类`BaseTypeHandler`。当做数据类型转换的时候，就会自动调用对应的TypeHandler 的方法。

在需要自定义一些类型转换规则，或者要在处理类型的时候做一些特殊的动作，可以编写自定义的TypeHandler，和系统自定义的TypeHandler一样，继承抽象类`BaseTypeHandler<T>`。有4个抽象方法必须实现，可以分为两类：

- set方法从java类型转换成JDBC类型
- get方法从JDBC类型转换成java类型

创建使用自定义TypeHandler的流程：

- 新建自定义TypeHandler，继承抽象类`BaseTypeHandler<T>`

  ```java
  public class MyTypeHandler extends BaseTypeHandler<String> {
      public void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
          // 设置String 类型的参数的时候调用，Java 类型到JDBC 类型
          System.out.println("---------------setNonNullParameter1："+parameter);
          ps.setString(i, parameter);
      }
      public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
          // 根据列名获取String 类型的参数的时候调用，JDBC 类型到java 类型
          System.out.println("---------------getNullableResult1："+columnName);
          return rs.getString(columnName);
      }
      // 后面两个方法省略…………
  }
  ```

- 在config配置文件中注册

  ```xml
  <typeHandlers>
  	<typeHandler handler="com.gupaoedu.type.MyTypeHandler"></typeHandler>
  </typeHandlers>
  ```

- 在需要使用的字段上指定

  - 插入值的时候，从java类型到JDBC类型，在字段属性中指定typeHandler

    ```xml
    <insert id="insertBlog" parameterType="com.gupaoedu.domain.Blog">
        insert into blog (bid, name, author_id)
        values (#{bid,jdbcType=INTEGER},
        #{name,jdbcType=VARCHAR,typeHandler=com.gupaoedu.type.MyTypeHandler},
        #{authorId,jdbcType=INTEGER})
    </insert>
    ```

  - 返回值的时候，从JDBC类型到java类型，在resultMap的列上指定typeHandler

    ```xml
    <result column="name" property="name" jdbcType="VARCHAR" typeHandler="com.gupaoedu.type.MyTypeHandler"/>
    ```

#### objectFactory

当把数据库返回的结果集转换为实体类的时候，需要创建对象的实例，由于不知道需要处理的类型是什么，有哪些属性，所以不能用new 的方式去创建。在MyBatis 里面，它提供了一个工厂类的接口，叫做ObjectFactory，专门用来创建对象的实例，里面定义了4 个方法。

| 方法                                                         | 作用                           |
| ------------------------------------------------------------ | ------------------------------ |
| void setProperties(Properties properties);                   | 设置参数时调用                 |
| <T> T create(Class<T> type);                                 | 创建对象（调用无参构造函数）   |
| <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs); | 创建对象（调用带参数构造函数） |
| <T> boolean isCollection(Class<T> type)                      | 判断是否集合                   |

ObjectFactory 有一个默认的实现类DefaultObjectFactory，创建对象的方法最终都调用了instantiateClass()，是通过反射来实现的。

如果想要修改对象工厂在初始化实体类的时候的行为，就可以通过创建自己的对象工厂，继承`DefaultObjectFactory `来实现（不需要再实现`ObjectFactory `接口）。

```java
public class MyObjectFactory extends DefaultObjectFactory {
    @Override
    public Object create(Class type) {
        if (type.equals(Blog.class)) {
        Blog blog = (Blog) super.create(type);
        blog.setName("by object factory");
        blog.setBid(1111);
        blog.setAuthorId(2222);
        return blog;
    }
    Object result = super.create(type);
    return result;
    }
}
```

可以直接用自定义的工厂类来创建对象：

```java
public class ObjectFactoryTest {
    public static void main(String[] args) {
        MyObjectFactory factory = new GPObjectFactory();
        Blog myBlog = (Blog) factory.create(Blog.class);
        System.out.println(myBlog);
	}
}
```

在config 文件里面注册，在创建对象的时候会被自动调用：

```xml
<objectFactory type="org.mybatis.example.MyObjectFactory">
	<!-- 对象工厂注入的参数-->
	<property name="value" value="666"/>
</objectFactory>
```

这样，就可以让MyBatis 的创建实体类的时候使用自定义的对象工厂。

#### plugins

插件是MyBatis 的一个很强大的机制，MyBatis 预留了插件的接口，让MyBatis 更容易扩展。

插件可以拦截这四个对象的这些方法，这四个对象称作MyBatis 的四大核心对象。

| 类（接口）       | 方法                                                         |
| ---------------- | ------------------------------------------------------------ |
| Executor         | update, query, flushStatements, commit, rollback, getTransaction, close, isClosed |
| ParameterHandler | getParameterObject, setParameters                            |
| ResultSetHandler | handleResultSets, handleOutputParameters                     |
| StatementHandler | prepare, parameterize, batch, update, query                  |

#### environments、environment

environments 标签用来管理数据库的环境，比如可以有开发环境、测试环境、生产环境的数据库。可以在不同的环境中使用不同的数据库地址或者类型。

```xml
<environments default="development">
    <environment id="development">
        <transactionManager type="JDBC"/>
        <dataSource type="POOLED">
            <property name="driver" value="com.mysql.jdbc.Driver"/>
            <property name="url"
            value="jdbc:mysql://127.0.0.1:3306/gp-mybatis?useUnicode=true"/>
            <property name="username" value="root"/>
            <property name="password" value="123456"/>
        </dataSource>
    </environment>
</environments>
```

一个environment 标签就是一个数据源，代表一个数据库。这里面有两个关键的标签，一个是事务管理器，一个是数据源。

##### transactionManager

如果配置的是JDBC，则会使用Connection 对象的commit()、rollback()、close()管理事务。

如果配置成MANAGED，会把事务交给容器来管理，比如JBOSS，Weblogic。如果跑的是本地程序，如果配置成MANAGE 不会有任何事务。

如果是Spring + MyBatis ， 则没有必要配置， 因为会直接在applicationContext.xml 里面配置数据源，覆盖MyBatis 的配置。

##### dataSource

dataSource 元素使用标准的 JDBC 数据源接口来配置 JDBC 连接对象的资源。

有三种内建的数据源类型：

- `UNPOOLED`：这个数据源的实现只是每次被请求时打开和关闭连接。虽然有点慢，但对于在数据库连接可用性方面没有太高要求的简单应用程序来说，是一个很好的选择。
- `POOLED`：这种数据源的实现利用“池”的概念将 JDBC 连接对象组织起来，避免了创建新的连接实例时所必需的初始化和认证时间。 这是一种使得并发 Web 应用快速响应请求的流行处理方式。
- `JNDI`：这个数据源的实现是为了能在如 EJB 或应用服务器这类容器中使用，容器可以集中或在外部配置数据源，然后放置一个 JNDI 上下文的引用。

#### mappers

<mappers>标签配置的是映射器，也就是Mapper.xml 的路径。这里配置的目的是让MyBatis 在启动的时候去扫描这些映射器，创建映射关系。

有四种指定Mapper 文件的方式：

- 使用相对于类路径的资源引用（resource）

  ```xml
  <mappers>
  	<mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
  </mappers>
  ```

- 使用完全限定资源定位符（绝对路径）（URL）

  ```xml
  <mappers>
      <mapper url="file:///var/mappers/AuthorMapper.xml"/>
  </mappers>
  ```

- 使用映射器接口实现类的完全限定类名

  ```xml
  <mappers>
  	<mapper class="org.mybatis.builder.AuthorMapper"/>
  </mappers>
  ```

- 将包内的映射器接口实现全部注册为映射器（最常用）

  ```xml
  <mappers>
      <package name="org.mybatis.builder"/>
  </mappers>
  ```


# MyBatis使用技巧

## 动态标签

NyBatis的动态SQL是基于OGNL表达式的，主要有四类：`if`、`choose(when, otherwise)`、`trim(where, set)`、`foreach`。

- `if`：需要判断的时候，条件写在`test`中，可用`where`改写

  ```xml
  <select id="selectDept" parameterType="int" resultType="com.test.crud.bean.Department">
      select * from tbl_dept where 1=1
      <if test="deptId != null">
      	and dept_id = #{deptId,jdbcType=INTEGER}
      </if>
  </select>
  ```

- `choose（when, otherwise）`：需要选择一个条件的时候

  ```xml
  <select id="getEmpList_choose" resultMap="empResultMap"
  parameterType="com.test.crud.bean.Employee">
  SELECT * FROM tbl_emp e
      <where>
          <choose>
              <when test="empId !=null">
              	e.emp_id = #{emp_id, jdbcType=INTEGER}
              </when>
              <when test="empName != null and empName != ''">
              	AND e.emp_name LIKE CONCAT(CONCAT('%', #{emp_name, jdbcType=VARCHAR}),'%')
              </when>
              <when test="email != null ">
             		AND e.email = #{email, jdbcType=VARCHAR}
              </when>
              <otherwise>
              </otherwise>
          </choose>
      </where>
  </select>
  ```

- `trim(where, set)`：需要去掉where、安定、逗号之类多余前缀或后缀的时候

  ```xml
  <insert id="insertSelective" parameterType="com.test.crud.bean.Employee">
      insert into tbl_emp
      <trim prefix="(" suffix=")" suffixOverrides=",">
          <if test="empId != null">
          	emp_id,
          </if>
          <if test="empName != null">
          	emp_name,
          </if>
          <if test="dId != null">
          	d_id,
          </if>
      </trim>
      <trim prefix="values (" suffix=")" suffixOverrides=",">
          <if test="empId != null">
          	#{empId,jdbcType=INTEGER},
          </if>
          <if test="empName != null">
          	#{empName,jdbcType=VARCHAR},
          </if>
          <if test="dId != null">
          	#{dId,jdbcType=INTEGER},
          </if>
      </trim>
  </insert>
  ```

- `foreach`：需要遍历集合的时候

  ```xml
  <delete id="deleteByList" parameterType="java.util.List">
      delete from tbl_emp where emp_id in
      <foreach collection="list" item="item" open="(" separator="," close=")">
      	#{item.empId,jdbcType=VARCHAR}
      </foreach>
  </delete>
  ```

## 批量操作

MyBatis 支持批量操作，包括批量的插入、更新、删除。可以直接传入一个List、Set、Map 或者数组，配合动态SQL 的标签，MyBatis 会自动生成语法正确的SQL 语句。

批量插入的语法是这样的，只要在values 后面增加插入的值就可以了。

```sql
insert into tbl_emp (emp_id, emp_name, gender,email, d_id) values ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? ) , ( ?,?,?,?,? )
```

在Mapper 文件里面，我们使用foreach 标签拼接values 部分的语句：

```xml
<!-- 批量插入-->
<insert id="batchInsert" parameterType="java.util.List" useGeneratedKeys="true">
<selectKey resultType="long" keyProperty="id" order="AFTER">
SELECT LAST_INSERT_ID()
</selectKey>
insert into tbl_emp (emp_id, emp_name, gender,email, d_id)
values
<foreach collection="list" item="emps" index="index" separator=",">
( #{emps.empId},#{emps.empName},#{emps.gender},#{emps.email},#{emps.dId} )
</foreach>
</insert>
```

批量更新的语法是这样的，通过case when，来匹配id 相关的字段值。

```sql
update tbl_emp set
    emp_name =
        case emp_id
            when ? then ?
            when ? then ?
            when ? then ? end ,
    gender =
        case emp_id
            when ? then ?
            when ? then ?
            when ? then ? end ,
    email =
        case emp_id
            when ? then ?
            when ? then ?
            when ? then ? end
where emp_id in ( ? , ? , ? )
```

所以在Mapper 文件里面最关键的就是case when 和where 的配置。

```xml
<update id="updateBatch">
    update tbl_emp set
    emp_name =
    <foreach collection="list" item="emps" index="index" separator=" " open="case emp_id"  close="end">
    	when #{emps.empId} then #{emps.empName}
    </foreach>
    ,gender =
    <foreach collection="list" item="emps" index="index" separator=" " open="case emp_id" close="end">
    	when #{emps.empId} then #{emps.gender}
    </foreach>
    ,email =
    <foreach collection="list" item="emps" index="index" separator=" " open="case emp_id" close="end">
    	when #{emps.empId} then #{emps.email}
    </foreach>
    where emp_id in
    <foreach collection="list" item="emps" index="index" separator="," open="("close=")">
    	#{emps.empId}
    </foreach>
</update>
```

当批量操作拼接的SQL语句过大时，需要修改mysql服务端接收数据包的大小限制

```properties
max_allowed_packet=4M(默认)
```

## 嵌套（关联）查询

在查询业务数据的时候经常会遇到跨表关联查询的情况，比如查询员工就会关联部门（一对一），查询成绩就会关联课程（一对一），查询订单就会关联商品（一对多），等等。

映射结果有两个标签，一个是`resultType`，一个是`resultMap`。

`resultType `是`select` 标签的一个属性，适用于返回JDK 类型（比如Integer、String等等）和实体类。这种情况下结果集的列和实体类的属性可以直接映射。

如果返回的字段无法直接映射，就要用`resultMap `来建立映射关系。

对于关联查询的这种情况，通常不能用`resultType `来映射。用`resultMap `映射，要么就是修改dto（Data Transfer Object），在里面增加字段，这个会导致增加很多无关的字段。要么就是引用关联的对象，比如Blog 里面包含了一个Author 对象，这种情况下就要用到关联查询（association，或者嵌套查询），MyBatis 可以自动做结果的映射。

### 一对一的关联查询

- 嵌套结果

  ```xml
  <!-- 根据文章查询作者，一对一查询的结果，嵌套查询-->
  <resultMap id="BlogWithAuthorResultMap" type="com.test.domain.associate.BlogAndAuthor">
      <id column="bid" property="bid" jdbcType="INTEGER"/>
      <result column="name" property="name" jdbcType="VARCHAR"/>
      <!-- 联合查询，将author 的属性映射到ResultMap -->
      <association property="author" javaType="com.test.domain.Author">
          <id column="author_id" property="authorId"/>
          <result column="author_name" property="authorName"/>
  	</association>
  </resultMap>
  ```

- 嵌套查询

  ```xml
  <!-- 联合查询(一对一)的实现，但是这种方式有“N+1”的问题-->
  <resultMap id="BlogWithAuthorQueryMap" type="com.test.domain.associate.BlogAndAuthor">
      <id column="bid" property="bid" jdbcType="INTEGER"/>
      <result column="name" property="name" jdbcType="VARCHAR"/>
      <association property="author" javaType="com.test.domain.Author" column="author_id" select="selectAuthor"/> 
  </resultMap>
  
  <!-- 嵌套查询-->
  <select id="selectAuthor" parameterType="int" resultType="com.test.domain.Author">
      select author_id authorId, author_name authorName
      from author where author_id = #{authorId}
  </select>
  ```

使用嵌套查询时可以开启延时加载来解决`N+1`的问题。

```xml
<!--延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。默认false -->
<setting name="lazyLoadingEnabled" value="true"/>
<!--当开启时，任何方法的调用都会加载该对象的所有属性。默认false，可通过select 标签的fetchType 来覆盖-->
<setting name="aggressiveLazyLoading" value="false"/>
<!-- Mybatis 创建具有延迟加载能力的对象所用到的代理工具，默认JAVASSIST -->
<setting name="proxyFactory" value="CGLIB" />
```

## 分页

在查询数据库的操作中，有两种分页方式，一种是逻辑分页（假分页），一种是物理分页（真分页）。

逻辑分页的原理是把所有数据查出来，在内存中删选数据。

物理分页是真正的分页，比如MySQL 使用limit 语句，Oracle 使用rownum 语句，SQLServer 使用top 语句。

### 逻辑分页

MyBatis 里面有一个逻辑分页对象`RowBounds`，里面主要有两个属性，offset 和limit（从第几条开始，查询多少条）。

可以在Mapper 接口的方法上加上这个参数，不需要修改xml 里面的SQL 语句。

```java
public List<Blog> selectBlogList(RowBounds rowBounds);
```

它的底层其实是对`ResultSet `的处理。它会舍弃掉前面offset 条数据，然后再取剩下的数据的limit 条。

## 物理分页

一种简单的办法就是传入参数（或者包装一个page 对象），在SQL 语句中翻页。

```xml
<select id="selectBlogPage" parameterType="map" resultMap="BaseResultMap">
    select * from blog limit #{curIndex} , #{pageSize}
</select>
```

需要在Java 代码里面去计算起止序号；每个需要翻页的Statement 都要编写limit 语句，会造成Mapper 映射器里面很多代码冗余。

最常用的做法就是使用分页的插件，这个是基于MyBatis 的拦截器实现的，比如`PageHelper`。

```java
// pageSize 每一页几条
PageHelper.startPage(pn, 10);
List<Employee> emps = employeeService.getAll();
// navigatePages 导航页码数
PageInfo page = new PageInfo(emps, 10);
return Msg.success().add("pageInfo", page);
```

`PageHelper`根据`PageHelper `的参数，改写SQL 语句。比如MySQL会生成limit 语句，Oracle 会生成rownum 语句，SQL Server 会生成top 语句。

## 通用Mapper

### Mapper继承

当表字段发生变化的时候，需要修改实体类和Mapper 文件定义的字段和方法。如果是增量维护，那么一个个文件去修改。如果是全量替换，还要去对比用MBG 生成的文件。字段变动一次就要修改一次，维护起来非常麻烦。

可以利用Mapper支持继承的特性将Mapper.xml和Mapper接口都分成两个文件。。一个是MBG 生成的，这部分是固定不变的。然后创建DAO 类继承生成的接口，变化的部分就在DAO 里面维护。

```java
public interface BlogMapperExt extends BlogMapper {
	public Blog selectBlogByName(String name);
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.test.mapper.BlogMapperExt">
    <!-- 只能继承statement，不能继承sql、resultMap 等标签-->
    <resultMap id="BaseResultMap" type="com.gupaoedu.domain.Blog">
        <id column="bid" property="bid" jdbcType="INTEGER"/>
        <result column="name" property="name" jdbcType="VARCHAR"/>
        <result column="author_id" property="authorId" jdbcType="INTEGER"/>
    </resultMap>
    <!-- 在parent xml 和child xml 的statement id 相同的情况下，会使用child xml 的statementid -->
    <select id="selectBlogByName" resultMap="BaseResultMap" statementType="PREPARED">
    	select * from blog where name = #{name}
    </select>
</mapper>
```

mybatis-config.xml 里面也要扫描：

```xml
<mappers>
    <mapper resource="BlogMapper.xml"/>
    <mapper resource="BlogMapperExt.xml"/>
</mappers>
```

以后就只要修改Ext 的文件就可以了。

### 泛型Mapper

编写一个支持泛型的通用接口，比如`BaseMapper<T>`，把实体类作为参数传入，接口里定义通用的增删改查的基础方法并指出泛型；自定义的Mapper接口继承通用接口自动获得对实体类的操作方法。遇到没有的方法，依然可以在自定义的Mapper里编写。

### MyBatis-Plus

MyBatis-Plus 是原生MyBatis 的一个增强工具，可以在使用原生MyBatis 的所有功能的基础上，使用plus 特有的功能。

MyBatis-Plus 的核心功能：

- **通用CRUD**：定义好Mapper 接口后，只需要继承BaseMapper<T> 接口即可获得通用的增删改查功能，无需编写任何接口方法与配置文件。
- **条件构造器**：通过EntityWrapper<T>（实体包装类），可以用于拼接SQL 语句，并且支持排序、分组查询等复杂的SQL。
- **代码生成器**：支持一系列的策略配置与全局配置，比MyBatis 的代码生成更好用。
- 另外MyBatis-Plus 也有分页的功能。



# MyBatis缓存

缓存是一般的ORM 框架都会提供的功能，目的就是提升查询的效率和减少数据库的压力。MyBatis 有一级缓存和二级缓存，并且预留了集成第三方缓存的接口。

## 缓存体系结构

MyBatis 跟缓存相关的类都在cache 包里面，其中有一个Cache 接口，只有一个默认的实现类`PerpetualCache`，它是用HashMap 实现的。

除此之外，还有很多的装饰器，通过这些装饰器可以额外实现很多的功能：回收策略、日志记录、定时刷新等等。在cache.decorators包下。但是无论怎么装饰，经过多少层装饰，最后使用的还是基本的实现类（默认`PerpetualCache`）。

所有的缓存实现类总体上可分为三类：基本缓存、淘汰算法缓存、装饰器缓存。

| 缓存实现类          | 描述             | 作用                                                         | 装饰条件                                        |
| ------------------- | ---------------- | ------------------------------------------------------------ | ----------------------------------------------- |
| 基本缓存            | 缓存基本实现类   | 默认是PerpetualCache，也可以自定义比如RedisCache、EhCache 等，具备基本功能的缓存类 | 无                                              |
| LruCache            | LRU 策略的缓存   | 当缓存到达上限时候，删除最近最少使用的缓存（Least Recently Use） | eviction="LRU"（默认）                          |
| FifoCache           | FIFO 策略的缓存  | 当缓存到达上限时候，删除最先入队的缓存                       | eviction="FIFO"                                 |
| SoftCache WeakCache | 带清理策略的缓存 | 通过JVM 的软引用和弱引用来实现缓存，当JVM内存不足时，会自动清理掉这些缓存，基于SoftReference 和WeakReference | eviction="SOFT" eviction="WEAK"                 |
| LoggingCache        | 带日志功能的缓存 | 比如：输出缓存命中率                                         | 基本                                            |
| SynchronizedCache   | 同步缓存         | 基于synchronized 关键字实现，解决并发问题                    | 基本                                            |
| BlockingCache       | 阻塞缓存         | 通过在get/put 方式中加锁，保证只有一个线程操作缓存，基于Java 重入锁实现 | blocking=true                                   |
| SerializedCache     | 支持序列化的缓存 | 将对象序列化以后存到缓存中，取出时反序列化                   | readOnly=false（默认）                          |
| ScheduledCache      | 定时调度的缓存   | 在进行get/put/remove/getSize 等操作前，判断缓存时间是否超过了设置的最长缓存时间（默认是一小时），如果是则清空缓存--即每隔一段时间清空一次缓存 | flushInterval 不为空                            |
| TransactionalCache  | 事务缓存         | 在二级缓存中使用，可一次存入多个缓存，移除多个缓存           | 在TransactionalCacheManager 中用Map维护对应关系 |

### 一级缓存

一级缓存也叫本地缓存，MyBatis 的一级缓存是在会话（`SqlSession`）层面进行缓存的。MyBatis 的一级缓存是默认开启的，不需要任何的配置。

一级缓存的`PerpetualCache`放在`Executor`中维护（`SimpleExecutor`/`ReuseExecutor`/`BatchExecutor `的父类`BaseExecutor `的构造函数中持有了`PerpetualCache`）。

在同一个会话里面，多次执行相同的SQL 语句，会直接从内存取到缓存的结果，不会再发送SQL 到数据库。但是不同的会话里面，即使执行的SQL 一模一样（通过一个Mapper 的同一个方法的相同参数调用），也不能使用到一级缓存。

![一级缓存](一级缓存.png)

#### 一级缓存的不足

使用一级缓存的时候，因为缓存不能跨会话共享，不同的会话之间对于相同的数据可能有不一样的缓存。在有多个会话或者分布式环境下，会存在脏数据的问题。如果要解决这个问题，就要用到二级缓存。

### 二级缓存

二级缓存是用来解决一级缓存不能跨会话共享的问题的，范围是`namespace `级别的，可以被多个`SqlSession` 共享（只要是同一个接口里面的相同方法，都可以共享），生命周期和应用同步。

二级缓存工作在一级缓存之前，只有取不到二级缓存的情况下才到一个会话中去取一级缓存。

MyBatis 用了一个装饰器的类来维护二级缓存，就是`CachingExecutor`。如果启用了二级缓存，MyBatis 在创建`Executor `对象的时候会对`Executor `进行装饰。

`CachingExecutor `对于查询请求，会判断二级缓存是否有缓存结果，如果有就直接返回，如果没有委派交给真正的查询器Executor 实现类，比如`SimpleExecutor `来执行查询，再走到一级缓存的流程。最后会把结果缓存起来，并且返回给用户。

![二级缓存](二级缓存.png)

#### 开启二级缓存

在mybatis-config.xml 中配置了（可以不配置，默认是true）

```xml
<setting name="cacheEnabled" value="true"/>
```

在Mapper.xml 中配置`<cache/>`标签

```xml
<!-- 声明这个namespace 使用二级缓存
最多缓存对象个数，默认1024
回收策略：LRU
自动刷新时间12000ms，未配置时只有调用时刷新
默认只读（安全），改为true 可读写时，对象必须支持序列化
-->
<cache type="org.apache.ibatis.cache.impl.PerpetualCache" size="1024"  eviction="LRU" flushInterval="120000" readOnly="false"/>
```

Mapper.xml 配置了`<cache>`之后，select()会被缓存。update()、delete()、insert()会刷新缓存。

可以在单个Statement ID 上显式关闭二级缓存（默认是true）

```xml
<select id="selectBlog" resultMap="BaseResultMap" useCache="false">
```

二级缓存使用`TransactionalCacheManager(TCM)`来管理，最后又调用了`TransactionalCache `的`getObject()`、`putObject` 和`commit()`方法，`TransactionalCache`里面又持有了真正的Cache 对象，比如是经过层层装饰的`PerpetualCache`。在`putObject `的时候，只是添加到了`entriesToAddOnCommit `里面，只有它的`commit()`方法被调用的时候才会调用`flushPendingEntries()`真正写入缓存。它就是在`DefaultSqlSession `调用`commit()`的时候被调用的。因此**二级缓存只有在事务被提交之后才生效**。

在`CachingExecutor `的`update()`方法里面会调用`flushCacheIfRequired(ms)`，`isFlushCacheRequired `就是从标签里面渠道的`flushCache `的值。而增删改操作的`flushCache `属性默认为true。因此**增删改操作会清空缓存**。

#### 何时开启二级缓存

- 在查询为主的应用中使用，比如历史交易、历史订单的查询
  - 因为所有的增删改都会刷新二级缓存，导致二级缓存失效
- 在一个Mapper 里面只操作单表的情况使用
  - 如果多个namespace 中有针对于同一个表的操作，如果在一个namespace 中刷新了缓存，另一个namespace 中没有刷新，就会出现读到脏数据的情况

#### 多个namespace共享一个二级缓存

跨namespace 的缓存共享的问题，可以使用`<cache-ref>`来解决：

```xml
<cache-ref namespace="com.test.crud.dao.DepartmentMapper" />
```

`cache-ref` 代表引用别的命名空间的Cache 配置，两个命名空间的操作使用的是同一个Cache。在关联的表比较少，或者按照业务可以对表进行分组的时候可以使用。

**注意：在这种情况下，多个Mapper 的操作都会引起缓存刷新，缓存的意义已经不大了。**

### 第三方缓存做二级缓存

除了MyBatis 自带的二级缓存之外，也可以通过实现Cache 接口来自定义二级缓存。

MyBatis 官方提供了一些第三方缓存集成方式，比如ehcache 和redis。

#### 使用redis作为二级缓存

pom 文件引入依赖：

```xml
<dependency>
    <groupId>org.mybatis.caches</groupId>
    <artifactId>mybatis-redis</artifactId>
    <version>1.0.0-beta2</version>
</dependency>
```

Mapper.xml 配置，type 使用`RedisCache`：

```xml
<cache type="org.mybatis.caches.redis.RedisCache" eviction="FIFO" flushInterval="60000" size="512" readOnly="true"/>
```



[http://www.mybatis.org/mybatis-3/zh/configuration.html]: