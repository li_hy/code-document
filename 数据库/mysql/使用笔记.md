# centos7安装mysql8

**安装mysql8资源库**

```shell
# 可在https://dev.mysql.com/downloads/repo/yum/ 下载
yum localinstall https://repo.mysql.com//mysql80-community-release-el7-1.noarch.rpm
```

**安装mysql8**

```shell
yum install mysql-community-server
```

**启动mysql并设置开机启动**

```shell
systemctl start mysqld
systemctl enable mysqld
```

**查看默认密码**

```shell
grep 'temporary password' /var/log/mysqld.log
```

**重置密码及设置**

```mysql
#修改密码校验规则和密码长度
set global validate_password.policy=0;
set global validate_password.length=1;
#修改密码
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
#设置root远程访问
update mysql.user set host='%' where user="root";
#授权
grant all privileges on *.* to 'root'@'%' with grant option;
```

# 密钥错误

CentOS yum安装Mysql8提示“公钥尚未安装”或“密钥已安装，但是不适用于此软件包”

>  https://blog.csdn.net/brantyou/article/details/124114996

**检查密钥**

```shell
 rpm --checksig  /var/cache/yum/x86_64/7/mysql80-community/packages/mysql-community-client-plugins-8.0.28-1.el7.x86_64.rpm
```

**生成密钥**

```shell
gpg --export -a 3a79bd29 > 3a79bd29.asc
rpm --import 3a79bd29.asc
rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
```

# centos 修改/etc/my.cnf文件不生效

```shell
chmod a+w /etc/my.cnf //去掉只读
vi /etc/my.cnf
#必须恢复为只读
chmod a-w /etc/my.cnf
```

**高版本的mysql添加了设定：如果这个文件全局可写，那么它就不会加载etc下的这个my.cnf，而是按照次序，加载其他某个目录下的另一个**

#centos中mysql默认数据存储位置

```mysql
#默认/var/lib/mysql
show global variables like “%datadir%”
```



# mysql主从同步

**修改主库my.cnf**

```shell
[mysqld]
log-bin=mysql-bin
server-id=1
#设置不要复制的数据库
binlog-ignore-db=mysql
binlog-ignore-db=information-schema
#设置需要复制的数据库
#binlog-do-db=gtds_hw
```

**在主库创建用于复制操作的账户**

```mysql
CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY 'slave';
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'%';
flush privileges;
```

**获取主库当前binary log文件名和位置（position）**

```mysql
SHOW MASTER STATUS;

#结果
#File			  Position	   Binlog_Ignore_DB							Executed_gtid_set
#mysql-bin.000032	104096		mysql,information-schema,xxl_job,gtds_hw	
```

**重启主库服务**

**配置从库my.cnf**

```shell
[mysqld]
log-bin=mysql-bin
server-id=2
```

**重启从库服务**

**在从库上设置主库参数**

```mysql
CHANGE MASTER TO 
MASTER_HOST='192.168.1.11',   # 主服务器地址
master_port=3307，# 主服务器端口
MASTER_USER='slave',        
MASTER_PASSWORD='123456',    
MASTER_LOG_FILE='master-bin.000001',  # File的值  
MASTER_LOG_POS=279;         # Position的值;
```

**开启主从同步**

```mysql
start slave;
 #查看同步状态 Slave_IO_Running 和 Slave_SQL_Running 都应该为Yes
show slave status;
```

**注意：同步之前主从服务器需要同步库的表结构必须一致**

# 备份

**创建用户**

```mysql
CREATE USER 'username'@'host' IDENTIFIED BY 'password';
```

**分配最小权限**

```mysql
GRANT SELECT, SHOW VIEW, LOCK TABLES ON db.* TO 'username'@'host';
```

**运行查看线程**

```mysql
GRANT PROCESS ON *.* TO 'username'@'host';
#刷新数据
FLUSH PRIVILEGES;
```

**编写备份脚本**

```bash
#!/bin/bash

# MySQL用户名
USER="your_username"
# MySQL密码
PASSWORD="your_password"
# 要备份的数据库
DATABASE="your_database"
# 要备份的表，多个表用空格分隔
TABLES="table1 table2 table3"
# 备份文件存放路径
BACKUP_DIR="/path/to/backup/directory"
# 当前日期
DATE=$(date +%Y%m%d%H%M)

# 构建备份文件名
BACKUP_FILE="$BACKUP_DIR/$DATABASE-$DATE.sql"

# 使用mysqldump备份指定的表
mysqldump -u $USER -p$PASSWORD $DATABASE$TABLES > $BACKUP_FILE

# 如果需要压缩备份文件，可以添加以下命令
gzip $BACKUP_FILE

# 删除旧的备份文件，保留最新的5个备份
find $BACKUP_DIR -name "$DATABASE-*.sql.gz" | sort -r | tail -n +6 | xargs rm --
```

**设置脚本执行权限**

```bash
chmod +x backup_tables.sh
```

**设置定时任务**

设置`cron`任务来定期执行这个脚本。使用`crontab -e`命令来编辑`cron`任务。然后添加一行来设置定时任务

```bash
0 * * * * /path/to/backup_tables.sh
```

**检查cron任务**

```bash
crontab -l
```

**重启cron服务**

```bash
systemctl restart crond
```

