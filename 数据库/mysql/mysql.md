

# mysql体系结构

![1555380992248](体系结构.png)

mysql是由`SQL接口`，`解析器`，`优化器`，`缓存`，`存储引擎`（SQL Interface、Parser、Optimizer、Caches&Buffers、Pluggable Storage Engines）组成。

- `Connectors`：不同语言中与SQL的交互
- `Management Services & Utilities`：系统管理和控制工具，例如备份恢复、mysql复制、集群等
- `Connection Pool`：连接池，管理缓存用户连接、用户名、密码、权限校验、线程处理等需要缓存的需求
- `SQL Interface`：SQL接口，接受用户的SQL命令，并返回用户需要查询的结果。
- `Parser`：解析器，SQL命令传递到解析器的时候会被解析器验证和解析。主要功能：
  - 将SQL语句分解成数据结构，并将这个结构传递到后续步骤
  - 如果在分解构成中遇到错误，那么就说明这个sql语句是不合理的
- `Optimizer`：查询优化器，SQL语句在查询之前会使用查询优化器对查询进行优化
- `Caches & Buffers`（高速缓冲区）：查询缓存，如果查询缓存有命中的查询结果，查询语句就可以直接去查询缓存中取数据
- `Engine`：存储引擎，mysql的存储引擎是插件式的，根据mysql提供的文件访问层的一个抽象接口来定制一种文件访问机制。常用的有`MyISAM`，`InnoDB`，`BDB`等。
- `file system`：文件系统，数据、日志（redo，undo）、索引、错误日志、查询记录、慢查询记录等

## mysql执行流程

![mysql执行流程](mysql执行流程.png)

## InnoDB 内存结构和磁盘结构

![InnoDB 内存结构和磁盘结构](InnoDB 内存结构和磁盘结构.png)

InnnoDB 的数据都是放在磁盘上的，InnoDB 操作数据有一个最小的逻辑单位，叫做页（索引页和数据页）。对于数据的操作，不是每次都直接操作磁盘，因为磁盘的速度太慢了。InnoDB 使用了一种缓冲池的技术，也就是把磁盘读到的页放到一块内存区域里面。这个内存区域就叫`Buffer Pool`。

读取相同的页，先判断是不是在缓冲池里面，如果是，就直接读取，不用再次访问磁盘。

修改数据的时候，先修改缓冲池里面的页。内存的数据页和磁盘数据不一致的时候，叫做脏页。InnoDB 里面有专门的后台线程把`Buffer Pool `的数据写入到磁盘，每隔一段时间就一次性地把多个修改写入磁盘，这个动作就叫做刷脏。

`Buffer Pool` 主要分为3 个部分： `Buffer Pool`、`Change Buffer`、`Adaptive Hash Index`，另外还有一个`（redo）log buffer`。

### Buffer Pool

Buffer Pool 缓存的是页面信息，包括数据页、索引页。

Buffer Pool 默认大小是128M（134217728 字节），可以调整。

```sql
--查看参数（系统变量）
SHOW VARIABLES like '%innodb_buffer_pool%';
```

InnoDB 用LRU算法来管理缓冲池（链表实现，不是传统的LRU，分成了young 和old），经过淘汰的数据就是热点数据。

### Change Buffer 写缓冲

如果某个数据页不是唯一索引，不存在数据重复的情况，也就不需要从磁盘加载索引页判断数据是不是重复（唯一性检查）。这种情况下可以先把修改记录在内存的缓冲池中，从而提升更新语句（Insert、Delete、Update）的执行速度。这一块区域就是`Change Buffer`。5.5 之前叫`Insert Buffer `插入缓冲，现在也能支持delete 和update。

最后把`Change Buffer `记录到数据页的操作叫做`merge`。在访问这个数据页的时候，或者通过后台线程、或者数据库shut down、redo log 写满时触发`merge`。

如果数据库大部分索引都是非唯一索引，并且业务是写多读少，不会在写数据后立刻读取，就可以使用`Change Buffer`（写缓冲）

```sql
--写多读少的业务，调大这个值
--代表Change Buffer 占Buffer Pool 的比例，默认25%
SHOW VARIABLES LIKE 'innodb_change_buffer_max_size';
```

### Adaptive Hash Index



### （redo）Log Buffer

如果Buffer Pool 里面的脏页还没有刷入磁盘时，数据库宕机或者重启，这些数据丢失；如果写操作写到一半，甚至可能会破坏数据文件导致数据库不可用。为了避免这个问题，InnoDB 把所有对页面的修改操作专门写入一个日志文件，并且
在数据库启动时从这个文件进行恢复操作（实现crash-safe）——用来实现事务的持久性。

![LogBuffer](LogBuffer.png)

这个文件就是磁盘的`redo log`（重做日志），对应于`/var/lib/mysql/`目录下的`ib_logfile0 `和`ib_logfile1`，每个48M。

这种日志和磁盘配合的整个过程， 就是MySQL 里的WAL 技术（Write-Ahead Logging），它的关键点就是先写日志，再写磁盘。

因为刷盘是随机I/O，而记录日志是顺序I/O，顺序I/O 效率更高；因此先把修改写入日志，可以延迟刷盘时机，进而提升系统吞吐。

`redo log` 也不是每一次都直接写入磁盘，在Buffer Pool 里面有一块内存区域（`Log Buffer`）专门用来保存即将要写入日志文件的数据，默认16M，它一样可以节省磁盘IO。

```sql
SHOW VARIABLES LIKE 'innodb_log%';

SHOW VARIABLES LIKE 'innodb_log_buffer_size';
```

需要注意：`redo log` 的内容主要是用于崩溃恢复。磁盘的数据文件，数据来自`buffer pool`。`redo log` 写入磁盘，不是写入数据文件。

在写入数据到磁盘的时候，操作系统本身是有缓存的。flush 就是把操作系统缓冲区写入到磁盘。

```sql
--log buffer 写入磁盘的时机,默认是1
--0: log buffer 将每秒一次地写入log file 中，并且log file 的flush 操作同时进行。该模式下，在事务提交的时候，不会主动触发写入磁盘的操作。
--1: 每次事务提交时MySQL 都会把log buffer 的数据写入log file，并且刷到磁盘中去。
--2: 每次事务提交时MySQL 都会把log buffer 的数据写入log file。但是flush 操作并不会同时进行。该模式下，MySQL 会每秒执行一次flush 操作。
SHOW VARIABLES LIKE 'innodb_flush_log_at_trx_commit';
```

redo log 的特点：

* redo log 是InnoDB 存储引擎实现的，并不是所有存储引擎都有
* 不是记录数据页更新之后的状态，而是记录这个页做了什么改动，属于物理日志
* redo log 的大小是固定的，前面的内容会被覆盖

### 磁盘结构

磁盘结构里面主要是各种各样的表空间，叫做`Table space`。

表空间可以看做是InnoDB 存储引擎逻辑结构的最高层，所有的数据都存放在表空间中。InnoDB 的表空间分为5 大类：

- 系统表空间 system tablespace
- 独占表空间 file-per-table tablespaces
- 通用表空间 general tablespaces
- 临时表空间 temporary tablespaces
- 回滚表空间undo tablespaces

#### 系统表空间system tablespace

在默认情况下InnoDB 存储引擎有一个共享表空间（对应文件`/var/lib/mysql/ibdata1`），也叫系统表空间。

InnoDB 系统表空间包含InnoDB `数据字典`和`双写缓冲区`，`Change Buffer` 和`Undo Logs`），如果没有指定`file-per-table`，也包含用户创建的表和索引数据。

##### 数据字典

由内部系统表组成，存储表和索引的元数据（定义信息）。

##### 双写缓冲

InnoDB 的页和操作系统的页大小不一致，InnoDB 页大小一般为16K，操作系统页大小为4K，InnoDB 的页写入到磁盘时，一个页需要分4 次写。

![innoDB写数据](innoDB写数据.png)

如果存储引擎正在写入页的数据到磁盘时发生了宕机，可能出现页只写了一部分的情况，比如只写了4K，就宕机了，这种情况叫做部分写失效（partial page write），可能会导致数据丢失。即使有`redo log`，，如果这个页本身已经损坏了，用它来做崩溃恢复是没有意义的。所以在对于应用redo log 之前，需要一个页的副本。如果出现了写入失效，就用页的副本来还原这个页，然后再应用`redo log`。这个页的副本就是`double write`，InnoDB 的双写技术。通过它实现了数据页的可靠性。

跟`redo log` 一样，`double write `由两部分组成，一部分是内存的`double write`，一部分是磁盘上的`double write`。因为`double write` 是顺序写入的，不会带来很大的开销。

##### undo log

`undo log`（撤销日志或回滚日志）记录了事务发生之前的数据状态（不包括select）。如果修改数据时出现异常，可以用`undo log` 来实现回滚操作（保持原子性）。

在执行undo 的时候，仅仅是将数据从逻辑上恢复至事务之前的状态，而不是从物理页面上操作实现的，属于逻辑格式的日志。

`redo Log` 和`undo Log` 与事务密切相关，统称为事务日志。

`undo Log `的数据默认在系统表空间`ibdata1 `文件中，因为共享表空间不会自动收缩，也可以单独创建一个undo 表空间。

```sql
show global variables like '%undo%';
```



#### 独占表空间file-per-table tablespaces

在默认情况下，所有的表共享一个系统表空间，这个文件会越来越大，而且它的空间不会收缩。可以让每张表独占一个表空间。这个开关通过`innodb_file_per_table` 设置，默认开启。

```sql
SHOW VARIABLES LIKE 'innodb_file_per_table';
```

开启后，则每张表会开辟一个表空间，这个文件就是数据目录下的ibd 文件， 存放表的索引和数据。

但是其他类的数据，如回滚（undo）信息，插入缓冲索引页、系统事务信息，二次写缓冲（Double write buffer）等还是存放在原来的共享表空间内。

#### 通用表空间general tablespaces

通用表空间也是一种共享的表空间，跟ibdata1 类似。可以创建一个通用的表空间，用来存储不同数据库的表，数据路径和文件可以自定义。

在创建表的时候可以指定表空间，用ALTER 修改表空间可以转移表空间。

不同表空间的数据是可以移动的，删除表空间需要先删除里面的所有表。

```sql
--创建通用表空间
create tablespace ts add datafile '/var/lib/mysql/ts.ibd' file_block_size=16K engine=innodb;
--创建表时指定表空间
create table tab(id integer) tablespace ts;
```

#### 临时表空间temporary tablespaces

存储临时表的数据，包括用户创建的临时表，和磁盘的内部临时表。对应数据目录下的`ibtmp1 `文件。当数据服务器正常关闭时，该表空间被删除，下次重新产生。

### binlog

除了InnoDB 架构中的日志文件，MySQL 的Server 层也有一个日志文件，叫做`binlog`，它可以被所有的存储引擎使用。

`binlog `以事件的形式记录了所有的DDL 和DML 语句（因为它记录的是操作而不是数据值，属于逻辑日志），可以用来做主从复制和数据恢复。

跟`redo log` 不一样，它的文件内容是可以追加的，没有固定大小限制。

在开启了`binlog `功能的情况下，我们可以把`binlog` 导出成SQL 语句，把所有的操作重放一遍，来实现数据的恢复。

`binlog `的另一个功能就是用来实现主从复制，它的原理就是从服务器读取主服务器的`binlog`，然后执行一遍。

### 更新语句的执行流程

![更新语句的执行流程](更新语句的执行流程.png)

# 索引

索引是为了加速对表中数据行的检索而创建的一种分散存储的**数据结构**。

## 为什么要用索引

- 索引能极大的减少存储引擎需要扫描的数据量
- 索引可以把随机IO变成顺序IO
- 索引可以在进行分组、排序等操作时，避免使用临时表

## mysql的索引结构--B+Tree

目前大部分数据库系统及文件系统都采用B-Tree或其变种B+Tree作为索引结构

### 平衡二叉查找树

平衡二叉搜索树（Self-balancing binary search tree）又被称为AVL树（有别于AVL算法），且具有以下性质：它是一 棵空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一棵平衡二叉树。

![1555383559520](平衡二叉查找树.png)

缺点：

- 太高（深），数据处的高（深）度决定IO操作次数，IO操作耗时大
- 太小，每一个磁盘块（节点、页）保存的数据量太小。没有很好的利益操作磁盘IO的数据交换特性，页没有利用好磁盘IO的预读能力（空间局部性原理），从而带来频繁的IO操作

### 多路平衡查找树--B-Tree

B-Tree，概括来说是一个节点可以拥有多于2个子节点的二叉查找树。与自平衡二叉查找树不同，B-Tree为系统最优化大块数据的读和写操作。B-Tree算法减少定位记录时所经历的中间过程，从而加快存取速度。

![1555384236439](B-Tree.png)

### 加强版多路平衡查找树--B+Tree

B+Tree 树的特点是能够保持数据稳定有序，其插入与修改拥有较稳定的对数时间复杂度。B+Tree元素自底向上插入，这与二叉树恰好相反。

![1555385149288](B+Tree.png)

### B+Tree与B-Tree的区别

- B+节点关键字搜索采用闭合区间
- B+非叶节点不保存数据相关信息，只保存关键字和子节点引用
- B+关键字对应的数据保存在叶子节点中
- B+叶子节点是顺序排列的，并且相邻节点具有顺序引用的关系

### 为什么选用B+Tree

- B+Tree是B-Tree的变种，拥有B-Tree的优势
- B+Tree扫库、表能力更强
- B+Tree的磁盘读写能力更强
- B+Tree的排序能力更强
- B+Tree的查询效率更加稳定（所有数据存储在叶节点，查询效率相同）

## Myisam索引

MyISAM的索引文件仅仅保存数据记录的地址。在MyISAM中，主索引和辅助索引（Secondary key）在结构上没有任何区别，只是主索引要求key是唯一的，而辅助索引的key可以重复。

MyISAM的索引方式也叫做“非聚集”的。

![1555385998398](Myisam索引.png)

![1555386194363](Myisam主、辅索引.png)

## innoDB索引

InnoDB的数据文件本身就是索引文件。这种索引叫做聚集索引（数据库表行中数据的物理顺序与键值的逻辑（索引）顺序相同）。

因为InnoDB的数据文件本身要按主键聚集，所以InnoDB要求表必须有主键（MyISAM可以没有），如果没有显式指定，则MySQL系统会自动选择一个可以唯一标识数据记录的列作为主键，如果不存在这种列，则MySQL自动为InnoDB表生成一个隐含字段作为主键，这个字段长度为6个字节，类型为长整形。

InnoDB的辅助索引data域存储相应记录主键的值而不是地址。换句话说，InnoDB的所有辅助索引都引用主键作为data域。聚集索引这种实现方式使得按主键的搜索十分高效，但是辅助索引搜索需要检索两遍索引：首先检索辅助索引获得主键，然后用主键到主索引中检索获得记录。

InnoDB主键不建议使用过长的字段，因为所有辅助索引都引用主索引，过长的主索引会令辅助索引变得过大。

![1555386658729](innoDB索引.png)

![1555386762552](innoDB主、辅索引.png)

## 索引知识

- 索引要建在离散性好的数据列上

  离散性越好，选择性就越好。在离散性差的列上建索引，mysql数据库最后可能会弃用索引而选择全表扫描。

- 最左匹配原则

  对索引中关键字进行计算（对比），一定是从左往右依次进行，且不可跳过。

- 联合索引列选择原则

  - 经常用的列优先（最左匹配原则）
  - 选择性（离散度）高的列优先（离散度高原则）
  - 宽度小的列优先（最少空间原则）

- 索引覆盖

  - 如果查询列可通过索引节点的关键字直接返回，则该索引称之为覆盖索引
  - 覆盖索引可减少数据库IO，将随机IO变为顺序IO，可提高查询性能

- 索引列的数据长度能少则少

- 索引一定不是越多越好，越全越好；一定是建合适的

- 匹配列前缀可能用到索引，如`like 999%`；`like %9999%`，`%9999`一定用不到索引

- where条件中`not in`和`<>`操作无法使用索引

- 匹配范围值，`order by`也可用到索引

- 多用指定列查询，只返回想要得到的数据列，少用`select *`

- 联合索引中如果不是按照索引最左列开始查找，无法使用索引

- 联合索引中精确匹配最左列并范围匹配另一列可以用到索引

- 联合索引中如果查询中有某个列的范围查询，则其右边的索引列都无法使用索引

- 不会出现在where语句中的字段不要加索引

# 存储引擎

- mysql的存储引擎是插拔式的插件方式
- 存储引擎是指定在表之上的，即一个库中的每一个表都可以指定专用的存储引擎
- 不管采用什么样的存储引擎，都会在数据区产生对应的一个frm文件（表结构定义描述文件）

## 存储引擎类型

### CSV存储引擎

数据以CSV文件存储

特点：

- 不能定义索引；列定义必须为`not null`；不能设置自增长列（不适用大表或者数据的在线处理）

- CSV数据的存储用`,`隔开，可直接编辑CSV文件进行数据的编排（数据安全性低）

  编辑之后，要生效需要适用`flush table XXX`命令

应用场景：

- 数据的快速导出导入
- 表格直接转换成CSV

### Archive存储引擎

压缩协议进行数据的存储，数据存储为ARZ文件格式

特点：

- 只支持`insert`和`select`两种操作
- 只允许自增ID列建立索引
- 行级锁
- 不支持事务
- 数据占用磁盘少

应用场景：

- 日志系统
- 大量的设备数据采集

### Memory存储引擎

数据都是存储在内存中，IO效率要比其他引擎高很多；服务重启数据丢失；内存数据表默认只有16M

特点：

- 支持hash索引，B tree索引，默认hash（查找复杂度O(1)）
- 字段长度都是固定长度`varchar(32)`（`char(32)`）
- 不支持大数据存储类型字段，如`blog`，`text`
- 表级锁

应用场景：

- 等值查找热度较高数据
- 查询结果内存中的计算，大多数都是采用这种存储引擎作为临时表存储需计算的数据

### Myisam

mysql5.5版本之前的默认存储引擎；较多的系统表也还是使用这个存储引擎；系统临时表也会用到这种引擎

特点：

- 有专门数据区存放表的数据量，执行`select count(*) from table`无需进行数据扫描
- 数据（MYD）和索引（MYI）分开存储
- 表级锁
- 不支持事务

### InnoDB

mysql5.5及以后版本的默认存储引擎

特点：

- 支持事务
- 行级锁
- 以聚集索引（主键索引）方式进行数据存储
- 支持外检关系保证数据完整性

### 各存储引擎的对比

![1555400276552](存储引擎对比.png)

# mysql查询优化

## 查询执行的路径

![1555400478290](查询执行的路径.png)

1. mysql客户端、服务端通信
2. 查询缓存
3. 查询优化处理
4. 查询执行引擎
5. 返回客户端

##  mysql客户端、服务端通信

mysql客户端与服务端的通信方式是“半双工”。

- 全双工：双向通信，发送同时也可以接收
- 半双工：双向通信，同时只能接收或者是发送，无法同时做操作
- 单工：只能单一方向传送

半双工通信：在任何一个时刻，要么是由服务器向客户端发送数据，要么是客户端向服务端发送数据，这两个动作不能同时发生。所以也无需将一个消息切成小块进行传输

特点和限制：

- 客户端一旦开始发送数据，另一端要接收完整的消息才能响应
- 客户端一旦开始接收数据，就没法停下发送指令

对于一个mysql连接，或者说一个线程，时刻都有一个状态来标识这个连接正在做什么。

可通过命令`show full processlist / show processlist`查看

常见的状态有：

- Sleep：线程正在等待客户端发送数据
- Query：连接线程正在执行查询
- Locked：线程正在等待表锁的释放
- Sorting result：线程正在对结果进行排序
- Sending data：向请求端返回数据

可通过`kill {id}`方式杀掉连接

## 查询缓存

工作原理：

缓存select操作的结果集合sql语句；新的select语句先去查缓存，判断是否存在可用的记录集。

判断标准：

与缓存的sql语句是否完全一样，区分大小写及非前后端的空格（可简单认为存储了一个key-value结构，key为sql，value为sql查询结果集）

参数：

- `query_cache_type`
  - 0：不启用查询缓存，默认值
  - 1：启用查询缓存，只要符合查询缓存的要求，客户端的查询语句和记录集都可以缓存起来供其他客户端使用。查询语句加上`SQL_NO_CACHE`将不缓存
  - 2：启用查询缓存，只要查询语句中添加了参数`SQL_CACHE`且符合查询缓存的要求，客户端的查询语句和记录集都可以缓存起来供其他客户端使用
- `query_cache_size`：缓存区的大小，最小为40K，默认为1M，推荐设置为64M/128M
- query_cache_limit：限制查询缓存区最大缓存的查询记录集，默认设置为1M

`show status like ‘Qcache’`命令可查看缓存情况

不会被缓存的情况：

- 当查询语句中有一些不确定的数据。如包含函数`now()`，`current_date()`等类似的函数，或者用户自定义的函数，存储函数，用户变量等都不会被缓存
- 当查询的结果大于`query_cache_limit`设置的值时，结果不会被缓存
- 对于InnoDB引擎来说，当一个语句在事务中修改了某个表，那么在这个事务提交之前，索引与这个表相关的查询都无法被缓存。因此长时间执行事务会大大降低缓存命中率
- 查询的表示系统表
- 查询语句不涉及到表

为什么mysq默认关闭了缓存：

- 在查询之前必须先检查是否命中缓存，浪费计算资源
- 如果这个查询可以被缓存，那么执行完成后，mysql发现查询缓存中没有这个查询，则会将结果存入查询缓存，这会带来额外的系统消耗
- 针对表进行写入或更新数据时，对应表的索引缓存都会失效
- 如果查询缓存很大或者碎片很多，这个操作可能带来很大的系统消耗

适用的业务场景：

- 以读为主的业务，数据生成之后就不常改变的业务。比如门户类、新闻类、报表类等

## 查询优化处理

查询优化处理的三个阶段：

- 解析sql

  通过lex词法分析，yacc语法分析将sql语句解析成解析树

- 预处理阶段

  根据mysql的语法规则进一步检查解析树的合法性，如：检查数据的表和列是否存在，解析名字和别名的设置；还会进行权限的验证

- 查询优化器

  优化器的主要作用就是找到最优的执行计划

查询优化器如何找到最优执行计划：

- 使用等价变化规则
  - 5=5 and a>5 改写成 a>5
  - a<b and a=5 改写成 b>5 and a=5
  - 基于联合索引，调整条件位置等
- 优化count、min、max等函数
  - min函数值需找索引最左边
  - max函数只需找索引最右边
  - myisam引擎count(**)
- 覆盖索引扫描
- 子查询优化
- 提前终止查询
  - 用limit关键字或者使用不存在的条件
- in的优化
  - 先进行排序再采用二分查找的方式
- ……

Mysql的查询优化器是基于成本计算的原则。他会尝试各种执行计划，数据抽样的方式进行试验（随机的读取一个4K的数据块进行分析）

### 执行计划

![1555402898551](执行计划.png)

id：select查询的序列号，标识执行的顺序

- id相同，执行顺序由上至下
- id不同，如果是子查询，id的序号会递增，id值越大优先级越高，越先被执行
- id相同又不同即两种情况同时存在，id如果相同，可以认为是一组，从上往下顺序执行；在所有组中，id值越大，优先级越高，越先执行

select_type：查询的类型，主要是用于区分普通查询、联合查询、子查询等

- SIMPLE：简单的select查询，查询中不包含子查询或者union
- PRIMARY：查询中包含子部分，最外层查询则被标记为primary
- SUBQUERY/MATERIALIZED：
  - SUBQUERY表示在select 或 where列表中包含了子查询
  - MATERIALIZED表示where 后面in条件的子查询
- UNION：若第二个select出现在union之后，则被标记为union
- UNION RESULT：从union表获取结果的select

table：查询涉及到的表

- 直接显示表名或者表的别名
- <unionM,N> 由ID为M,N 查询union产生的结果
- <subqueryN> 由ID为N查询生产的结果

type：访问类型，sql查询优化中一个很重要的指标，结果值从好到坏依次是：system > const > eq_ref > ref > range > index > ALL

- system：表只有一行记录（等于系统表），const类型的特例，基本不会出现，可以忽略不计
- const：表示通过索引一次就找到了，const用于比较primary key 或者 unique索引
- eq_ref：唯一索引扫描，对于每个索引键，表中只有一条记录与之匹配。常见于主键 或 唯一索引扫描
- ref：非唯一性索引扫描，返回匹配某个单独值的所有行，本质是也是一种索引访问
- range：只检索给定范围的行，使用一个索引来选择行
- index：Full Index Scan，索引全表扫描，把索引从头到尾扫一遍
- ALL：Full Table Scan，遍历全表以找到匹配的行

possible_keys：查询过程中有可能用到的索引

key：实际使用的索引，如果为NULL，则没有使用索引

rows：根据表统计信息或者索引选用情况，大致估算出找到所需的记录所需要读取的行数

filtered：指返回结果的行占需要读到的行(rows列的值)的百分比。表示返回结果的行数占需读取行数的百分比，filtered的值越大越好

Extra：额外信息

- Using filesort ：mysql对数据使用一个外部的文件内容进行了排序，而不是按照表内的索引进行排序读取
- Using temporary：使用临时表保存中间结果，也就是说mysql在对查询结果排序时使用了临时表，常见于order by 或 group by
- Using index：表示相应的select操作中使用了覆盖索引（Covering Index），避免了访问表的数据行，效率高
- Using where ：使用了where过滤条件
- select tables optimized away：基于索引优化MIN/MAX操作或者MyISAM存储引擎优化COUNT(*)操作，不必等到执行阶段在进行计算，查询执行计划生成的阶段即可完成优化

## 查询执行引擎

调用插件式存储引擎的原子API的功能进行执行计划的执行

## 返回客户端

- 有需要做缓存的，执行缓存操作
- 增量的返回结果：开始生成第一条结果时，mysql就开始往请求端逐步返回数据。（好处：mysql服务器无需保存过多的数据，浪费内存；用户体验好，马上就拿到了数据）



# 如何定位慢sql

- 业务驱动
- 测试驱动
- 慢查询日志

## 慢查询日志配置

```sql
show variables like 'slow_query_log'
--开启慢查询日志
set global slow_query_log = on
--日志保存路径
set global slow_query_log_file = '/var/lib/mysql/gupaoedu-slow.log'
--日志索引
set global log_queries_not_using_indexes = on
--慢sql标准
set global long_query_time = 0.1 (秒)
```

## 慢查询日志分析

![1555403712900](慢查询日志.png)

- Time：日志记录的时间
- User@Host：执行的用户及主机
- Query_time：查询消耗时间
- Lock_time：锁表时间
- Row_sent：发送给请求方的记录条数
- Row_examined：语句扫描的记录条数
- SET timestamp：语句执行的时间点
- select……：执行的具体语句

## 慢查询日志分析工具

```sql
mysqldumpslow -t 10 -s at /var/lib/mysql/gupaoedu-slow.log
```

![1555403933662](慢查询工具.png)

其他工具：mysqlsla；pt-query-digest

# 事务

数据库操作的最小工作单元，是作为单个逻辑工作单元执行的一系列操作；事务是一组不可再分割的操作集合（工作逻辑单元）;

mysql中如何开启事务：

```sql
begin / start transaction -- 手工
commit / rollback -- 事务提交或回滚
set session autocommit = on/off; -- 设定事务是否自动开启
```

JDBC 编程：

```java
connection.setAutoCommit（boolean）;
```

Spring 事务AOP编程：

```java
expression=execution（com.gpedu.dao.*.*(..)）
```

## 事务ACID特性

- 原子性（Atomicity）
  最小的工作单元，整个工作单元要么一起提交成功，要么全部失败回滚
- 一致性（Consistency）
  事务中操作的数据及状态改变是一致的，即写入资料的结果必须完全符合预设的规则，不会因为出现系统意外等原因导致状态的不一致
- 隔离性（Isolation）
  一个事务所操作的数据在提交之前，对其他事务的可见性设定（一般设定为不可见）
- 持久性（Durability）
  事务所做的修改就会永久保存，不会因为系统意外导致数据的丢失

## 事务并发带来的问题

### 脏读（dirty read）

A事务读取B事务尚未提交的更改数据，并在这个数据的基础上操作。如果恰巧B事务回滚，那么A事务读到的数据改变是不被承认的。

![1555404478238](脏读.png)

在这个场景中，B希望取款500元而后又撤销了动作，而A往相同的账户中转账100元，就因为A事务读取了B事务尚未提交的数据，因而造成账户白白丢失了500元。

在Oracle数据库中，不会发生脏读的情况。  

### 不可重复读（unrepeatable read）

不可重复读是指 A事务读取了B事务已经提交的更改数据。

![1555404583522](不可重复读.png)

在同一事务中，T4时间点和T7时间点读取账户存款余额不一样。

### 幻象读（phantom read）

A事务读取B事务提交的新增数据，这时A事务将出现幻象读的问题。幻象读一般发生在计算统计数据的事务中或范围查询的事务中。

![1555404718125](幻读.png)

如果新增数据刚好满足事务的查询条件，这个新数据就进入了事务的视野，因而产生了两个统计不一致的情况。  

幻象读和不可重复读是两个容易混淆的概念，前者是指读到了其他已经提交事务的新增数据，而后者是指读到了已经提交事务的更改数据（更改或删除），为了避免这两种情况，采取的对策是不同的，防止读取到更改数据，只需要对操作的数据添加行级锁，阻止操作中的数据发生变化，而防止读取到新增数据，则往往需要添加表级锁——将整个表锁定，防止新增数据（Oracle使用多版本数据的方式实现）。  

### 第一类丢失更新

A事务撤销时，把已经提交的B事务的更新数据覆盖了。

![1555404935127](第一类丢失更新.png)

A事务在撤销时，“不小心”将B事务已经转入账户的金额给抹去了。

### 第二类丢失更新

A事务覆盖B事务已经提交的数据，造成B事务所做操作丢失。

![1555405000476](第二类丢失更新.png)

上面的例子里由于支票转账事务覆盖了取款事务对存款余额所做的更新，导致银行最后损失了100元，相反如果转账事务先提交，那么用户账户将损失100元。

## 事务四种隔离级别

- Read Uncommitted（未提交读） --未解决并发问题
  事务未提交对其他事务也是可见的，脏读（dirty read）
- Read Committed（提交读） --解决脏读问题
  一个事务开始之后，只能看到自己提交的事务所做的修改，不可重复读（nonrepeatable
  read）
- Repeatable Read (可重复读) --解决不可重复读问题
  在同一个事务中多次读取同样的数据结果是一样的，这种隔离级别未定义解决幻读的问题
- Serializable（串行化） --解决所有问题
  最高的隔离级别，通过强制事务的串行执行

## InnoDB引擎对隔离级别的支持程度

![1555405270853](隔离支持.png)

# 锁

锁是用于管理不同事务对共享资源的并发访问

表锁与行锁的区别：

- 锁的粒度：表锁 > 行锁
- 加锁效率：表锁 > 行锁
- 冲突概率：表锁 > 行锁
- 并发性能：表锁 < 行锁

InnoDB存储引擎支持行锁和表锁（另类的行锁，锁住所有行）

## InnoDB锁类型

- Shared Locks ：共享锁（行锁）
- Exclusive Locks：排他锁（行锁）
- Intention Shared Locks：意向共享锁（表锁）
- Intention Exclusive Locks：意向排他锁（表锁）
- AUTO-INC Locks：自增锁

行锁的算法：

- Record Locks：记录锁
- Gap Locks：间隙锁
- Next-key Locks：临键锁

官网统称8大锁

### 共享锁和排他锁

共享锁又称读锁，简称S锁。共享锁就是多个事务对于同一数据可以共享一把锁，都能访问到数据，但是只能读不能修改。

加锁释放锁的方式

```sql
select * from users WHERE id=1 LOCK IN SHARE MODE;
commit/rollback
```

排他锁又称写锁，简称X锁。排他锁不能与其他锁并存，如一个事务获取了一个数据行的排他锁，其他事务就不能再获取该行的锁（共享锁、排他锁），只有该获取了排他锁的事务是可以对数据行进行读取和修改（其他事务要读取数据可来自于快照）

加锁释放锁的方式

```sql
--delete / update / insert 默认加上X锁
SELECT * FROM table_name WHERE ... FOR UPDATE;
commit/rollback
```

InnoDB的行锁是通过给索引上的索引项加锁来实现的。

只有通过索引条件进行数据检索，InnoDB才能使用行级锁，否则InnoDB将使用表锁（锁住索引的所有记录）

```sql
--表锁
lock tables xx read/write;
```

### 意向共享锁（IS）和意向排他锁（IX）

意向共享锁表示事务准备给数据行加入共享锁，即一个数据行加共享锁前必须先取得该表的IS锁，意向共享锁之间是可以相互兼容的

意向排他锁表示事务准备给数据行加入排他锁，即一个数据行加排他锁前必须先取得该表的IX锁，意向排他锁之间是可以相互兼容的

意向锁是InnoDB数据操作之前自动加的，不需要用户干预

意向锁的意义：当事务项去进行锁表时，可以先判断意向锁是否存在，存在时则可快速返回该表不能启用表锁

### 自增锁

针对自增列自增长的一个特殊的表级别锁。默认取值1，代表连续，事务未提交ID永久丢失

```sql
show variables like 'innodb_autoinc_lock_mode';
```

### 记录锁、间隙锁、临键锁

临键锁：锁住记录+区间（左开右闭）

当sql执行按照索引进行数据的检索时，查询条件为范围查找（between and、<、>等）并有数据命中则此时sql语句加上的锁为临键锁，锁住索引的记录+区间（左开右闭）

![1555465790280](next-key lock算法.png)

间隙锁：锁住数据不存在的区间（左开右开）

当sql执行按照索引进行数据的检索时，查询条件的数据不存在，这时sql语句加上的锁即为间隙锁，锁住索引不存在的区间（左开右开）

间隙锁只在RR事务隔离级别存在

![1555465897881](gap lock算法.png)

记录锁：锁住具体的索引项

当sql执行按照唯一性（Primary key、Unique Key）索引进行数据的检索时，查询条件等值匹配且查询的数据时存在的，这时sql语句加上的锁即为记录锁，锁住具体的索引项。

![1555465966597](record lock算法.png)

### 利用锁解决脏读

![1555466186429](利用锁解决脏读.png)

事务B在操作记录时加上X锁，事务A读取时会等待X锁释放后再获取数据，从而避免脏读

### 利用锁解决不可重复读

![1555466349804](利用锁解决不可重复读.png)

事务A在读取记录时加S锁，事务B修改记录时会等待S锁释放后再修改，从而避免不可重复读

### 利用锁解决幻读

![1555466496658](利用锁解决幻读.png)

事务A在读取数据时加临键锁，事务B在插入数据时会等待临键锁释放再插入，从而避免幻读

## 死锁

- 多个并发事务（2个或者以上）
- 每个事务都持有锁（或者是已经在等待锁）
- 每个事务都需要再继续持有锁
- 事务直接产生加锁的循环等待，形成死锁

如何避免死锁：

- 类似的业务逻辑以固定的顺序访问表和行
- 大事务拆小。大事务更倾向于死锁，如果业务允许，将大事务拆小
- 在同一个事务中，尽可能做到一次锁定所需要的索引资源，减少死锁概率
- 降低隔离级别，如果业务允许，将隔离级别调低也是较好的选择
- 为表添加合理的索引。可以看到如果不走索引将会为表的每一行记录添加上锁（或者说是表锁）

# MVCC

MVCC（Multiversion concurrency control（多版本并发控制））：

并发访问（读或写）数据库时，对正在事务内处理的数据做多版本的管理。以达到用来避免写操作的阻塞，从而优化读操作的并发问题。

## MVCC逻辑流程

### 插入流程

![1555467208048](MVCC插入流程.png)

### 删除流程

![1555467267738](MVCC删除流程.png)

### MVCC修改流程

修改操作是先做命中数据行的copy，将原行数据的删除版本号的值设置为当前事务ID

![1555467346611](MVCC修改流程.png)

### MVCC查询流程

数据查询规则：

- 查找数据行版本早于当前事务版本的数据行（也就是行的系统版本号小于或等于事务的系统版本号），这样可以确保事务读取的行要么是在事务开始前已经存在的，要么是事务自身插入或修改过的
- 查找删除版本号为null或大于当前事务版本号的记录（确保取出的行记录在事务开启之前没有被删除）

![1555467690275](MVCC查询流程.png)

## MVCC版本控制案例

数据准备

```sql
insert into teacher(name,age) value ('seven',18) ;
insert into teacher(name,age) value ('qing',20) ;
```

tx1

```sql
begin; 					   		   ----------1
select * from users ; 				----------2
commit;
```

tx2

```sql
begin; 								 ----------3
update teacher set age =28 where id =1; ----------4
commit;
```

### 案例一（1,  2,  3,  4,  2）

![1555468103602](MVCC案例一.png)

### 案例二（3， 4， 1， 2）

![1555468249856](MVCC案例二.png)

将会出现脏读，InnoDB中是使用Undo log来实现多版本并发控制

# Undo Log

undo意为取消，以撤销操作为目的，返回指定某个状态的操作

undo log指事务开始之前，在操作任何数据之前，首先将需要操作的数据备份到一个地方（Undo Log）

Undo Log是为了实现事务的原子性而出现的产物

事务处理过程中如果出现了错误或者用户执行了rollback语句，mysql可以利用Undo Log中的备份将数据恢复到事务开始之前的状态

UndoLog在Mysql innodb存储引擎中用来实现多版本并发控制

事务未提交之前，Undo保存了未提交之前的版本数据，Undo中的数据可作为数据旧版本快照供其他并发事务进行快照读

![1555468858298](undo log.png)

# 当前读、快照读

## 快照读

sql读取的数据是快照版本，也就是历史版本，普通的select就是快照读

InnoDB快照读，数据的读取将由cache（原本数据）+undo（事务修改过的数据）两部分组成

## 当前读

sql读取的数据时最新版本。通过锁机制来保证读取的数据无法通过其他事务进行修改

undate、delete、insert、select … lock in share mode、select … for update都是当前读

# Redo Log

Redo就是重做，以恢复操作为目的，重现操作

Redo Log指事务中操作的任何数据，将最新的数据备份到一个地方（Redo Log）

Redo Log的持久不是随着事务的提交才写入的，而是在事务的执行过程中便开始写入redo中。具体的落盘策略可以进行配置

Redo Log是为了实现事务的持久性而出现的产物

防止在发生故障的时间点尚有脏页未写入磁盘，在重启mysql服务的时候，根据Redo Log进行重做，从而达到事务的未入磁盘数据进行持久化这一特性

![1555469400933](Redo Log.png)

指定Redo log 记录在{datadir}/ib_logfile1&ib_logfile2 可通过innodb_log_group_home_dir 配置指定目录存储

一旦事务成功提交且数据持久化落盘之后，此时Redo log中的对应事务数据记录就失去了意义，所以Redo log的写入是日志文件循环写入的

- 指定Redo log日志文件组中的数量 innodb_log_files_in_group 默认为2
- 指定Redo log每一个日志文件最大存储量innodb_log_file_size 默认48M
- 指定Redo log在cache/buffer中的buffer池大小innodb_log_buffer_size 默认16M

Redo buffer 持久化Redo log的策略， Innodb_flush_log_at_trx_commit：

- 取值 0： 每秒提交 Redo buffer --> Redo log OS cache -->flush cache to disk（可能丢失一秒内的事务数据）
- 取值 1 ：默认值，每次事务提交执行Redo buffer --> Redo log OS cache -->flush cache to disk（最安全，性能最差的方式）
- 取值 2： 每次事务提交执行Redo buffer --> Redo log OS cache 在每一秒执行 ->flush cache to disk操作

# mysql配置优化

## mysql服务器参数类型

基于参数的作用域：

- 全局参数

  set global autocommit = ON/OFF;

- 会话参数(会话参数不单独设置则会采用全局参数)

  set session autocommit = ON/OFF;

注意：

- 全局参数的设定对于已经存在的会话无法生效
- 会话参数的设定随着会话的销毁而失效
- 全局类的统一配置建议配置在默认配置文件中，否则重启服务会导致配置失效

## 配置文件路径

mysql --help 寻找配置文件的位置和加载顺序：

Default options are read from the following files in the given order:
/etc/my.cnf /etc/mysql/my.cnf /usr/etc/my.cnf ~/.my.cnf

```sql
mysql --help | grep -A 1 'Default options are read from the following files in the given order'
```

## 全局配置文件配置

最大连接数配置：max_connections

系统句柄数配置：

- /etc/security/limits.conf
- ulimit -a

mysql句柄数配置：/usr/lib/systemd/system/mysqld.service

### 常见全局配置文件配置

```properties
port = 3306
socket = /tmp/mysql.sock
basedir = /usr/local/mysql
datadir = /data/mysql
pid-file = /data/mysql/mysql.pid
user = mysql
bind-address = 0.0.0.0
max_connections=2000
lower_case_table_names = 0 #表名区分大小写
server-id = 1
tmp_table_size=16M
transaction_isolation = REPEATABLE-READ
ready_only=1
```

## mysql内存参数配置

每一个connection内存参数配置：

- sort_buffer_size connection排序缓冲区大小
  - 建议256K(默认值)-> 2M之内
  - 当查询语句中有需要文件排序功能时，马上为connection分配配置的内存大小
- join_buffer_size connection关联查询缓冲区大小
  - 建议256K(默认值)-> 1M之内
  - 当查询语句中有关联查询时，马上分配配置大小的内存用这个关联查询，所以有可能在一个查询语句中会分配很多个关联查询缓冲区

上述配置4000连接占用内存：4000*(0.256M+0.256M) = 2G

Innodb_buffer_pool_size：innodb buffer/cache的大小（默认128M）

Innodb_buffer_pool：

- 数据缓存
- 索引缓存
- 缓冲数据
- 内部结构

大的缓冲池可以减小多次磁盘I/O访问相同的表数据以提高性能

参考计算公式：

Innodb_buffer_pool_size = （总物理内存 - 系统运行所用 - connection 所用）* 90%

## mysql其他参数配置

wait_timeout：服务器关闭非交互连接之前等待活动的秒数

innodb_open_files：限制Innodb能打开的表的个数

innodb_write_io_threads/innodb_read_io_threads：innodb使用后台线程处理innodb缓冲区数据页上的读写 I/O(输入输出)请求

innodb_lock_wait_timeout：InnoDB事务在被回滚之前可以等待一个锁定的超时秒数

# 数据库表设计

## 数据库设计

第一范式（ 1NF）：

字段具有原子性,不可再分。 所有关系型数据库系统都满足第一范式）数据库表中的字段都是单一属性的， 不可再分；

第二范式（ 2NF）：

要求实体的属性完全依赖于主键。 所谓完全依赖是指不能存在仅依赖主键一部分的属性，如果存在， 那么这个属性和主关键字的这一部分应该分离出来形成一个新的实体， 新实体与原实体之间是一对多的关系。为实现区分通常需要为表加上一个列，以存储各个实例的惟一标识。简而言之， 第二范式就是属性完全依赖主键。

第三范式（ 3NF）：

满足第三范式（ 3NF） 必须先满足第二范式（ 2NF）。 简而言之， 第三范式（ 3NF）要求一个数据库表中不包含已在其它表中已包含的非主键信息。

简单来说

- 每一列只有一个单一的值，不可再拆分
- 每一行都有主键能进行区分
- 每一个表都不包含其他表已经包含的非主键信息。

## 数据库表设计

- 充分的满足第一范式设计将为表建立太大量的列
  - 数据从磁盘到缓冲区，缓冲区脏页到磁盘进行持久的过程中，列的数量过多会导致性能下降。过多的列影响转换和持久的性能
- 过分的满足第三范式化造成了太多的表关联
  - 表的关联操作将带来额外的内存和性能开销
- 使用innodb引擎的外键关系进行数据的完整性保证
  - 外键表中数据的修改会导致Innodb引擎对外键约束进行检查，就带来了额外的开销