

# scope

在POM 4中，`<dependency>`中引入了`<scope>`，它主要管理依赖的部署。目前`<scope>`可以使用5个值：

* `compile`，缺省值，适用于所有阶段，会随着项目一起发布。 
* `provided`，类似`compile`，期望JDK、容器或使用者会提供这个依赖。如`servlet.jar`。 
* `runtime`，只在运行时使用，如JDBC驱动，适用运行和测试阶段。 
* `test`，只在测试时使用，用于编译和运行测试代码。不会随项目发布。 
* `system`，类似`provided`，需要显式提供包含依赖的jar，Maven不会在Repository中查找它。

# 常见错误

## `Failed to read artifact descriptor`

没有先把父pom进行打包

## centos下找不到JAVA_HOME

安装多个jdk或先安装maven在配置jdk时会提示

```
Error: JAVA_HOME is not defined correctly.
We cannot execute JAVA_HOME/bin/java
```

修改maven配置文件，配置` export JAVA_HOME=$(/usr/libexec/java_home) `

```bash
#打开maven配置文件
vi `which mvn`

#增加
export JAVA_HOME=$(/usr/libexec/java_home)
```

