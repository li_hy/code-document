# Shell 环境

Linux的Shell种类众多，常见的有：

- Bourne Shell(/usr/bin/sh或/bin/sh)
- Bourne Again Shell(/bin/bash)
- C Shell(/usr/bin/csh)
- K Shell(/usr/bin/ksh)
- Shell for Root(/sbin/sh)
- ……

日常用的是Bash，即Bourne Again Shell。Bash 是大多数Linux系统默认的Shell。

一般情况下不区分Bourne Shell 和 Bourne Again Shell，所以像`#!/bin/sh`同样也可以改为`#!/bin/bash`。

> `#!`是一个约定的标记，它告诉系统这个脚本需要什么解释器来执行，即使用哪一种Shell。

## Shell 脚本的运行方法

### 作为可执行程序

```shell
#给脚本加可执行权限
chmod +x test.sh

#执行脚本
./test.sh
```

> 注意，一定要写成./test.sh，而不是test.sh，运行其它二进制的程序也一样，直接写test.sh，linux系统会去PATH里寻找有没有叫test.sh的，而只有/bin, /sbin, /usr/bin，/usr/sbin等在PATH里，当前目录通常不在PATH里，所以写成test.sh是会找不到命令的，要用./test.sh告诉系统，就在当前目录找。

### 作为解释器参数

直接运行解释器，其参数就是shell脚本的文件名。这种方式运行的脚本，不需要在第一行指定解释器信息，写了也没用。

```shell
/bin/sh test.sh
/bin/php test.php
```

# Shell 语法

## Shell 变量

### 定义变量

定义变量时，变量名不加美元符号（$，PHP语言中变量需要）

```shell
param="shell"
```

注意，变量名和等号之间不能有空格。同时，变量名的命名须遵循如下规则：

- 首个字符必须为字母（a-z，A-Z）
- 中间不能有空格，可以使用下划线（_）
- 不能使用标点符号
- 不能使用bash里的关键字（可用help命令查看保留关键字）

除了显式地直接赋值，还可以用语句给变量赋值

```shell
#将/etc下目录的文件名循环处理
for file in `ls /etc`
```

### 使用变量

使用一个定义过的变量，只要在变量名前加美元号

```shell
param="shell"
echo $param
echo ${param}
```

变量名外面的花括号是可选的，加不加都行，加花括号是为了帮助解释器识别变量的边界

```shell
#不加花括号解释器会把$skillScript当成一个变量（其值为空）
for skill in coding do
    echo "I am good at ${skill}Script"
done
```

### 二次赋值

```shell
param="shell"
echo $param
param="bash"
echo $param
```

> 注意，第二次赋值的时候不能写$param="bash"，使用变量的时候才加美元符（$）。

### Shell 字符串

字符串可以用单引号，也可以用双引号，也可以不用引号。

#### 单引号

```shell
str='this is a string'
```

单引号字符串的限制：

- 单引号里的任何字符都会原样输出，单引号字符串中的变量是无效的；
- 单引号字串中不能出现单引号（对单引号使用转义符后也不行）。

#### 双引号

```shell
param='qinjx'
str="Hello, I know your are \"$param\"! \n"
```

双引号的优点：

- 双引号里可以有变量
- 双引号里可以出现转义字符

#### 拼接字符串

直接在常量或变量后面写即可，多个空格会忽略只显示一个空格。

```shell
param="shell"
greeting="hello, "$param" !"
greeting_1="Hello, ${param} !"
echo "hi,"$greeting $greeting_1
#输出结果为：hi,hello, shell ! Hello, shell !
```

#### 获取字符串长度

```shell
string="abcd"
echo ${#string} #输出 4
```

#### 提取子字符串

```shell
string="alibaba is a great company"
#从下标为1的字符开始取4个字符（下标从0开始）
echo ${string:1:4} #输出liba
```

#### 查找子字符串

```shell
string="alibaba is a great company"
#查找is在string中的位置（下标从1开始）
echo `expr index "$string" is` #输出3
```

### Shell 数组

bash支持一维数组，并且没有限定数组的大小。数组元素下标由0开始。

#### 数组的定义

用括号来表示数字，数组元素用空格分隔。

```shell
array_name=(value0 value1 value2)
```

还可以单独定义数组的各个分量。

```shell
array_name[0]=value0
array_name[1]=value1
array_name[n]=valuen
```

可以不是有连续的下标，而且下标的范围没有限制。

#### 读取数组

```shell
valuen=${array_name[n]}
```

可以使用`@`或`*`获取数组中的所有元素

```shell
echo ${array_name[@]}
echo ${array_name[*]}
```

#### 获取数组的长度

```shell
# 取得数组元素的个数
length=${#array_name[@]}
# 或者
length=${#array_name[*]}
# 取得数组单个元素的长度
lengthn=${#array_name[n]}
```

## Shell 注释

以"#"开头的行就是注释，会被解释器忽略。

sh里没有多行注释，只能每一行加一个#号。

## Shell 传递参数

在执行 Shell 脚本时，向脚本传递参数，脚本内获取参数的格式为：**$n**。**n** 代表一个数字，1 为执行脚本的第一个参数，2 为执行脚本的第二个参数，以此类推……

```shell
#!/bin/bash
#Shell传参测试脚本 test.sh

echo "Shell 传递参数实例！";
echo "执行的文件名：$0";
echo "第一个参数为：$1";
echo "第二个参数为：$2";
echo "第三个参数为：$3";
```

```shell
./test.sh 1 2 3
#执行结果
#Shell 传递参数实例！
#执行的文件名：test.sh
#第一个参数为：1
#第二个参数为：2
#第三个参数为：3
```

Shell脚本中有几个特殊字符用来处理参数：

- `$#` ：传递到脚本的参数个数
- `$*`：以一个单字符串显示所有向脚本传递的参数
- `$$`：脚本运行的当前进程ID号
- `$!`：后台运行的最后一个进程的ID号
- `$@`：与`$*`相同，但是使用时加引号，并在引号中返回每个参数
  - 相同点：都是引用所有参数
  - 不同点：只有在双引号中体现出来。假设在脚本运行时写了三个参数 1、2、3，，则 " * " 等价于 "1 2 3"（传递了一个参数），而 "@" 等价于 "1" "2" "3"（传递了三个参数）
- `$-`：显示Shell使用的当前选项，与`set`命令功能相同
- `$?`：显示最后命令的退出状态。0表示没有错误，其他任何值表明有错误。

## Shell 运算符

Shell支持多种运算符：

- 算数运算符
- 关系运算符
- 布尔运算符
- 字符串运算符
- 文件测试运算符

**算数运算符**

| 运算符 | 说明                                          | 举例                         |
| :----- | :-------------------------------------------- | :--------------------------- |
| +      | 加法                                          | `expr $a + $b` 结果为 30。   |
| -      | 减法                                          | `expr $a - $b` 结果为 -10。  |
| *      | 乘法                                          | `expr $a * $b` 结果为  200。 |
| /      | 除法                                          | `expr $b / $a` 结果为 2。    |
| %      | 取余                                          | `expr $b % $a` 结果为 0。    |
| =      | 赋值                                          | a=$b 将把变量 b 的值赋给 a。 |
| ==     | 相等。用于比较两个数字，相同则返回 true。     | [ $a == $b ] 返回 false。    |
| !=     | 不相等。用于比较两个数字，不相同则返回 true。 | [ $a != $b ] 返回 true。     |

注意：条件表达式要放在方括号之间，并且要有空格，例如: [$a==$b] 是错误的，必须写成 [ $a == $b ]。

**关系运算符**

关系运算符只支持数字，不支持字符串，除非字符串的值是数字。

下表列出了常用的关系运算符，假定变量 a 为 10，变量 b 为 20：

| 运算符 | 说明                                                  | 举例                       |
| :----- | :---------------------------------------------------- | :------------------------- |
| -eq    | 检测两个数是否相等，相等返回 true。                   | [ $a -eq $b ] 返回 false。 |
| -ne    | 检测两个数是否相等，不相等返回 true。                 | [ $a -ne $b ] 返回 true。  |
| -gt    | 检测左边的数是否大于右边的，如果是，则返回 true。     | [ $a -gt $b ] 返回 false。 |
| -lt    | 检测左边的数是否小于右边的，如果是，则返回 true。     | [ $a -lt $b ] 返回 true。  |
| -ge    | 检测左边的数是否大于等于右边的，如果是，则返回 true。 | [ $a -ge $b ] 返回 false。 |
| -le    | 检测左边的数是否小于等于右边的，如果是，则返回 true。 | [ $a -le $b ] 返回 true。  |

**布尔运算符**

下表列出了常用的布尔运算符，假定变量 a 为 10，变量 b 为 20：

| 运算符 | 说明                                                | 举例                                     |
| :----- | :-------------------------------------------------- | :--------------------------------------- |
| !      | 非运算，表达式为 true 则返回 false，否则返回 true。 | [ ! false ] 返回 true。                  |
| -o     | 或运算，有一个表达式为 true 则返回 true。           | [ $a -lt 20 -o $b -gt 100 ] 返回 true。  |
| -a     | 与运算，两个表达式都为 true 才返回 true。           | [ $a -lt 20 -a $b -gt 100 ] 返回 false。 |

**逻辑运算符**

下表列出了常用的逻辑运算符，假定变量 a 为 10，变量 b 为 20:

| 运算符 | 说明       | 举例                                       |
| :----- | :--------- | :----------------------------------------- |
| &&     | 逻辑的 AND | [[ $a -lt 100 && $b -gt 100 ]] 返回 false  |
| \|\|   | 逻辑的 OR  | [[ $a -lt 100 \|\| $b -gt 100 ]] 返回 true |

**字符串运算符**

下表列出了常用的字符串运算符，假定变量 a 为 "abc"，变量 b 为 "efg"：

| 运算符 | 说明                                      | 举例                     |
| :----- | :---------------------------------------- | :----------------------- |
| =      | 检测两个字符串是否相等，相等返回 true。   | [ $a = $b ] 返回 false。 |
| !=     | 检测两个字符串是否相等，不相等返回 true。 | [ $a != $b ] 返回 true。 |
| -z     | 检测字符串长度是否为0，为0返回 true。     | [ -z $a ] 返回 false。   |
| -n     | 检测字符串长度是否为0，不为0返回 true。   | [ -n $a ] 返回 true。    |
| str    | 检测字符串是否为空，不为空返回 true。     | [ $a ] 返回 true。       |

**文件测试运算符**

文件测试运算符用于检测 Unix 文件的各种属性。

属性检测描述如下：

| 操作符  | 说明                                                         | 举例                      |
| :------ | :----------------------------------------------------------- | :------------------------ |
| -b file | 检测文件是否是块设备文件，如果是，则返回 true。              | [ -b $file ] 返回 false。 |
| -c file | 检测文件是否是字符设备文件，如果是，则返回 true。            | [ -c $file ] 返回 false。 |
| -d file | 检测文件是否是目录，如果是，则返回 true。                    | [ -d $file ] 返回 false。 |
| -f file | 检测文件是否是普通文件（既不是目录，也不是设备文件），如果是，则返回 true。 | [ -f $file ] 返回 true。  |
| -g file | 检测文件是否设置了 SGID 位，如果是，则返回 true。            | [ -g $file ] 返回 false。 |
| -k file | 检测文件是否设置了粘着位(Sticky Bit)，如果是，则返回 true。  | [ -k $file ] 返回 false。 |
| -p file | 检测文件是否是有名管道，如果是，则返回 true。                | [ -p $file ] 返回 false。 |
| -u file | 检测文件是否设置了 SUID 位，如果是，则返回 true。            | [ -u $file ] 返回 false。 |
| -r file | 检测文件是否可读，如果是，则返回 true。                      | [ -r $file ] 返回 true。  |
| -w file | 检测文件是否可写，如果是，则返回 true。                      | [ -w $file ] 返回 true。  |
| -x file | 检测文件是否可执行，如果是，则返回 true。                    | [ -x $file ] 返回 true。  |
| -s file | 检测文件是否为空（文件大小是否大于0），不为空返回 true。     | [ -s $file ] 返回 true。  |
| -e file | 检测文件（包括目录）是否存在，如果是，则返回 true。          | [ -e $file ] 返回 true。  |

## Shell 命令

### expr

`expr `是一款表达式计算工具，使用它能完成表达式的求值操作。

```shell
#!/bin/bash

val=`expr 2 + 2`
echo "两数之和为 : $val"

#结果为：两数之和为 : 4
```

两点注意：

- 表达式和运算符之间要有空格，例如 2+2 是不对的，必须写成 2 + 2，这与我们熟悉的大多数编程语言不一样。
- 完整的表达式要被\` \`包含，注意这个字符不是常用的单引号，在 Esc 键下边。

### echo

Shell 的 echo 指令用于字符串的输出。

```shell
#显示普通字符串
echo "It is a test"
#双引号可忽略
echo It is a test

#显示转义字符
echo "\"It is a test\""
#双引号可忽略
echo \"It is a test\"

#显示变量
read name #从标准输入中读取一行,并把输入行的每个字段的值指定给 shell 变量
echo "$name It is a test"

#显示换行
echo -e "OK!\n" # -e 开启转义
echo "It it a test"

#显示不换行
echo -e "OK! \c" # -e 开启转义 \c 不换行
echo "It is a test"

#显示结果定向至文件
echo "It is a test" > myfile

#显示命令执行结果
echo `date` #这里使用的是反引号`，而不是单引号'
```

### printf

printf 使用引用文本或空格分隔的参数，外面可以在printf中使用格式化字符串，还可以制定字符串的宽度、左右对齐方式等。默认printf不会像 echo 自动添加换行符，我们可以手动添加 `\n`。

printf 命令的语法：

```
printf  format-string  [arguments...]
```

**参数说明：**

- **format-string:** 为格式控制字符串
- **arguments:** 为参数列表。

格式替代符：

- `%s` ：替换字符串，可加`-数字`设置字符串长度，例如：%-10s 指一个宽度为10个字符（-表示左对齐，没有则表示右对齐），任何字符都会被显示在10个字符宽的字符内，如果不足则自动以空格填充，超过也会将内容全部显示出来。
- `%c` ：替换字符
- `%d` ：替换十进制整数
- `%f`：替换小数，可加`.数字`来控制精度，例如：%.2f 指格式化为小数，其中.2指保留2位小数
- 如果没有 arguments，那么 %s 用NULL代替，%d 用 0 代替

### test

test 命令用于检查某个条件是否成立，它可以进行数值、字符和文件三个方面的测试。

数值测试

```shell
num1=100
num2=100
if test $[num1] -eq $[num2]
then
    echo '两个数相等！'
else
    echo '两个数不相等！'
fi

#结果：两个数不相等！
```

字符串测试

```shell
num1="W3Cschool"
num2="W3Cschool"
if test num1=num2
then
    echo '两个字符串相等!'
else
    echo '两个字符串不相等!'
fi

#结果：两个字符串相等！
```

文件测试

```shell
cd /bin
if test -e ./bash
then
    echo '文件已存在!'
else
    echo '文件不存在!'
fi

#结果：文件已存在!
```

Shell还提供了与( -a )、或( -o )、非( ! )三个逻辑操作符用于将测试条件连接起来，其优先级为："!"最高，"-a"次之，"-o"最低。

```shell
cd /bin
if test -e ./notFile -o -e ./bash
then
    echo '有一个文件存在!'
else
    echo '两个文件都不存在'
fi

#结果：有一个文件存在!
```

## Shell 流程控制

### if else

if 语句语法格式

```shell
if condition
then
    command1 
    command2
    ...
    commandN 
fi
```

if else 语法格式：

```shell
if condition
then
    command1 
    command2
    ...
    commandN
else
    command
fi
```

if else-if else 语法格式：

```shell
if condition1
then
    command1
elif condition2
    command2
else
    commandN
fi
```

if else语句经常与test命令结合使用

```shell
num1=$[2*3]
num2=$[1+5]
if test $[num1] -eq $[num2]
then
    echo '两个数字相等!'
else
    echo '两个数字不相等!'
fi

#结果：两个数字相等!
```

### for 循环

for循环一般格式为：

```shell
for var in item1 item2 ... itemN
do
    command1
    command2
    ...
    commandN
done
```

### while

while循环用于不断执行一系列命令，也用于从输入文件中读取数据；命令通常为测试条件。

```shell
while condition
do
    command
done
```

while循环可用于读取键盘信息。下面的例子中，输入信息被设置为变量FILM，按`Ctrl-D`结束循环。

```shell
echo '按下 <CTRL-D> 退出'
echo -n '输入你最喜欢的电影名: '
while read FILM
do
    echo "是的！$FILM 是一部好电影"
done
```

### 无限循环

无限循环语法格式：

```shell
while :
do
    command
done
```

或者

```shell
while true
do
    command
done
```

或者

```shell
for (( ; ; ))
```

### until 循环

until循环执行一系列命令直至条件为真时停止。

until循环与while循环在处理方式上刚好相反。

一般while循环优于until循环，但在某些时候—也只是极少数情况下，until循环更加有用。

until 语法格式:

```shell
until condition
do
    command
done
```

条件可为任意测试条件，测试发生在循环末尾，因此循环至少执行一次。

### case

Shell case语句为多选择语句。可以用case语句匹配一个值与一个模式，如果匹配成功，执行相匹配的命令。case语句格式如下：

```shell
case 值 in
模式1)
    command1
    command2
    ...
    commandN
    ;;
模式2）
    command1
    command2
    ...
    commandN
    ;;
esac
```

case工作方式如上所示。取值后面必须为单词in，每一模式必须以右括号结束。取值可以为变量或常数。匹配发现取值符合某一模式后，其间所有命令开始执行直至 ;;。

取值将检测匹配的每一个模式。一旦模式匹配，则执行完匹配模式相应命令后不再继续其他模式。如果无一匹配模式，使用星号 * 捕获该值，再执行后面的命令。

### 跳出循环

在循环过程中，有时候需要在未达到循环结束条件时强制跳出循环，Shell使用两个命令来实现该功能：break和continue。

#### break

break命令允许跳出所有循环（终止执行后面的所有循环）。

#### continue

continue命令与break命令类似，只有一点差别，它不会跳出所有循环，仅仅跳出当前循环。

## Shell 函数

linux shell 可以用户定义函数，然后在shell脚本中可以随便调用。

shell中函数的定义格式如下：

```shell
[ function ] funname [()]
{
    action;
    [return int;]
}
```

说明：

- 1、可以带function fun() 定义，也可以直接fun() 定义,不带任何参数。
- 2、参数返回，可以显示加：return 返回，如果不加，将以最后一条命令运行结果，作为返回值。 return后跟数值n(0-255)

```shell
#!/bin/bash
demoFun(){
    echo "这是我的第一个 shell 函数!"
}
echo "-----函数开始执行-----"
demoFun
echo "-----函数执行完毕-----"
```

带有return语句的函数

```shell
#!/bin/bash
funWithReturn(){
    echo "这个函数会对输入的两个数字进行相加运算..."
    echo "输入第一个数字: "
    read aNum
    echo "输入第二个数字: "
    read anotherNum
    echo "两个数字分别为 $aNum 和 $anotherNum !"
    return $(($aNum+$anotherNum))
}
funWithReturn
echo "输入的两个数字之和为 $? !"
```

函数返回值在调用该函数后通过 $? 来获得。

注意：所有函数在使用前必须定义。这意味着必须将函数放在脚本开始部分，直至shell解释器首次发现它时，才可以使用。调用函数仅使用其函数名即可。

### 函数参数

在Shell中，调用函数时可以向其传递参数。在函数体内部，通过 $n 的形式来获取参数的值，例如，$1表示第一个参数，$2表示第二个参数...

```shell
#!/bin/bash
funWithParam(){
    echo "第一个参数为 $1 !"
    echo "第二个参数为 $2 !"
    echo "第十个参数为 $10 !"
    echo "第十个参数为 ${10} !"
    echo "第十一个参数为 ${11} !"
    echo "参数总数有 $# 个!"
    echo "作为一个字符串输出所有参数 $* !"
}
funWithParam 1 2 3 4 5 6 7 8 9 34 73

#输出结果：
#第一个参数为 1 !
#第二个参数为 2 !
#第十个参数为 10 !
#第十个参数为 34 !
#第十一个参数为 73 !
#参数总数有 11 个!
#作为一个字符串输出所有参数 1 2 3 4 5 6 7 8 9 34 73 !

```

注意，$10 不能获取第十个参数，获取第十个参数需要${10}。当n>=10时，需要使用${n}来获取参数。

## Shell 文件包含

Shell 可以包含外部脚本。这样可以很方便的封装一些公用的代码作为一个独立的文件。被包含的文件不需要可执行权限。

Shell 文件包含的语法格式如下：

```shell
. filename   # 注意点号(.)和文件名中间有一空格
#或
source filename
```