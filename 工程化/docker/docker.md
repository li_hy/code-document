# 什么是docker

Docker 是一个开源的应用容器引擎，基于 `Go 语言`并遵从Apache2.0协议开源。

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

# docker的优点

与传统的虚拟化方式相比，docker具有众多的优势

## 更高效的利用系统资源

由于容器不需要进行硬件虚拟以及运行完整操作系统等额外开销，docker对系统资源的利用率更高。相比虚拟机技术，一个相同配置的主角往往可以运行更多数量的应用。

## 更快速的启动时间

传统的虚拟机技术启动应用服务往往需要数分钟，而docker容器应用，由于直接运行与宿主内核，无需启动完整的操作系统，因此可以做到秒级、甚至毫秒级的启动时间。大大节约了开发、测试、部署的时间。

## 一致的运行环境

开发过程中一个常见的问题是环境一致性。由于开发环境、测试环境、生产环境不一致，导致有些bug并未在开发过程中被发现。而docker的镜像提供了除内核外完整的运行时环境，确保了应用运行环境一致性。

## 持续交付和部署

docker可以通过定制应用镜像来实现持续集成、持续交付、部署。开发人员可以通过dockerfile来进行镜像构建，并`结合持续集成（Continuous Intergration）`系统进行集成测试，而运维人员则可以直接在生产环境中快速部署该镜像，甚至结合`持续部署（Continuous Delivery/Deployment）`系统进行指导部署。

## 更轻松的迁移

由于docker确保了执行环境的一致性，使得应用的迁移更加容易。docker可以在很多平台上运行，无论是物理机、虚拟机、公有云、私有云，甚至笔记本，其运行结果都是一致的。

## 更轻松的维护和扩展

docker使用的分层存储以及镜像的技术，使得应用重复部分的复用更为容易，也使得应用的维护更新更加简单，基于基础镜像进一步扩展镜像也变得非常简单。此外，docker团队同各个开源项目团队一起维护了一大批高质量的官方镜像，既可以直接在生产环境适应，又可以作为基础进一步定制，大大降低了应用服务的镜像制作成本。

|      特性      |        容器        |   虚拟机   |
| :------------: | :----------------: | :--------: |
|    **启动**    |        秒级        |   分钟级   |
|  **硬盘使用**  |      一般为MB      |   一般GB   |
|    **性能**    |      接近原生      |  弱于原生  |
| **系统支持量** | 单机支持上千个容器 | 一般几十个 |

# 基本概念

docker包括三个基本概念

- 镜像（image）
- 容器（container）
- 仓库（repository）

## docker镜像

操作系统分为内核和用户空间。对应linux而言，内核启动后，会挂载`root`文件系统为其提供用户空间支持。而docker镜像就相当于是一个`root`文件系统。

docker镜像是一个特殊的文件系统，除了提供容器运行时所需要的程序、库、资源、配置等文件外，还包含了一些为运行时准备的一些配置参数（如匿名卷、环境变量、用户等）。镜像不包含任何动态数据，其内容在构建之后也不会被改变。

### 分层存储

业务镜像包含操作系统完整的root文件系统，其体积往往是庞大的，因此在docker设计时，就充分利用`Union FS`的技术，将其设计为分层存储的架构。所以严格来说，镜像只是一个虚拟的概念，其实际体现并非有一个文件组成，而是由一组文件系统组成，或者说由多层文件系统联合组成。

镜像构建时，会一层层构建，前一层是后一层的基础。每一层构建完就不会再发生改变，后一层上的任何改变只发生在自己这一层。因此，在构建镜像的时候，需要额外消息，每一层尽量只包含该层需要添加的东西。

分层存储的特征还使得镜像的复用、定制变得更为容易。甚至可以用之前构建好的镜像作为基础层，然后进一步添加新的层，以定制自己所需的内容，构建新的镜像。

## docker容器

镜像和容器的关系，就像是面向对象程序设计中`类`和`实例`一样，镜像是静态的定义，容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。

容器的实质是进程，但与直接在宿主执行的进程不同，容器进程运行于属于自己的独立的命名空间。因此容器可以拥有自己的root文件系统、自己的网络配置、自己的进程空间，甚至直接的用户ID空间。

容器也是分成存储的，每一个容器运行时，是以镜像为基础层，在其上创建一个当前容器的存储层，可以称这个为容器运行时读写而准备的存储层为容器存储层。

容器存储层的生存周期和容器一样，容器消亡时，容错存储层也随之消亡。因此任何保存在容器存储层的信息都会随容器删除而丢失。

按照docker最佳实践的要求，容器不应该向其存储层内写入任何数据，容器存储层要保持无状态化。所有的文件写入操作，都应该使用`数据卷（Volume）`、或者绑定宿主目录，在这些位置的读写会跳过容器存储层，直接对宿主（或网络存储）发生读写，其性能和稳定性更高。

数据卷的生存周期独立于容器，容器消亡，数据卷不会消亡。因此，使用数据卷后，容器可以随意删除、重新润，数据不会丢失。

## docker registry

镜像构建完成后，可以很容易的在当前宿主上运行，但是，如果需要在其他服务器上使用这个镜像，我们就需要一个集中的存储、分发镜像的服务，即`docker registry`服务。

一个docker registry中可以包含多个`仓库（Repository）`；每个仓库可以包含多个`标签（Tag）`；每个标签对应一个镜像。

通常，一个仓库包含一个软件不同版本的镜像，而标签就常用于对应该软件的各个版本。可以通过<仓库名>:<标签>的格式来指定具体是这个软件哪个版本的镜像。如果不给出标签，将以`latest`作为默认标签。

## docker reistry公开服务

docker reistry公开服务是开放给用户使用、允许用户管理镜像的Registry服务。一般这类公开服务允许用户免费上传、下载公开的镜像，并可能提供收费服务提供用户管理私有镜像。

最常用的Registry公开范围是官方的Docker Hub，也是默认的Registry。国内常见的有阿里云加速器、DaoCloud加速器、灵雀云加速器等。

国内也有一些云服务商提供类似与Docker Hub的公开服务。如时速云镜像仓库、网易云镜像仓库、DaoCloud镜像仓库、阿里云镜像库等。

## 私有docker registry

除了使用公开服务外，用户还可以在本地搭建私有的docker registry。官方提供了docker registry镜像，可以直接使用作为私有registry服务。

# docker安装

## windows docker安装

win7、win8等需要利用`docker toolbox`来安装，win10专业版开启`Hyper-V`后即可安装。

### win10安装docker

- 开启Hyper_v

  - 右键点击`开始`，选择`应用和功能`，在窗口中选择`相关设置`下的`程序和功能`，在窗口中选择`启用或关闭Windows功能`,在窗口中选择`Hyper-v`后确定。

- 在docker官网下载最新[windows版本][https://www.docker.com/products/docker-engine]
- 运行安装文件，一路next完成安装。

  ![windows10安装](windows10安装.png)

###` Hyper-V`与`VirtualBox`/`VMware`冲突的解决方法

由于`Hyper-V`与`VirtualBox`、`VMware`不能共存，因此当`Hyper-V`打开时，`VirtualBox`、`VMware`就不能正常工作。

解决方法就是禁用Hyper-V。途径有多种：

第一种方法：使用管理员权限运行命令：`bcdedit /set hypervisorlaunchtype off`。

​                   对应的打开Hyper-V的命令：`bcdedit /set hypervisorlaunchtype auto`。

第二种方法：使用”添加或删除Windows组件“图形界面程序，在里面取消勾选`Hyper-V`。

上述方法都需要**重启计算机**。

## centos安装docker

### docker支持的CentOS版本

- CentOS 7 (64-bit)
- CentOS 6.5 (64-bit) 或更高的版本

### 前提条件

目前，CentOS 仅发行版本中的内核支持 Docker。

Docker 运行在 CentOS 7 上，要求系统为64位、系统内核版本为 3.10 以上。

Docker 运行在 CentOS-6.5 或更高的版本的 CentOS 上，要求系统为64位、系统内核版本为 2.6.32-431 或者更高版本。

### 使用 yum 安装（CentOS 7下）

通过 

uname -r 

命令查看你当前的内核版本

```
[root@runoob ~]# uname -r 3.10.0-327.el7.x86_64
```

### 使用yum安装 Docker

从 2017 年 3 月开始 docker 在原来的基础上分为两个分支版本: Docker CE 和 Docker EE。

Docker CE 即社区免费版，Docker EE 即企业版，强调安全，但需付费使用。

#### 移除旧的版本

```
$ sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine
```

#### 安装一些必要的系统工具

```
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```

#### 添加软件源信息

```
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

#### 更新 yum 缓存

```
sudo yum makecache fast
```

#### 安装 Docker-ce

```
sudo yum -y install docker-ce
```

#### 启动 Docker 后台服务

```
sudo systemctl start docker
```

### 使用脚本安装 Docker

- 使用 `sudo` 或 `root` 权限登录 Centos。

- 确保 yum 包更新到最新。

```
$ sudo yum update
```

- 执行 Docker 安装脚本。

```
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sudo sh get-docker.sh
```

执行这个脚本会添加 `docker.repo` 源并安装 Docker。

- 启动 Docker 进程。

```
sudo systemctl start docker
```

## 镜像加速器

国内访问Docker Hub有时会遇到困难，此时可以配置镜像加速器。国内很多云服务商都提供了加速器服务，如`阿里云加速器`，`DaoCloud加速器`、`灵雀云加速器`。

注册用户并申请加速器，会获得人如`https://xxxxxx.mirror.aliyuncs.com`这样的地址。需要将其配置给docker引擎。

新版的 docker 使用` /etc/docker/daemon.json`（Linux） 或者 `%programdata%\docker\config\daemon.json`（Windows） 来配置 Daemon。

在该配置文件中加入（没有该文件的话，先建一个）：

```json
{
  "registry-mirrors": ["https://xxxxxx.mirror.aliyuncs.com"]
}
```

linux在修改完成后需要加载配置并重启docker，windows可直接重启。

```
systemctl daemon-reload
systemctl restart docker
```



# 使用镜像

## 获取镜像

从docker registry 获取镜像的命令是`docker pull`，格式为：

```
docker pull [选项][docker registry地址]<仓库名>:<标签>
```

镜像名称的格式：

- docker registry地址：<域名/IP>[:端口号]。默认地址是Docker Hub。
- 仓库名：仓库名是两段式名称：<用户名>/<软件名>。对于Docker Hub如果不给出用户名，则默认`library`，也就是官方镜像。

## 运行镜像

```
docker run [参数] <镜像名>
```

## 列出镜像

```
docker images [镜像名] [参数]
```

## docker commit

`docker commit`命令可以将容器的存储层保持下来成为镜像。即在原有镜像的基础上，再叠加上容器的存储层，并构成新的镜像。以后再运行这个新镜像就会拥有原容器最后的文件变化。

`docker commit`的语法格式为：

```
docker commit [选项] <容器ID或容器名> [<仓库名>[:<标签>]]
```

***慎用`docker commit`***

- 命令执行过程中有很多文件被修改或添加，会导致大量无关内容被添加到镜像中，会导致镜像极为臃肿
- `docker commit`对镜像的操作都是黑箱操作，除制作者之外无人可知执行过什么命令、怎么生产的镜像。
- 后期修改镜像，每一层修改都会让镜像更加臃肿，所擅长的上一次的东西并不会丢失，会一直跟随这个镜像，即使根本无法访问到。

`docker commit`可用于被入侵后保存现场等情况下。不要使用其来定制镜像。

## 使用`Dockerfile`定制镜像

Dockerfile是一个文本文件，其内容包含了一条条的指令（Instruction），每一条指令构建一层，因此每一条指令的内容就是描述该层应当如何构建。

```
FROM xxx基础镜像
RUN xxx脚本命令
COPY xxx
ADD xxx
CMD xxx
......
RUN xxx脚本命令
......
```

在编写`RUN`指令时要尽量让一种意义的操作在一个`RUN`指令中执行。因为将所有操作分层会将很多运行时不需要的东西都被装进镜像，如编译环境、更新的软件包等，会产生非常臃肿、非常多层的镜像，不仅增加了构建部署的时间，也很容易出错。而且Union FS是有最大层数限制的，比如AUFS曾经最大不得超过42层，现在是不得超过127层。

### Dockerfile 语法

#### FROM

指定基础镜像，必须为第一个命令

```dockerfile
#格式：
　　FROM <image>
　　FROM <image>:<tag>
　　FROM <image>@<digest>
#示例：
　　FROM mysql:5.6
```

tag或digest是可选的，如果不使用这两个值时，会使用latest版本的基础镜像。

#### MAINTAINER

维护者信息

```dockerfile
#格式：
    MAINTAINER <name>
#示例：
    MAINTAINER xxx
    MAINTAINER xxx@163.com
    MAINTAINER xxx <xxx@163.com>
```

#### LABEL

用于为镜像添加元数据

```dockerfile
#格式：
    LABEL <key>=<value> <key>=<value> <key>=<value> ...
#示例：
　　LABEL version="1.0" description="这是一个Web服务器" by="IT笔录"
```

使用LABEL指定元数据时，一条LABEL指定可以指定一或多条元数据，指定多条元数据时不同元数据之间通过空格分隔。推荐将所有的元数据通过一条LABEL指令指定，以免生成过多的中间镜像。

#### ADD

将本地文件添加到容器中，tar类型文件会自动解压(网络压缩资源不会被解压)，可以访问网络资源，类似wget，下载后的文件权限自动设置为 600。

```dockerfile
格式：
    ADD <src>... <dest>
    ADD ["<src>",... "<dest>"] #用于支持包含空格的路径
示例：
    ADD hom* /mydir/          # 添加所有以"hom"开头的文件
    ADD hom?.txt /mydir/      # ? 替代一个单字符,例如："home.txt"
    ADD test relativeDir/     # 添加 "test" 到 `WORKDIR`/relativeDir/
    ADD test /absoluteDir/    # 添加 "test" 到 /absoluteDir/
```

#### COPY

功能类似ADD，但是是不会自动解压文件，也不能访问网络资源。使用 COPY 指令，源文件的各种元数据都会保留。比如读、写、执行权限、文件变更时间等。

#### RUN

构建镜像时执行的命令

```dockerfile
#RUN用于在镜像容器中执行命令，其有以下两种命令执行方式：
#shell执行
#格式：
    RUN <command>
#exec执行
#格式：
    RUN ["executable", "param1", "param2"]
#示例：
    RUN ["executable", "param1", "param2"]
    RUN apk update
    RUN ["/etc/execfile", "arg1", "arg1"]
```

RUN指令创建的中间镜像会被缓存，并会在下次构建中使用。如果不想使用这些缓存镜像，可以在构建时指定--no-cache参数（例如软件的更新操作，若缓存的话就不能获得最新的更新），如：docker build --no-cache。

RUN可以写多个，每一个指令都会建立一层，所有应该用`&&`将多个RUN命令组合到一起。

#### CMD

构建容器后调用，也就是在容器启动时才进行调用。

```dockerfile
#格式：
    CMD ["executable","param1","param2"] #执行可执行文件，优先
    CMD ["param1","param2"] #设置了ENTRYPOINT，则直接调用ENTRYPOINT添加参数
    CMD command param1 param2 #执行shell内部命令
#示例：
    CMD echo "This is a test." | wc -
    CMD ["/usr/bin/wc","--help"]
```

CMD不同于RUN，CMD用于指定在容器启动时所要执行的命令，而RUN用于指定镜像构建时所要执行的命令。

#### ENTRYPOINT

配置容器，使其可执行化。配合CMD可省去"application"，只使用参数。

```dockerfile
#格式：
    ENTRYPOINT ["executable", "param1", "param2"] #可执行文件, 优先
    ENTRYPOINT command param1 param2 #shell内部命令
#示例：
    FROM ubuntu
    ENTRYPOINT ["top", "-b"]
    CMD ["-c"]
```

ENTRYPOINT与CMD非常类似，不同的是通过docker run执行的命令不会覆盖ENTRYPOINT，而docker run命令中指定的任何参数，都会被当做参数再次传递给ENTRYPOINT。Dockerfile中只允许有一个ENTRYPOINT命令，多指定时会覆盖前面的设置，而只执行最后的ENTRYPOINT指令。

#### EXPOSE

指定于外界交互的端口

```dockerfile
格式：
    EXPOSE <port> [<port>...]
示例：
    EXPOSE 80 443
    EXPOSE 8080    
    EXPOSE 11211/tcp 11211/udp
```

EXPOSE并不会让容器的端口访问到主机。要使其可访问，需要在docker run运行容器时通过-p来发布这些端口，或通过-P参数来发布EXPOSE导出的所有端口。

#### VOLUME

用于指定持久化目录

```dockerfile
#格式：
    VOLUME ["/path/to/dir"]
#示例：
    VOLUME ["/data"]
    VOLUME ["/var/www", "/var/log/apache2", "/etc/apache2"]
```

一个卷可以存在于一个或多个容器的指定目录，该目录可以绕过联合文件系统，并具有以下功能：

- 卷可以容器间共享和重用
- 容器并不一定要和其它容器共享卷
- 修改卷后会立即生效
- 对卷的修改不会对镜像产生影响
- 卷会一直存在，直到没有任何容器在使用它

#### WORKDIR

工作目录，类似于cd命令

```dockerfile
#格式：
    WORKDIR /path/to/workdir
#示例：
    WORKDIR /a  #这时工作目录为/a
    WORKDIR b   #这时工作目录为/a/b
    WORKDIR c   #这时工作目录为/a/b/c
```

通过WORKDIR设置工作目录后，Dockerfile中其后的命令RUN、CMD、ENTRYPOINT、ADD、COPY等命令都会在该目录下执行。在使用docker run运行容器时，可以通过-w参数覆盖构建时所设置的工作目录。

#### USER

指定运行容器时的用户名或 UID，后续的 RUN 也会使用指定用户。使用USER指定用户时，可以使用用户名、UID或GID，或是两者的组合。当服务不需要管理员权限时，可以通过该命令指定运行用户。并且可以在之前创建所需要的用户

```dockerfile
#格式:
　　USER user
　　USER user:group
　　USER uid
　　USER uid:gid
　　USER user:gid
　　USER uid:group

 #示例：
　　USER www
```

使用USER指定用户后，Dockerfile中其后的命令RUN、CMD、ENTRYPOINT都将使用该用户。镜像构建完成后，通过docker run运行容器时，可以通过-u参数来覆盖所指定的用户。

#### ENV

设置环境变量

```dockerfile
#格式：
    ENV <key> <value>  #<key>之后的所有内容均会被视为其<value>的组成部分，因此，一次只能设置一个变量
    ENV <key>=<value> ...  #可以设置多个变量，每个变量为一个"<key>=<value>"的键值对，如果<key>中包含空格，可以使用\来进行转义，也可以通过""来进行标示；另外，反斜线也可以用于续行
#示例：
    ENV myName John Doe
    ENV myDog Rex The Dog
    ENV myCat=fluffy
```

#### ARG

用于指定传递给构建运行时的变量

```dockerfile
#格式：
    ARG <name>[=<default value>]
#示例：
    ARG site
    ARG build_user=www
```

与ENV不同的是，容器运行时不会存在这些环境变量。可以用 docker build --build-arg <参数名>=<值> 来覆盖。

#### ONBUILD

用于设置镜像触发器

```dockerfile
#格式：
	ONBUILD [INSTRUCTION]
#示例：
　　ONBUILD ADD . /app/src
　　ONBUILD RUN /usr/local/bin/python-build --dir /app/src
```

当所构建的镜像被用做其它镜像的基础镜像，该镜像中的触发器将会被触发。

#### HEALTHCHECK

健康检查

```dockerfile
#格式
	HEALTHCHECK [选项] CMD <命令> #设置检查容器健康状况的命令
	#HEALTHCHECK NONE ：如果基础镜像有健康检查指令，使用这行可以屏蔽掉其健康检查指令

	#HEALTHCHECK 支持下列选项：
    #--interval=<间隔> ：两次健康检查的间隔，默认为 30 秒；
    #--timeout=<时长> ：健康检查命令运行超时时间，如果超过这个时间，本次健康检查就被视为失败，默认 30 秒；
    #--retries=<次数> ：当连续失败指定次数后，则将容器状态视为 unhealthy ，默认 3次。

#示例
    FROM nginx
    RUN apt-get update && apt-get install -y curl && rm -rf /var/lib/apt/lists/*
    HEALTHCHECK --interval=5s --timeout=3s \
    CMD curl -fs http://localhost/ || exit 1
```

#### 完整示例

```dockerfile
# This my first nginx Dockerfile
# Version 1.0

# Base images 基础镜像
FROM centos

#MAINTAINER 维护者信息
MAINTAINER tianfeiyu 

#ENV 设置环境变量
ENV PATH /usr/local/nginx/sbin:$PATH

#ADD  文件放在当前目录下，拷过去会自动解压
ADD nginx-1.8.0.tar.gz /usr/local/  
ADD epel-release-latest-7.noarch.rpm /usr/local/  

#RUN 执行以下命令 
RUN rpm -ivh /usr/local/epel-release-latest-7.noarch.rpm
RUN yum install -y wget lftp gcc gcc-c++ make openssl-devel pcre-devel pcre && yum clean all
RUN useradd -s /sbin/nologin -M www

#WORKDIR 相当于cd
WORKDIR /usr/local/nginx-1.8.0 

RUN ./configure --prefix=/usr/local/nginx --user=www --group=www --with-http_ssl_module --with-pcre && make && make install

RUN echo "daemon off;" >> /etc/nginx.conf

#EXPOSE 映射端口
EXPOSE 80

#CMD 运行以下命令
CMD ["nginx"]
```

一张图解释常用指令的意义

![常用命令意义](常用命令意义.png)



## 构建镜像

在编写好`Dockerfile`文件后，在`Dockerfile`文件所在的目录执行`docker build`命令就可以构建镜像。

`docker build`命令格式：

```
docker build [选项] <上下文路径/URL/->
```

## 删除本地镜像

```
docker rmi [选项] <镜像1> [<镜像2> ...]
```

其中，`<镜像>`可以是`镜像短ID`，`镜像长ID`，`镜像名`或者`镜像摘要`。

`镜像短ID`一般取`镜像长ID`前3个字符以上，只要足够区分于别的镜像就可以。



# 容器操作

启动容器有两种方式，一种是基于镜像新建一个容器并启动，另一个是将在终止状态（stopped）的容器重新启动。

## 新建并启动

```
dockor run
```

当利用`docker run`来创建容器时，docker在后台运行的标准操作包括：

- 检测本地是否存在指定的镜像，不存在就从共有仓库下载
- 利用镜像创建并启动一个容器
- 分配一个文件系统，并在制度的镜像层外面挂载一层可读写层
- 从宿主主机配置的网桥接口中桥接一个虚拟接口道容器中去
- 从地址池配置一个ip地址给容器
- 执行用户指定的应用程序
- 执行完毕后容器被终止

## 启动已终止容器

```
docker start
```

所有运行时的参数应该在`docker run`时指定，`docker start`不能指定运行时的参数，而是直接用了`docker run`是指定的参数。

## 终止容器

```
docker stop
```

此外，当docker容器中指定的应用终结时，容器也自动终止。处于终止状态的容器，可以通过`docker start`命令来重新启动。

此外，`docker restart`命令会将一个运行态的容器终止，然后再重新启动。

## 进入容器

### attach命令

```
docker attach
```

使用`attach`命令有时候并不方便，当多个窗口同时attach到同一个容器的时候，所有窗口都会同步显示。当某个窗口因命令阻塞时，其他窗口也无法执行操作。

### nsenter命令

```
nsenter --target $PID --mount --uts --ipc --net --pid
```



`nsenter`会启动一个新的shell进程（默认是/bin/bash），同时会把这个新进程切换到和目标（target）进程相同的命名空间，这样就相当于进入了容器内部。nsenter要正常工作需要有root权限。

## 导出和导入容器

```
docker export $PID > 文件名.tar
```

这样可以将容器快照导出到本地文件

```
cat 快照文件名 | docker import -新镜像名
```

这样可以将容器快照文件导入为镜像

```
docker import http://xxxxxxxxx 镜像名
```

也可以通过指定的URL或者某个目录来导入。

用户即可以使用`docker load`来导入镜像存储文件到本地镜像库，也可以使用`docker import`来导入一个容器快照到本地镜像库。这两种的区别在于容器快照文件将丢失所有的历史记录和元数据信息（即仅保存容器当时的快照状态），而镜像存储文件将保持完整记录。此外，容容器快照文件导入时可以重新指定标签等元数据信息。

## 删除容器

可以使用

```
docker rm 容器名
```

来删除一个处于终止状态的容器。

如果要删除一个运行中的容器，可以添加`-f`参数。docker会发送`SIGKILL`信号给容器。

清理所有处于终止状态的容器

```
docker rm $(docker ps -a -q)
```

这个命令其实会试图删除所有的包括还在运行中的容器，不过`docker rm`默认不会删除运行中的容器。

# 访问仓库

仓库（Repository）是集中存放镜像的地方。

一个容易混淆的概念是注册服务器（Registry）。实际上注册服务器是管理仓库的具体服务器，每个服务器上可以有多个仓库，而每个仓库下面有多个镜像。从这方面来说，仓库可以被认为是一个具体的项目或目录。例如对于仓库地址`dl.dockerpool.com/ubuntu`来说，`dl.dockerpool.com`是注册服务器地址，`ubuntu`是仓库名。

## Docker Hub

目前Docker官方维护了一个公共仓库Docker Hub。登录需要在docker官网注册。目前官网注册调用了一个google的验证码生成器，若要注册需翻墙。

不过用户无需登录即通过`docker search`命令来查找官方仓库中的镜像，并利用`docker pull`命令来将镜像下载到本地。

## 自动创建

自动创建运行用户通过Docker Hub指定一个目标网站（目前支持GitHub或BitBucket）上的项目，一旦项目发生新的提交，则自动执行创建。

要配置自动创建，包括以下步骤：

- 创建并登录Docker Hub，以及目标网站
- 在目标网站中连接帐户到Docker Hub
- 在Docker Hub中配置一个自动创建
- 选取一个目标网站中的项目（需要含Dockerfile）和分支
- 指定Dockerfile的位置，并提交创建

之后，可以在Docker Hub的自动创建页面中跟踪每次创建的状态。

## 私有仓库

`docker-registry`是官方提供的工具，可以用于构建私有镜像仓库。

```
docker run -d -p 5000:5000 registry
```

用户可以通过指定参数来配置私有仓库位置，还可以指定本地路径下的配置文件。

默认情况下，仓库会被创建在容器的`/tmp/registry`下，可以通过`-v`参数来将镜像文件存放在本地的指定路径。

创建好私有仓库之后，就可以使用`docker tag`来标记一个镜像，然后推送到仓库（使用`docker push`命令），别的机器上就可以下载了。

# docker数据管理

docker在容器中管理数据主要有两种方式：

- 数据卷（Data volumes）
- 数据卷容器（Data volume containers）

## 数据卷

数据卷是一个可供一个或多个容器使用的特殊目录，它绕过UFS，可以提供很多特性：

- 数据卷可以在容器直接共享和重用
- 对数据卷的修改会立马生效
- 对数据卷的更新不会影响镜像
- 数据卷默认会一直存在，即使容器被删除

数据卷的使用，类似与Linux下对目录或文件进行mount，镜像中的被指定为挂载点的目录中的文件会隐藏掉，能显示看的是挂载的数据卷。

### 创建一个数据卷

在用`docker run`命令的时候，使用`-v`标记来创建一个数据卷并挂载到容器里。一次run中可多次使用可以挂载多个数据卷。

### 删除数据卷

数据卷是被设计用来持久化数据的，它的生命周期独立于容器，docker不会在容器被删除后自动删除数据卷。如果要在删除容器的同时移除数据卷，可以使用`docker rm -v`命令。

## 数据卷容器

数据卷容器其实就是一个正常的容器，专门用提供数据卷供其他容器挂载的。其他容器可以通过`--volumes-from`来挂载容器中的数据卷。

可以使用超过一个`--volumes-from`参数来指定从各个容器挂载不同的数据卷，也可以从其他研究挂载了数据卷的容器来级联挂载数据卷。

使用`--volumes-from`参数所挂载数据卷的容器组件并不需要保持在运行状态。

如果删除了挂载的容器，数据卷并不会被指定删除。如果要删除一个数据卷，编写在删除最后一个还挂载这他的容器时使用`docker rm -v`命令来指定同时删除关联的容器。

### 利用数据卷容器来备份、回复、迁移数据卷

#### 备份

首先使用`--volumes-from`标记来创建一个加载dbdata容器卷的容器，并从主机挂载当前目录到容器的/backup目录下

```
docker run --volumes-from dbdata -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /dbdata
```

容器启动后，使用例如`tar`命令来将dbdata卷备份为容器中/backup/backup.tar文件，也就是主机当前目录下的名为`backup.tar`的文件。

#### 恢复

如果要恢复数据到一个容器，首先创建一个带有空数据卷的容器dbdata2.

```
docker run -v /dbdata --name dbdata2 ubuntu /bin/bash
```

然后创建另一个容器，挂载dbdata2容器卷中的数据卷，并使用`untar`解压备份文件到挂载的容器卷中。

```
docker run --volumes-from dbdata2 -v $(pwd):/backup busyb ox tar xvf /backup/backup.tar
```

# docker网络功能

docker允许通过外部访问容器或容器互联的方式来提供网络服务。

## 外部访问容器

容器中可以运行一下网络应用，要让外部也可以访问这些应用，可以同`-p`或者`-P`参数来指定端口映射。

当使用`-P（大写）`标记时，docker会随机映射一个`49000~49900`的端口到内部容器开发的网络端口。

`-p（小写）`则可以指定要映射的端口，并且在一个指定端口上只可以绑定一个容器。支持的格式有：`ip:hostPort:containerPort（映射所有接口地址）` | `ip::containerPort（映射到指定地址的指定端口）` | `hostPort:containerPort（映射到指定地址的任意端口）`

可使用`docker port`来查看当前映射的端口配置

**注意：**

- 容器有自己的内部网络和ip地址（使用`docker inspect`可以获取所有的变量，docker还可以有一个可变的网络配置）
- `-p`标记可以多吃使用来绑定多个端口

## 容器互联

容器的连接（linking）系统是除了端口映射外，另一种跟容器中应用交互的方式。该系统会在源和接收容器直接创建一个隧道，接收容器可以看到源容器指定的信息。

连接系统依据容器的名称来执行，可使用`–name`标记为容器自定义命名。

使用`--link`参数可以让容器之间安全的进行交互，而且不要映射它们的端口到宿主直接上。

docker通过2中方式为容器公开连接信息：

- 环境变量
- 更新`/etc/hosts`文件

可使用`env`命令来查看web容器的环境变量。

除了环境变量，docker还添加host信息到父容器的`/etc/hosts`文件中。

## 高级网络配置

当docker启动时，会自动在主机上创建一个`docker0`的虚拟网桥，实际上是Linux的一个bridge，可以理解为一个软件交换机。它会挂载到它的网口直接进行转发。

同时，docker随机分配一个本地未占用的私有网段中的一个地址给`docker0`接口。

当创建一个docker容器的时候，同时会长久一对`veth pair`接口（当数据发送到一个接口时，另一个接口也可以收到相同的数据包）。这对接口一段在容器内，即`eth0`；另一端在本地并被挂载到`docker0`网桥，名称以`veth`开头。通过这种方式，主机可以跟容器通信，容器之间也可以相互通信。docker就创建了主机和所有容器直接一个虚拟共享网络。

![docker虚拟网络](docker虚拟网络.png)

## docker 网络类型

- bridge
  - 建立一个虚拟桥接网络，在此网络下的container可以通过ip或容器名互相通信
  - docker安装后会生成一个默认的网络docker0，创建容器时若不指定网络都会加入到docker0中
- host
  - 复制物理机的网络信息，使用物理机的ip和端口，容易造成端口冲突
- none
  - 只有一个回环地址，不能与任何网络交互

## 自定义网络

```bash
#创建网络
docker network create my-net
#移除网络
docker network rm my-net
#容器连接到网络
docker network connect my-net my-nginx
#离开自定义网络
docker network disconnect my-net my-nginx
```



# docker常用操作命令

- 查找镜像
  - docker search <image>
- 拉取镜像		
  - docker pull <image>
- 创建并启动容器   

  - docker run –-name [name] -d  -p [宿主端口]:[docker 端口] -v [宿主路径]:[docker 路径] 镜像名
    - —name 重命名容器
    - -d 后台运行
    - -p 映射端口
    - -v 映射文件夹 (windwos需打开磁盘共享)
- 停用容器

  - docker stop 容器名
- 启动容器

  - docker start 容器名
- 查看镜像

  - docker images
- 查看容器

  - docker ps -a
    - ps 查看正在运行的容器
    - -a 查看所有容器
- 上传文件

  - docker cp [宿主文件（绝对路径）] 容器名 [容器路径]
- 下载文件

  - docker cp 容器名 [容器文件] : [宿主路径]
- 登录容器

  - docker exec -it 容器名 /bin/sh
- 配置本地仓库
  - docker run -v [本地仓库路径]:/var/lib/registry -p [本地端口]:5000 registry
  - registry的启动默认使用http，docker push/pull默认使用https，所以需在配置中添:[“本地ip:本地端口”]
  - 首先创建镜像 docker tag 镜像名 本地仓库ip:本地仓库端口/镜像名:版本号
  - 上传 docker push 本地仓库ip:本地仓库端口/镜像名
  - 下载 docker pull 本地仓库ip:本地仓库端口/镜像名
- 获取容器/镜像元数据
  - docker inspect [OPTIONS] NAME|ID [NAME|ID...]
- 查看dockr消耗资源
  - docker  stats
- 查看容器的挂载目录
  - docker inspect --format='{{.GraphDriver.Data.MergedDir}}' 容器 ID 或名字

# 搭建容器示例

## haproxy + percona-xtradb-cluster + mysql集群

### 集群工具percona-xtradb-cluster

```bash
# 拉取pxc镜像
docker pull percona/percona-xtradb-cluster:5.7.21
	
# 复制pxc镜像(实则重命名)
docker tag percona/percona-xtradb-cluster:5.7.21 pxc

# 删除pxc原来的镜像
docker rmi percona/percona-xtradb-cluster:5.7.21
```

### 设置网络

```bash
# 创建一个单独的网段，给mysql数据库集群使用
docker network create --subnet=172.18.0.0/24 pxc-net
```

### 创建volume

```bash
#准备3个数据卷（若映射外部文件夹需要注意写权限）
docker volume create --name v1
docker volume create --name v2
docker volume create --name v3
```

### 搭建mysql集群

```bash
#主节点
# MYSQL_ROOT_PASSWORD： mysql root密码
# CLUSTER_NAME: 集群名称
# XTRABACKUP_PASSWORD: XTRABACKUP密码（XTRABACKUP用于集群中节点的同步）
docker run -d -p 3301:3306 -v v1:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=root --privileged --name=node1 --net=pxc-net --ip 172.18.0.2 pxc

#从节点
# CLUSTER_JOIN=node1: 将该数据库加入到node1节点上组成集群
docker run -d -p 3302:3306 -v v2:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=root -e CLUSTER_JOIN=node1 --privileged --name=node2 --net=pxc-net --ip 172.18.0.3 pxc

docker run -d -p 3303:3306 -v v3:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=root -e CLUSTER_JOIN=node1 --privileged --name=node3 --net=pxc-net --ip 172.18.0.4 pxc
```

### 负载均衡haproxy

```bash
# 拉取haproxy镜像
docker pull haproxy

# 创建haproxy配置文件，使用bind mounting的方式方便以后修改配置
touch /tmp/haproxy/haproxy.cfg

# 创建容器
docker run -it -d -p 8888:8888 -p 3306:3306 -v /tmp/haproxy:/usr/local/etc/haproxy --name haproxy01 --privileged --net=pxc-net haproxy

#若负载均衡为生效需要进入容器加载配置文件
docker exec -it haproxy01 bash
haproxy -f /usr/local/etc/haproxy/haproxy.cfg
```

`haproxy.cfg`内容

```shell
global
	#工作目录，这边要和创建容器指定的目录对应
	chroot /usr/local/etc/haproxy
	#日志文件
	log 127.0.0.1 local5 info
	#守护进程运行
	daemon

defaults
	log	global
	mode	http
	#日志格式
	option	httplog
	#日志中不记录负载均衡的心跳检测记录
	option	dontlognull
 	#连接超时（毫秒）
	timeout connect 5000
 	#客户端超时（毫秒）
	timeout client  50000
	#服务器超时（毫秒）
 	timeout server  50000

    #监控界面	
    listen  admin_stats
	#监控界面的访问的IP和端口
	bind  0.0.0.0:8888
	#访问协议
 	mode        http
	#URI相对地址
 	stats uri   /dbs_monitor
	#统计报告格式
 	stats realm     Global\ statistics
	#登陆帐户信息
 	stats auth  admin:admin
	#数据库负载均衡
	listen  proxy-mysql
	#访问的IP和端口，haproxy开发的端口为3306
 	#假如有人访问haproxy的3306端口，则将请求转发给下面的数据库实例
	bind  0.0.0.0:3306  
 	#网络协议
	mode  tcp
	#负载均衡算法（轮询算法）
	#轮询算法：roundrobin
	#权重算法：static-rr
	#最少连接算法：leastconn
	#请求源IP算法：source 
 	balance  roundrobin
	#日志格式
 	option  tcplog
	#在MySQL中创建一个没有权限的haproxy用户，密码为空。
	#Haproxy使用这个账户对MySQL数据库心跳检测
 	option  mysql-check user haproxy
	server  MySQL_1 172.18.0.2:3306 check weight 1 maxconn 2000  
 	server  MySQL_2 172.18.0.3:3306 check weight 1 maxconn 2000  
	server  MySQL_3 172.18.0.4:3306 check weight 1 maxconn 2000 
	#使用keepalive检测死链
 	option  tcpka
```

在MySQL数据库上创建用户，用于心跳检测

```mysql
CREATE USER 'haproxy'@'%' IDENTIFIED BY '';
-- 如果创建失败，可以先输入一下命令
    drop user 'haproxy'@'%';
    flush privileges;
    CREATE USER 'haproxy'@'%' IDENTIFIED BY '';
```

在浏览器中查看

http://centos_ip:8888/dbs_monitor
用户名密码都是:admin

### 需要注意的点

1. volume如果映射到外部文件夹需要增加写权限

2. mysql容器创建后无法远程的需要进容器增加远程访问权限

   ```sql
   GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; 
   FLUSH PRIVILEGES;
   ```

3. 集群主节点必须开启远程访问权限，否则无法连接

4. percona xtradb cluster不要选最新版本，有闪退的问题，原因不明（可能是权限问题）

5. 集群搭建成功后如果直接关闭docker会造成集群没有主节点，重启后无法运行各个节点，可到node1节点的volume文件夹下修改`grastate.dat`文件，将`safe_to_bootstrap`的值该为1（1表示此节点时安全的，可以启动）。如果要保证集群重启后能正常启动需要逐个关闭节点，最后关闭的节点`safe_to_bootstrap`的值为1。集群启动时必须先从最后关闭的节点开始。