# jenkins默认配置

jenkins安装目录：`/var/lib/jenkins `
jenkins日志目录：`/var/log/jenkins/jenkins.log`
jenkins默认配置：`/etc/default/jenkins`



# 全局工具配置

## maven

![1591868237627](1591868237627.png)

## maven配置

![1591868157088](1591868157088.png)

## jdk

![1591868178755](1591868178755.png)

## git

![1591868196367](1591868196367.png)





# Jenkins 以 root 权限运行

- 将 jenkins 账号加入到 root 组中
  - `gpasswd -a jenkins root`
- 编辑jenkins 的配置文件， 将 `JENKINS_USER` 的值修改为 root



# webhook

- 安装gitlab插件
  - `gitlab hook plugin`
  - `gitlab plugin`
- 在gitlab中生成`api-token`
  - ![1591867339108](1591867339108.png)
  - 生成api-token只显示一次，需要保存
- 设置gitlab
  - jenkins全局配置-->gitlab
  - ![1591867146992](1591867146992.png)
  - 增加credentials
  - ![1591867488120](1591867488120.png)
- 创建项目，构建触发器
  - ![1591867610168](1591867610168.png)
  - 点开高级，设置`secret token`
  - ![1591867660311](1591867660311.png)
- 在gitlab中配置webhook
  - 在项目settings-->integrations中填写上一步的`GitLab webhook URL`和`secret token`
  - 选择触发trigger
  - 关闭`enable ssl verification`
  - ![1591867868535](1591867868535.png)

# pipeline后台进程自动退出问题

 在普通的shell环境中，nohup，并且& 某个程序后，会抛到后台执行，在退出当前shell环境后，程序依然可以执行。但是在Jenkins的pipeline中，通过nohup，且使用&之后，step结束后，执行的程序还是会退出，导致程序起不来。

解决方法：

`sh 'JENKINS_NODE_COOKIE=dontKillMe ./script/start.sh'`

```shell
stage('run jar') {
          steps{
            sh "cd /home/message/ && JENKINS_NODE_COOKIE=dontKillMe ./start.sh"
          }   
      }
```

脚本

```shell
#!/bin/sh
#关闭进程
ps -ef | grep d2-message | grep -v grep | awk '{print $2}'| xargs kill -9

#BUILD_ID=dontKillMe 测试无效
nohup java -jar -Xms128m -Xmx512m /**/**.jar --spring.profiles.active=prod &
#延时10秒，可查看jar包运行信息
sleep 10s

```

# 安装插件提速

修改`/var/lib/jenkins/updates/default.json`

```shell
sed -i 's/http:\/\/updates.jenkins-ci.org\/download/https:\/\/mirrors.tuna.tsinghua.edu.cn\/jenkins/g' default.json && sed -i 's/http:\/\/www.google.com/https:\/\/www.baidu.com/g' default.json
```

 

## 

ssh-copy-id root@172.17.16.17