# centos7 安装gitlab

## 硬件要求

### CPU 

- 1核心CPU最多支持100个用户，所有的workers和后台任务都在同一个核心工作这将导致GitLab服务响应会有点缓慢。
- 2核心支持500用户，这也是官方推荐的最低标准。
- 4核心支持2,000用户。
- 8核心支持5,000用户。
- 16核心支持10,000用户。
- 32核心支持20,000用户。
- 64核心支持40,000用户。
- 如果想支持更多用户，可以使用 集群式架构

### Memory 

安装使用GitLab需要至少4GB可用内存(RAM + Swap)，由于操作系统和其他正在运行的应用也会使用内存，所以安装GitLab前一定要注意当前服务器至少有4GB的可用内存。

少于4GB内存会导致在reconfigure的时候出现各种诡异的问题，而且在使用过程中也经常会出现500错误.

- 1GB 物理内存 + 3GB 交换分区 是最低的要求，但不建议使用这样的配置。
- 2GB 物理内存 + 2GB 交换分区 支持100用户，但服务响应会很慢。
- 4GB 物理内存 支持100用户，也是 官方推荐 的配置。
- 8GB 物理内存 支持 1,000 用户。
- 16GB 物理内存 支持 2,000 用户。
- 32GB 物理内存 支持 4,000 用户。
- 64GB 物理内存 支持 8,000 用户。
- 128GB 物理内存 支持 16,000 用户。
- 256GB 物理内存 支持 32,000 用户。
- 如果想支持更多用户，可以使用 集群式架构

即使服务器有足够多的RAM， 也要给服务器至少分配2GB的交换分区。 因为使用交换分区可以在可用内存波动的时候降低GitLab出错的几率。
## 安装过程

### 安装和配置必要的依赖

在CentOS 7 (and RedHat/Oracle/Scientific Linux 7),下面这些命令是在防火墙中开放HTTP和SSH的访问

```shell
sudo yum install -y curl policycoreutils-python openssh-server
sudo systemctl enable sshd
sudo systemctl start sshd
sudo firewall-cmd --permanent --add-service=http
sudo systemctl reload firewalld
```

### 安装Postfix来实现邮件通知的功能

```shell
sudo yum install postfix
sudo systemctl enable postfix
sudo systemctl start postfi
```

若使用其他的邮件服务器，可以跳过这些命令，在gitlab安装完毕后自行安装其他的邮件服务器

##### 配置GitLab的仓库地址并下载安装包

添加仓库地址

```shell
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
```

安装GitLab

```shell
yum install -y gitlab-ee
```

另，如果在线安装慢可以在https://packages.gitlab.com/gitlab/gitlab-ee下载安装包，使用rpm命令安装

```shell
rpm -ivh gitlab-ee-12.4.0-ee.0.el7.x86_64.rpm
```

## 常见配置及命令

### 配置、服务

- gitlab配置文件：/etc/gitlab/gitlab.rb

  - 修改访问地址

    ```properties
    #修改两个冲突端口号
    external_url 'http://ip:8899'
    unicorn['port'] = 8088
    ```

    

- 重新加载配置：gitlab-ctl reconfigure

- 重启服务：gitlab-ctl restart

- 启动服务：gitlab-ctl start

- 停止服务：gitlab-ctl stop

### 日志位置

- 日志路径： /var/log/gitlab
- 查看所有日志：gitlab-ctl tail
- 查看nginx日志：gitlab-ctl tail nginx/gitlab_access.log
- 查看指数据库日志：gitlab-ctl tail postgresql

### 数据库

- 重启数据库： gitlab-ctl restart postgresql
- 数据库配置文件：（修改内容后，需要修改对应的 /etc/gitlab/gitlab.rb 配置，否则重新加载gitlab配置文件后修改会失效）
  - /var/opt/gitlab/gitlab-rails/etc/database.yml
  - /var/opt/gitlab/postgresql/data/postgresql.conf
  - /var/opt/gitlab/postgresql/data/pg_hba.conf

## 常见502错误，解决方案

- 首先看看配置文件/etc/gitlab/gitlab.rb 中的端口号是否被占用


- 另外一个原因是gitlab占用内存太多，导致服务器崩溃
  - 内存问题 解决办法,启用swap分区，步骤如下：

    ```shell
    #查看swap分区是否启动（无）
    cat /proc/swaps 
    #创建swap大小为bs*count=4294971392(4G)；
    dd if=/dev/zero of=/data/swap bs=512 count=8388616
    #通过mkswap命令将上面新建出的文件做成swap分区
    mkswap /data/swap
    #查看内核参数vm.swappiness中的数值是否为0，如果为0则根据实际需要调整成60
    #查看vm.swappiness
    cat /proc/sys/vm/swappiness
    #设置vm.swappiness
    sysctl -w vm.swappiness=60
    
    #若想永久修改，则编辑/etc/sysctl.conf文件，改文件中有vm.swappiness变量配置，默认为0
    
    #启用分区
    swapon /data/swap
    
    echo "/data/swap swap swap defaults 0 0" >> /etc/fstab
    
    #再次查看swap分区是否启动
    cat /proc/swaps
    ```

  - 重启gitlab。