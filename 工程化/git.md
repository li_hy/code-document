# 常见问题

#### 每次push都要输入账号密码

采用的是https方式提交代码，如果采用的是ssh方式只需要在版本库中添加用户的sha的key就可以实现提交时无需输入用户名和密码。

```bash
git remote -v
git remote rm origin
git remote add origin git@gitee.com:username/repository.git
git push -u origin master
```

生成公钥命令

```bash
ssh-keygen -t rsa -C "xxxxx@xxxxx.com"
```

#### 配置多lab账号

在`C:\Users\用户名\.ssh`下配置`config`文件

```bash
#github(li_hy2004@163.com)
Host github.com
HostName github.com
User lihy2002
IdentityFile C:/Users/Administrator/.ssh/id_rsa

#gitee(li_hy2004@163.com)
Host gitee.com
HostName gitee.com
User li_hy2002
IdentityFile C:/Users/Administrator/.ssh/id_rsa_gitee
```

#### 文件名/路径太长

配置支持长文件名

```bash
git config --system core.longpaths true
```

#### 提交代码的时候，遇到warning: LF will be replaced by CRLF警告

LF和CRLF的真相是什么 1.LF和CRLF都是换行符，在各操作系统下，换行符是不一样的，Linux/UNIX下是LF,而Windows下是CRLF，早期的MAC OS是CR,后来的OS X在更换内核后和UNIX一样也是LF。

需要修改git全局配置，禁止git自动将LF转化成CRLF。

```bash
git config --global core.autocrlf false
```

