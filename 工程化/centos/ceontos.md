# 安装注意

- 时区：东八区
- 分区
  - `/`：根分区（必须）
  - `swap`：交换分区，相当于windows的虚拟内存；建议8G/16G（官方为内存2倍）
  - `/boot`：启动分区，存放启动文件，例如内核kernel；建议1G
  - `/home`：用户分区
  - `/data`：业务分区
- `KDUMP`：黑匣子，不启用
- 网络：可后期再配置，在常规中将“可用时自动链接到这个网络”开启，否则开机不能自动联网
  - 主机名：根据业务规范

# BASH

## 提示符

命令`$PS1`，显示提示符格式

## shell语法

shell语法格式：命令 + 空格 + 选项 + 空格 + 参数

- 命令：整体shell命令的主题
- 选项：会影响或微调命令的行为，通常以`-`，`--`开头
- 参数：命令作用的对象

**Linux中凡是以`.`开始的文化都是隐藏文件**

## bash基本特性

### 自动补全：`tab`

按一次`tab`键会补全唯一补齐，按两次`tab`键会列出已输入字符开头的命令/选项/参数

适合用于补全文件路径

### 快捷键

- `ctrl+c`：终止当前运行的程序
- `ctrl+d`：退出，等价`exit`
- `ctrl+l`：清屏
- `ctrl+a`：光标移到命令行最前端
- `ctrl+e`：光标移到到命令行最后段
- `ctrl+u`：删除光标前所有字符
- `ctrl+k`：删除光标后所有字符
- `ctrl+r`：利用关键词搜索历史命令
- `alt+.`，`esc+.`：引用上一个命令的最后一个参数，等价于`!$`

### 历史命令

`history`命令：显示之前输入的所有命令，并带行数

再次执行某条历史命令可使用`!`+行号

`!$`：引用上一条命令的最后一个参数

### 命令别名

`alias`：查看系统当前的别名

`alias 别名='...'`：建立别名（临时的，仅在当前shell有效）

`unalias 别名`：取消别名

`type -a 命令名称`：查看命令类型

永久别名

在`/etc/bashrc`下增加`alias 别名='...'`，保存后使用`source ~/.bashrc`命令生效



 **`type`命令**

查看命令类型，例如该命令是alias，还是内置命令，还是某个文件

**`file`**命令

查看文件类型，例如文本文件，二进制文件，管道文件，设备文件，链接文件

**`stat`**命令

查看文件的属性，例如文件的名称，大小，权限，atime，ctime，mtime等

## 常用操作

### 网络操作

- `hostnamectl set-hostname xxx` 修改主机名

- `vi /etc/sysconfig/network-scripts/ifcfg-ensxx` 修改网络配置

  - ```bash
    #主要需要修改的参数
    
    #静态ip
    BOOTPROTO=static
    #网卡UUID
    UUID=85d4875f-7763-4351-b021-227cb29b3430
    #开机启动
    ONBOOT=yes
    #DNS
    DNS1=218.2.2.2
    DNS2=218.4.4.4
    #ip
    IPADDR=192.168.0.53
    #子网掩码
    NETMASK=255.255.255.0
    #网关
    GATEWAY=192.168.0.1
    
    ```

- `service network start/restart/stop` 网卡服务开启/重启/关闭

- `firewall-cmd --permanent --zone=public --add-port/remove-port=80/tcp` 增加/删除防火墙端口

- `firewall-cmd --list-all` 列出防火墙开放端口

- `systemctl status/start/stop/restart firewalld.service` 查看/启动/关闭/重启防火墙

- `systemctl enable/disable 服务名` 设置开机启动/不启动服务

### 上传/下载

- `yum -y install lrzsz`：安装`rz/sz`命令
- `rz`：上传文件
- `sz 文件名`：下载文件

### 设置selinux

- `getenforce`：查看selinux，`Enforcing`默认状态，表示强制启动；`Permissive`宽容，大部分规则都放下；`Disabled`禁用，不设置任何规则

- `setenforce 0/1/2`：设置`Permissive`/`Enforcing`/`Disabled`，重启后失效

- 修改`/etc/selinux/config`文件

  ```properties
  SELINUCX=Permissive/Enforcing/Disabled
  ```

### 内存管理

- 查看内存：`free -m`
- 释放内存
  - 释放网页缓存(To free pagecache) : `echo 1 > /proc/sys/vm/drop_caches`
  - 释放目录项和索引(To free dentries and inodes) : `echo 2 >/proc/sys/vm/drop_caches`
  - 释放网页缓存，目录项和索引（To free pagecache, dentries and inodes）: `echo 3 >/proc/sys/vm/drop_caches`

### 不把info日志记录到/var/log/message日志中

- 修改`/ect/rsyslog.cnf`

  ```bash
  #记录所有 info 级别以上的日志到 /var/log/messages，但会排除 auth 和 authpriv 设施的日志（这通常包括 SSH 登录尝试等）
  *.info;auth,authpriv.none;mail.none      -/var/log/messages
  #修改为
  *.none                /var/log/messages
  
  ```

- 检查配置，使用`rsyslogd -N1`检查语法是否正确

- 重启 `rsyslog `服务，`systemctl restart rsyslog`

## 常用命令

`ls -lrt`：查看链接文件的链接路径

`which`：查看应用路径

`pwd`：查看当前路径

# 文件

## 文件结构

Linux是以单根方式组织文件的。centos7的目录结构如下所示

![centos7目录结构](centos7目录结构.png)

- `bin`：普通用户使用的命令，`/sbin/bin`的引用，例如`/bin/ls`，`/bin/date`
- `sbin`：管理员使用的命令，`/sbin/sbin`的引用，例如`/sbin/service`
- `dev`：设备文件，例如`dev/sda`，`dev/sda1`，`dev/tty1`，`/dev/pts/1`，`/dev/zero（零设备）`，`/dev/null（空设备，类似回收站）`，`/dev/random（参数随机数）`
- `root`：root用户的HOME目录
- `home`：存储普通用户HOME目录
- `proc`：虚拟的文件系统，反映出来的是内核，进程信息或实时状态
- `usr`：系统文件，相对于`C:\Windows`
  - `usr/local`：软件安装的目录，相对于`C:\Program`
  - `/usr/bin`：普通用户使用的应用程序
  - `/usr/sbin`：管理员使用的应用程序
  - `/usr/lib`：库文件Glibc（32位）
  - `/usr/lib64`：库文件Glibc（64位）
- `boot`：存放的系统启动相关文件，例如`kernel`，`grub（引导装载程序）`
- `etc`：配置文件，例如`/etc/syscongif/network（网络配置）`，`/etc/ssh/sshd_config..（应用配置）`
- `lib`：库文件Glibc（32位），是`/usr/lib`的引用
- `lib64`：库文件Glibc（63位），是`/usr/lib64`的引用
- `tmp`：临时文件（全局可写：进程产生的临时文件）
- `var`：存放的是一些变化文件，比如数据库，日志，邮件等
  - `/var/lib/mysql`：mysql
  - `var/ftp`：vsftpd
  - `/var/spool/mail`：mail
  - `/var/log`：log
  - `/var/tmp`：临时文件（进程产生的临时文件）

## 文件命令

### 文件操作

- `touch`：创建文件（无则创建，有则修改时间）
- `mkdir`：创建目录
- `cp`：复制并重命名
- `mv`：移动
- `rm`：删除
  - `-r`：递归
  - `-f`：强制
  - `-v`：详细过程

使用`{,}`可省略重复部分，例如`touch /home/dir/{001,002}`等于`touch /home/dir/001`和`touch /home/dir/002`

### 文件查看

- `cat`：适合看内容较少的文件
  - `-n`：显示行号
  - `-A`：包括控制字符（换行符/制表符），Windows的换行符是`^M$`，Linux是`$`
    - 若在Windows下使用记事本编写脚本，因换行符不一致，在Linux下会无法执行，可用`dos2unix`程序转换
- `less`，`more`：可分页显示文件内容
- `head`，`tail`：分别查看文件的指定的n行和最后指定的n行，默认10行
- `tailf`：动态查看文件最后指定的n行
  - `vim`，`gedit`编辑文件时，索引号会改变
- `grep`：针对文件内容进行过滤，支持正则

### 文件编辑 vi

#### vi的工作模式

![vi工作模式](vi工作模式.png)



#### 命令模式

- 光标定位
  - `hjkl建`：光标上下左右移动
  - `0`， `$`：光标移至当前行首和行尾
  - `gg`：光标定位到第一行
  - `G`：光标定位到最后一行
  - `nG`：n代表数字，光标定位到第n行
  - `/string`：查找string并定位到第一个string，使用`n`，`N`可以上下移动
  - `^d`：以d开通的字符并定位到第一个d
  - `/txt$`：以txt结尾的字符并定位到第一个txt
- 文本编辑（少量）
  - `y`：复制，yy复制一行，3yy复制3行，ygg从光标行复制到第一行，yG从光标行复制到最后一行
  - `d`：删除，dd，3dd，dgg，dG与上类似
  - `p`：粘贴
  - `x`：删除光标所在的字符
  - `D`：从光标处删除到行尾
  - `u`：undo撤销
  - `^r`：redo重做
  - `r`：可以用来修改一个字符
- 进入其他模式
  - 进入插入模式
    - `i`：插入到光标前
    - `I`：插入到当前行首
    - `o`：在当前行下新开一行
    - `O`：在当前行之上新开一行
    - `a`：插入到光标后
    - `A`：插入到当前行尾
  - 进入末行模式（扩展命令模式）
    - `:`
  - 进入可视模式
    - `v`：进入可视模式
    - `^v`：进入可视块模式
    - `V`：进入可视行模式
    - `R`：进入替换模式

#### 插入模式

- `^p`，`^n`：自动补全

#### 可视块模式

- 块插入（在指定块钱加入字符）：选择块，`I`，在块前插入字符，`ESC`
- 块替换：选择块，`r`，输入替换的字符
- 块删除：选择块，`d`或者`x`
- 块复制：选择块，`y`

#### 扩展命令模式

##### 保存退出

- `:n`：进入第n行
- `:w`：保存
- `q`：退出
- `:wq`：保存并退出
- `:w!`：强制保存
- `:q!`：不保存并退出
- `:wq!`：强制保存退出
- `:x`：保存并退出（ZZ可在命令模式下保存退出）
- `X`：给文件加密码

##### 查找替换

- ：范围 s/old/new/选项
  - `1,5 s/root/sa/`：把1-5行的root替换为sa
  - `5,$ s/root/sa/`：把第5行到最后一行的root替换为sa
  - `1,$ s/root/sa/`：等同`:% s/root/sa/g`，%表示全文，g表示全局
  - `:% s#/dev/sda#/var/cc#g`：分隔符`/`可以用`#`，`@`等其他符号代替，用来区别替换字符中的`/`
  - `:,8 s/root/sa/`：把当前行到第8行的root替换为sa
  - `:4,9 s/^#//`：4-9行的开头#替换为空
  - `:5,10 s/.*/#&/`：5-10行前加入#（.*表示整行，&表示引用查找的内容）
- 读入文件/写文件（另存）
  - `:w`：存储到当前文件
  - `:w /tmp/aa`：另存为/tmp/aa
  - `:1,3 w /tmp/bb`：将1-3行另存到/tmp/bb
  - `:r /etc/hosts`：读入文件到当前行后
  - `:5 r /etc/hosts`：读入文件到第5行后
- 设置环境
  - 临时环境
    - `:set nu(number)`：设置行号
    - `:set ic`：不区分大小写
    - `:set ai`：自动缩进
    - `:set list`：显示控制字符（换行符、tab符）
    - `:set nonu`：取消设置行号
    - `:set noic`：区分大小写
    - `set nolist`：不显示控制字符
  - 永久设置
    - `/etc/vimrc`：影响所有系统用户
    - `~/.vimrc`：影响某一个用户

## 文件时间

Linux文件有四种时间

- 访问时间：`atime`，查看内容
- 修改时间：`mtime`，修改内容
- 改变时间：`ctime`，文件属性，比如权限
- 删除时间：`dtime`，文件被删除

`ls -l 文件名`仅看的是文件的修改时间。

`stat 文件名`：查看文件的详细信息（其中包括文件的时间属性）

RHEL6开始加入了relatime属性，atime延迟修改，必须满足其中一个条件：

- 自上次atime修改后，已达到86400秒
- 发生写操作时

## 文件类型

通过颜色判断文件的类型不一定是正确的；Linux系统中的文件，没有扩展名。

判断文件类型的方法：

- 使用`ls -l 文件名`命令，查看第一个字符
  - `-`：普通文件（文本文件、二进制文件、压缩文件、电影、图片……）
  - `d`：目录文件（显示为蓝色）
  - `b`：设备文件（块设备），硬盘、U盘等
  - `c`：设备文件（字符设备），打印机、终端等
  - `s`：套接字文件
  - `p`：管道文件
  - `l`：链接文件（淡蓝色）
- 使用`file`命令查看文件类型

# /etc/profile、/etc/bashrc、~/.bash_profile、~/.bashrc文件

### /etc/profile

此文件为系统的每个用户设置环境信息，当用户第一次登录时，该文件被执行。并从 `/etc/profile.d `目录的配置文件中收集 shell 的设置。如果有对` /etc/profile` 修改的话必须得 `source `一下你的修改才会生效，此修改对每个用户都生效。

### /etc/bashrc

为每一个运行 bash shell 的用户执行此文件。当 bash shell 被打开时，该文件被读取。如果你想对所有的使用 bash 的用户修改某个配置并在以后打开的 bash 都生效的话可以修改这个文件，修改这个文件不用重启，重新打开一个 bash 即可生效。

### ~/.bash_profile

每个用户都可使用该文件输入专用于自己使用的 shell 信息，当用户登录时，该文件仅仅执行一次！默认情况下,它设置一些环境变量，执行用户的`~/ .bashrc` 文件。 此文件类似于 `/etc/profile`，也是需要需要 `source `才会生效，`/etc/profile` 对所有用户生效，`~/.bash_profile` 只对当前用户生效。`~/.profile`(由Bourne Shell和Korn Shell使用)和`.login`(由C Shell使用)两个文件是`.bash_profile`的同义词，目的是为了兼容其它Shell。

### ~/.bashrc

该文件包含专用于用户的 bash shell 的 bash 信息，当登录时以及每次打开新的 shell 时，该文件被读取。（每个用户都有一个` ~/.bashrc` 文件，在用户目录下） 此文件类似于` /etc/bashrc`，不需要重启就可以生效，重新打开一个 bash 即可生效，`/etc/bashrc` 对所有用户新打开的 bash 都生效，但 `~/.bashrc` 只对当前用户新打开的 bash 生效。
` ~/.bashrc`文件会在bash shell调用另一个bash shell时读取，也就是在shell中再键入bash命令启动一个新shell时就会去读该文件。这样可有效分离登录和子shell所需的环境。但一般 来说都会在`/.bash_profile`里调用`/.bashrc`脚本以便统一配置用户环境。

### 区别和联系

- 在 `/etc`目录是系统级（全局）的配置文件，当在用户主目录下找不到`~/.bash_profile` 和`~/.bashrc`时，就会读取这两个文件。
- `/etc/profile` 中设定的变量(全局)的可以作用于任何用户，而 `~/.bashrc` 中设定的变量(局部)只能继承` /etc/profile` 中的变量，他们是“父子”关系。
- `~/.bash_profile` 是交互式、login 方式进入 bash 运行的；` ~/.bashrc` 是交互式 non-login 方式进入 bash 运行的。通常二者设置大致相同，所以通常前者会调用后者。设置生效：可以重启生效，也可以使用命令：`source`。
- `~/.bash_history`是bash shell的历史记录文件，里面记录了你在bash shell中输入的所有命令。可通过HISSIZE环境变量设置在历史记录文件里保存记录的条数

# 用户

## 用户/组基本概念

![用户和组](用户和组.png)

- 系统上的每个进程（运行的程序）都是作为特定用户运行的
- 每个文件是由一个特定的用户拥有
- 访问文件和目录受到用户的限制
- 与正在运行的进程相关联的用户确定该进程可访问的文件和目录

### 相关命令

- `id 用户名`：查看用户信息
- `ll`：查看文件的owner
- `ps aux|less`：查看运行进程的username

### 和用户组相关的一些文件

- `/etc/passwd`：用户信息，分为7段，分别表示：用户名、密码占位符、uid、gid、描述、HOME、shell。如：`root:x:0:0:root:/root:/bin/bash`
- `/etc/shadow`：密码信息，分为9段，分别表示：用户名、密码（分3段，用`$`分割，分别表示：加密算法（1：MD5；5：SHA-256；6：SHA-512）、salt、加密后的密码）、最后改变密码的时间、密码的最小年龄、密码的最大年龄、密码到期前的警告天数、密码过期后的宽限天数、密码的过期时间、保留字段。如：`root: $6$9w5Td6lg
  $bgpsy3olsq9WwWvS5Sst2W3ZiJpuCGDY.4w4MRk3ob/i85fl38RH15wzVoom ff9isV1 PzdcXmixzhnMVhMxbvO:15775:0:99999:7:::`
- `/etc/group`：用户组信息，分为4段，分别表示：组名称、占位符、gid、成员列表。例如：`mail:x:12:postfix,tom`

### root 用户

- uid是0
- 拥有所有权力
- 该用户有权力覆盖文件系统上的普通权限
- 按照或删除软件并管理系统文件和目录
- 大多数设备只能由root控制

## 用户管理

- `groupadd`：创建用户组
- `groupdel`：删除用户组
- `useradd`：创建用户（如果创建一个用户时，未指定任何组（包括主组和附加组），系统会创建一个和用户名相同的组作为用户的主组）
- `userdel`：删除用户，不能删除用户的HOME目录和mail spool，需要删除的话要加`-r`
- `passwd`：修改用户密码
- `usermod`：修改用户信息
- `gpasswd`：修改用户组信息

### useradd参照文件

`/etc/login.defs`和`/etc/default/useradd`这两个文件会影响到创建用户的行为。

`/etc/login.defs`常用参数：

- `MAIL_DIR`：邮件路径，默认`/var/spool/mail`
- `PASS_MAX_DAYS`：密码最大有效期
- `PASS_MIN_DAYS`：两次修改密码的最新间隔时间
- `PASS_MIN_LEN`：密码最小长度（对root无效）
- `PASS_WARN_AGE`：密码过期前多少天开始提示
- `CREATE_HOME`：是否创建用户目录
- `ENCRYPT_METHOD`：密码加密方式

`/etc/default/useradd`常用参数：

- `GROUP=100`：用户默认组
- `HOME=/home`：用户目录
- `INACTIVE=-1`：密码过期宽限天数
- `EXPIRE=`：密码失效时间
- `SHELL=/bin/bash`：默认shell
- `CREATE_MAIL_SPOOL=yes`：是否建立邮箱

## no shell

shell是用户登录后运行的第一个程序。

`/sbin/nologin`：用户无法登陆系统，实现管理。仅作为运行进程的用户。

`/bin/bash`：可以登录系统，实现管理。

## sudo普通用户提权

普通用户使用管理员权限有2种方式：

- 使用`su - [root]`切换为root账号使用，需要提供root密码
- 将用户加入`wheel`组，然后使用`sudo`+命令，只需要提供用户密码

# 文件权限

文件权限设置：可以赋予某个用户或组能够以何种方式访问某个文件

## UGO

![UGO](UGO.png)

权限对象：

- `U`：属主（user）
- `G`：属组（group）
- `O`：其他人（other）

权限类型：

- `r`：读	4
- `w`：写     2
- `x`：执行  1

| 权限        | 对文件的影响         | 对目录的影响                                 |
| ----------- | -------------------- | -------------------------------------------- |
| r（读取）   | 可以读取文件的内容   | 可以列出目录的内容（文件名）                 |
| w（写入）   | 可以更改文件的内容   | 可以创建或删除目录中的任一文件               |
| x（可执行） | 可以作为命令执行文件 | 可以访问目录的内容（取决于目录中文件的权限） |

设置权限：

- `chown`：更改文件的属主、属组
  - `chown [用户名称][.用户组，名称] file`
- `chgrp`：更改文件的属组

更改权限：`chmod`

- 使用符号设置

| 对象        | 赋值符      | 权限类型 |
| ----------- | ----------- | -------- |
| u           | +(增加权限) | r        |
| g           | -(减少权限) | w        |
| o           | =(覆盖权限) | x        |
| a(所有用户) |             |          |

- 使用数字设置

| 数字（三位数字，分别代表UGO） | 权限         |
| ----------------------------- | ------------ |
| 7                             | r            |
| 4                             | w            |
| 1                             | x            |
|                               | 取消所有权限 |

​	

## ACL（文件权限列表）

- `getfacl file`：查看文件权限列表
- `setfacl -m u/o:用户名/用户组名:r/w/x/- file`：修改用户/组对文件的权限
- `setfacl -x u/o:用户名/用户组名 file`：删除用户/组对文件的权限
- `setfacl -b file`：删除所有acl权限

设置ACL权限后，使用`ll`命令查看文件信息时会在文件权限后多一个`+`

### ACL高级特性

- `mask`（权限掩码）：用于临时降低用户或组（除属主和其他人）的权限，当有任何ACL行为时，mask会恢复原始权限
  - `setfacl -m m::- file`
- `default`：指定用户对文件夹及以后的文件夹下新建的文件有权限
  - `setfacl -m d:u:用户名:权限 文件夹`：只能针对以后的文件，不包括当前文件夹

## 特殊权限

- `suid`：用户将继承此程序的所有者权限，让本来没有相应权限的用户运行这个程序时可以访问他没有权限访问的资源，例如`password`

  - 仅对二进制程序有效
  - 执行者对应该程序需要具有`x`的可执行权限
  - 本权限仅在执行程序的过程中有效
  - 执行者将具有该程序拥有者的权限

- `sgid`：对于文件：此用户将继承此程序的所属组权限（不常用）；

  ​			 对于目录：此文件夹下所有用户新建文件都自动继承此目录的用户组

  - 对二进制程序有用
  - 程序执行者对应该程序来说需具备`x`的权限
  - 只有用在目录上

- `sticky`：粘滞位；设定后，目录中的用户只能删除、移动和改名自己的文件或目录

  - 设定`sticky`之后，只有`root`、文件所有者、目录所有者才可以删除、移动和改名文件

### 基本操作

```shell
chmod u+s file #添加SUID
chmod g+s file/dir #添加SGID
chmod +s file #同时添加SUID和SGID
chmod -s file #同时删除SUID和SGID
chmod o+t dir #添加Sticky
chmod +t dir #同上
```



suid：user, 占据属主的执行权限位；

- `s`： 属主拥有x权限
- `S`：属主没有x权限

sgid：group,  占据`group`的执行权限位；

- `s`： group拥有x权限
- `S`：group没有x权限

sticky：other, 占据`ohter`的执行权限位；

- `t`：other拥有`x`权限
- `T`：other没`x`权限

## 文件属性

命令`chattr`用于设置文件或目录属性（权限），是针对的所有用户，包括`root`,包括8个属性：

- a：让文件或目录仅供附加用途。
- b：不更新文件或目录的最后存取时间。
- c：将文件或目录压缩后存放。
- d：将文件或目录排除在倾倒操作之外。
- i：不得任意更动文件或目录。
- s：保密性删除文件或目录。
- S：即时更新文件或目录。
- u：预防意外删除。

语法：

```shell
chattr [-RV][-v<版本编号>][+/-/=<属性>][文件或目录...]
```

## 进程掩码：`umask`

新建文件、目录的默认权限会受到`umask`的影响，`umask`表示要减掉的权限。`umask`在`shell`、进程中可以单独设置。

`shell`中默认的`umask`值为0022，表示需要减去所属组和其他人的写权限（文件默认没有执行权限）

临时修改`shell`的`umask`值

```shell
umask 000
```

永久修改`shell`的`umask`值：修改对应的环境变量配置文件 `/etc/profile`

```shell
[root@localhost ~]# vim /etc/profile
...省略部分内容...
if [ $UID -gt 199]&&[ "'id -gn'" = "'id -un'" ]; then
    umask 002
    #如果UID大于199（普通用户），则使用此umask值
else
    umask 022
    #如果UID小于199（超级用户），则使用此umask值
fi
…省略部分内容…
```

# 进程

进程是已启动的可执行程序的运行实例，进程有以下几个组成部分：

- 已分配内存的地址空间
- 安全属性，包括所有权凭据和特权
- 程序代码的一个或多个执行线程
- 检查状态

程序：二进制文件，静态

进程：是程序运行的过程，动态，有生命周期及运行状态

## 进程的生命周期

![进程的生命周期](进程的生命周期.png)

父进程负责自己的地址空间（fork）创建一个新的（子）进程结构，每个新进程分配一个唯一的进程ID（PID），满足跟踪安全性之需。PID和父进程ID（PPID）是子进程环境的元素。任何进程都可以创建子进程，所有进程都是第一个系统进程的后代。

Centos的第一个系统进程：

- Centos5/6 ：init
- Centos7：systemd

子进程继承父进程的安全性身份、过去和当前的文件描述符、端口和资源特权、环境变量，以及程序代码。随后，子进程可能exec自己的程序代码。通常父进程在子进程运行期间处于睡眠（sleeping）状态。当子进程完成时发出（exit）信号请求，在退出时，子进程已经关闭或丢弃其资源环境，剩余的部分称之为僵停（zombie）。父进程在子进程退出时受到信号而被唤醒，清理剩余的结构，然后继续执行其自己的程序代码。

在多任务处理操作系统中，每个CPU（或核心）在一个时间点上只能处理一个进程。在进程运行时，它对CPU时间和资源分配的要求会不断变化，从而为进程分配一个状态，它随着环境要求而改变。

![进程状态](进程状态.png)

## 查看进程

- `ps`：静态查看进程

- `top`：动态查看进程

- `pstree`：查看进程树

- 查看进程PID

  - `cat /run/进程名称.pid`
  - `ps aux |greo 进程名称`
  - `pgrep -l 进程名称`
  - `pgrep 进程名称`
  - `pidof 进程名称`

  ### ps命令

![ps](ps.png)

`ps`命令显示的信息

- USER：运行进程的用户
- PID：进程ID
- %CPU：CPU占用率
- %MEM：内存占用率
- VSZ：占用虚拟内存（KB）
- RSS：占用实际内存（驻留内存）
- TTY：进程运行的终端
- STAT：进程状态
  - R：运行
  - S：可中断睡眠
  - D：不可中断睡眠
  - T：停止的进程
  - Z：僵尸进程
  - X：死掉的进程（不可见）
  - 第二位特殊状态
    - s：进程的领导者，父进程
    - <：优先级较高的进程
    - N：优先级较低的进程
    - +：表示是前台进程组
    - l：以多线程的方式运行
- START：进程的启动时间
- TIME：进程占用CPU的总时间
- COMMAND：进程文件，进程名

### top命令

使用`top`命令会显示两部分信息

- 系统整体统计信息
- 进程信息

#### 系统整体统计信息

![top-系统整体统计信息](top-系统整体统计信息.png)

**第一行：**
13:42:59 当前系统时间
6 days, 9:29 系统已经运行了6天6小时29分钟（在这期间没有重启过）
3 users 当前有3个用户登录系统
load average: 3.06,3.01, 1.79 load average后面的三个数分别是1分钟、5分钟、15分钟的负载情况。
load average数据是每隔5秒钟检查一次活跃的进程数，然后按特定算法计算出的数值。如果这个数除以逻辑 CPU的数量，结果高于5的时候就表明系统在超负荷运转了。

**第二行： Tasks 任务（进程）**
系统现在共有131个进程，其中处于运行中的有3个，127个在休眠（sleep），stoped状态的有0个，zombie状态（僵尸）的有1个。

**第三行：cpu状态**
10.6% us 用户空间占用CPU的百分比。
2.2% sy 内核空间占用CPU的百分比。
0.0% ni 改变过优先级的进程占用CPU的百分比
84.5% id 空闲CPU百分比
2.5% wa IO等待占用CPU的百分比
0.1% hi 硬中断（Hardware IRQ）占用CPU的百分比
0.0% si 软中断（Software Interrupts）占用CPU的百分比
在这里CPU的使用比率和windows概念不同，如果你不理解用户空间和内核空间，需要充充电了。

**第四行：内存状态**
8300124k total 物理内存总量（8GB）
5979476k used 使用中的内存总量（5.7GB）
2320648k free 空闲内存总量（2.2G）
455544k buffers 缓存的内存量 （434M）

**第五行：swap交换分区**
8193108k total 交换区总量（8GB）
41568k used 使用的交换区总量（40.6M）
8151540k free 空闲交换区总量（8GB）
4217456k cached 缓冲的交换区总量（4GB）

#### 进程信息

![top-进程信息](top-进程信息.png)

- PID： 进程id
- USER： 进程所有者
- PR ：进程优先级
- NI： nice值。负值表示高优先级，正值表示低优先级
- VIRT： 进程使用的虚拟内存总量，单位kb。VIRT=SWAP+RES
- RES： 进程使用的、未被换出的物理内存大小，单位kb。RES=CODE+DATA
- SHR： 共享内存大小，单位kb
- S： 进程状态。D=不可中断的睡眠状态 R=运行 S=睡眠 T=跟踪/停止 Z=僵尸进程
- %CPU： 上次更新到现在的CPU时间占用百分比
- %MEM： 进程使用的物理内存百分比
- TIME+： 进程使用的CPU时间总计，单位1/100秒
- COMMAND： 进程名称（命令名/命令行）

在`top`目录运行期间按以下按键：

- M：按内存的使用排序
- P：按CPU使用排序
- N：按PID的大小排序
- R：对排序进行反转
- f：自定义显示字段
- 1：显示所有CPU负载
- z：彩色
- W：保存top环境设置
- `<`：向前
- `>`：向后
- q：退出
- s：立即刷新

## 信号控制进程

使用`kill -l`命令可列出所有支持的信号

- `kill -信号序号（1）/短命令（HUP）/长命令（SIGHUP） 进程PID`：给单个进程发送信号
- `killall -信号 进程名称`：给一批同名进程发送信号
- `pkill -信号 终端名称/用户名称`：给某个终端或用户发送信号（多用于踢出某个用户）

### 常用信号

- 1) SIGHUP：重新加载配置
- 2) SIGINT：键盘中断(^C)
- 3) SIGQUIT：键盘退出
- 9) SIGKILL：强制终止
- 15) SIGTERM：终止（正常结束），缺省信号
- 18) SIGCONT：继续
- 19) SIGSTOP：停止
- 20) SIGTSTP：暂停(^Z)

## 相对优先级 nice

由于不是每个进程都与其他进程同样重要，可告知进程调度程序为不同的进程使用不同的调度策略。常规系统上运行的大多数进程所使用的调度策略为`SCHED_OTHER`（也称为`SCHED_NORMAL`）,但还有其它一些调度策略用于不同的目的。

`SCHED_OTHER`调度策略运行的进程的相对优先级称为进程的`nice`值，可以有40种不同级别的`nice`值。

nice值越高表示优先级越低，例如+19，该进程容易将CPU使用量让给其他进程。

nice值越低表示优先级越高，例如-20，该进程更不倾向于让出CPU。

![优先级](优先级.png)

### 查看进程的nice级别

- 使用`top`查看`nice`级别
  - `NI`：实际nice级别
  - `PR`：将nice级别显示为映射到更大优先级队列，-20映射到0，+19映射到39
- 使用`ps`查看`nice`级别
  - `ps axo pid,command,nice,cls –sort=-nice`
  - TS表示该进程使用的调度策略为`SCHED_OTHER`

### 更改nice级别

启动进程时，通常会继承父进程的`nice`级别，默认为0。

- 可在启动时增加`nice -n nice值 进程`来修改进程的`nice`级别
- 在`top`中使用`r`来修改`nice`级别
- 使用`renice -nice值 pid`来修改`nice`级别

## 作业控制jobs

作业控制是一个命令行功能，运行一个shell实例来运行和管理多个命令。

如果没有作业控制，父进程fork()一个进程后，将sleeping，直到子进程退出。

使用作业控制，可以选择暂停、恢复、以及异步运行命令，让shell可以在子进程运行期间返回接受其他命令。

- `命令 &`：加上`&`表示命令将在后台运行
- `jobs`：查看后台作业
- `bg [%]作业号`：将对应作业号的作业在后台运行
- `fg [%]作业号`：将对应作业号的作用调回到前台，不加作业号则调回最后加入后台的作业
-  `^Z`：挂起当前程序
-  `kill %作业号`：终止对应作业号的作业（必须加`%`，否则终止的是进程）

安装`screen`可以在掉线后恢复之前的操作。

# IO重定向

shell中的`FD(File Descripter)`（文件描述符）可以理解为一个指向文件的指针。默认有三个FD：0（stdin（标准输入）），1（stdout（标准输出）），2（stderr（标准错误））。分别指向的是：Keyboard设备文件，Moniter设备文件，和Moniter设备文件。

![IO重定向](IO重定向.png)

进程使用文件描述符来管理打开的文件

使用`ls /proc/pid($$(当前进程))/fd`命令来查看进程的FD

重定向命令：

- 输出重定向
  - `[n]>`：覆盖输出，`1>`等同于`>`
  - `[n]>>`：追加输出
  - `[n]>|`：强行覆盖
  - `>&-` ：强制关闭标准输出 
  - `&>`，`&>>`：合并标准输出与标准错误
- 输入重定向
  - `<`：输入重定向，`0<`等同于`<`
  - `<>file`*：将file作为标准输入和标准输出。通常用于设备文件（/dev下面的文件），底层系统程序员可用之测试设备驱动，其他的很少用。
  - `n <>file` ：将file作为文件描述符n的输入和输出
  - `<<[-]label `：将shell的标准输入作为命令的输入，直到行中之包含label，`-`表示忽视前面的空格
  - `<&-` ：强制关闭标准输入



## 进程管道

重定向是用来控制输出到文件中，管道是用来控制输出到程序中。

管道的用法：`command1 | command2 | command3 | ...`，将一个进程的输出作为另一个进程的输入

![管道](管道.png)



# systemctl

Centos7开机第一个程序从`init`完全换成了`systemd`这种启动方式。

`systemd`是靠管理unit的方式来控制开机服务，开机级别等功能。 在`/usr/lib/systemd/system`目录下包含了各种unit文件，有service后缀的服务unit，有target后缀的开机级别unit等。

因为`systemd`在开机要想执行自启动，都是通过这些`*.service` 的unit控制的，服务又分为系统服务（system）和用户服务（user）。

- 系统服务：开机不登陆就能运行的程序（常用于开机自启）。
- 用户服务：需要登陆以后才能运行的程序。

配置文件目录

- `systemctl`脚本目录：`/usr/lib/systemd/`
- 系统服务目录：`/usr/lib/systemd/system/`
- 用户服务目录：`/usr/lib/systemd/user/`

## 文件示例及说明

```bash
#service配置文件分为[Unit]、[Service]、[Install]三个部分。

#[Unit]部分：指定服务描述、启动顺序、依赖关系，包括Description、Documentation、After、Before、Wants、Requires。
[Unit]
#Description指定当前服务的简单描述。
Description=nginx代理服务

#Documentation指定服务的文档，可以是一个或多个文档的URL，可选，一般不用配置该项。
Documentation=http://nginx.org/en/docs

#启动顺序，After和Before。
#注意，After和Before字段只涉及启动顺序，不涉及依赖关系。

#After表示当前服务在network.target之后启动，可以指定多个服务，以空格隔开。
After=network.target sshd.service
After=sshd-keygen.service

#Before表示当前服务在tomcat.target之前启动，可以设置多个，以空格隔开，可选，根据实际需要配置。
Before=tomcat.service

#依赖关系，Wants和Requires，可选，根据实际需要配置。
#Wants为"弱依赖"关系，即如果"mysqld.service"启动失败或停止运行，不影响nginx.service继续执行。
#Requires为"强依赖"关系，即如果"mysqld.service"启动失败或异常退出，那么nginx.service也必须退出。
#想要添加多个服务，可以多次使用此选项，也可以设置一个空格分隔的服务列表。
#注意，Wants与Requires只涉及依赖关系，与启动顺序无关，默认情况下是同时启动的。
Wants=mysqld.service
Requires=mysqld.service

#[Service]部分：指定启动行为，包括Type、EnvironmentFile、ExecStart、ExecReload、ExecStop、PrivateTmp。
[Service]
#Type指定服务的启动类型，必须为simple, exec, forking, oneshot, dbus, notify, idle 之一。常用simple和forking。
# simple（默认值）：ExecStart启动的进程为该服务主进程。
# exec：exec与simple类似，不同之处在于，只有在该服务的主服务进程执行完成之后，systemd才会认为该服务启动完成。 其他后继单元必须一直阻塞到这个时间点之后才能继续启动。
# forking：ExecStart将以fork()方式启动，此时父进程将会退出，子进程将成为主进程。
# oneshot：oneshot与simple类似，不同之处在于，只有在该服务的主服务进程退出之后，systemd才会认为该服务启动完成，才会开始启动后继单元。 此种类型的服务通常需要设置RemainAfterExit=选项。当Type= 与 ExecStart=都没有设置时，Type=oneshot 就是默认值。
# dbus：类似于simple，但会等待D-Bus信号后启动。
# notify：类似于simple，启动结束后会发出通知信号，然后 Systemd 再启动其他服务。
# idle：类似于simple，但是要等到其他任务都执行完，才会启动该服务。一种使用场合是为让该服务的输出，不与其他服务的输出相混合。
# 建议对长时间持续运行的服务尽可能使用Type=simple(这是最简单和速度最快的选择)。注意，因为simple类型的服务 无法报告启动失败、也无法在服务完成初始化后对其他单元进行排序，所以，当客户端需要通过仅由该服务本身创建的IPC通道(而非由systemd创建的套接字或D-bus之类)连接到该服务的时候，simple类型并不是最佳选择。在这种情况下， notify或dbus(该服务必须提供D-Bus接口)才是最佳选择， 因为这两种类型都允许服务进程精确的安排何时算是服务启动成功、何时可以继续启动后继单元。notify类型需要服务进程明确使用sd_notify()函数或类似的API，否则，可以使用forking作为替代(它支持传统的UNIX服务启动协议)。最后，如果能够确保服务进程调用成功、服务进程自身不做或只做很少的初始化工作(且不大可能初始化失败)，那么exec将是最佳选择。注意，因为使用任何 simple 之外的类型都需要等待服务完成初始化，所以可能会减慢系统启动速度。 因此，应该尽可能避免使用 simple 之外的类型(除非必须)。另外，也不建议对长时间持续运行的服务使用 idle 或 oneshot 类型。
Type=forking

#EnvironmentFile指定当前服务的环境参数文件。该文件内部的key=value键值对，可以用$key的形式，在当前配置文件中获取。
EnvironmentFile=/etc/nginx/nginx.conf

#启动命令
# ExecStart指定启动进程时执行的命令。
# ExecReload指定当该服务被要求重新载入配置时所执行的命令。另外，还有一个特殊的环境变量 $MAINPID 可用于表示主进程的PID，例如可以这样使用：/bin/kill -HUP $MAINPID。强烈建议将 ExecReload= 设为一个能够确保重新加载配置文件的操作同步完成的命令行。
# ExecStop指定停止服务时执行的命令。
# ExecStartPre指定启动服务之前执行的命令。不常用。
# ExecStartPost指定启动服务之后执行的命令。不常用。
# ExecStopPost指定停止服务之后执行的命令。不常用。
ExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.conf
ExecReload=/usr/local/nginx/sbin/nginx -s reload
ExecStop=/usr/local/nginx/sbin/nginx -s quit

# 设为 true表示在进程的文件系统名字空间中挂载私有的 /tmp 与 /var/tmp 目录， 也就是不与名字空间外的其他进程共享临时目录。 这样做会增加进程的临时文件安全性，但同时也让进程之间无法通过 /tmp 或 /var/tmp 目录进行通信。
# 适用于web系统服务，不适用于mysql之类的数据库用户服务，数据库用户服务设为false。
PrivateTmp=true

#[Install]部分：指定服务的启用信息，只有在systemctl的enable与disable命令在启用/停用单元时才会使用此部分。
[Install]
# “WantedBy=multi-user.target”表示当系统以多用户方式（默认的运行级别）启动时，这个服务需要被自动运行。
WantedBy=multi-user.target
```



# yum

## 使用第三方源

### yum使用163/阿里镜像

- 首先备份/etc/yum.repos.d/CentOS-Base.repo

  ```bash
  mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
  ```

- 下载源文件替换

  - 阿里镜像

    ```bash
    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    ##或者
    curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    ```

  - 163镜像

    ```bash
    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo
    ##或者
    curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo
    ```

  - 使用阿里镜像官方提示

    > 非阿里云ECS用户会出现 Couldn't resolve host 'mirrors.cloud.aliyuncs.com' 信息

    测试时有部分缓存无法下载

    ![1565663664843](1565663664843.png)

- 生成缓存

  ```bash
  yum clean all
  yum makecache
  ```

### epel源

elel源在阿里有镜像，163没有

```bash
wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
```

## yum命令

- `yum repolist`：查询可用的仓库
- `yum clean all`：情况缓存及其它文件
- `yum makecache` ：重建缓存
- 安装软件
  - `yum -y install 软件名` 可一次安装多个，也可以使用`*`模糊匹配；`-y`参数表示不需要确认
  - `yum -y 软件名` ：重新安装
  - `yum -y update 软件名` ：更新
  - `yum -y install 本地路径`：从本地安装
  - `yum -y install 网络路径`：从网络安装
  - `yum -y groupinfo 软件名`：安装软件组（一个软件通常会依赖多个软件）
- 卸载软件
  - `yum -y remove 软件名`：卸载软件（不包括依赖包）
  - `yum -y groupremove 软件名`
- 查询软件
  - `yum list 软件名` 可以从查询多个软件，也可以使用`*`模糊匹配，查询结果会显示软件是否安装，是否可更新，安装包所在源等信息
  - `yum list | grep 字符串`：查询包含特定字符串的软件
  - `yum list installed`：查询已安装的软件（只查询rpm数据库，编译安装的不在此列）
  - `yum info 软件名`：显示软件的详细信息
  - `yum group list`
  - `yum search 字符串`：查询包名及描述
- 操作历史
  - `yum history`：查看yum操作的历史记录
  - `yum history info 编号`：查看yum操作编号对应的具体操作
  - `yum history undo 编号`：撤销yum编号对应的操作



## yum 安装no more mirrors to try

在CentOS 使用yum 安装软件的时候 出现错误no more mirrors to try，导致软件无法继续安装。

可能原因是不正当的删除造成的。

尝试命令

```bash
yum clean all 
yum makecache 
yum -y update
```

然后重新安装

## 卸载

```bash
#搜索软件信息
yum list installed | grep 软件名
#卸载软件
yum -y remove 软件名
```

## yum升级被PackageKit锁定

PackageKit是一个离线更新服务，可在`/etc/yum/pluginconf.d/langpacks.conf`中将`enable=1`改为`enable=0`，重启后即可解决。

## yum-config-manager: command not found

系统默认没有安装这个命令，这个命令在`yum-utils` 包里，可以通过命令`yum -y install yum-utils `安装

# 常用软件安装

## openjdk 1.8

```bash
 # java-1.8.0-openjdk只有jre
 yum -y install java-1.8.0-openjdk-devel.x86_64
```



## git

- 下载编译工具 

  ```bash
  yum -y groupinstall "Development Tools"
  ```

- 下载依赖包 

  ```bash
  yum -y install zlib-devel perl-ExtUtils-MakeMaker asciidoc xmlto openssl-devel
  ```

- 下载 Git 最新版本的源代码，不要下载rc版本

  ```bash
  wget https://www.kernel.org/pub/software/scm/git/git-版本号.tar.gz
  ```

- 解压

  ```bash
  tar -zxvf git-版本号.tar.gz
  ```

- 进入目录配置，指定存放路径

  ```bash
  ./configure --prefix=/usr/local/git
  ```

- 安装

  ```bash
  make && make install
  ```

- 配置全局路径

  ```bash
  #重启后失效
  #export PATH="/usr/local/git/bin:$PATH" 
  #写入/etc/bashrc配置文件
  echo "export PATH=/usr/local/git/bin:$PATH" >>/etc/bashrc
  #使配置生效
  source /etc/profile
  ```

## docker

- 查看内核版本，必须高于3.10

  ```bash
  uname -r 
  ```

- 移除旧版本（最小化安装不需要）

  ```bash
  yum remove docker docker-client docker-client-latest docker-common \
             docker-latest docker-latest-logrotate docker-logrotate \
             docker-selinux docker-engine-selinux docker-engine
  ```

- 安装必要系统工具

  ```bash
  yum install -y yum-utils device-mapper-persistent-data lvm2
  ```

- 添加软件源信息

  ```bash
  yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
  ```

  - 系统默认没有安装这个命令，这个命令在`yum-utils` 包里，可以通过命令`yum -y install yum-utils `安装

- 更新 yum 缓存

  ```bash
  yum makecache fast
  ```

- 安装 Docker-ce

  ```bash
  yum -y install docker-ce
  ```

- 设置docker参数

  - `/etc/docker/daemon.json`

    ```json
    {
        //使用国内镜像
        "registry-mirrors": ["https://i12uq6cs.mirror.a
                             liyuncs.com"],
        //设置私有镜像仓库,docker客户端使用https去访问仓库，但是仓库是http
        //会提示错误：Error response from daemon: Get https://xxxx/v2/: http: server gave HTTP response to HTTPS client
        "insecure-registries":["192.168.0.50:8080"]
    }
    ```

  - `/usr/lib/systemd/system/docker.service`

    ```bash
    #原始配置
    ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
    #配置私有镜像仓库（测试未通过）
    #ExecStart=/usr/bin/dockerd --insecure-registry=192.168.0.50:8080
    #配置远程访问
    #ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
    ```

- 重载配置

  ```bash
  #修改配置后必须执行此命令
  systemctl daemon-reload
  systemctl restart docker
  ```

  

## Harbor

- 安装`docker`，参考上一章节

- 安装`docker-compose`

  ```bash
  yum install python-pip;pip install docker-compose
  ```

- 下载Harbor

  下载地址：[https://github.com/goharbor/harbor/releases](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fgoharbor%2Fharbor%2Freleases)

- 配置harbor.yml，主要修改以下配置

  ```yaml
  #配置主机
  hostname: 192.168.0.50
  # http 配置
  http:
    # 端口
    port: 8080
    # admin初次访问密码
  harbor_admin_password: root
  
  # Harbor DB configuration
  database:
    # 数据库密码
    password: root
  
  # 数据保存位置
  data_volume: /home/harbor/data
  
  ```

- 执行安装

  ```bash
  ./install.sh
  ```

- docker相关操作

  - docker登录

    ```bash
    #非80端口要加端口
    docker login 192.168.0.50:8080
    ```

  - docker上传镜像

    ```bash
    #打tag
    docker tag hello-world 192.168.0.54:18088/library/hello-world
    #上传
    docker push 192.168.0.54:18088/library/hello-world
    ```

- 开机启动

  ```bash
  #增加启动配置文件
  vi /lib/systemd/system/harbor.service 
  #在文件中增加
  [Unit]
  Description=Redis
  After=network.target
  
  [Service]
  ExecStart= /harbor/docker-compose start
  
  [Install]
  WantedBy=multi-user.target
  ```

  

## maven

- yum安装maven
  - `yum -y install maven`
  - 增加路径到`/etc/profile`
    - `MAVEN_HOME=/usr/bin/mvn
      PATH=$PATH:$JAVA_HOME/bin:$MAVEN_HOME/bin`
  - 增加`.mavenrc`文件
    - `cd $HOME`
    - `vim .mavenrc`
    - `JAVA_HOME=$(/usr/lib/jvm/jre-1.8.0-openjdk.x86_64)`
- 解压安装
  - `wget http://mirrors.hust.edu.cn/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz`
  - `mkdir -p /opt/maven`
  - `tar -xzvf apache-maven-3.5.3-bin.tar.gz -C /opt/maven/`
  - `ln -s /opt/maven/apache-maven-3.5.3 /usr/local/maven`
  - `echo 'export M2_HOME=/usr/local/maven' >> /etc/profile`
  - `echo 'export PATH=$M2_HOME/bin:$PATH' >> /etc/profile`
  - `source /etc/profile`
  - 

# 忘记root密码

- 重启时在启动页面，选择你要启动的内核 按 E， 进入 grub 编辑页面
- 找到 linux16 那一行，在language 后面 也就是`LANG=zh_CN.UTF-8`，空格在后面追加`rw single init=/bin/bash`
- ctrl + x 重新启动
- 进入bash界面后，可以输入`passwd`命令重新设置root密码
- 如果开启了selinux，执行命令`touch /.autorelabel`命令
- 输入`exec /sbin/init`命令重启系统
- 使用新设置的密码进入系统

# 虚拟机

## xshell登录centos7很慢解决办法

使用xshell登录到centos系统虚拟机，可以登录上去，但是认证速度特别慢。

因为在登录时，需要反向解析dns，因此，修改linux配置文件，vi /etc/ssh/sshd_config，将其注释去掉，并将yes改为no，即可解决。

   ```
vi /etc/ssh/sshd_config
将#UseDNS yes 改为： UseDNS no
   
systemctl restart sshd
   ```



# 常见错误

## 错误设置selinux 导致不能启动 Failed to load SELinux policy. Freezing

**错误原因**

配置关闭SELinux，结果误操作

应修改配置文件`/etc/selinux/config`中的`SELINUX`参数的值

```properties
#SELINUX=enforcing  #原始配置

SELINUX=disabled     #正确
```

但是误将`SELINUXTYPE`看成`SELINUX`，设置了`SELINUXTYPE`参数：

```properties
#SELINUXTYPE=targeted   原始配置 这个不必修改。
SELINUXTYPE=disabled   #错误
```



**错误结果**

重启后 机器就报 `Failed to load SELinux policy. Freezing` 错误，导致一直不能启动 

**解决办法**

-  重启时在启动页面，选择你要启动的内核 按 E， 进入 grub 编辑页面
- 找到 linux16 那一行，在language 后面 也就是LANG=zh_CN.UTF-8，空格 加上 selinux=0 或者 enforcing=0 
- ctrl + x 启动，就看到熟悉的登录界面
- 修改selinux配置文件，正确关闭selinux 

## xshell登录时出现`WARNING!The remote SSH server rejected X11 forwarding request.`

X11转发是SSH的一项功能，允许你在本地机器上显示远程服务器上的图形用户界面。

**解决办法**

1. **在远程服务器上启用X11转发**：
   可以在远程服务器的SSH配置文件（通常是`/etc/ssh/sshd_config`）中启用X11转发。找到以下行并确保没有注释掉（没有前面的`#`字符）：

   ```bash
   X11Forwarding yes
   ```

2. **在客户端机器上启用X11转发**：
   在Xshell中，确保启用了X11转发。你可以转到"文件" > "属性" > "SSH" > "隧道"，并确保"启用X11转发"的复选框已勾选。（**如果不需要图形界面可取消选择，其他设置就可以不用配置**）

3. **防火墙或安全组设置**：
   确保防火墙或安全组设置允许X11的流量通过。通常，这是通过允许TCP端口6000到6010的流量来完成的。

4. **重新启动SSH会话**：
   在做了上述更改后，确保重新启动你的SSH会话，以便更改生效。

# 删除系统日志

在 CentOS 7 中，`/var/log/messages` 文件通常用于记录系统级别的消息，如内核消息、服务启动和停止通知等。如果你不希望将 `info` 级别的日志记录到 `/var/log/messages` 中，你可能需要调整日志记录配置。

在 CentOS 7 中，通常使用 `rsyslog` 作为日志守护进程。你可以通过编辑 `/etc/rsyslog.conf` 或 `/etc/rsyslog.d/` 目录下的配置文件来调整日志记录级别。

以下是一些可能的步骤来实现你的需求：

1. **编辑 rsyslog 配置文件**

首先，备份原始的 `rsyslog` 配置文件：


```bash
sudo cp /etc/rsyslog.conf /etc/rsyslog.conf.bak
```
然后，编辑 `/etc/rsyslog.conf` 或 `/etc/rsyslog.d/` 目录下的相关配置文件。
2. **调整日志级别**

如果你只想过滤某个特定服务的 `info` 级别日志，你可以为该服务设置较低的日志级别。例如，如果你想要过滤 `sshd` 的 `info` 日志，你可以在配置文件中添加或修改如下行：


```bash
*.info;auth,authpriv.none;mail.none      /var/log/messages

#如下修改则不记录日志
*.none 	/var/log/messages
```
这将会记录所有 `info` 级别以上的日志到 `/var/log/messages`，但会排除 `auth` 和 `authpriv` 设施的日志（这通常包括 SSH 登录尝试等）。
3. **创建单独的日志文件**

如果你想要保留这些 `info` 级别的日志，但不想它们出现在 `/var/log/messages` 中，你可以为它们创建一个单独的日志文件。例如：


```bash
if $programname == 'sshd' and $syslogseverity <= 6 then /var/log/sshd-info.log
& stop
```
这将会把 `sshd` 的 `info` 级别（及其以下级别）的日志记录到 `/var/log/sshd-info.log` 文件中。
4. **重启 rsyslog 服务**

完成配置更改后，重启 `rsyslog` 服务以使更改生效：


```bash
sudo systemctl restart rsyslog
```
5. **检查配置**

在做出更改后，建议检查配置文件的语法是否正确：


```bash
rsyslogd -N1
```
如果没有错误消息，那么你的配置应该是正确的。
6. **监控日志**

最后，你可以使用 `tail -f /var/log/messages` 和 `tail -f /var/log/sshd-info.log`（或其他你创建的日志文件）来查看日志是否按预期记录。

请注意，具体的配置可能会根据你的系统和服务的需求而有所不同。始终建议在生产环境中进行更改之前先在测试环境中验证配置。